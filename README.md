# README #

This is a library project, therefore was meant to be used as a set of classes for other projects,
You can uncompress intellij_config.zip to make a valid project from the dir.

The project is separated in 
*src_test* > Contains all the "test" code for the lib.
*src_lib* > Contains the Grar classes.
 
### What is this repository for? ###

* This repo is used as a central place to update the code inside Grar. As Grar is a set of classes that we use in multiple projects and extend everyday, having a repo to keep it updated seems logical.


### How do I get set up? ###

* You need IntellijIdea and uncompress intellij_config.zip (do not worry, the uncompressed files will not be "marked" as changed or new for git, as they're ignored by default)

* Open the project, adjust the SDK (if needed), and add the missing dependencies (so far greensock and signals) from the Project Structure.

As a rule of thumb, all dependencies (Grar included) should be added as a Global library in Intellij (that's why the lib folder is empty), so choose a folder in your disk to keep your libraries, and start linking those folders in Intellij

### Contribution guidelines ###

* So far this code is maintained by Grar.

### Who do I talk to? ###

* Repo owner or admin