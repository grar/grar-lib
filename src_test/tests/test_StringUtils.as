/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 2:20 PM.
 *
 */
package tests {
import grar.utils.StringUtils;

public class test_StringUtils extends AbsTest {

	public function test_StringUtils() {
		_class = StringUtils;
		super();
	}

	override public function run():void {
		exec( StringUtils.numberToText, [1234.5] );
		exec( StringUtils.addHtmlLinks, ["Please, go to http://www.twitter.com/ to find out more"] );
		exec( StringUtils.clip, ["This text is too long to show entirely", 10] );
		exec( StringUtils.generateGUID, [] );
		exec( StringUtils.formatNumber, [12345.4, ",", ".", 0] );
	}

}
}
