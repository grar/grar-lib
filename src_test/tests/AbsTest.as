/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 7:07 PM.
 *
 */
package tests {
import flash.display.Sprite;

import grar.utils.DebugUtils;
import grar.utils.StringUtils;

public class AbsTest extends Sprite {

	protected var _classname:String ;
	protected var _class:Class ;

	public function AbsTest() {
		_classname = StringUtils.replace( String(_class), "class " ) ;
		init() ;
	}

	protected function init():void {
		// init basic code required by the classes to test.
	}

	public function run():void {
		// run test.
	}

	protected function exec( method:Function, params:Array=null ):void {
		var funcName:String = DebugUtils.getFunctionName( method, _class );
		var res:String = method.apply( null, params );
		log2( funcName, params, res );
	}

	protected function log2(method:String, params:Array, ...args):void {
		var str:String = _classname + "::" + method + "(";
		if ( params.length ) {
			str += params.join( "," );
		}
		str += ") > " + args[0];
		trace( str );
	}

	protected function log(...args):void {
		trace( "[" + _classname+"] " + args.join(",") );
	}
}
}
