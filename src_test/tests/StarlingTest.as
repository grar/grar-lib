/**
 * Code by Rodrigo López Peker (grar) on 2/24/16 11:02 PM.
 *
 */
package tests {
import flash.display3D.Context3DProfile;
import flash.filesystem.File;
import flash.geom.Rectangle;
import flash.system.Capabilities;

import grar.utils.AppUtils;

import starling.core.Starling;

import tests.starling.MyAssetsLib;

public class StarlingTest {
	public function StarlingTest() {
	}

	public function run():void {
//		AppUtils.retinaFactor = 1 ;
		// init files
		if ( File.applicationDirectory.name == "bin" ) {
			MyAssetsLib.assetsDir = new File( File.applicationDirectory.nativePath ).parent.resolvePath( "bin-assets/assets" );
		} else {
			MyAssetsLib.assetsDir = File.applicationDirectory.resolvePath( "assets" );
		}
		Starling.multitouchEnabled = true; // useful on mobile devices
		var sw:int = AppUtils.stage.stageWidth;
		var sh:int = AppUtils.stage.stageHeight;
		var viewPort:Rectangle = new Rectangle( 0, 0, sw, sh );
		var starling:Starling = new Starling( Boot, AppUtils.stage, viewPort, null, "auto", Context3DProfile.BASELINE_EXTENDED );
		starling.stage.stageWidth = sw;  // <- same size on all devices!
		starling.stage.stageHeight = sh; // <- same size on all devices!
		starling.enableErrorChecking = Capabilities.isDebugger;
		starling.simulateMultitouch = false;
		starling.addEventListener( "rootCreated", onStarlingInit );
		starling.supportHighResolutions = true;
		starling.showStats = true;
		starling.start();
	}

	private function onStarlingInit( event:* ):void {
		(Starling.current.root as Boot).start();
	}
}
}

import com.greensock.TweenLite;

import grar.utils.AppUtils;

import starling.core.Starling;
import starling.display.Sprite;
import starling.utils.Align;

import tests.StarlingTest;

import tests.starling.MyAssetsLib;
import tests.starling.StarlingFontsDemo;
import tests.starling.StarlingUI;

class Boot extends Sprite {

	public function Boot() {
	}

	public function start():void {
		scaleX = scaleY = 1 / AppUtils.retinaFactor;
		MyAssetsLib.init( onAssetsLoaded );
		TweenLite.delayedCall( 0, function ():void {
			Starling.current.showStatsAt( Align.LEFT, Align.BOTTOM, 1 );
		} );
	}

	private function onAssetsLoaded():void {
		new StarlingUI( this );
	}

}
