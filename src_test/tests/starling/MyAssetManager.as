/**
 * Code by Rodrigo López Peker on 2/7/16 11:27 AM.
 *
 */
package tests.starling {
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Rectangle;
import flash.utils.ByteArray;

import grar.utils.StringUtils;

import starling.utils.AssetManager;

public class MyAssetManager extends AssetManager {

	public function MyAssetManager( scaleFactor:Number = 1, useMipmaps:Boolean = false ) {
		super( scaleFactor, useMipmaps );
	}

	public var colorsMap:Object = {};
	private static var _ba:ByteArray;

	public function addColorMap( atlasId:String, colors:Array ):void {
		colorsMap[atlasId] = {};
		colorsMap[atlasId].colors = colors;
		colorsMap[atlasId].xml = "";
	}

	override protected function loadRawAsset( rawAsset:Object, onProgress:Function, onComplete:Function ):void {
		var preprocess:Function = function ( asset:Object ):void {
			// get the rawasset id::
			var id:String = getIdFromPath( rawAsset as String );
			var map:Object = colorsMap[id];

			if ( map ) {
				if ( asset is Bitmap ) {
					var bd:BitmapData = Bitmap( asset ).bitmapData;
					if ( map.xml ) {
						addColors( bd );
//						var bmp:Bitmap = new Bitmap(bd.clone()) ;
//						AppUtils.stage.addChild(bmp) ;
					}
				} else {
					map.xml = String( asset );
					processColors();
					if ( !_ba ) {
						_ba = new ByteArray();
					} else {
						_ba.clear();
					}
					_ba.writeUTFBytes( map.xml );
					asset = _ba;
				}
			}

			const subtexture:String = '<SubTexture name="${name}" x="${x}" y="${y}" width="${w}" height="${h}" frameX="${fx}" frameY="${fy}" frameWidth="${fw}" frameHeight="${fh}"/>';

			function processColors():void {
				var xml:XML = new XML( map.xml );
				var tw:int = xml.@w;
				var th:int = xml.@h;

				var str:String = map.xml;
				var closeNode:String = "</TextureAtlas>";

				str = StringUtils.replace( str, closeNode, "" );
				var arr:Array = map.colors;
				map.data = [];
//				var qs:int = 4;
//				var padding:int = 0;
//				var qb:int = 2;
//				var fs:int = qs + qb * 2;
//				var qs:int = 2 ;
				var out:String = "";
				for ( var i:int = 0; i < arr.length; i++ ) {
					var color:uint = arr[i];
					var o:Object = {};
					o.name = "c_" + color.toString( 16 );
					o.tx = i * 4;
					o.ty = th - 4;
					o.x = o.tx + 1;
					o.y = o.ty + 1;
					o.w = 2;
					o.h = 2;
					o.fw = 0;
					o.fh = 0;
					o.fx = 0;
					o.fy = 0;
					map.data[i] = o;
					str += StringUtils.replacePlaceholders( subtexture, o ) + "\n";
//					<SubTexture name="houses1"	x="0"	y="463"	width="546"	height="251" frameX="-0" frameY="-34" frameWidth="546" frameHeight="285"/>
				}
				str += closeNode;
				map.xml = str;
			}

			function addColors( bd:BitmapData ):void {
				var arr:Array = map.data;
				var r:Rectangle = new Rectangle();
				for ( var i:int = 0; i < arr.length; i++ ) {
					var o:Object = arr[i];
					var color:uint = map.colors[i];
					r.setTo( o.tx, o.ty, o.w + 2, o.h + 2 );
					bd.fillRect( r, 0xff << 24 | color );
				}
			}

			onComplete( asset );
		};

		super.loadRawAsset( rawAsset, onProgress, preprocess );
	}

	private function getIdFromPath( path:String ):String {
		var idx:int = path.lastIndexOf( "/" );
		path = path.substr( idx + 1, path.length );
		return path.split( "." ).shift();
	}
}
}
