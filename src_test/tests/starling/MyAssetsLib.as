/**
 * Code by Rodrigo López Peker on 2/1/16 10:49 PM.
 *
 */
package tests.starling {
import flash.filesystem.File;
import flash.geom.Rectangle;

import starling.display.DisplayObjectContainer;
import starling.display.Image;
import starling.display.Image;
import starling.display.Sprite;
import starling.textures.Texture;

public class MyAssetsLib {
	public static var man:MyAssetManager;
	public static var onComplete:Function;
	public static var assetsDir:File;

	public function MyAssetsLib() {
	}

	public static function init( callback:Function ):void {
		onComplete = callback;
		man = new MyAssetManager( 1 );
		// fonts?
		man.addColorMap( "fonts", [
			0x555555,
			0xdedede,
			0xff0000,
			0x00ff00,
			0x333333,
			0x0,
			0x1360ae,
			0x7ebc2b,
			0xffffff,
			0xe3cba7,
			0xb9baba
		] );
		man.keepFontXmls = true;
		man.enqueue(assetsDir);
		man.loadQueue( onProgress )
	}

	public static function getColorImage( color:uint, doc:DisplayObjectContainer = null, image:Image = null, tw:int = 0,
										  th:int = 0 ):Image {
		var id:String = "c_" + color.toString( 16 );
		var img:Image = getImage( id, doc, image, false );
		if ( tw > 0 && th > 0 ) {
			img.readjustSize( tw, th );
		}
		return img;
	}

	private static function onProgress( p:Number ):void {
		if ( p == 1 ) {
			onComplete();
			onComplete = null;
		}
	}


	public static function getSprite( id:String, doc:DisplayObjectContainer = null ):Sprite {
		var spr:Sprite = new Sprite();
		spr.addChild( getImage( id ) );
		if ( doc )
			doc.addChild( spr );
		return spr;
	}

	public static function setScale9Special( img:Image, size:int, offsetRect:Rectangle, rect:Rectangle=null ):Rectangle {
		if( !rect ) rect = new Rectangle() ;
		rect.x = offsetRect.x + size ;
		rect.y = offsetRect.y + size ;
		rect.width = img.texture.width - size * 2 - offsetRect.width ;
		rect.height = img.texture.height - size*2 - offsetRect.height;
		img.scale9Grid = rect ;
		return rect ;
	}

	public static function setScale9( img:Image, size:int, rect:Rectangle=null ):Rectangle {
		if( !rect ) rect = new Rectangle() ;
		rect.x = size ;
		rect.y = size ;
		rect.width = img.texture.width - size*2 ;
		rect.height = img.texture.height - size*2 ;
		img.scale9Grid = rect ;
		return rect ;
	}

	public static function getTx( id:String ):Texture {
		return man.getTexture( id );
	}

	public static function getImage( id:String, doc:DisplayObjectContainer = null, image:Image = null,
									 center:Boolean = false ):Image {
		var tx:Texture = getTx( id );

		if ( !image ) {
			image = new Image( tx );
		} else {
			image.texture = tx;
			image.readjustSize();
		}
//		image.scaleX = image.scaleY = Settings.scale ;
		if ( center ) {
			image.alignPivot();
		}
		if ( doc )
			doc.addChild( image );
		return image;
	}
}
}
