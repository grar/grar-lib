/**
 * Code by Rodrigo López Peker (grar) on 2/24/16 11:08 PM.
 *
 */
package tests.starling {
import starling.display.Sprite;
import starling.text.TextField;
import starling.text.TextFieldAutoSize;
import starling.text.TextFormat;

import tests.starling.abs.AbSSprite;

public class StarlingFontsDemo extends AbSSprite {

	public function StarlingFontsDemo( doc:Sprite = null ) {
		super( doc );
	}

	override protected function initialize():void {
		var format:TextFormat = new TextFormat("helvetica_light_i", 60, 0xFFFFFF, "left" ) ;
		var tf:TextField = new TextField(200,200,"hola mundo!", format ) ;
		format.size = 20 ;
		tf.format.size = 20 ;
		tf.format


//		tf.border = true ;
//		tf.autoScale = true ;
		tf.x = tf.y = 20 ;
		tf.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS ;
		addChild(tf) ;

	}
}
}
