/**
 * Code by Rodrigo López Peker (grar) on 3/18/16 10:45 PM.
 *
 */
package tests.starling.ui {
import com.greensock.TweenLite;

import grar.starling.abs.AbsButton;

import starling.display.Image;
import starling.display.Sprite;

import tests.starling.MyAssetsLib;

public class MacCheckbox extends AbsButton {

	private var bg1:Image;
	private var bg2:Image;
	private var ico:Image;
	private var _checked:Boolean;

	public function MacCheckbox( doc:Sprite ) {
		super( doc );
	}

	override protected function initialize():void {
		super.initialize();
		_useHandCursor = false ;
		activate( true );
		bg1 = getImg( "chk_bg", this );
		bg2 = getImg( "chk_bg_over", this );
//		MyAssetsLib.setScale9( bg1, 6 ) ;
//		bg1.width = 200 ;
		ico = getImg( "tick", this, ico, true );
		setSize( bg1.texture.width, bg1.texture.height );

		ico.alpha = 0;
		bg2.alpha = 0;
		draw() ;
	}

	override protected function draw():void {
		super.draw();
		ico.x = _w >> 1;
		ico.y = _h / 2 + 1 ;
	}

	override protected function press():void {
		super.press();
		TweenLite.to( bg2, 0.2, {alpha: 1} );
	}

	override protected function release( touching:Boolean ):void {
		super.release( touching );
		var o:Object = {alpha: 0};
		if ( touching ) {
			checked = !checked;
			o.onComplete = switchTexture;
		}
		TweenLite.to( bg2, 0.3, o );
	}

	private function switchTexture():void {
		getImg( _checked ? "chk_sel_bg_over" : "chk_bg_over", null, bg2 );
	}

	private function getImg( id:String, doc:Sprite = null, img:Image = null, center:Boolean = false ):Image {
		return MyAssetsLib.getImage( id, doc, img, center );
	}

	public function get checked():Boolean {
		return _checked;
	}

	public function set checked( value:Boolean ):void {
		if ( _checked == value ) return;
		_checked = value;
		var o:Object ;
		if ( _checked ) {
			o = {alpha:1, scale:1} ;
			getImg( "chk_sel_bg", null, bg1 );
		} else {
			o = {alpha:0, scale:0} ;
			getImg( "chk_bg", null, bg1 );
		}
		TweenLite.to( ico, 0.2, o ) ;
	}
}
}
