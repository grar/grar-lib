/**
 * Code by Rodrigo López Peker (grar) on 3/20/16 10:47 PM.
 *
 */
package tests.starling.ui {
import flash.text.TextField;
import flash.text.TextFormat;

import grar.utils.AppUtils;

import tests.starling.ui.osxui.FlashSpriteContainer;

public class FlashTextInput extends TextField {

	private var format:TextFormat = new TextFormat();

	public function FlashTextInput() {
		super();
		init();
	}

	public function debugSize( flag:Boolean ):void {
		border = flag;
		borderColor = uint(format.color) ;
	}

	private function init():void {
//		border = true;
//		borderColor = 0x0;
		// list fonts.
//		var arr:Array = Font.enumerateFonts(true) ;
//		for each( var f:Font in arr ) trace( f.fontName ) ;
		format.font = "Helvetica Neue";
		format.color = 0x0;
		format.size = 27;
		defaultTextFormat = format;
		embedFonts = false;
		type = "input";
		setTextFormat( format );
//		AppUtils.stage.addChild( this );
		FlashSpriteContainer.instance.addChild( this );

		// 1 line fields.
		autoHeightTextField( this, 2 );
	}

	public static function autoHeightTextField( textField:TextField, extraHeight:int = 0, numLines:int = 1 ):void {
		if ( numLines > 0 ) {
			var buffer:String = textField.text;
			var str:String = "";
			for ( var i:int = 0; i < numLines; i++ ) str += i + "ojygM" + "\n";
			str = str.substr( 0, str.length - 1 );
			textField.text = str;
		}
		if ( numLines <= 0 && extraHeight < 4 ) extraHeight = 4;
		textField.height = textField.textHeight + numLines * 2 * AppUtils.scale + extraHeight;
		if ( numLines > 0 ) textField.text = buffer;
	}
}
}
