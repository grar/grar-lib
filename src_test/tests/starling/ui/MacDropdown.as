/**
 * Code by Rodrigo López Peker (grar) on 3/18/16 10:45 PM.
 *
 */
package tests.starling.ui {
import com.greensock.TweenLite;

import grar.starling.abs.AbsButton;

import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;
import starling.text.TextFieldAutoSize;
import starling.text.TextFormat;

import tests.starling.MyAssetsLib;

public class MacDropdown extends AbsButton {

	private var bg:Image;
	private var lbl:TextField;
	private var _label:String ;
	private var bg_btn:Image;

	public function MacDropdown( doc:Sprite ) {
		initSize( 382, 40, true );
		super( doc );
	}

	override protected function initialize():void {
		super.initialize();
		_useHandCursor = false;
		activate( true );
		bg = getImg( "dropdown_bg", null, bg );
		MyAssetsLib.setScale9( bg, 10 );

		bg_btn = getImg("dropdown_btn", null, null ) ;


		lbl = new TextField(_w-20,_h, "Fullscreen", new TextFormat("helveticaneue_medium", 27, 0x282828, "left" )) ;
		lbl.autoSize = TextFieldAutoSize.VERTICAL ;
		lbl.x = 18 ;

		addChild(bg) ;
		addChild(bg_btn) ;
		addChild(lbl) ;

		invalidateDraw();
	}

	override protected function draw():void {
		super.draw();
		bg.width = _w - bg_btn.width + 2 ;
		bg.height = _h +1 ;
		bg_btn.x = _w - bg_btn.width ;
		lbl.y = ( _h - lbl.height >> 1 ) + 2 ;
		lbl.width = bg_btn.x - lbl.x * 2 ;
	}

	override protected function press():void {
		super.press();
//		lbl.format.color = 0xFFFFFF ;
//		getImg("button_bg_pressed", null, bg ) ;
		invalidateDraw() ;
//		TweenLite.to( bg2, 0.2, {alpha: 1} );
	}

	override protected function release( touching:Boolean ):void {
		super.release( touching );
//		lbl.format.color = 0x282828;
//		getImg("button_bg", null, bg ) ;
		invalidateDraw() ;
//		var o:Object = {alpha: 0};
//		if ( touching ) {
//			checked = !checked;
//			o.onComplete = switchTexture;
//		}
//		TweenLite.to( bg2, 0.3, o );
	}

	private function switchTexture():void {
//		getImg( _checked ? "chk_sel_bg_over" : "chk_bg_over", null, bg2 );
	}


	private function getImg( id:String, doc:Sprite = null, img:Image = null, center:Boolean = false ):Image {
		return MyAssetsLib.getImage( id, doc, img, center );
	}

	public function get label():String {
		return _label;
	}

	public function set label( value:String ):void {
		_label = value;
		lbl.text = _label ;
		invalidateDraw() ;
	}
}
}
