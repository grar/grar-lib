/**
 * Code by Rodrigo López Peker (grar) on 3/18/16 10:45 PM.
 *
 */
package tests.starling.ui {
import com.greensock.TweenLite;

import grar.starling.abs.AbsButton;

import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;
import starling.text.TextFieldAutoSize;
import starling.text.TextFormat;

import tests.starling.MyAssetsLib;

public class MacButton extends AbsButton {

	private var bg:Image;
	private var lbl:TextField;
	private var _label:String ;

	public function MacButton( doc:Sprite ) {
		initSize( 242, 40, true );
		super( doc );
	}

	override protected function initialize():void {
		super.initialize();
		_useHandCursor = false;
		activate( true );
		bg = getImg( "button_bg", null, bg );
		MyAssetsLib.setScale9( bg, 10 );
		bg = getImg( "button_bg_pressed", null, bg );
		MyAssetsLib.setScale9( bg, 10 );
		bg = getImg( "button_bg", null, bg );

		lbl = new TextField(_w,_h, "label", new TextFormat("h4", 27, 0x232323 )) ;
		lbl.autoSize = TextFieldAutoSize.VERTICAL ;
		addChild(bg) ;
		addChild(lbl) ;

		draw() ;
	}

	override protected function draw():void {
		super.draw();
		bg.width = _w ;
		bg.height = _h + 2 ;
		lbl.width = _w ;
		lbl.y = ( _h - lbl.height >> 1 ) + 2 ;
	}

	override protected function press():void {
		super.press();
		lbl.format.color = 0xFFFFFF ;
		getImg("button_bg_pressed", null, bg ) ;
		invalidateDraw() ;
//		TweenLite.to( bg2, 0.2, {alpha: 1} );
	}

	override protected function release( touching:Boolean ):void {
		super.release( touching );
		lbl.format.color = 0x282828;
		getImg("button_bg", null, bg ) ;
		invalidateDraw() ;
//		var o:Object = {alpha: 0};
//		if ( touching ) {
//			checked = !checked;
//			o.onComplete = switchTexture;
//		}
//		TweenLite.to( bg2, 0.3, o );
	}

	private function switchTexture():void {
//		getImg( _checked ? "chk_sel_bg_over" : "chk_bg_over", null, bg2 );
	}


	private function getImg( id:String, doc:Sprite = null, img:Image = null, center:Boolean = false ):Image {
		return MyAssetsLib.getImage( id, doc, img, center );
	}

	public function get label():String {
		return _label;
	}

	public function set label( value:String ):void {
		_label = value;
		lbl.text = _label ;
		invalidateDraw() ;
	}
}
}
