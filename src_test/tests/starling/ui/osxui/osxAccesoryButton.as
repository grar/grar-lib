/**
 * Code by Rodrigo López Peker (grar) on 3/21/16 6:14 PM.
 *
 */
package tests.starling.ui.osxui {
import grar.starling.abs.AbsButton;

import starling.display.Image;
import starling.display.Sprite;

import tests.starling.MyAssetsLib;

public class osxAccesoryButton extends AbsButton {

	public var img:Image;
	public var centerTexture:Boolean;

	public function osxAccesoryButton( doc:Sprite = null ) {
		initSize(10,10,true) ;
		dispatchStateEvents = true ;
		super( doc );
	}

	override protected function initialize():void {
		super.initialize();
		_useHandCursor = false ;
		centerTexture = true;
		img = new Image( null );
		addChild( img );
		invalidateDraw();
	}

	public function setTextureId( id:String, doInvalidateDraw:Boolean=false ):void {
		img = MyAssetsLib.getImage( id, this, img, centerTexture );
		_w = img.width ;
		_h = img.height ;
		if( doInvalidateDraw ) invalidateDraw() ;
	}

	override protected function press():void {
		super.press();
//		setTextureId( udata.pressId );
	}

	override protected function release( touching:Boolean ):void {
		super.release( touching );
//		setTextureId( udata.outId );
	}

	override protected function draw():void {
//		super.draw();
		if ( _hitArea && _useHitArea ) {
			_hitArea.x = -_hitAreaOffset;
			_hitArea.y = -_hitAreaOffset;
			if( centerTexture ){
				_hitArea.x -= _w >> 1 ;
				_hitArea.y -= _h >> 1 ;
			}
			_hitArea.readjustSize( _w + _hitAreaOffset * 2, _h + _hitAreaOffset * 2 );
		}
	}

	public function setTx( outId:String, pressId:String ):void {
		udata.outId = outId;
		udata.pressId = pressId;
		setTextureId( outId );
	}
}
}
