/**
 * Code by Rodrigo López Peker (grar) on 3/21/16 6:14 PM.
 *
 */
package tests.starling.ui.osxui {
import com.greensock.TweenLite;

import flash.events.MouseEvent;
import flash.geom.Point;

import grar.data.CommonDef;
import grar.starling.abs.AbsSprite;

import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;

import tests.starling.MyAssetsLib;

public class osxSliderStep extends AbsSprite {

	private var bar_bg:Image;
	private var tracker:osxAccesoryButton;
	private var isDragging:Boolean;
	private var grabPoint:Point;
	private var _numNotches:int;
	private var _notchContainer:Sprite;

	public function osxSliderStep( doc:Sprite = null ) {
		initSize( 300, 20, true );
		super( doc );
	}

	override protected function initialize():void {
		super.initialize();
		useHandCursor = false;
		grabPoint = new Point();
		isDragging = false;
		bar_bg = MyAssetsLib.getImage( "slider_track", this, bar_bg, false );

//		tracker = MyAssetsLib.getImage( "slider_scrubber_round", this, tracker, true );
		tracker = new osxAccesoryButton( this );
		tracker.useHitArea = true;
		tracker.hitAreaOffset = 10;
		tracker.centerTexture = true;
		tracker.setTextureId("slider_scrubber");
		udata.trackerCenterX = tracker.img.texture.width / 2 ;

		_notchContainer = new Sprite();
		addChild(_notchContainer) ;


		MyAssetsLib.setScale9( bar_bg, 3 );
		invalidateDraw();

		numNotches = 10 ;
		activate( true );
	}

	override public function activate( flag:Boolean ):void {
		super.activate( flag );
		UIHelper.listener( tracker, CommonDef.HOVER, onButtonPress, flag );
	}

	private function onButtonPress( e:Event ):void {
		if ( isDragging ) return;
		isDragging = true;
		tracker.setTextureId("slider_scrubber_press");
		FlashSpriteContainer.instance.stage.addEventListener( MouseEvent.MOUSE_MOVE, handleStageMouse );
		FlashSpriteContainer.instance.stage.addEventListener( MouseEvent.MOUSE_UP, handleStageMouse );
		FlashSpriteContainer.instance.localMouse( tracker, grabPoint );
	}

	private function handleStageMouse( event:MouseEvent ):void {
		if ( event.type == MouseEvent.MOUSE_MOVE ) {
			// move!
			var p:Point = FlashSpriteContainer.instance.localMouse( this );
//			tracker.x = p.x - grabPoint.x ;
			var tx:Number = p.x - grabPoint.x;
			TweenLite.to( tracker, 0.1, {x: tx, onUpdate:updateTrackerPos } );
		} else {
			tracker.setTextureId("slider_scrubber");
			FlashSpriteContainer.instance.stage.removeEventListener( MouseEvent.MOUSE_MOVE, handleStageMouse );
			FlashSpriteContainer.instance.stage.removeEventListener( MouseEvent.MOUSE_UP, handleStageMouse );
			isDragging = false;
		}
	}

	private function updateTrackerPos():void {
		constrainTrackerPos();
	}

	private function constrainTrackerPos():void {
		if ( tracker.x < udata.trackerCenterX ) tracker.x = udata.trackerCenterX;
		else if ( tracker.x > _w-udata.trackerCenterX ) tracker.x = _w-udata.trackerCenterX;
	}

	override protected function draw():void {
		super.draw();
//		_hitArea.readjustSize( _w, _h );
		bar_bg.width = _w;
		bar_bg.y = _h - bar_bg.texture.height >> 1;
		tracker.y = ( _h >> 1 ) - 6 ;
		tracker.x = _w >> 1;
		_notchContainer.y = tracker.y - tracker.img.height/2 ;
	}

	private function redrawNotches():void {
		var min:int = udata.trackerCenterX ;
		var maxW:int = _w - min * 2 ;
		var sep:Number = maxW / (_numNotches-1) ;

		// clear notches.
		_notchContainer.removeChildren(0, -1, true) ;

		for ( var i:int = 0; i < _numNotches; i++ ) {
			var notch:Image = MyAssetsLib.getImage( "slider_notch", _notchContainer, null, false );
			notch.y = - notch.height ;
			notch.x = min + i * sep - notch.width/2 ;
		}

	}

	public function get numNotches():int {
		return _numNotches;
	}

	public function set numNotches( value:int ):void {
		_numNotches = value;
		redrawNotches();
	}

}
}
