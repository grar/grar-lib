/**
 * Code by Rodrigo López Peker (grar) on 3/21/16 3:30 PM.
 *
 */
package tests.starling.ui.osxui {
import flash.display.Sprite;
import flash.geom.Point;

import grar.utils.AppUtils;

import starling.display.DisplayObject;

public class FlashSpriteContainer extends Sprite {

	public static var instance:FlashSpriteContainer;

	public function FlashSpriteContainer() {
		instance = this;
		AppUtils.stage.addChild( this );
		scaleX = scaleY = 1 / AppUtils.retinaFactor;
	}

	public function localMouse( obj:DisplayObject, p:Point = null ):Point {
		p = getMousePos( p );
		return obj.globalToLocal( p, p );
	}

	public function getMousePos( p:Point = null ):Point {
		if ( !p ) p = UIHelper.POINT;
		p.setTo( mouseX / AppUtils.retinaFactor, mouseY / AppUtils.retinaFactor );
		return p;
	}
}
}
