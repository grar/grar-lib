/**
 * Code by Rodrigo López Peker (grar) on 3/21/16 1:15 AM.
 *
 */
package tests.starling.ui.osxui {
import starling.display.Sprite;
import starling.text.TextField;
import starling.text.TextFieldAutoSize;
import starling.text.TextFormat;

public class osxTextFactory {
	public function osxTextFactory() {
	}

	public static function getTF(tw:int, th:int, doc:Sprite=null, color:uint = 0x212121 ):TextField{
		var format:TextFormat = new TextFormat("h4", 27, color ) ;
		var tf:TextField = new TextField(tw, th, "", format ) ;
		tf.autoSize = TextFieldAutoSize.VERTICAL ;
		tf.batchable = true ;
		if( doc ) doc.addChild(tf) ;
		return tf ;
	}
}
}
