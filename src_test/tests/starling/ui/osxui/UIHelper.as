/**
 * Code by Rodrigo López Peker (grar) on 3/21/16 11:59 AM.
 *
 */
package tests.starling.ui.osxui {

import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;

import starling.display.DisplayObject;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.EventDispatcher;

public class UIHelper {
	public function UIHelper() {
	}


	public static var MATRIX:Matrix = new Matrix();
	public static var RECT:Rectangle = new Rectangle();
	public static var POINT:Point = new Point();

	public static function init():void {
	}

	public static function getQuad( w:int, h:int, color:uint, doc:Sprite = null, centered:Boolean = false ):Quad {
		var q:Quad = new Quad( w, h, color );
		if ( doc )
			doc.addChild( q );

		if ( centered ) {
			q.alignPivot();
		}
//		q.touchable = false;
		return q;
	}


	//===================================================================================================================================================
	//
	//      ------  SIGNALS AND EVENT LISTENERS MANAGERs.
	//
	//===================================================================================================================================================

	public static function listenerGroup( target:*, types:Array, callback:Function, add:Boolean,
										  weak:Boolean = true ):void {
		var method:String = add ? 'addEventListener' : 'removeEventListener';
		for each( var type:String in types ) {
			var params:Array = [type, callback];
			if ( weak && add ) params.push( 0, false, true );
			target[method].apply( null, params );
		}
	}

	public static function starlingListener( target:*, type:*, callback:Function,
											 add:Boolean ):void {
		var method:String = add ? 'addEventListener' : 'removeEventListener';
		var params:Array = [type, callback];
		if ( type is Array ) {
			for each( var t:String in type ) {
				params = [t, callback];
				target[method].apply( null, params );
			}
		} else {
			// string, normal type.
			target[method].apply( null, params );
		}
	}

	public static function starlingListeners( targets:Array, type:String, callback:Function, add:Boolean ):void {
		var method:String = add ? 'addEventListener' : 'removeEventListener';
		var params:Array = [type, callback];
		for each( var btn:* in targets ) {
			if ( btn ) {
				btn[method].apply( null, params );
			}
		}
	}

	public static function starlingListenerGroup( target:*, types:Array, callback:Function, add:Boolean ):void {
		var method:String = add ? 'addEventListener' : 'removeEventListener';
		for each( var type:String in types ) {
			var params:Array = [type, callback];
			target[method].apply( null, params );
		}
	}

	public static function listeners( targets:Array, type:String, callback:Function, add:Boolean,
									  weak:Boolean = true ):void {
		var method:String = add ? 'addEventListener' : 'removeEventListener';
		var params:Array = [type, callback];
		if ( weak && add ) params.push( 0, false, true );
		for each( var btn:* in targets ) {
			if ( btn ) {
				btn[method].apply( null, params );
			}
		}
	}

	public static function tapListener( target:EventDispatcher, callback:Function, add:Boolean ):void {
		var method:String = add ? 'addEventListener' : 'removeEventListener';
		target[method].apply( null, [Event.TRIGGERED, callback] );
	}

	public static function listener( target:*, type:*, callback:Function,
									 add:Boolean, weak:Boolean = false ):void {
		var method:String = add ? 'addEventListener' : 'removeEventListener';
		var params:Array = [type, callback];
		if ( weak && add ) params.push( 0, false, true );
		if ( type is Array ) {
			for each( var t:String in type ) {
				params = [t, callback];
				target[method].apply( null, params );
			}
		} else {
			// string, normal type.
			target[method].apply( null, params );
		}
	}

	public static function signals( signals:Array, callbacks:Array, add:Boolean ):void {
		var method:String = add ? 'add' : 'remove';
		if ( signals.length != callbacks.length ) return;
		var len:int = signals.length;
		for ( var i:int = 0; i < len; i++ ) {
			signals[i][method].apply( null, [callbacks[i]] );
		}
	}

	public static function signal( signal:*, callback:Function, add:Boolean ):void {
		var method:String = add ? 'add' : 'remove';
		signal[method].apply( null, [callback] );
	}

	public static function signalGroup( signalProp:String, items:Array, callback:Function,
										add:Boolean ):void {
		var method:String = add ? 'add' : 'remove';
		var len:int = items.length;
		for ( var i:int = 0; i < len; i++ ) {
			if ( signalProp in items[i] ) items[i][signalProp][method].apply( null, [callback] );
		}
	}

	public static function callGroup( items:Array, methodName:String ):void {
		var len:int = items.length;
		for ( var i:int = 0; i < len; i++ ) {
			if ( items[i] ) {
				if ( items[i].hasOwnProperty( methodName ) ) {
					items[i][methodName]();
				}
			}
		}
	}

	public static function activateGroup( items:Array, flag:Boolean ):void {
		var len:int = items.length;
		for ( var i:int = 0; i < len; i++ ) {
			if ( items[i] ) items[i].activate( flag );
		}
	}


	public static function isMouseHit( img:DisplayObject, offsetX:Number = NaN, offsetY:Number = NaN ):Boolean {
		var p:Point = FlashSpriteContainer.instance.getMousePos();
		var r:Rectangle = img.getBounds( img.stage );
		if ( !isNaN( offsetX ) ) {
			if ( isNaN( offsetY ) )
				offsetY = offsetX;

			r.inflate( offsetX, offsetY );
		}
		return r.contains( p.x, p.y );
//		trace("rect:", r, p, "hit?", r.contains( p.x, p.y) ) ;
//		trace("r2", r) ;
//		trace("r3", r) ;
//		img.globalToLocal( p, p );
//		return img.hitTest( p );
	}
}
}
