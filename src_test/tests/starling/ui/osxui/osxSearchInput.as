/**
 * Code by Rodrigo López Peker (grar) on 3/21/16 2:13 AM.
 *
 */
package tests.starling.ui.osxui {
import com.greensock.TweenLite;

import flash.events.Event;
import flash.events.FocusEvent;
import flash.geom.Point;

import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;

import tests.starling.MyAssetsLib;

public class osxSearchInput extends osxTextInput {
	private var icoContainer:Sprite;
	private var ico_search:Image;
	private var ico_clear:Image;
	private var lbl_tf:TextField;
	private var _label:String;
	protected var _icoSep:int;

	public function osxSearchInput( doc:Sprite ) {
		if ( _w == 0 ) initSize( 300, 44, true );
		super( doc );
	}

	override protected function initialize():void {
		super.initialize();
		_icoSep = 11;
		_textPadding = 0;
		icoContainer = new Sprite();
		addChild( icoContainer );
		ico_search = MyAssetsLib.getImage( "ico_search", icoContainer, ico_search, true );
		ico_clear = MyAssetsLib.getImage( "btn_clear", this, ico_clear, true );
		udata.icoW2 = ico_search.texture.width / 2;
		udata.icoClearW2 = ico_clear.texture.width / 2;
		udata.icoClearOff = 8;
		lbl_tf = osxTextFactory.getTF( _w, -1, icoContainer, 0xB2B2B2 );
		label = "Search";
		ico_search.x = 0;
		ico_clear.visible = false;
		lbl_tf.x = ico_search.x + udata.icoW2 + _icoSep;
	}

	override protected function initStyle():void {
		borderOff = 6;
		borderOffOnFocus = 12;
		udata.skinBg = "search_bg";
		udata.skinBgFocus = "search_bg_focus";
		udata.skinBgDisabled = "search_bg_disabled";
		udata.skinBorder = "search_bg_focus_border";
		udata.borderS9 = 14;
		udata.bgS9 = 8;
	}

	override protected function onEnabled():void {
		super.onEnabled();
		if ( _enabled ) {
			icoContainer.alpha = 1;
		} else {
			icoContainer.alpha = 0.5;
		}
	}

	override protected function release( touching:Boolean ):void {
		super.release( touching );
		if ( touching ) {
			var p:Point = FlashSpriteContainer.instance.getMousePos();
			ico_clear.globalToLocal( p, p );
			if ( ico_clear.hitTest( p ) ) {
				clearText();
			}
		}
	}

	protected function clearText():void {
		input_tf.text = "";
		handleTextChange( null );
		ico_clear.visible = false;
		unfocus();
	}

	override protected function draw():void {
		super.draw();
		ico_search.y = _h >> 1;
		lbl_tf.y = _h - lbl_tf.height >> 1;
		icoContainer.x = _w - udata.lblIcoW >> 1;
		ico_clear.y = _h >> 1;
		ico_clear.x = _w - ico_clear.y;
	}

	override public function activate( flag:Boolean ):void {
		super.activate( flag );
	}

	override protected function handleFocus( e:FocusEvent ):void {
		super.handleFocus( e );
		var tx:int;
		if ( _hasFocus ) {
			// move search.
			tx = udata.icoW2 + _icoSep;
			TweenLite.to( icoContainer, 0.15, {x: tx} );
//			icoContainer.x = tx ;
			input_tf.textColor = 0x0;
		} else {

		}
	}

	override protected function delayedRemoveFocus():void {
		super.delayedRemoveFocus();
		input_tf.textColor = 0x565656;
		if ( input_tf.text == "" ) {
			var tx:int = _w - udata.lblIcoW >> 1;
			TweenLite.to( icoContainer, 0.25, {x: tx} );
		}
	}

	override protected function handleTextChange( e:Event ):void {
		super.handleTextChange( e );
		var txt:String = input_tf.text;
		ico_clear.visible = txt;
		lbl_tf.visible = txt == "";
	}

	override protected function repositionField():void {
		super.repositionField();
		var tx:int = ( udata.icoW2 + _icoSep ) * 2;
		input_tf.x = stageBounds.x + tx;
		input_tf.width = ico_clear.x - tx - udata.icoClearW2 - ( ico_clear.y - udata.icoClearW2 );
	}

	public function get label():String {
		return _label;
	}

	public function set label( value:String ):void {
		lbl_tf.text = value;
		dly( 0, function () {
			lbl_tf.width = lbl_tf.textBounds.width + 2;
			udata.lblIcoW = udata.icoW2 + _icoSep + lbl_tf.width;
			invalidateDraw();
		} );
	}
}
}
