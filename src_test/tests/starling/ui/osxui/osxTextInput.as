/**
 * Code by Rodrigo López Peker (grar) on 3/21/16 2:13 AM.
 *
 */
package tests.starling.ui.osxui {
import com.greensock.TweenLite;

import flash.events.Event;
import flash.events.FocusEvent;
import flash.geom.Point;
import flash.geom.Rectangle;

import grar.starling.abs.AbsButton;
import grar.utils.AppUtils;

import starling.display.Image;
import starling.display.Sprite;

import tests.starling.MyAssetsLib;
import tests.starling.ui.FlashTextInput;

public class osxTextInput extends AbsButton {

	public var bg:Image;
	protected var offset_bg_rect:Rectangle;
	protected var border:Image;
	protected var borderOff:int = 6;
	protected var input_tf:FlashTextInput;
	protected var stageBounds:Rectangle;
	protected var _hasFocus:Boolean;
	protected var _textPadding:int;
	protected var borderOffOnFocus:int;

	public function osxTextInput( doc:Sprite ) {
		if ( _w == 0 ) initSize( 200, 44, true );
		super( doc );
	}

	override protected function initialize():void {
		super.initialize();
		offset_bg_rect = new Rectangle( 1, 0, 1, 2 );
		initStyle();
		_textPadding = 6;
		_useHandCursor = false;
		initTextures();
		input_tf = new FlashTextInput();
//		input_tf.debugSize( true );
		input_tf.visible = false;
		border.visible = false;

		// we're using a subclass as well, so we need those elements initialized
		dly( 0, function () {
			invalidateDraw();
			activate( true );
		} );
	}

	protected function initStyle():void {
		borderOff = 6;
		borderOffOnFocus = 12;
		udata.skinBg = "tf_bg";
		udata.skinBgFocus = "tf_bg_focus";
		udata.skinBgDisabled = "tf_bg_disabled";
		udata.skinBorder = "tf_bg_focus_border";
		udata.borderS9 = 8;
		udata.bgS9 = 2;
	}

	override protected function onEnabled():void {
//		super.onEnabled();
		if ( _enabled ){
			bg = MyAssetsLib.getImage( udata.skinBg, null, bg );
		} else {
			bg = MyAssetsLib.getImage( udata.skinBgDisabled, null, bg );
		}
	}

	protected function initTextures():void {
		border = MyAssetsLib.getImage( udata.skinBorder, this, border, true );
		MyAssetsLib.setScale9( border, udata.borderS9 );

		// scale9 for bg skin.
		bg = MyAssetsLib.getImage( udata.skinBg, null, bg );
		MyAssetsLib.setScale9Special( bg, udata.bgS9, offset_bg_rect );
		bg = MyAssetsLib.getImage( udata.skinBgFocus, null, bg );
		MyAssetsLib.setScale9Special( bg, udata.bgS9, offset_bg_rect );
		bg = MyAssetsLib.getImage( udata.skinBgDisabled, null, bg );
		MyAssetsLib.setScale9Special( bg, udata.bgS9, offset_bg_rect );
		bg = MyAssetsLib.getImage( udata.skinBg, this, bg );
	}

	override public function activate( flag:Boolean ):void {
		super.activate( flag );
		UIHelper.listenerGroup( input_tf, [FocusEvent.FOCUS_OUT, FocusEvent.FOCUS_IN], handleFocus, flag );
		UIHelper.listener( input_tf, Event.CHANGE, handleTextChange, flag );
	}

	protected function handleTextChange( e:Event ):void {
	}

	protected function handleFocus( e:FocusEvent ):void {
		var has_focus:Boolean = e.type == FocusEvent.FOCUS_IN;
		if( has_focus == _hasFocus ) return ;
		input_tf.visible = true;
		if ( has_focus ) {
			_hasFocus = true;
			repositionField();
//			input_tf.visible = true;
			bg = MyAssetsLib.getImage( udata.skinBgFocus, null, bg );
			var tw:int = _w + borderOff * 2;
			var th:int = _h + borderOff * 2;
			setProps( border, {width: tw + borderOffOnFocus, height: th + borderOffOnFocus, alpha: 0} );
			TweenLite.to( border, 0.3, {width: tw, height: th, alpha: 1} );
			border.visible = true;
		} else {
			// remove HAS FOCUS after 1 frame, to AVOID jumps when u press on the component area.
			dly( 0, delayedRemoveFocus );
		}
	}

	protected function delayedRemoveFocus():void {
		_hasFocus = false;
		bg = MyAssetsLib.getImage( udata.skinBg, null, bg );
		kill( border );
		border.visible = false;
	}

	override protected function press():void {
		super.press();
		focus();
	}

	public function unfocus():void {
		AppUtils.stage.focus = null ;
	}

	public function focus():void {
		if ( _hasFocus ) {
			kill( delayedRemoveFocus ) ;
			// select all text by default.
			var p:Point = FlashSpriteContainer.instance.getMousePos() ;
			if( !input_tf.hitTestPoint( p.x, p.y, false )){
				selectText();
			}
		}
		AppUtils.stage.focus = input_tf;
	}

	private function selectText():void {
		input_tf.setSelection( 0, input_tf.text.length );
	}

	protected function repositionField():void {
		stageBounds = bg.getBounds( root, stageBounds );
		input_tf.x = stageBounds.x + _textPadding;
		input_tf.width = _w - _textPadding * 2;
		input_tf.y = stageBounds.y + ( stageBounds.height - input_tf.height >> 1 ) - 2;
//		input_tf.height = _h ;
	}

	override protected function draw():void {
		super.draw();
		bg.x = -offset_bg_rect.x;
		bg.width = _w + offset_bg_rect.x + offset_bg_rect.width;
		bg.height = _h + offset_bg_rect.y + offset_bg_rect.height;
		border.x = _w >> 1;
		border.y = _h >> 1;
		border.width = _w + borderOff * 2;
		border.height = _h + borderOff * 2;
	}
}
}
