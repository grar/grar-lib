/**
 * Code by Rodrigo López Peker (grar) on 3/21/16 1:53 AM.
 *
 */
package tests.starling.ui.osxui {
import grar.starling.abs.AbsSprite;

import starling.display.Sprite;

public class osxAddRemove extends AbsSprite {
	private var less_btn:Btn;
	private var more_btn:Btn;

	public function osxAddRemove( doc:Sprite ) {
		super( doc );
	}

	override protected function initialize():void {
		super.initialize();
		less_btn = new Btn( this, false );
		more_btn = new Btn( this, true );
		more_btn.x = less_btn.w - 2;
	}
}
}

import grar.starling.abs.AbsButton;

import starling.display.Image;
import starling.display.Sprite;

import tests.starling.MyAssetsLib;

class Btn extends AbsButton {

	private var bg:Image;
	private var ico:Image;
	public var typeMore:Boolean;

	public function Btn( doc:Sprite, iconTypeMore:Boolean ) {
		initSize( 40, 40, true );
		maxDragDist = 5 ;
		typeMore = iconTypeMore;
		super( doc );
	}

	override protected function initialize():void {
		super.initialize();
		// scale9 for bg skin.
		bg = getImg( "sq_btn", this, bg );
		// apply scale to button.
		ico = getImg( typeMore ? "sq_btn_plus" : "sq_btn_minus", this, ico, true );
//		ico.scale = _w / bg.texture.width;
		invalidateDraw();
	}

	override protected function draw():void {
		super.draw();
		bg.width = _w;
		bg.height = _h;
		ico.x = _w >> 1;
		ico.y = _h >> 1;
	}

	override protected function press():void {
		super.press();
		getImg( "sq_btn_press", null, bg );
//		invalidateDraw();
	}

	override protected function release( touching:Boolean ):void {
		super.release( touching );
		getImg( "sq_btn", null, bg );
//		invalidateDraw();
	}

	override protected function onEnabled():void {
		super.onEnabled();
	}

	private function getImg( id:String, doc:Sprite = null, img:Image = null, center:Boolean = false ):Image {
		return MyAssetsLib.getImage( id, doc, img, center );
	}
}