/**
 * Code by Rodrigo López Peker (grar) on 3/21/16 1:13 AM.
 *
 */
package tests.starling.ui.osxui {
import flash.geom.Rectangle;

import grar.starling.abs.AbsButton;

import starling.display.Image;
import starling.display.Quad;
import starling.display.Sprite;
import starling.text.TextField;

import tests.starling.MyAssetsLib;

public class osxButton extends AbsButton {

	private var bg:Image;
	private var lbl:TextField;
	private var _label:String;
	private var offset_bg_rect:Rectangle;

	public function osxButton( doc:Sprite, label:String, w:int = 260 ) {
		initSize( w, 40, true );
		super( doc );
		maxDragDist = 5 ;
		this.label = label;
	}

	override protected function initialize():void {
		super.initialize();
		_useHandCursor = false;
		activate( true );

		offset_bg_rect = new Rectangle( 1, 0, 1, 2 );

		// scale9 for bg skin.
		bg = getImg( "btn", null, bg );
		MyAssetsLib.setScale9Special( bg, 8, offset_bg_rect );
		bg = getImg( "btn_press", null, bg );
		MyAssetsLib.setScale9Special( bg, 8, offset_bg_rect );
		bg = getImg( "btn_disabled", null, bg );
		MyAssetsLib.setScale9Special( bg, 8, offset_bg_rect );

		bg = getImg( "btn", null, bg );
		lbl = osxTextFactory.getTF( _w, -1 );
		addChild( bg );
		addChild( lbl );
		draw();
	}


	override protected function draw():void {
		super.draw();
		bg.x = -offset_bg_rect.x ;
		bg.width = _w + offset_bg_rect.x + offset_bg_rect.width ;
		bg.height = _h + offset_bg_rect.y + offset_bg_rect.height ;

		lbl.width = _w;
		lbl.y = ( _h - lbl.height >> 1 ) ;
	}

	override protected function onEnabled():void {
		if( _enabled ){
			lbl.format.color = 0x282828;
			getImg( "btn", null, bg );
		} else {
			getImg( "btn_disabled", null, bg );
			lbl.format.color = 0xA0A0A0;
		}
		invalidateDraw() ;
	}

	override protected function press():void {
		super.press();
		lbl.format.color = 0xFFFFFF;
		getImg( "btn_press", null, bg );
		invalidateDraw();
//		TweenLite.to( bg2, 0.2, {alpha: 1} );
	}

	override protected function release( touching:Boolean ):void {
		super.release( touching );
		lbl.format.color = 0x282828;
		getImg( "btn", null, bg );
		invalidateDraw();
	}

	private function getImg( id:String, doc:Sprite = null, img:Image = null, center:Boolean = false ):Image {
		return MyAssetsLib.getImage( id, doc, img, center );
	}

	public function get label():String {
		return _label;
	}

	public function set label( value:String ):void {
		_label = value;
		lbl.text = _label;
		invalidateDraw();
	}
}
}
