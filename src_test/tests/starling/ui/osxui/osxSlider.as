/**
 * Code by Rodrigo López Peker (grar) on 3/21/16 6:14 PM.
 *
 */
package tests.starling.ui.osxui {
import com.greensock.TweenLite;

import flash.events.MouseEvent;
import flash.geom.Point;

import grar.data.CommonDef;
import grar.starling.abs.AbsSprite;

import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;

import tests.starling.MyAssetsLib;

public class osxSlider extends AbsSprite {

	private var bar_bg:Image;
	private var barFill_bg:Image;
	private var tracker:osxAccesoryButton;
	private var isDragging:Boolean;
	private var grabPoint:Point;

	public function osxSlider( doc:Sprite = null ) {
		initSize( 300, 20, true );
		super( doc );
	}

	override protected function initialize():void {
		super.initialize();
		useHandCursor = false;
		grabPoint = new Point();
		isDragging = false;
		bar_bg = MyAssetsLib.getImage( "slider_track", this, bar_bg, false );
		barFill_bg = MyAssetsLib.getImage( "slider_track_fill", this, barFill_bg, false );

//		tracker = MyAssetsLib.getImage( "slider_scrubber_round", this, tracker, true );
		tracker = new osxAccesoryButton( this );
		tracker.useHitArea = true;
		tracker.hitAreaOffset = 10;
		tracker.centerTexture = true;
		tracker.setTextureId("slider_scrubber_round");

		MyAssetsLib.setScale9( bar_bg, 3 );
		MyAssetsLib.setScale9( barFill_bg, 3 );
		invalidateDraw();

		activate( true );
	}

	override public function activate( flag:Boolean ):void {
		super.activate( flag );
		UIHelper.listener( tracker, CommonDef.HOVER, onButtonPress, flag );
	}

	private function onButtonPress( e:Event ):void {
		if ( isDragging ) return;
		isDragging = true;
		tracker.setTextureId("slider_scrubber__round_press");
		FlashSpriteContainer.instance.stage.addEventListener( MouseEvent.MOUSE_MOVE, handleStageMouse );
		FlashSpriteContainer.instance.stage.addEventListener( MouseEvent.MOUSE_UP, handleStageMouse );
		FlashSpriteContainer.instance.localMouse( tracker, grabPoint );
	}

	private function handleStageMouse( event:MouseEvent ):void {
		if ( event.type == MouseEvent.MOUSE_MOVE ) {
			// move!
			var p:Point = FlashSpriteContainer.instance.localMouse( this );
//			tracker.x = p.x - grabPoint.x ;
			var tx:Number = p.x - grabPoint.x;
			TweenLite.to( tracker, 0.1, {x: tx, onUpdate:updateTrackerPos } );
		} else {
			tracker.setTextureId("slider_scrubber_round");
			FlashSpriteContainer.instance.stage.removeEventListener( MouseEvent.MOUSE_MOVE, handleStageMouse );
			FlashSpriteContainer.instance.stage.removeEventListener( MouseEvent.MOUSE_UP, handleStageMouse );
			isDragging = false;
		}
	}

	private function updateTrackerPos():void {
		constrainTrackerPos();
		barFill_bg.width = tracker.x;
	}

	private function constrainTrackerPos():void {
		if ( tracker.x < 0 ) tracker.x = 0;
		else if ( tracker.x > _w ) tracker.x = _w;
	}

	override protected function draw():void {
		super.draw();
//		_hitArea.readjustSize( _w, _h );
		bar_bg.width = _w;
		barFill_bg.width = _w / 2;
		bar_bg.y = barFill_bg.y = _h - bar_bg.texture.height >> 1;
		tracker.y = _h >> 1;
		tracker.x = _w >> 1;
	}
}
}
