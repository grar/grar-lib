/**
 * Code by Rodrigo López Peker on 12/30/15 3:41 PM.
 *
 */
package tests.starling.abs {
import starling.display.Sprite;

public class AbSSpriteState extends AbSSprite {

	protected var _isOpen:Boolean;
	protected var _autoOpenedClosed:Boolean = false;

	/**
	 * constructor.
	 * @param doc
	 */
	public function AbSSpriteState( doc:Sprite = null ) {
		super( doc );
	}


	public function open():void {
		_isOpen = true;

		if ( !_uiReady )
			setupUI();

		dispatchEventWith( Def.OPEN );

		if ( _autoOpenedClosed )
			opened();
	}

	public function opened():void {
		_isOpen = true;
		if ( !_active )
			activate( true );

		dispatchEventWith( Def.OPENED );
	}

	public function close():void {
		_isOpen = false;
		if ( _active )
			activate( false );

		dispatchEventWith( Def.CLOSE );

		if ( _autoOpenedClosed )
			closed();
	}

	public function closed():void {
		_isOpen = false;
		if ( _active )
			activate( false );

		if ( _uiReady )
			clearUI();

		dispatchEventWith( Def.CLOSED );
	}

	public function get isOpen():Boolean {
		return _isOpen;
	}

}
}
