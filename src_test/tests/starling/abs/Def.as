/**
 * Code by Rodrigo López Peker on 2/2/16 6:53 PM.
 *
 */
package tests.starling.abs {
public class Def {
	public function Def() {
	}


	public static const OPEN:String = "open" ;
	public static const CLOSE:String = "close" ;
	public static const OPENED:String = "opened" ;
	public static const CLOSED:String = "closed" ;
	public static const NAV_CHANGE:String = "navChange" ;
	// key press event!
	public static const ENTER:String = "enter" ;
	public static const KEYBOARD_ACTIVATE:String = "keyboardActivate" ;
	public static const KEYBOARD_DEACTIVATE:String = "keyboardDeactivate" ;
	public static const FOCUS_IN:String = "focusIn" ;
	public static const FOCUS_OUT:String = "focusOut" ;
	public static const CHANGE:String = "change" ;

	public static const SCREEN_HOME:String = "screenHome" ;
	public static const SCREEN_SERVICES:String = "screenServices" ;
	public static const SCREEN_BORROW:String = "screenBorrow" ;
	public static const SCREEN_JOB_BOARD:String = "screenJobBoard" ;
	public static const SCREEN_CALENDAR:String = "screenCalendar" ;
	public static const SCREEN_COMMUNITY:String = "screenCommunity" ;
	public static const SCREEN_MY_PROFILE:String = "screenMyProfile" ;


	public static const SCREEN_SETTINGS:String = "screenSettings" ;
}
}
