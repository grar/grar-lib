/**
 * Code by Rodrigo López Peker (grar) on 2/24/16 11:08 PM.
 *
 */
package tests.starling {

import starling.core.Starling;
import starling.display.Sprite;
import starling.text.TextField;
import starling.text.TextFieldAutoSize;
import starling.text.TextFormat;

import tests.starling.abs.AbSSprite;
import tests.starling.ui.osxui.FlashSpriteContainer;
import tests.starling.ui.osxui.osxAddRemove;
import tests.starling.ui.osxui.osxButton;
import tests.starling.ui.osxui.osxSearchInput;
import tests.starling.ui.osxui.osxSlider;
import tests.starling.ui.osxui.osxSliderStep;
import tests.starling.ui.osxui.osxTextInput;

public class StarlingUI extends AbSSprite {

	public function StarlingUI( doc:Sprite = null ) {
		super( doc );
		Starling.current.stage.color = 0xf2f2f2;
	}

	override protected function initialize():void {
		// global container to add Flash elements.
		new FlashSpriteContainer();

		var container:Sprite = new Sprite();
		addChild( container );
		container.x = 20;
		container.y = 20;

		var btn:osxButton = new osxButton( container, "Press here...", 260 );
		btn.move( 0, 0 );

		var btn2:osxButton = new osxButton( container, "Disabled button...", 260 );
		btn2.enabled = false;
		btn2.move( 0, 50 );
//
		var addRemove_btns:osxAddRemove = new osxAddRemove( container );
		addRemove_btns.move( 0, 100 );
//
		var inputText:osxTextInput = new osxTextInput( container );
		inputText.move( 0, 200 );

		var searchText:osxSearchInput = new osxSearchInput( container );
		searchText.move( 0, 250 );

		var searchText2:osxSearchInput = new osxSearchInput( container );
		searchText2.label = "Search disabled";
		searchText2.enabled = false;
		searchText2.move( 0, 300 );

		var sliderH:osxSlider = new osxSlider( container );
		sliderH.move( 0, 400 );

		var sliderStep:osxSliderStep = new osxSliderStep( container );
		sliderStep.move( 0, 480 );


		/*var check:MacCheckbox = new MacCheckbox( this );
		 check.move( 50, 50 );

		 var panel_bg:Image = MyAssetsLib.getImage( "tab_area_bg", this );
		 MyAssetsLib.setScale9( panel_bg, 10 );
		 panel_bg.width = 350;
		 panel_bg.height = 350;
		 addChildAt( panel_bg, 0 );
		 panel_bg.x = 25;
		 panel_bg.y = 50;

		 var btn:MacButton = new MacButton( this );
		 btn.label = "More Options...";
		 btn.move( 50, 100 );

		 var radio:MacRadioButton = new MacRadioButton( this );
		 radio.move( 50, 150 );

		 var dropdown:MacDropdown = new MacDropdown( this );
		 dropdown.move( 50, 200 );

		 // labels.
		 var text1:TextField = addTf( "Hola mundo", "h1", 27, 0x232323 );
		 var text2:TextField = addTf( "Hola mundo", "h2", 27, 0xFFFFFF );
		 var text3:TextField = addTf( "Hola mundo", "h3", 27, 0xFFFFFF );
		 var text4:TextField = addTf( "Hola mundo", "h4", 27, 0x232323 );
		 text1.x = text2.x = text3.x = text4.x = 50;
		 text1.y = 350;
		 text2.y = 400;
		 text3.y = 450;
		 text4.y = 500;

		 var tf2:FlashTextInput = new FlashTextInput();
		 tf2.x = 100;
		 tf2.y = 100;
		 tf2.width = 300;
		 tf2.height = 50;
		 tf2.text = "Hola";*/
	}

	private function addTf( label:String, font:String, size:int, color:uint ):TextField {
		var format:TextFormat = new TextFormat( font, size, color, "left" );
		var tf:TextField = new TextField( 200, -1, label, format );
		tf.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
		addChild( tf );
		return tf;
	}
}
}
