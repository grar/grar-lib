/**
 * Code by Rodrigo López Peker on 2/17/16 4:16 PM.
 *
 */
package {
import flash.display.Sprite;

import grar.utils.AppUtils;

import tests.StarlingTest;

import tests.test_StringUtils;

[SWF(width="800", height="600", backgroundColor="#323232", frameRate="60")]
public class MainTest extends Sprite {

	public static var instance:MainTest;

	public function MainTest() {
		AppUtils.init( stage, false );
		// use as an entry point to test the grar library.
		init();
	}

	private function init():void {
//		new test_StringUtils().run();
		new StarlingTest().run() ;
	}
}
}