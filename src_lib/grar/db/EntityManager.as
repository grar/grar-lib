/**
 * Code by Rodrigo López Peker (grar) on 3/1/16 2:20 PM.
 *
 */
package grar.db {

import flash.data.SQLConnection;
import flash.data.SQLResult;
import flash.data.SQLSchemaResult;
import flash.data.SQLStatement;
import flash.data.SQLTableSchema;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;
import flash.filesystem.File;
import flash.net.Responder;
import flash.utils.Dictionary;
import flash.utils.describeType;

import grar.utils.ClassUtils;

public class EntityManager {

	private static const MDATA_TABLE:String = "Table";
	private static const MDATA_COLUMN:String = "Column";
	private static const MDATA_ID:String = "Id";

	private static var defaultDatabaseName:String = "db.sqlite";
	private static var conn:SQLConnection;
	private static var _dbFile:File;

	private static var descDictionary:Dictionary;
	
	public function EntityManager() {
	}

	public static function open( dbFileName:String = null ):void {
		if ( _dbFile ) {
			trace("Database " + _dbFile.name + " already loaded");
			return;
		}
		_dbFile = dbFileName ?
				File.applicationStorageDirectory.resolvePath( dbFileName ) :
				File.applicationStorageDirectory.resolvePath( defaultDatabaseName );
		descDictionary = new Dictionary();
		conn = new SQLConnection();
		conn.addEventListener( SQLEvent.OPEN, function ( e:SQLEvent ):void {
			trace( "DB " + _dbFile.name + " successfully opened" );
		} );
		conn.open( _dbFile );
	}

	public static function close():void {
		_dbFile = null;
		descDictionary = null;
		conn.close();
	}

	public static function registerClass( clazz:Object ):void {
		if ( !clazz is IEntity ) throw new Error("Only classes implementing IEntity are allowed");
		if ( !_dbFile ) throw new Error( "ERROR > call EntityManager.load() first to generate an instance" );
		var desc:XML = describeType( clazz );
//		trace(desc) ;

		// look for the metadata tags on the members.
		var metadata:Object = getMetadataKeys( desc.factory[0], MDATA_TABLE );
		var tableName:String = metadata ? metadata.name : ClassUtils.getClassNameSimple(clazz);

		var query:String = "CREATE TABLE IF NOT EXISTS " + tableName;

		var fieldNames:Array = new Array(); // the names of the model object's fields
		var fields2Columns:Dictionary = new Dictionary(); // contains the mapping between model fields and DB columns

		// variables describing aspects of the DB schema:
		var idColumnName:String; // the name of the primary key
		var colTypes:Array = new Array(); // the types of the columns
		var colMappers:Array = new Array(); // the mapping functions to convert from DB to model
		var idColumnDef:String = ""; // the query portion describing the primary key
		var columnsDef:String = ""; // the query portion describing the primary key

		for each ( var a:XML in desc.factory.variable + desc.factory.accessor ) {
			metadata = getMetadataKeys( a );
			if ( !metadata ) continue;

//			trace( "JSON::", JSON.stringify( metadata ) );
//			if ( metadata.hasOwnProperty( "transient" ) ) continue;
			if ( metadata.metaName == MDATA_ID ) {
				idColumnName = metadata.name;
				idColumnDef = idColumnName + " TEXT PRIMARY KEY";
				if ( metadata.hasOwnProperty( "increment" ) ) idColumnDef += " " + metadata.increment;
				fieldNames.push(String(a.@name));
				fields2Columns[String(a.@name)] = idColumnName;
				colTypes.push("TEXT");
				colMappers.push(NaN);
				continue;
			}
			var name:String = metadata.hasOwnProperty( "name" ) ? metadata.name : a.@name;
			var type:String = metadata.hasOwnProperty( "type" ) ? metadata.type : convertToDbType( a.@type );
			var mapper:String = metadata.hasOwnProperty( "mapper" ) ? metadata.mapper : null;
//???		if (mapper && !EntityManager.hasOwnProperty( mapper )) throw new Error("Unknown mapping function");
			fieldNames.push(String(a.@name));
			fields2Columns[String(a.@name)] = name;
			colTypes.push(type);
			colMappers.push(mapper ? mapper : NaN);
			columnsDef += name + " " + type + ", ";
		}
		columnsDef = columnsDef.substring( 0, columnsDef.length - 2 );
		query += " (" + idColumnDef + ", " + columnsDef + ")";

		descDictionary[ ClassUtils.getClassName(clazz) ] = {
			tableName:tableName,
			idName:idColumnName,
			fieldNames:fieldNames, // class's field names
			fields2Columns: fields2Columns, // dictionary binding the above 2
			colTypes:colTypes,
			colMappers:colMappers,
			xml:desc
		};

		var state:SQLStatement = new SQLStatement();
		state.sqlConnection = conn;
		state.text = query;
		state.execute();
	}

	/**
	 * Get the parsed description of a class.
	 * The parameter can be either the Class or an object belonging to the Class
	 */
	[Inline]
	private static function getDescription( clazz:Object ):Object {
		return descDictionary[ ClassUtils.getClassName(clazz) ];
	}

	public static function save( obj:IEntity, success:Function = null, failure:Function = null ):void {
		var desc:Object = getDescription(obj);
		if (! desc ) {
			throw new Error( "ERROR > call EntityManager::registerClass(" + ClassUtils.getClassNameSimple(obj) + ") first" );
		}

		var values:String = "";
		var query:String = "INSERT INTO " + desc.tableName + "(";
		if ( obj.id ) {
			query += desc.idName + ", ";
			values += "'" + obj.id + "', ";
		}
		for ( var i:int=0; i<desc.colNames.length; i++ ) {
			query += desc.colNames[i] + ", ";
			values += "'" + obj[desc.fieldNames[i]] + "', ";
			// TODO: var mapper:String = metadata && metadata.hasOwnProperty( "mapper" ) ? metadata.mapper : null; USE IT TO MAP VALUE
		}
		query = query.substring( 0, query.length - 2 );
		query += ") VALUES (" + values;
		query = query.substring( 0, query.length - 2 );
		query += ")";
		trace(query);

		// TODO: we can use a pool here?
		var state:SQLStatement = new SQLStatement();
		state.sqlConnection = conn;
		state.text = query;
		state.addEventListener( SQLEvent.RESULT, function ( e:SQLEvent ):void { trace( "Save successfully!" ) } );
		state.addEventListener( SQLErrorEvent.ERROR, function ( e:SQLErrorEvent ):void { trace( e.error.message ) } );
		state.execute();
	}

	public static function saveBulk( objs:Array, success:Function = null, failure:Function = null ):void {
		var responder:Responder;
		if ( success || failure ) {
			responder = new Responder( success, failure );
		}
		conn.begin( null, responder ); // start transaction
		for each ( var obj:IEntity in objs ) {
			save( obj );
		}
		conn.commit(); // end transaction
	}

	public static function update( obj:IEntity ):void {
		var desc:Object = getDescription(obj);
		if ( !desc ) {
			throw new Error( "ERROR > call EntityManager::registerClass(" + ClassUtils.getClassNameSimple( obj ) + ") first" );
		}

		// TODO: create parametric query
		var query:String = "UPDATE " + desc.tableName + " SET ";
		if ( obj.id ) query += desc.idName + "=" + obj.id + ", ";
		for ( var i:int = 0; i < desc.colNames.length; i++ ) {
			query += desc.colNames[i] + "='" + obj[desc.fieldNames[i]] + "', ";
			// TODO: var mapper:String = metadata && metadata.hasOwnProperty( "mapper" ) ? metadata.mapper : null; USE IT TO MAP VALUE
		}
		query = query.substring( 0, query.length - 2 );
		query += " WHERE " + desc.idName + "='" + obj.id + "'";
		trace( query );

		var state:SQLStatement = new SQLStatement();
		state.sqlConnection = conn;
		state.text = query;
		state.addEventListener( SQLEvent.RESULT, function ( e:SQLEvent ):void { trace( "Update successfully!" ) } );
		state.addEventListener( SQLErrorEvent.ERROR, function ( e:SQLErrorEvent ):void { trace( e.error.message ) } );
		state.execute();
	}

	public static function remove( obj:IEntity ):void {
		var desc:Object = getDescription(obj);
		if ( !desc ) {
			throw new Error( "ERROR > call EntityManager::registerClass(" + ClassUtils.getClassNameSimple( obj ) + ") first" );
		}

		var query:String = "DELETE FROM " + desc.tableName + " WHERE " + desc.idName + "='" + obj.id + "'";

		var state:SQLStatement = new SQLStatement();
		state.sqlConnection = conn;
		state.text = query;
		state.addEventListener( SQLEvent.RESULT, function ( e:SQLEvent ):void { trace( "Remove successfully!" ) } );
		state.addEventListener( SQLErrorEvent.ERROR, function ( e:SQLErrorEvent ):void { trace( e.error.message ) } );
		state.execute();
	}

	public static function getAll( clazz:Class, fields:String = null ):Array {
		var desc:Object = getDescription(clazz);
		if ( !desc ) {
			throw new Error( "ERROR > call EntityManager::registerClass(" + ClassUtils.getClassNameSimple( clazz ) + ") first" );
		}

		var result:Array;
		if ( !fields ) fields = '*';
		var query:String = "SELECT " + fields + " FROM " + desc.tableName + " ORDER BY " + desc.idName;

		var state:SQLStatement = new SQLStatement();
		state.sqlConnection = conn;
		state.text = query;
		state.addEventListener( SQLEvent.RESULT, function ( e:SQLEvent ):void {
			var sqlResult:SQLResult = state.getResult();
			if ( sqlResult.data != null ) {
				result = [];
				for ( var r:uint = 0; r < sqlResult.data.length; r++ ) {
					var entity:IEntity = new clazz();
					for ( var i:int = 0; i < desc.colNames.length; i++ ) {
						entity[desc.fieldNames[i]] = sqlResult.data[r][desc.colNames[i]];
						// TODO: var mapper:String = metadata && metadata.hasOwnProperty( "mapper" ) ? metadata.mapper : null; USE IT TO MAP VALUE
					}
					result.push( entity );
				}
			}
		} );
		state.addEventListener( SQLErrorEvent.ERROR, function ( e:SQLErrorEvent ):void { trace( e.error.message ); } );
		state.execute();
		return result;
	}

	public static function getById( clazz:Class, id:String, fields:String=null ):IEntity {
		var desc:Object = getDescription(clazz);
		if ( !desc ) {
			throw new Error( "ERROR > call EntityManager::registerClass(" + ClassUtils.getClassNameSimple( clazz ) + ") first" );
		}

		var entity:IEntity;
		if (!fields) fields = '*';
		var query:String = "SELECT " + fields + " FROM " + desc.tableName + " WHERE " + desc.idName + "='" + id + "'";

		var state:SQLStatement = new SQLStatement();
		state.sqlConnection = conn;
		state.text = query;
		state.addEventListener( SQLEvent.RESULT, function ( e:SQLEvent ):void {
			var sqlResult:SQLResult = state.getResult();
			if ( sqlResult.data != null ) {
				entity = new clazz();
				for ( var i:int = 0; i < desc.colNames.length; i++ ) {
					try {
						entity[desc.fieldNames[i]] = sqlResult.data[0][desc.colNames[i]];
					}
					catch (e:Error) {
						// for a fucking reason, obj==null cannot be assigned to array; assign to null directly
						entity[desc.fieldNames[i]] = null;
					}
					// TODO: var mapper:String = metadata && metadata.hasOwnProperty( "mapper" ) ? metadata.mapper : null; USE IT TO MAP VALUE
				}
			}
		} );
		state.addEventListener( SQLErrorEvent.ERROR, function ( e:SQLErrorEvent ):void { trace( e.error.message ); } );
		state.execute();

		return entity;
	}

	//===================================================================================================================================================
	//
	//      ------  queries
	//
	//===================================================================================================================================================

	public static function execute( query:Query, success:Function=null, failure:Function=null ):void {
		var sql:String = toSQL(query);
		trace("SQL:", sql);

		var state:SQLStatement = new SQLStatement();
		state.sqlConnection = conn;
		state.text = sql;
		if ( success ) state.addEventListener( SQLEvent.RESULT, success );
		if ( failure ) state.addEventListener( SQLErrorEvent.ERROR, failure );
		state.execute();
	}

	/**
	 * Returns a query string for SQL
	 */
	private static function toSQL(query:Query):String {
		var s:String = "";
		var desc:Object = getDescription( query.getClass() ); // in fact 'query.table' is the model class
		if ( !desc ) {
			throw new Error( "ERROR > call EntityManager::registerClass(" + table + ") first" );
		}
		var table:String = (query.getDbase()) ? query.getDbase() + "." + desc.tableName : desc.tableName;

		var columns:Array = new Array();
		if (query.getFields() && query.getFields().length>0) {
			for (var field:String in query.getFields) {
				var col:String = field=="*" ? "*" : desc.fields2Columns[field];
				columns.push( col ? col : field );
			}
		}
		else {
			columns.push("*");
		}

		switch ( query.getAction() ) {
			case Query.ACTION_FIND:
				s += "SELECT " + (query.getDistinct() ? "DISTINCT " : "") + columns.join( "," ) + " FROM " + table;
				break;
			case Query.ACTION_SAVE:
				s += "INSERT INTO " + table + " (";
				for ( var f:String in query.getBody() ) {
					s += desc.fields2Columns[f] + ",";
				}
				s = s.substring( 0, s.length - 1 );
				s += ") VALUES (";
				for ( var f:String in query.getBody() ) {
					var val:Object = query.getBody()[f];
					if ( val is String ) val = "'" + val + "'";
					s += val + ",";
				}
				s = s.substring( 0, s.length - 1 );
				s += ")";
				break;
			case Query.ACTION_UPDATE:
				s += "UPDATE " + table + " SET ";
				for ( var f:String in query.getBody() ) {
					var val:Object = query.getBody()[f];
					if ( val is String ) val = "'" + val + "'";
					s += desc.fields2Columns[f] + "=" + val + ",";
				}
				s = s.substring( 0, s.length - 1 );
				break;
			case Query.ACTION_REMOVE:
				s += "DELETE ";
				s += " FROM " + table;
				break;
			default:
				throw new Error( "query with wrong action!" );
		}
		if ( query.getWhere() ) {
			s += " WHERE " + parseSQLExpression( query.getWhere(), desc.fields2Columns );
		}
		if ( query.getGroupBy() ) {
			s += " GROUP BY " + query.getGroupBy();
		}
		if ( query.getSort() ) {
			s += " ORDER BY " + query.getSort();
			if ( query.getDesc() ) {
				s += " DESC";
			}
		}
		if ( query.getLimit() ) {
			s += " LIMIT ";
			if ( query.getSkip() ) {
				s += query.getSkip() + ",";
			}
			s += query.getLimit();
		}
		return s;
	}

	private static function parseSQLExpression( expr:QueryExpression, fields2Columns:Dictionary=null ):String {
		var s:String = "";
		var oper:String = expr.oper;
		var left:Object = expr.left;
		var right:Object = expr.right;
		if ( !right || right == "NULL" ) {
			if ( oper == QueryExpression.EqualTo ) oper = QueryExpression.Is;
			if ( oper == QueryExpression.DifferentThan ) oper = QueryExpression.IsNot;
		}

		if ( oper == QueryExpression.Or ) s += "(";
		if ( left ) {
			if ( left is String ) {
				s += fields2Columns[left];
			}
			else {
				s += parseSQLExpression( left as QueryExpression, fields2Columns );
			}
		}

		s += " " + oper + " ";

		if ( right == "NULL" ) {
			s += "NULL";
		}
		else if ( right is String ) {
			s += "'" + right + "'";
		}
		else if ( right is Number ) {
			s += right;
		}
		else if ( right is QueryExpression ) {
			if ( oper == QueryExpression.Not ) s += "(";
			s += parseSQLExpression( right as QueryExpression, fields2Columns );
			if ( oper == QueryExpression.Not ) s += ")";
		}
		else { // Array
			s += "(";
			for each ( var e:Object in right ) {
				if ( e is QueryExpression ) {
					s += parseSQLExpression( QueryExpression(e), fields2Columns ) + ",";
				}
				else if ( e is String ) {
					s += "'" + e + "',";
				}
				else { // cross-fingers
					s += e + ",";
				}
			}
			s = s.substr( 0, s.length - 1 );
			s += ")";
		}
		if ( oper == QueryExpression.Or ) s += ")";
		return s;
	}

	//===================================================================================================================================================
	//
	//      ------  utils
	//
	//===================================================================================================================================================

	private static function getMetadataKeys( a:XML, key:String=null ):Object {
		var meta:XML;
		if (key) {
			meta = a.metadata.(@name == key)[0];
		}
		else {
			meta = a.metadata.(@name == MDATA_COLUMN || @name == MDATA_ID)[0];
		}
		if ( !meta ) return null;
		var o:Object = {};
		for each( var node:XML in meta.arg ) {
			var key:String = node.@key;
			var val:String = node.@value;

//			// undefined key, we can consider it an "action" maybe?
//			if ( key == "" ) {
//				if ( !o["_anonymous"] ) o["_anonymous"] = [];
//				o["_anonymous"].push( val );
//				continue;
//			}

			// force the type?
			var val2:String = val.toLowerCase();
			if ( val2 == "false" ) o[key] = false;
			else if ( val2 == "true" ) o[key] = true;
			else if ( !isNaN( Number( val2 ) ) ) o[key] = Number( val2 );
			else o[key] = val;
		}
		o['metaName'] = meta.@name;
		return o;
	}

	private static function convertToDbType( as3Type:String ):String {
		switch ( as3Type ) {
			case "String":
				return "TEXT";
		}
		return "TEXT";
	}

	public static function schemaHasTable( tableName:String ):Boolean {
		tableName = tableName.toLowerCase();
		conn.loadSchema();
		var schema:SQLSchemaResult = conn.getSchemaResult();
		for each( var table:SQLTableSchema in schema.tables ) {
			if ( table.name.toLowerCase() == tableName ) return true;
		}
		return false;
	}

	public static function openDatabaseLocation():void {
		if ( !_dbFile ) {
			throw new Error( "ERROR > call EntityManager::load() first to generate an instance" );
		}
		_dbFile.parent.openWithDefaultApplication() ;
	}

}

}
