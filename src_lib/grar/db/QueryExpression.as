/**
 * Created by tasos on 14/03/16 22:02.
 */
package grar.db {

public class QueryExpression {

	public static const EqualTo:String = "=";
	public static const DifferentThan:String = "!=";
	public static const LessThan:String = "<";
	public static const LessThanOrEqualTo:String = "<=";
	public static const GreaterThan:String = ">";
	public static const GreaterThanOrEqualTo:String = ">=";
	public static const Like:String = "LIKE";
	public static const Is:String = "IS";
	public static const IsNot:String = "IS NOT";
	public static const In:String = "IN";
	public static const NotIn:String = "NOT IN";

	public static const And:String = "AND";
	public static const Or:String = "OR";
	public static const Not:String = "NOT";

	private var _left:Object; // can be QueryExpression or String (i.e. column name)
	private var _oper:String; // one of the comparison and logical operators above
	private var _right:Object; // can be QueryExpression or Array of Query Expressions

	public function QueryExpression( left:Object, oper:String, right:Object) {
		_left = left;
		_oper = oper;
		_right = right;
		if (! _right is QueryExpression && ! _right is Array ) {
			throw new Error("Invalid 'right' expression type");
		}
	}

	public function get left():Object {
		return _left;
	}

	public function get oper():String {
		return _oper;
	}

	public function get right():Object {
		return _right;
	}

	public function toString():String {
		return "{" + (_left?_left:"") + " " + String( _oper ) + " " + _right + "}";
	}

}

}
