/**
 * Created by akota on 10/3/2016.
 */
package grar.db {
import app.model.ToolNode;

import grar.utils.ClassUtils;

public class Query {

	public static const ACTION_FIND:String = "find";
	public static const ACTION_SAVE:String = "save";
	public static const ACTION_UPDATE:String = "update";
	public static const ACTION_REMOVE:String = "remove";

	private var _dbase:String; // the database (relevant only to SQL)
	private var _class:Class; // model class which maps to the SQL table or noSQL collection
	private var _fields:Array; // fields to select upon retrieval
	private var _action:String; // one of ACTION_*
	private var _limit:Number;
	private var _skip:Number;
	private var _groupBy:String;
	private var _sort:String;
	private var _desc:Boolean; // relevant to sort
	private var _distinct:Boolean;
	private var _body:Object; // the object used to add or update an existing entry
	private var _where:QueryExpression;

	public function Query(dbase:String = null) {
		_dbase = dbase;
	}

	public function from(clazz:Class):Query {
		_class = clazz;
		return this;
	}

	public function find(fields:Array=null):Query {
		_action = ACTION_FIND;
		return this;
	}

	public function save(body:Object):Query {
		_action = ACTION_SAVE;
		_body = body;
		return this;
	}

	public function update(fields:Array):Query {
		_action = ACTION_UPDATE;
		return this;
	}

	public function remove():Query {
		_action 	            = ACTION_REMOVE;
		return this;
	}

	public function limit(limit:int):Query {
		_limit = limit;
		return this;
	}

	public function skip(skip:int):Query {
		_skip = skip;
		return this;
	}

	public function groupBy(groupBy:String):Query {
		_groupBy = groupBy;
		return this;
	}

	public function sort(sort:String, desc:Boolean=false):Query {
		_sort = sort;
		_desc = desc;
		return this;
	}

	public function where( expr:QueryExpression ):Query {
		_where = expr;
		return this;
	}

	public function and( expr:QueryExpression ):Query {
		_where = new QueryExpression(_where, QueryExpression.And, expr);
		return this;
	}

	public function or( expr:QueryExpression ):Query {
		_where = new QueryExpression( _where, QueryExpression.Or, expr );
		return this;
	}

	public function desc():Query {
		_desc = true;
		return this;
	}

	public function asc():Query {
		_desc = false;
		return this;
	}

	public function distinct():Query {
		_distinct = true;
		return this;
	}

	public function getDbase():String {
		return _dbase;
	}

	public function getClass():Class {
		return _class;
	}

	public function getAction():String {
		return _action;
	}

	public function getFields():Array {
		return _fields;
	}

	public function getLimit():Number {
		return _limit;
	}

	public function getSkip():Number {
		return _skip;
	}

	public function getGroupBy():String {
		return _groupBy;
	}

	public function getSort():String {
		return _sort;
	}

	public function getDesc():Boolean {
		return _desc;
	}

	public function getDistinct():Boolean {
		return _distinct;
	}

	public function getBody():Object {
		return _body;
	}

	public function getWhere():QueryExpression {
		return _where;
	}

	public function toString():String {
		var s:String = "Query{ table=";
		s += ClassUtils.getClassNameSimple(_class);
		s += ", action=" + _action;
		if (_fields ) s += ", fields=" + String(_fields);
		if (_limit ) s += ", limit=" + _limit;
		if ( _skip ) s += ", skip=" + _skip;
		if ( _groupBy ) s += ", groupBy=" + _groupBy;
		if ( _sort ) s += ", sort=" + _sort;
		if ( _desc ) s += ", desc=" + _desc;
		if ( _distinct ) s += ", distinct=" + _distinct;
		if ( _body ) s += ", body=" + _body;
		if ( _where ) s += ", where=" + _where;
		s += "}";
		return s;
	}

	public static function unitTest():void {
		var query:Query = new Query('dbTest');
		query.from(ToolNode).find(['name','price'] ).where( new QueryExpression( "_id", QueryExpression.EqualTo, "123") );
		trace("query=", query);
	}

}

}
