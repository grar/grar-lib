/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 9/30/14 9:50.
 */
package grar.media {

import com.greensock.TweenLite;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Stage;
import flash.events.ErrorEvent;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.LocationChangeEvent;
import flash.geom.Rectangle;
import flash.media.StageWebView;

import grar.utils.AppUtils;

import org.osflash.signals.Signal;

public class CommonWebClientHelper {

	private static var _stage:Stage;
	private static var _viewport:Rectangle;
	private static var _swv:StageWebView;
	public static var onURLChange:Signal;
	public static var onURLComplete:Signal;
	public static var onURLError:Signal;
//	public static var showOnLoaded:Boolean = false ;
	private static var _useNativeBrowser:Boolean;

	private static var _y:Number;
	private static var _x:Number;
	private static var _w:Number;
	private static var _h:Number;
	private static var _invalidatingDraw:Boolean;
	public static const userData:Object = {};
	private static var _snapshot_bd:BitmapData;
	private static var useRetina:Boolean;
	public static var pageLoaded:Boolean;

	public function CommonWebClientHelper() {
	}

	public static function init():void {
		onURLChange = new Signal();
		onURLComplete = new Signal();
		onURLError = new Signal();
		useRetina = AppUtils.retinaFactor>1 ;
	}

	public static function setupWebView( p_stage:Stage, p_viewport:Rectangle,
										 p_useNative:Boolean = false ):void {
		if ( !onURLChange ) init();
		_stage = p_stage;
		_useNativeBrowser = p_useNative;
		_swv = new StageWebView( _useNativeBrowser );
		viewport = p_viewport;
		addListeners( true );
	}

	public static function navto( p_url:String ):void {
		_swv.addEventListener( Event.COMPLETE, handleLocationComplete );
		pageLoaded = false ;
		_swv.loadURL( p_url );
		trace( "WebView navigated to "+p_url+" (title: " + _swv.title + ", location: " + _swv.location + ")" );
	}

	public static function snapshot():void {
		if ( _snapshot_bd ) _snapshot_bd.dispose();

		if( useRetina ){
			_viewport.width <<=1 ;
			_viewport.height <<=1 ;
			_swv.viewPort = _viewport ;
		}
		_snapshot_bd = new BitmapData( _viewport.width, _viewport.height, false, 0x0 );
		_swv.drawViewPortToBitmapData( _snapshot_bd ) ;
		if( useRetina ) {
			_viewport.width >>= 1;
			_viewport.height >>= 1;
		}
		_swv.viewPort = _viewport ;
	}

	public static function resetWeb():void {}

	public static function showWeb( p_flag:Boolean ):void {
		if ( p_flag ) {
			_swv.stage = _stage;
			_swv.viewPort = _viewport;
			_swv.assignFocus();
		} else {
			_swv.stage = null;
			_swv.loadString( "" );
			pageLoaded = true ;
		}
	}

	public static function set viewport( p_value:Rectangle ):void {
		_viewport = p_value;
		// adjust if needed.
		_x = _viewport.x ;
		_y = _viewport.y ;
		_w = _viewport.width ;
		_h = _viewport.height ;
		_swv.viewPort = _viewport ;
	}

	public static function get viewport():Rectangle {
		return _viewport;
	}

	public static function get title():String {
		return isShow ? _swv.title : "";
	}

	public static function get location():String {
		return isShow ? _swv.location : "";
	}


	public static function get isShow():Boolean {
		return _swv.stage != null;
	}


	private static function addListeners( p_flag:Boolean ):void {
		if ( p_flag ) {
//			_swv.addEventListener( Event.COMPLETE, handleLocationComplete );
			_swv.addEventListener( LocationChangeEvent.LOCATION_CHANGING, handleLocationChanging );
			_swv.addEventListener( LocationChangeEvent.LOCATION_CHANGE, handleLocationChanging );
			_swv.addEventListener( IOErrorEvent.IO_ERROR, trace );
			_swv.addEventListener( ErrorEvent.ERROR, handleError );
		} else {
			_swv.removeEventListener( Event.COMPLETE, handleLocationComplete );
			_swv.removeEventListener( LocationChangeEvent.LOCATION_CHANGING, handleLocationChanging );
			_swv.removeEventListener( LocationChangeEvent.LOCATION_CHANGE, handleLocationChanging );
			_swv.removeEventListener( IOErrorEvent.IO_ERROR, trace );
			_swv.removeEventListener( ErrorEvent.ERROR, handleError );
		}
	}


	private static function handleLocationChanging( event:LocationChangeEvent ):void {
		trace("location changing::") ;
		pageLoaded = false ;
		onURLChange.dispatch( event.location );
	}

	private static function handleLocationComplete( event:Event ):void {
		trace( "Location complete" );
		_swv.removeEventListener( Event.COMPLETE, handleLocationComplete );
		/*if( showOnLoaded ) {
			showOnLoaded = false ;
			_swv.stage = _stage ;
		}*/
		pageLoaded = true ;
		onURLComplete.dispatch( _swv.location );
		onURLChange.dispatch( _swv.location );
		_swv.addEventListener( Event.COMPLETE, handleLocationComplete );
	}

	private static function handleError( event:ErrorEvent ):void {
//		trace("Hey, we have an error::", event.errorID, " > ", event.text);
		onURLError.dispatch( event.text );
	}

	public static function get useNativeBrowser():Boolean {
		return _useNativeBrowser;
	}

	public static function set useNativeBrowser( p_flag:Boolean ):void {
		if ( _useNativeBrowser == p_flag ) return;
		_useNativeBrowser = p_flag;
		// remove previous instance.
		showWeb( false );
		addListeners( false );
		_swv.dispose();
		_swv = null;
		_swv = new StageWebView( _useNativeBrowser );
		addListeners( true );
	}

	public static function get y():Number {return _y;}

	public static function set y( value:Number ):void {
		if ( _y == value )return;
		_y = value;
		if ( !_invalidatingDraw )invalidateDraw();
	}

	public static function get x():Number {
		return _x;
	}

	public static function set x( value:Number ):void {
		if ( _x == value )return;
		_x = value;
		if ( !_invalidatingDraw )invalidateDraw();
	}

	public static function get w():Number {
		return _w;
	}

	public static function set w( value:Number ):void {
		if ( _w == value )return;
		_w = value;
		if ( !_invalidatingDraw )invalidateDraw();
	}

	public static function get h():Number {
		return _h;
	}

	public static function set h( value:Number ):void {
		if ( _h == value )return;
		_h = value;
		if ( !_invalidatingDraw ) invalidateDraw();
	}

	private static function invalidateDraw():void {
		_invalidatingDraw = true;
		TweenLite.delayedCall( 0, draw );
	}

	public static function draw():void {
		if ( !_invalidatingDraw ) return;
		_invalidatingDraw = false;
		_viewport.setTo( _x, _y, _w, _h );
		_swv.viewPort = _viewport;
	}

	public static function setViewport( px:Number, py:Number, pw:Number, ph:Number ):void {
		_viewport.setTo( px, py, pw, ph );
		_x = px;
		_y = py;
		_w = pw;
		_h = ph;
		invalidateDraw();
	}

	public static function get swv():StageWebView{
		return _swv ;
	}
	public static function get snapshot_bd():BitmapData {
		return _snapshot_bd;
	}
}

}
