/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 3/29/14 11:08 AM.
 */
package grar.air {
import grar.utils.*;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.JPEGEncoderOptions;
import flash.display.Loader;
import flash.display.PNGEncoderOptions;
import flash.events.Event;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.geom.Matrix;
import flash.net.URLRequest;
import flash.system.Capabilities;
import flash.utils.ByteArray;

import grar.crypto.util.Base64;

public class FileUtils {

	public static const FILE_DESKTOP:File = File.desktopDirectory;
	public static const FILE_STORAGE:File = File.applicationStorageDirectory;
	public static var TMP_FILE:File = new File();
	private static const HELPER_MATRIX:Matrix = new Matrix();

	public function FileUtils() {
	}

	public static function saveCleanedJSON( p_json:Object ):void {
		var f:File = FILE_DESKTOP.resolvePath( 'database_cleaned.json' );
		var json_str:String;
		if ( p_json is String ) {
			json_str = p_json as String;
			json_str = StringUtils.removeJsonComments( String( json_str ) );
		} else if ( p_json is Object ) {
			json_str = JSON.stringify( p_json, null, ' ' );
		}
		saveStringToFile( f, json_str );
	}

	public static function saveStringToFile( p_file:File, p_data:String, p_filename:String = '' ):File {
		if ( !p_file ) {
			if ( !p_filename ) {
				p_file = File.createTempFile();
			} else {
				p_file = File.desktopDirectory.resolvePath( p_filename );
			}
		}
		var ba:ByteArray = new ByteArray();
		ba.writeUTFBytes( p_data );
		var fileStream:FileStream = new FileStream();
		fileStream.open( p_file, FileMode.WRITE );
		fileStream.writeBytes( ba );
		fileStream.close();
		return p_file;
	}

	public static function saveCSVToFile( p_file:File, p_data:Array, p_fields:Array = null ):File {
		var csv:String = generateCSV( p_data, p_fields, p_fields );
		if ( !p_file ) p_file = File.desktopDirectory.resolvePath( 'data' + new Date().time + '.csv' );
		var ba:ByteArray = new ByteArray();
		ba.writeUTFBytes( csv );
		var fileStream:FileStream = new FileStream();
		fileStream.open( p_file, FileMode.WRITE );
		fileStream.writeBytes( ba );
		fileStream.close();
		return p_file;
	}

	public static function generateCSV( p_data:Array, p_fieldsTitles:Array = null,
										p_sortedFields:Array = null ):String {
		if ( !p_data || p_data.length == 0 ) return null;
		var obj:Object;
		if ( !p_fieldsTitles ) {
			p_fieldsTitles = [];
			obj = p_data[0];
			// get the properties as header titles.
			for ( var prop:String in obj ) p_fieldsTitles.push( prop );
		}
		var output:String = p_fieldsTitles.join( ',' ) + '\n';
		var len:int = p_data.length;
		for ( var i:int = 0; i < len; i++ ) {
			obj = p_data[i];
			// get the properties as header titles.
//			if(!p_fieldsTitles) for (prop in obj ) p_fieldsTitles.push(prop);
			if ( !p_sortedFields ) {
				for ( prop in obj ) {
					output += obj[prop] + ',';
				}
			} else {
				for each( prop in p_sortedFields ) {
					output += obj[prop] + ',';
				}
			}

			output = output.substr( 0, output.length - 1 ) + '\n';
		}
		output = output.substr( 0, output.length - 1 );
		return output;
	}

	public static function getHTMLTableFromArray( p_data:Array, p_titles:Array = null, p_fields:Array = null,
												  p_tableRoot:String = '' ):String {
		if ( !p_data ) return '';
		var html:String = '';
		if ( p_tableRoot ) html += p_tableRoot;
		var len:int = p_data.length;
		var prop:String;
		if ( !p_titles ) {
			p_titles = [];
			if ( p_fields ) {
				for each( prop in p_fields ) {
					p_titles.push( prop );
				}
			} else {
				for ( prop in p_data[0] ) {
					p_fields.push( prop );
					p_titles.push( prop );
				}
			}
		}

		// header
		html += '<tr>';
		for each( var title:String in p_titles ) html += '<th>' + StringUtils.capitalize( title ) + '</th>';
		html += '</tr>';

		for ( var i:int = 0; i < len; i++ ) {
			var obj:Object = p_data[i];
			html += '<tr>';
			for each( prop in p_fields ) {
				html += '<td>' + obj[prop] + '</td>';
			}
			/*for (prop in obj ) {
			 html += '<td>' + obj[prop] + '</td>';
			 }*/
			html += '</tr>';
		}
		html += '</table>';
		return html;
	}

	public static function generateXMLFromArray( p_data:Array, p_mainNodeName:String = 'data',
												 p_useNodes:Boolean = false ):XML {
		var xml:XML = XML( '<' + p_mainNodeName + '/>' );
		var list:XMLList = new XMLList();
		var len:int = p_data.length;
		var prop:String;
		var output:String = '';
		for ( var i:int = 0; i < len; i++ ) {
			var obj:Object = p_data[i];
			// get the properties as header titles.
//			if(!p_fields) for (prop in obj ) p_fields.push(prop);
			var item:XML = <item/>;
			for ( prop in obj ) {
				if ( p_useNodes ) {
					item.appendChild( XML( '<' + prop + '>' + obj[prop] + '</' + prop + '>' ) );
				} else {
					item.@[prop] = obj[prop];
				}
			}
			xml.appendChild( item );
		}
		return xml;
	}

	public static function fileExists( p_path:String ):Boolean {
		if ( !p_path ) return false;
		if ( p_path.indexOf( '/' ) == -1 ) return false;
		TMP_FILE.url = p_path;
		return TMP_FILE.exists;
	}

	public static function createThumbFromURL( p_url:String, p_thumbURL:String, p_quality:uint = 80,
											   p_callback:Function = null ):void {
		var l:Loader = new Loader();
		l.contentLoaderInfo.addEventListener( Event.COMPLETE, imageLoaded, false, 0, true );
		l.load( new URLRequest( p_url ) );
		function imageLoaded( e:Event ):void {
			l.contentLoaderInfo.removeEventListener( Event.COMPLETE, imageLoaded );
			var bd:BitmapData = Bitmap( e.target.content ).bitmapData;
			l.unload();
			l = null;
			var maxW:int = 300;
			var maxH:int = 300;
			var r1:Number = bd.width / bd.height;
			var r2:Number = maxW / maxH;
			var tw:int = maxW;
			var th:int = maxH;
			var scale:Number;
			if ( r1 < r2 ) {
				tw = th * r1;
				scale = th / bd.height;
			} else {
				th = tw / r1;
				scale = tw / bd.width;
			}
			var m:Matrix = HELPER_MATRIX;
			m.identity();
			m.scale( scale, scale );
			var output:BitmapData = new BitmapData( tw, th, false, 0x0 );
			output.draw( bd, m, null, null, null, true );
			bd.dispose();
			FileUtils.saveJPG( output, p_thumbURL, p_quality );
			if ( p_callback ) p_callback( output );
			output.dispose();
		}
	}

	public static function resizeBitmapData( bd:BitmapData, maxW:int = 300, maxH:int = 300 ):BitmapData {
		var r1:Number = bd.width / bd.height;
		var r2:Number = maxW / maxH;
		var tw:int = maxW;
		var th:int = maxH;
		var scale:Number;
		if ( r1 < r2 ) {
			tw = th * r1;
			scale = th / bd.height;
		} else {
			th = tw / r1;
			scale = tw / bd.width;
		}
		var m:Matrix = HELPER_MATRIX;
		m.identity();
		m.scale( scale, scale );
		var output:BitmapData = new BitmapData( tw, th, bd.transparent, 0x0 );
		output.draw( bd, m, null, null, null, true );
		return output;
//			bd.dispose();
//			FileUtils.saveJPG( output, p_thumbURL, p_quality );
//			if ( p_callback ) p_callback( output );
	}

	public static function saveJPG( p_bd:BitmapData, fileUrl:String, p_quality:uint = 80 ):void {
		TMP_FILE.url = fileUrl;
		var ba:ByteArray = new ByteArray();
		p_bd.encode( p_bd.rect, new JPEGEncoderOptions( p_quality ), ba );
		var fs:FileStream = new FileStream();
		fs.open( TMP_FILE, FileMode.WRITE );
		fs.writeBytes( ba );
		fs.close();
		ba.clear();
		ba = null;
		fs = null;
	}

	public static function savePNG( p_bd:BitmapData, p_path:String, p_fastCompression:Boolean = false ):void {
		TMP_FILE.url = p_path;
		var ba:ByteArray = new ByteArray();
		p_bd.encode( p_bd.rect, new PNGEncoderOptions( p_fastCompression ), ba );
		var fs:FileStream = new FileStream();
		fs.open( TMP_FILE, FileMode.WRITE );
		fs.writeBytes( ba );
		fs.close();
		ba.clear();
		ba = null;
		fs = null;
	}

	public static function getExtension( path:String ):String {
		if ( !path ) {
			return '';
		} else if ( path.length <= 4 ) {
			return path;
		}
		return path.split( '.' ).pop();
	}

	public static function getFilename( p_path:String, p_includeExtension:Boolean = true ):String {
		if ( !p_path ) return '';
		var fn:String = p_path.split( '/' ).pop();
		if ( !p_includeExtension ) {
			var arr:Array = fn.split( '.' );
			arr.pop();
			fn = arr.join( '.' );
		}
		return fn;
	}

	public static function removeFile( p_url:String ):Boolean {
		TMP_FILE.url = p_url;
		TMP_FILE.deleteFile();
		return !TMP_FILE.exists;
	}

	public static function isImageFile( p_path:String ):Boolean {
		return IMAGE_FORMATS.indexOf( getExtension( p_path ) ) > -1;
	}

	public static function isVideoFile( p_path:String ):Boolean {
		return VIDEO_FORMATS.indexOf( getExtension( p_path ) ) > -1;
	}

	public static function isPDFFile( p_path:String ):Boolean {
		return PDF_FORMATS.indexOf( getExtension( p_path ) ) > -1;
	}

	public static function isHTMLFile( p_path:String ):Boolean {
		return HTML_FORMATS.indexOf( getExtension( p_path ) ) > -1;
	}

	public static function read( file:File, p_bytes:ByteArray = null ):ByteArray {
		var stream:FileStream = new FileStream();
		stream.open( file, FileMode.READ );
		if ( !p_bytes ) {
			p_bytes = new ByteArray();
		} else {
			p_bytes.clear();
		}
		stream.readBytes( p_bytes );
		stream.close();
		return p_bytes;
	}

	public static function readJSON( p_file:File, isObject:Boolean ):* {
		var str:String = readString( p_file );
		var json:Object = JSON.parse( str );
		return isObject ? json : json as Array;
	}

	public static function readString( p_file:File ):String {
		if ( !p_file || !p_file.exists ) return null;
		var ba:ByteArray = read( p_file );
		return ba.readUTFBytes( ba.bytesAvailable );
	}

	public static function write( file:File, p_ba:ByteArray, writer:Function = null ):void {
		const out:FileStream = new FileStream();
		out.open( file, FileMode.WRITE );
		if ( !writer ) {
			out.writeBytes( p_ba );
		} else {
			writer( out );
		}
		out.close();
	}

	public static function parseCSV( p_data:String ):Array {
		var regs:Array = p_data.split( "\r" ).join( "" ).split( "\n" );
		var props_arr:Array = regs.shift().split( "," );
		var output:Array = [];
		var numRecords:int = regs.length;
		var numFields:int = props_arr.length;
		for ( var i:int = 0; i < numRecords; i++ ) {
			if ( regs[i] == "" ) continue;
			var record:Array = regs[i].split( "," );
			var obj:Object = {};
			for ( var j:int = 0; j < numFields; j++ ) {
//				trace(props_arr[j]) ;
				obj[props_arr[j]] = record[j];
			}
			output.push( obj );
		}
		return output;
	}

	public static const PDF_FORMATS:Array = ['pdf'];
	public static const HTML_FORMATS:Array = ['html', 'htm', 'php'];
	public static const VIDEO_FORMATS:Array = ['mp4', 'mov', 'flv'];
	public static const IMAGE_FORMATS:Array = ['jpeg', 'jpg', 'png'];
	public static const TEXT_FORMATS:Array = ["txt", "info", "xml"];

	/**
	 *    @return An Array of Files representing the root directories of the
	 *            operating system.
	 */
	public static function getRootDirectories():Array {
		var v:Array = File.getRootDirectories();
		var os:String = Capabilities.os;

		if ( os.indexOf( "Mac" ) > -1 ) {
			v = File( v[0] ).resolvePath( "Volumes" ).getDirectoryListing();
		}
		else if ( os.indexOf( "Linux" ) > -1 ) {
			//todo: need to impliment Linux
		}

		return v;
	}

	public static function getMime( p_filename:String ):String {
		var ext:String = getExtension( p_filename ).toLowerCase();
		var mime:String = "";
		if ( VIDEO_FORMATS.indexOf( ext ) > -1 ) {
			mime = "video";
			mime += "/" + ext;
		} else if ( IMAGE_FORMATS.indexOf( ext ) > -1 ) {
			mime = "image";
			if ( ext == "jpg" ) ext = "jpeg";
			mime += "/" + ext;
		} else if ( TEXT_FORMATS.indexOf( ext ) > -1 ) {
			mime = "text/plain";
		} else {
			mime = "application/octet-stream"
		}
		return mime;
	}

	private static const READEABLE_BYTES_HASH:Array = ['bytes', 'kb', 'MB', 'GB', 'TB', 'PB'];

	public static function redeableBytes( bytes:uint ):String {
		var exp:Number = Math.floor( Math.log( bytes ) / Math.log( 1024 ) );
		return (bytes / Math.pow( 1024, Math.floor( exp ) )).toFixed( 2 ) + " " + READEABLE_BYTES_HASH[exp];
	}


	public static function decodeStringData( p_data:*, p_compression:String = "zlib" ):String {
		var base64_str:String;
		if ( p_data is ByteArray ) {
//			ba = FileUtils.read( f ) ;
			p_data.position = 0;
			base64_str = p_data.readUTFBytes( p_data.bytesAvailable );
		} else {
			base64_str = String( p_data );
		}
		trace( "FILEUTILS::decodeStringData - base64 str DECODING LEN:", base64_str.length );
		var ba:ByteArray = Base64.decodeToByteArray( base64_str );
		ba.uncompress( p_compression );
		ba.position = 0;
		return ba.readUTFBytes( ba.bytesAvailable );
	}

	public static function encodeStringData( p_data:String, p_compression:String = "zlib" ):String {
		var ba:ByteArray = new ByteArray();
		ba.writeUTFBytes( p_data );
		trace( "FILEUTILS::encodeStringData - pre compress,", redeableBytes( ba.length ) );
		ba.compress( "zlib" );
		trace( "FILEUTILS::encodeStringData - post compress:", redeableBytes( ba.length ) );
		var base64_str:String = Base64.encodeByteArray( ba );
//			ba.position = 0 ;
//			ba.clear() ;
//			ba.writeUTFBytes( base64_str );
//			trace("FILEUTILS::encodeStringData - base64 str size:", redeableBytes( ba.length ) );
//			trace("string len ENCODING LEN:", str.length ) ;
//			FileUtils.saveStringToFile( f, str );
//			ba.clear() ;
		return base64_str;
	}


	public static function deleteDirContents( file:File ):void {
		if ( !file.exists || !file.isDirectory )
			return;
		var list:Array = file.getDirectoryListing();
		var f:File;
		for each( f in list ) {
			deleteFile( f, true );
		}
	}

	public static function deleteFile( file:File, recursive:Boolean = true ):void {
		if ( !file || !file.exists ) return;
		if ( file.isDirectory ) {
			file.deleteDirectory( recursive );
		} else {
			file.deleteFile();
		}
	}

	public static function getUrlForWebView( url:String ):String {
		TMP_FILE.url = url;
		return new File( TMP_FILE.nativePath ).url;
	}

	/**
	 * Parse a config string to be a redeable object.
	 * The string should be based on a "ini" type of config.
	 * key=value
	 * key=value...
	 * @param str
	 * @return
	 */
	public static function parseConfigString( str:String ):Object {
		if ( !str || str.indexOf( "=" ) == -1 ) return null;
		str = str.toLowerCase();
		str = StringUtils.replaceMultipleLines( str );
		str = StringUtils.replace( str, " ", "" );
		str = StringUtils.replace( str, "\t", "" );
		var lines:Array = str.split( "\n" );
		var out:Object = {};
		for each( var line:String in lines ) {
			var parts:Array = line.split( "=" );
//			if( parts.length != 2 ) continue ;
//			if( parts[0]==""||parts[1]=="" ) continue ;
			if ( parts.length == 2 && parts[0] != "" && parts[1] != "" ) {
				var p:String = parts[0];
				var v:String = parts[1];
				var value:*;
				if ( v == "true" ) value = true;
				else if ( v == "false" ) value = true;
				else if ( !isNaN( Number( v ) ) ) value = Number( v );
				out[p] = value;
			}
		}
		return out;
	}

	/**
	 * Converts an Object to a "ini" type of config string.
	 * @param config
	 * @return
	 */
	public static function parseToConfigString( config:Object ):String {
		if( !config || typeof config!="object") return null ;
		var lines:Array = [];
		for ( var p:String in config ) {
			lines.push( p + "=" + config[ p] );
		}
		return lines.join( "\n" );
	}
}
}
