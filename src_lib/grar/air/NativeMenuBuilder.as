/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 4/8/14 4:09 AM.
 */
package grar.air {
import flash.display.NativeMenu;
import flash.display.NativeMenuItem;
import flash.events.Event;

/**
 * Builds a simple native menu. Feel free to contribute more to this builder!
 *
 * Example usage:
 * <code>
 *        const rootMenu:NativeMenu = NativeMenuBuilder.newNativeMenu()
 *            .withSubmenu("File")
 *                .addOption("Open", opOpenSelected)
 *                .addOption("Save", onSaveSelected)
 *                .addDivider()
 *                .addOption("Exit", onExitSelected)
 *                .buildSubmenu()
 *            // (The edit menu will not do anything)
 *            .withSubmenu("Edit")
 *                .addOption("Copy")
 *                .addOption("Paste")
 *                .buildSubmenu()
 *            .addDivider()
 *            .addOption("About", onAboutSelected)
 *            .build();
 *
 *        if (NativeApplication.supportsMenu) {
	 *			nativeApplication.menu = rootMenu;
	 *		} else if (NativeWindow.supportsMenu) {
	 *			nativeWindow.menu = rootMenu;
	 *		} else {
	 *			trace("Native menu not supported!!");
	 *		}
 * </code>
 *
 * @author Steven Wallace
 */
public class NativeMenuBuilder {

	//----------------------------------------------------------------------
	//
	//  Private constants/variables
	//
	//----------------------------------------------------------------------

	/**
	 * Lock to ensure that we are the only ones who can call the constructor
	 */
	private static const _lock:Object = { };

	/**
	 * The menu that is being built
	 */
	private var _menu:NativeMenu;

	/**
	 * The parent builder, if applicable
	 */
	private var _parent:NativeMenuBuilder;

	//----------------------------------------------------------------------
	//
	//  Construction
	//
	//----------------------------------------------------------------------

	/**
	 * Do not use the constructor. Instead, use the static <code>newNativeMenu</code> method.
	 */
	public function NativeMenuBuilder( lock:Object) {
		if (lock !== _lock) {
			throw new Error("Please use NativeMenuBuilder.newNativeMenu() method instead");
		}
	}

	/**
	 * Creates a new <code>NativeMenuBuilder</code>
	 */
	public static function newNativeMenu(p_root:NativeMenu=null):NativeMenuBuilder {
		const builder:NativeMenuBuilder = new NativeMenuBuilder(_lock);
		if(!p_root) p_root = new NativeMenu();
		builder._menu = p_root ;
		builder._parent = null;
		return builder;
	}

	//----------------------------------------------------------------------
	//
	//  Public methods
	//
	//----------------------------------------------------------------------

	/**
	 * Adds a submenu to the native menu
	 * @param    label Label to use for this submenu
	 * @return The NativeMenuBuilder instance for the submenu created
	 */
	public function withSubmenu(label:String):NativeMenuBuilder {
		// Add submenu
		const submenu:NativeMenu = new NativeMenu();
		_menu.addSubmenu(submenu, label);

		// Return builder for that submenu
		const builder:NativeMenuBuilder = new NativeMenuBuilder(_lock);
		builder._menu = submenu;
		builder._parent = this;
		return builder;
	}

	/**
	 * Adds an menu option to the menu
	 * @param    label Label to use for this menu option
	 * @param    selectHandler Event handler to be called when this item is clicked on
	 * @return The current NativeMenuBuilder
	 */
	public function addOption(label:String, selectHandler:Function = null, p_key:String = null, p_keyMod:Array = null ):NativeMenuBuilder {
		// Add menu item
		const menuItem:NativeMenuItem = new NativeMenuItem(label);
		if( p_key ) menuItem.keyEquivalent = p_key ;
		if( p_keyMod ) menuItem.keyEquivalentModifiers = p_keyMod;

		if (selectHandler != null) {
			menuItem.addEventListener(Event.SELECT, selectHandler);
		}
		_menu.addItem(menuItem);
		return this;
	}

	/**
	 * Adds a divider to the current menu
	 * @return The current NativeMenuBuilder
	 */
	public function addDivider():NativeMenuBuilder {
		const divider:NativeMenuItem = new NativeMenuItem("", true);
		_menu.addItem(divider);
		return this;
	}

	/**
	 * Finishes building a submenu and returns the parent NativeMenuBuilder
	 * @return the parent NativeMenuBuilder
	 */
	public function buildSubmenu():NativeMenuBuilder {
		if (_parent) {
			return _parent;
		} else {
			throw new Error("Cannot build submenu as this is the root menu");
		}
	}

	/**
	 * Finishes building a menu and returns the built NativeMenu
	 * @return The build NativeMenu
	 */
	public function build():NativeMenu {
		if (!_parent) {
			return _menu;
		} else {
			throw new Error("Cannot build native menu, as this is a submenu builder.");
		}
	}

}
}