/**
 * Code by Rodrigo López Peker (grar) on 3/5/16 11:22 AM.
 *
 */
package grar.data {
public class CommonDef {

	// sprite states.
	public static const OPEN:String = "open";
	public static const OPENED:String = "opened";
	public static const CLOSED:String = "closed";
	public static const CLOSE:String = "close";

	// buttons
	public static const OUT:String = "out";
	public static const HOVER:String = "hover";
	public static const TAP:String = "tap";

	public function CommonDef() {
	}
}
}
