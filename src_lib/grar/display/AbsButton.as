/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 3/16/15 10:28 AM.
 */
package grar.display {
import com.greensock.TweenLite;
import com.greensock.easing.Quad;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;

public class AbsButton extends AbsSprite {

	protected var _selected:Boolean;
	protected var _enabled:Boolean = true;
	protected var _isOver:Boolean;
	protected var _isDown:Boolean;
	public var idx:int;
	public var tapEvent:MouseEvent;
	public var onEnableState:Function;
	public var onSelectState:Function;
	public var onHover:Function;
	public var dispatchHover:Boolean = true;

	// hold action.
	public var holdActionDelay:Number = 0.7;
	public var holdActionRepeatDelay:Number = 0.06;
	public var holdActionCallback:Function = null;

	override public function reset():void {
		super.reset();
		onHover = null;
		onEnableState = null;
		onSelectState = null;
		if ( parent ) {
			parent.removeChild( this );
		}

		if ( _selected ) {
			_selected = false;
			onSelect();
		}

		if ( active ) {
			activate( false );
		}
	}

	/**
	 * Constructor.
	 * @param doc
	 */
	public function AbsButton( doc:Sprite = null ) {
		super( doc );
	}

	override protected function initialize():void {
		super.initialize();
		tapEvent = new MouseEvent( "tap", true, true, 0, 0, this );
		this.mouseChildren = false;
		this.buttonMode = true;
	}

	override public function activate( flag:Boolean ):void {
		if ( flag == _active ) return;
		super.activate( flag );
		if ( _enabled ) {
			mouseEnabled = flag;
			addEvents( flag );
		}
	}

	public function cancelTap():void {
		if ( _isDown ) Display.stage.removeEventListener( MouseEvent.MOUSE_UP, handleMouse );
		unhover();
//			if( UIMan.pressedButton == this ) UIMan.pressedButton = null ;
		_isOver = false;
		_isDown = false;
	}

	// === Press&Hold quikc action!.
	protected function holdTicker():void {
		if ( holdActionCallback != null ) {
			holdActionCallback();
			if ( holdActionRepeatDelay > 0 ) {
				dly( holdActionRepeatDelay, holdTicker );
			}
		}
	}

	protected function handleMouse( e:MouseEvent ):void {
		switch ( e.type ) {
			case MouseEvent.ROLL_OVER :
				_isOver = true;
				mouseOver();
				break;
			case MouseEvent.ROLL_OUT :
				_isOver = false;
				mouseOut();
				break;
			case MouseEvent.MOUSE_DOWN :
//					UIMan.pressedButton = this ;
				_isDown = true;
				mousePress();
				hover();
				if ( holdActionCallback && holdActionDelay >= 0 ) {
					TweenLite.delayedCall( holdActionDelay, holdTicker );
				}
				Display.stage.addEventListener( MouseEvent.MOUSE_UP, handleMouse );
				break;
			case MouseEvent.MOUSE_UP :
//					if( UIMan.pressedButton == this ) UIMan.pressedButton = null ;
				Display.stage.removeEventListener( MouseEvent.MOUSE_UP, handleMouse );
				_isDown = false;
				if ( holdActionCallback ) {
					TweenLite.killTweensOf( holdTicker );
				}
				mouseRelease();
				if ( _isOver ) {
					tap();
					tapEvent.localX = mouseX;
					tapEvent.localY = mouseY;
					tapEvent.relatedObject = this;
					dispatchEvent( tapEvent );
				} else {
					dispatchEvent( e );
				}
				unhover();
				break;
		}
//			if ( this.hasEventListener( e.type ) ) dispatchEvent( e );
	}


	public function killPress():void {
		Display.stage.removeEventListener( MouseEvent.MOUSE_UP, handleMouse );
		_isDown = false;
		if ( holdActionCallback ) {
			TweenLite.killTweensOf( holdTicker );
		}
		mouseRelease();
		unhover();
	}

	public function unhover():void {
		if ( dispatchHover && onHover && !_selected )
			onHover( this, false );
		// release tap!
//		if ( useAlphaAnimation ) tweenAlpha( 1, false );
	}

	public function hover():void {
		if ( dispatchHover && onHover && !_selected )
			onHover( this, true );
//		if ( useAlphaAnimation ) tweenAlpha( 0.4, false );
	}

	protected function mouseRelease():void {
	}

	protected function tap():void {
	}

	protected function mousePress():void {
	}

	// used for mouseTap delayed states.
	protected function outState():void {
	}

	protected function mouseOut():void {
	}

	protected function mouseOver():void {
	}

	protected function mouseClick():void {
	}

	protected function onSelect():void {
		if ( onSelectState ) onSelectState( this, _selected );
		_selected ? mouseOver() : mouseOut();
	}

	protected function onEnabled():void {
		if ( onEnableState ) onEnableState( this, _enabled );
	}

	// animation.
	protected function tweenAlpha( p_value:Number, p_easeIn:Boolean, p_duration:Number = 0.43 ):void {
		TweenLite.killTweensOf( this, {alpha: true} );
		TweenLite.to( this, p_duration, {alpha: p_value, ease: p_easeIn ? Quad.easeIn : Quad.easeOut} );
	}

	protected function get isHovering():Boolean {
		if ( !this.stage ) return false;
		return this.hitTestPoint( Display.main.mouseX, Display.main.mouseY, true );
	}

	public function isHittingMouse( p_shapeFlag:Boolean = false ):Boolean {
		return hitTestPoint( Display.main.mouseX, Display.main.mouseY, p_shapeFlag );
	}

	public function get enabled():Boolean {
		return _enabled;
	}

	public function set enabled( value:Boolean ):void {
		if ( _enabled == value ) return;
		_enabled = value;
		buttonMode = mouseEnabled = _enabled;
		addEvents( value ); // maybe this is used later ?
		onEnabled();
	}

	public function get selected():Boolean {
		return _selected;
	}

	public function set selected( value:Boolean ):void {
		if ( _selected == value ) return;
		_selected = value;
		onSelect();
	}

	override public function dispatchEvent( event:Event ):Boolean {
		if ( hasEventListener( event.type ) ) {
			return super.dispatchEvent( event );
		} else {
			return false;
		}
	}

	public function get isDown():Boolean {
		return _isDown;
	}

	protected function addEvents( flag:Boolean ):void {
		if ( flag ) {
			addEventListener( MouseEvent.ROLL_OVER, handleMouse );
			addEventListener( MouseEvent.ROLL_OUT, handleMouse );
			addEventListener( MouseEvent.MOUSE_DOWN, handleMouse );
		} else {
			removeEventListener( MouseEvent.ROLL_OVER, handleMouse );
			removeEventListener( MouseEvent.ROLL_OUT, handleMouse );
			removeEventListener( MouseEvent.MOUSE_DOWN, handleMouse );
			Display.stage.removeEventListener( MouseEvent.MOUSE_UP, handleMouse );
		}
	}
}
}
