/**
 * Code by Rodrigo López Peker (grar) on 2/18/16 7:09 PM.
 *
 */
package grar.display {
import com.greensock.TweenLite;
import com.greensock.TweenMax;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.geom.Rectangle;

import grar.utils.AppUtils;

public class AbsSprite extends Sprite {

	protected var _w:int;
	protected var _h:int;
	// stage dimensions.
	protected var _sw:int;
	protected var _sh:int;
	protected var _active:Boolean;
	protected var _maskRect:Rectangle;
	protected var _ready:Boolean;

	protected var _useDrawInvalidation:Boolean;
	protected var _waitingDraw:Boolean;

	public var componentId:String;
	public var userData:Object;

	public function AbsSprite( doc:DisplayObjectContainer = null ) {
		if ( doc ) {
			doc.addChild( this );
		}
		_sw = AppUtils.appW;
		_sh = AppUtils.appH;
		initialize();
	}

	protected function initialize():void {
		userData = {};
	}

	protected function draw():void {
	}

	public function activate( flag:Boolean ):void {
		_active = flag;
	}

	public function setupUI():void {
		_ready = true;
	}

	public function clearUI():void {
		_ready = false;
		if ( _active ) activate( false );
	}

	public function reset():void {}

	public function dispose():void {}

	public function setMask( tw:int = 0, th:int = 0 ):void {
		if ( tw <= 0 ) tw = _w;
		if ( th <= 0 ) th = _h;
		if ( !_maskRect ) _maskRect = new Rectangle();
		_maskRect.setTo( 0, 0, tw, th );
		scrollRect = _maskRect;
	}


	//===================================================================================================================================================
	//
	//      ------  Size, position and drawing.
	//
	//===================================================================================================================================================

	public function setSize( tw:int = -1, th:int = -1, useScale:Boolean = false ):void {
		if ( tw == -1 && th == -1 )
			return;

		if ( tw > -1 ) {
			if ( useScale ) tw = ss( tw );
			_w = tw;
		}

		if ( th > -1 ) {
			if ( useScale ) th = ss( th );
			_h = th;
		}

		if ( _useDrawInvalidation && !_waitingDraw ) {
			invalidateDraw();
		} else {
			if ( _waitingDraw ) {
				TweenLite.killTweensOf( onDrawInvalidated );
				_waitingDraw = false;
			}
			draw();
		}
	}

	protected function invalidateDraw():void {
		if ( _waitingDraw ) {
			return;
		}
		_waitingDraw = true;
		TweenLite.delayedCall( 0, onDrawInvalidated );
	}

	protected function onDrawInvalidated():void {
		_waitingDraw = false;
		draw();
	}

	protected function useAppSize():void {
		_w = _sw;
		_h = _sh;
	}

	protected function initSize( targetW:int, targetH:int, useGlobalScale:Boolean = false ):void {
		_w = targetW;
		_h = targetH;
		if ( useGlobalScale ) {
			_w *= AppUtils.scale;
			_h *= AppUtils.scale;
		}
	}

	public function move( px:Number, py:Number, useScale:Boolean = false ):void {
		this.x = useScale ? ss( px ) : px;
		this.y = useScale ? ss( py ) : py;
	}

	public function ss( value:Number ):Number {
		return AppUtils.scale * value;
	}

	public function set w( value:int ):void {
		_w = value;
		_useDrawInvalidation ? invalidateDraw() : draw();
	}

	public function set h( value:int ):void {
		_h = value;
		_useDrawInvalidation ? invalidateDraw() : draw();
	}

	public function get w():int {
		return _w;
	}

	public function get h():int {
		return _h;
	}


	//===================================================================================================================================================
	//
	//      ------  Utils
	//
	//===================================================================================================================================================

	public function enableMouse( flag:Boolean ) {
		mouseChildren = mouseEnabled = flag;
	}

	public function setProps( obj:Object, props:Object ):void {
		if ( !obj ) {
			obj = this;
		}
		props.immediateRender = true;
		TweenMax.to( obj, 0, props );
	}

	public function dly( seconds:Number, callback:Function, ...args ):void {
		TweenLite.killDelayedCallsTo( callback );
		if ( args && args[0] is Array )
			args = args[0];

		TweenLite.delayedCall( seconds, callback, args );
	}

	public function matchSize( obj:DisplayObject, tw:int = -1, th:int = -1 ):void {
		if ( tw < 0 ) tw = _w;
		if ( th < 0 ) th = _h;
		obj.width = tw;
		obj.height = th;
	}

//		Utility to color an object (sprite, bitmap, movieclip)
	public function tint( obj:Object, color:* = null ):void {
		if ( !obj )
			return;

		if ( typeof(color) == "number" && color < 0 )
			color = null;

		setProps( obj, {tint: color} );
	}

//		Utility to kill tweens and delays.
	public function kill( ...args ):void {
		var o:Object;
		for each( o in args ) {
			if ( o is Array ) {
				kill.apply( null, args[0] );
			} else {
				TweenLite.killTweensOf( o );
			}
		}
	}

	public function get active():Boolean {
		return _active;
	}

	public function get ready():Boolean {
		return _ready;
	}
}
}
