/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 12/22/14 11:18 AM.
 */
package grar.display {


	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.PixelSnapping;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.filters.BitmapFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class Scale9Bitmap extends Sprite {

		//===================================================================================================================================================
		//
		//      ------  STATIC API
		//
		//===================================================================================================================================================

		private static const POINT: Point = new Point( 0, 0 );
		private static const MATRIX: Matrix = new Matrix();
		private static var RECT: Rectangle = new Rectangle();
		private static var _lib: Object = {};
		private static var _filter_bd: BitmapData = new BitmapData( 2, 2, true );
		private static var _filter_bdRect: Rectangle = _filter_bd.rect;

		public static function deleteSkin( p_id: String, p_disposeBaseBitmap: Boolean = false ): void {
			if ( !_lib[p_id] ) {
				trace( "Error: [Scale9Bitmap] id=", p_id, " already deleted" );
			}
			if ( p_disposeBaseBitmap && _lib[p_id].bd ) {
				BitmapData( _lib[p_id].bd ).dispose();
			}
			// remove the slices from memory.
			for each( var bd: BitmapData in _lib[p_id].slices ) bd.dispose();
			_lib[p_id].bd = null;
			_lib[p_id].slices = null;
			_lib[p_id].padding = null;
			delete _lib[p_id];
		}

		public static function hasSkin( p_skinId: String ): Boolean {
			return _lib[p_skinId] != null;
		}

		public static function addSkinFromDisplayObject( p_id: String, p_do: DisplayObject, p_padding: Rectangle,
														 p_debugColor: * = null, bounds: Rectangle = null ): void {
			if ( !p_do ) {
				trace( "Error: [Scale9Bitmap]::addSkinFromDisplayObject, display object must be != NULL." );
				return;
			}
			var bd: BitmapData;
			if ( p_do is Bitmap ) {
				bd = Bitmap( p_do ).bitmapData;
			} else if ( p_do is BitmapData ) {
				bd = BitmapData( p_do );
			} else {
//				var bounds: Rectangle = p_do.getBounds( p_do );
				if ( !bounds ) bounds = p_do.getBounds( p_do );
				var finalBounds: Rectangle = bounds.clone(); //new Rectangle();
				var finalFiltersBounds: Rectangle = new Rectangle();

				// analyze display object tree filters herarchy.
//				analyzeFilterRects( p_do, finalBounds, finalFiltersBounds, bounds );
				analyzeFilterRectsTree( p_do, finalBounds, finalFiltersBounds, bounds );

				finalFiltersBounds.width += finalFiltersBounds.x;
				finalFiltersBounds.height += finalFiltersBounds.y;

				MATRIX.identity();
				MATRIX.translate( -finalBounds.x, -finalBounds.y );

				if ( p_debugColor != null ) {
					bd = new BitmapData( finalBounds.width, finalBounds.height, true, p_debugColor );
				} else {
					bd = new BitmapData( finalBounds.width, finalBounds.height, true, 0x0 );
				}
//				bd = new BitmapData( finalBounds.width, finalBounds.height, false, 0xff00ff );
				bd.drawWithQuality( p_do, MATRIX, null, null, null, true, StageQuality.BEST );
//				trace("Padding, " ,finalBounds, p_padding ) ;
			}
			Scale9Bitmap.addSkin( p_id, bd, bounds, p_padding, p_debugColor, finalFiltersBounds );
		}


		public static function createPadding( p_value: Number ): Rectangle {
			return new Rectangle( p_value, p_value, -p_value, -p_value );
		}

		public static function addSkin( p_id: String, p_bd: BitmapData, p_bounds: Rectangle, p_padding: Rectangle,
										p_debugColor: * = null, p_filterBounds: Rectangle = null ): void {
			if ( _lib[p_id] ) {
				trace( "Error: [Scale9Bitmap] id=", p_id, " already taken!" );
				return;
			}
			if ( !p_padding ) p_padding = new Rectangle( 5, 5, 5, 5 );

			// if size is negative
			if ( p_padding.x < 0 ) p_padding.x *= -1;
			if ( p_padding.y < 0 ) p_padding.y *= -1;
			if ( p_padding.width < 0 ) p_padding.width *= -1;
			if ( p_padding.height < 0 ) p_padding.height *= -1;

			// use special padding, negative relationships.
			if ( p_padding.width == 0 ) p_padding.width = p_padding.x;
			if ( p_padding.height == 0 ) p_padding.height = p_padding.y;
			// if position is "absolute"
			if ( p_padding.width > p_bd.width / 2 ) p_padding.width = p_bd.width - p_padding.width;
			if ( p_padding.height > p_bd.height / 2 ) p_padding.height = p_bd.width - p_padding.width;

			var o: Object = _lib[p_id] = {};
			o.bd = p_bd;
			o.slices = generateSlices( p_bd, p_padding, p_debugColor, p_filterBounds );
			o.ow = p_bd.width;
			o.oh = p_bd.height;
			o.bounds = p_bounds;
			o.fbounds = p_filterBounds;
			o.padding = p_padding;
		}

		private static function analyzeFilterRectsTree( drawing: DisplayObject, r: Rectangle, r2: Rectangle,
														p_totalBounds: Rectangle ): void {
			if ( drawing is DisplayObjectContainer ) {
				var doc: DisplayObjectContainer = drawing as DisplayObjectContainer;
				for ( var i: int = 0; i < doc.numChildren; i++ ) {
					analyzeFilterRectsTree( doc.getChildAt( i ), r, r2, p_totalBounds );
				}
			}
			analyzeFilterRects( drawing, r, r2, p_totalBounds );
		}

		private static function analyzeFilterRects( drawing: DisplayObject, r: Rectangle, r2: Rectangle,
													p_totalBounds: Rectangle ): void {
			if ( drawing.filters && drawing.filters.length > 0 ) {
				var fb: Rectangle;
				for each ( var filter: BitmapFilter in drawing.filters ) {
					fb = _filter_bd.generateFilterRect( _filter_bdRect, filter );
					fb.width -= 2 ;
					fb.height -= 2 ;
					var tmp2: Rectangle = r2.union( fb );
					fb.width += p_totalBounds.width;
					fb.height += p_totalBounds.height;
					var tmp1: Rectangle = r.union( fb );
					r.setTo( tmp1.x, tmp1.y, tmp1.width, tmp1.height );
					r2.setTo( tmp2.x, tmp2.y, tmp2.width, tmp2.height );
				}
			}
		}

		private static function generateSlices( p_bd: BitmapData, p_padding: Rectangle, p_debugColor: *,
												p_filterBounds: Rectangle = null ): Array {
//			if ( p_debugColor > -1 ) transparent = false;
			var ml: int = p_padding.x;
			var mr: int = p_padding.width;
			var mt: int = p_padding.y;
			var mb: int = p_padding.height;
			var ow: int = p_bd.width;
			var oh: int = p_bd.height;
			if ( p_filterBounds ) {
				ml += -p_filterBounds.x;
				mr += p_filterBounds.width;
				mt += -p_filterBounds.y;
				mb += p_filterBounds.height;
			}

			var centerW: int = ow - ml - mr;
			var leftW: int = ml;
			var rightW: int = mr;

			var transparent: Boolean = true;
			var color: uint = 0x0;
			if ( p_debugColor != null ) color = p_debugColor;
			var tlb: BitmapData = new BitmapData( leftW, mt, transparent, color );
			var tcb: BitmapData = new BitmapData( centerW, mt, transparent, color );
			var trb: BitmapData = new BitmapData( rightW, mt, transparent, color );

			var clb: BitmapData = new BitmapData( leftW, oh - mt - mb, transparent, color );
			var ccb: BitmapData = new BitmapData( centerW, oh - mt - mb, transparent, color );
			var crb: BitmapData = new BitmapData( rightW, oh - mt - mb, transparent, color );

			var blb: BitmapData = new BitmapData( leftW, mb, transparent, color );
			var bcb: BitmapData = new BitmapData( centerW, mb, transparent, color );
			var brb: BitmapData = new BitmapData( rightW, mb, transparent, color );

			var py: int = 0;

			RECT.setTo( 0, py, ml, mt );
			tlb.copyPixels( p_bd, RECT, POINT, null, null, true );

			RECT.setTo( ml, py, tcb.width, tcb.height );
			tcb.copyPixels( p_bd, RECT, POINT, null, null, true );

//			RECT.setTo( ow - ml, 0, trb.width, trb.height );
			RECT.setTo( ow - mr, py, trb.width, trb.height );
			trb.copyPixels( p_bd, RECT, POINT, null, null, true );

			// ----
			py = mt;

			RECT.setTo( 0, py, clb.width, clb.height );
			clb.copyPixels( p_bd, RECT, POINT, null, null, true );

			RECT.setTo( ml, py, ccb.width, ccb.height );
			ccb.copyPixels( p_bd, RECT, POINT, null, null, true );

			RECT.setTo( ow - mr, py, crb.width, crb.height );
			crb.copyPixels( p_bd, RECT, POINT, null, null, true );

			// ----
			py = oh - mb;

			RECT.setTo( 0, py, blb.width, blb.height );
			blb.copyPixels( p_bd, RECT, POINT, null, null, true );

			RECT.setTo( ml, py, bcb.width, bcb.height );
			bcb.copyPixels( p_bd, RECT, POINT, null, null, true );

			RECT.setTo( ow - mr, py, brb.width, brb.height );
			brb.copyPixels( p_bd, RECT, POINT, null, null, true );

			return [tlb, tcb, trb, clb, ccb, crb, blb, bcb, brb];
		}


		//===================================================================================================================================================
		//
		//      ------  INSTANCE
		//
		//===================================================================================================================================================

		/**
		 * Constructor.
		 * @param p_doc
		 * @param p_skinId
		 */
		public function Scale9Bitmap( p_doc: DisplayObjectContainer = null, p_skinId: String = null ) {
			var pixelSnap: String = PixelSnapping.ALWAYS;
			var smooth: Boolean = true;
			tl = addChild( new Bitmap( null, pixelSnap, smooth ) ) as Bitmap;
			tc = addChild( new Bitmap( null, pixelSnap, smooth ) ) as Bitmap;
			tr = addChild( new Bitmap( null, pixelSnap, smooth ) ) as Bitmap;
			cl = addChild( new Bitmap( null, pixelSnap, smooth ) ) as Bitmap;
			cc = addChild( new Bitmap( null, pixelSnap, smooth ) ) as Bitmap;
			cr = addChild( new Bitmap( null, pixelSnap, smooth ) ) as Bitmap;
			bl = addChild( new Bitmap( null, pixelSnap, smooth ) ) as Bitmap;
			bc = addChild( new Bitmap( null, pixelSnap, smooth ) ) as Bitmap;
			br = addChild( new Bitmap( null, pixelSnap, smooth ) ) as Bitmap;
			if ( p_doc != null ) p_doc.addChild( this );
			if ( p_skinId ) useSkin( p_skinId );
//			UIUtils.debugDot( this, 0, 0, 5, 0x0000ff );
		}

		public var debugSliceSpace: int = 0;
		private var _padding: Rectangle;
		private var _filterBounds: Rectangle;
		private var _bounds: Rectangle;

		private var tl: Bitmap;
		private var tc: Bitmap;
		private var tr: Bitmap;
		private var cl: Bitmap;
		private var cc: Bitmap;
		private var cr: Bitmap;
		private var bl: Bitmap;
		private var bc: Bitmap;
		private var br: Bitmap;
		private var _ow: Number;
		private var _oh: Number;
		private var _ml: Number;
		private var _mr: Number;
		private var _mt: Number;
		private var _mb: Number;
		private var _rect: Rectangle = new Rectangle();
		private var _invalidateSize: Boolean = false;
		private var _skin: Object;
		private var _width: Number;

		override public function get width(): Number {
			return _width;
		}

		override public function set width( value: Number ): void {
			if ( _width == value ) return;
			if ( value < _ml + _mr ) value = _ml + _mr;
			_width = value;
			_invalidateSize = true;
			( useInvalidation ) ? invalidate() : render();
		}

		private var _height: Number;
		public var useInvalidation: Boolean = true;

		override public function get height(): Number {
			return _height;
		}

		override public function set height( value: Number ): void {
			if ( _height == value ) return;
			if ( value < _mt + _mb ) value = _mt + _mb;
			_height = value;
			_invalidateSize = true;
			( useInvalidation ) ? invalidate() : render();
		}

		private var _pivotX: Number = 0;

		public function get pivotX(): Number {
			return _pivotX;
		}

		public function set pivotX( value: Number ): void {
			_pivotX = value;
		}

		private var _pivotY: Number = 0;

		public function get pivotY(): Number {
			return _pivotY;
		}

		public function set pivotY( value: Number ): void {
			_pivotY = value;
		}

		public function useSkin( p_skinId: String ): void {
			if ( !p_skinId ) {
				trace( "Error, Scale9Bitmap::useSkin, p_skinId != NULL." );
				return;
			}
			if ( !_lib[p_skinId] ) {
				trace( "Error, Scale9Bitmap::useSkin, id=", p_skinId, " doesn't have a skin generated." );
				return;
			}

			_skin = _lib[p_skinId];
			_padding = _skin.padding;
			_filterBounds = _skin.fbounds;

			_ow = _skin.ow;
			_oh = _skin.oh;
			_bounds = _skin.bounds;

			_ml = _padding.x;
			_mr = _padding.width;
			_mt = _padding.y;
			_mb = _padding.height;

			tl.bitmapData = _skin.slices[0];
			tc.bitmapData = _skin.slices[1];
			tr.bitmapData = _skin.slices[2];
			cl.bitmapData = _skin.slices[3];
			cc.bitmapData = _skin.slices[4];
			cr.bitmapData = _skin.slices[5];
			bl.bitmapData = _skin.slices[6];
			bc.bitmapData = _skin.slices[7];
			br.bitmapData = _skin.slices[8];

			if ( _width == 0 ) _width = _ow;
			if ( _height == 0 ) _height = _oh;

			// calculate pivots based on bounds.
			_pivotX = -_bounds.x / _bounds.width;
			_pivotY = -_bounds.y / _bounds.height;

			// invalidate size
			setSize( _width, _height );
		}

		public function setSize( p_w: Number, p_h: Number ): void {
			if ( p_w < _ml + _mr ) p_w = _ml + _mr;
			if ( p_h < _mt + _mb ) p_h = _mt + _mb;
			_width = p_w;
			_height = p_h;
			_invalidateSize = true;
			render();
		}

		public function render(): void {
			// Skin not defined yet!.
			if ( !_skin ) return;
			if ( _invalidateSize ) {
				_invalidateSize = false;
				arrange();
			}
		}

		public function invalidate(): void {
			addEventListener( Event.ENTER_FRAME, onInvalidation );
		}

		public function arrange(): void {
			var offx: Number = _bounds.x;
			var offy: Number = _bounds.y;

			var tw: Number = _width - _ml - _mr;
			var th: Number = _height - _mt - _mb;

			offx = -_pivotX * _width;
			offy = -_pivotY * _height;

			tl.y = tc.y = tr.y = _filterBounds.y + offy;
			cl.y = cc.y = cr.y = _mt + debugSliceSpace + offy;
			bl.y = bc.y = br.y = _height - _mb + debugSliceSpace * 2 + offy;

			tl.x = cl.x = bl.x = _filterBounds.x + offx;
			tc.x = bc.x = cc.x = _mr + debugSliceSpace + offx;
			tr.x = cr.x = br.x = ( _width - _mr ) + debugSliceSpace * 2 + offx;
//			tr.x = cr.x = br.x = ( _width - _mr ) * 0.5 + debugSliceSpace * 2 ;

			tc.width = cc.width = bc.width = tw;
			cl.height = cc.height = cr.height = th;
		}

		public function move( p_x: int, p_y: int ): void {
			this.x = p_x;
			this.y = p_y;
		}

		private function onInvalidation( event: Event ): void {
			removeEventListener( Event.ENTER_FRAME, onInvalidation );
			render();
		}
	}
}