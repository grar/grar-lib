/**
 * Code by Rodrigo López Peker on 12/30/15 3:41 PM.
 *
 */
package grar.starling.abs {
import com.greensock.TweenLite;

import grar.utils.AppUtils;

import starling.display.DisplayObject;
import starling.display.Sprite;
import starling.text.TextField;

public class AbsSprite extends Sprite {

	protected var _w:int;
	protected var _h:int;
	protected var _sw:int;
	protected var _sh:int;

	public var componentId:String;
	// store anything inside.
	public var udata:Object = {};

	protected var _useDrawInvalidation:Boolean;
	protected var _waitingDraw:Boolean;
	protected var _active:Boolean;
	protected var _uiReady:Boolean;

	/**
	 * constructor.
	 * @param doc
	 */
	public function AbsSprite( doc:Sprite = null ) {
//		_sw = Starling.current.stage.stageWidth * Settings.scaleFactor;
//		_sh = Starling.current.stage.stageHeight * Settings.scaleFactor;
		_sw = AppUtils.appW;
		_sh = AppUtils.appH;
		if ( doc ) {
			doc.addChild( this );
		}
		initialize();
//		invalidateDraw();
	}

	public function reset():void {}

	protected function initialize():void {
	}

	protected function draw():void {

	}

	public function activate( flag:Boolean ):void {
		_active = flag;
	}

	public function setupUI():void {
		_uiReady = true;
	}

	public function clearUI():void {
		_uiReady = false;
	}

	//===================================================================================================================================================
	//
	//      ------  size stuffs
	//
	//===================================================================================================================================================
	protected function useAppSize():void {
		_w = AppUtils.appW;
		_h = AppUtils.appH;
	}

	protected function initSize( tw:int, th:int, useScale:Boolean = false ):void {
		if ( useScale ) {
			tw *= AppUtils.scale;
			th *= AppUtils.scale;
		}
		_w = tw;
		_h = th;
	}

	public function setSize( targetW:int = -1, targetH:int = -1, useGlobalScale:Boolean = false ):void {
		if ( targetW == -1 && targetH == -1 )
			return;

		if ( targetW > -1 ) {
			if ( useGlobalScale ) targetW = ss( targetW );
			_w = targetW;
		}

		if ( targetH > -1 ) {
			if ( useGlobalScale ) targetH = ss( targetH );
			_h = targetH;
		}
		invalidateDraw();
	}

	protected function onDrawInvalidated():void {
		_waitingDraw = false;
		draw();
	}

	protected function invalidateDraw():void {
		if ( !_useDrawInvalidation ) {
			if ( _waitingDraw ) {
				TweenLite.killTweensOf( onDrawInvalidated );
				_waitingDraw = false;
			}
			draw();
		} else {
			if ( _waitingDraw )
				return;
			_waitingDraw = true;
			TweenLite.delayedCall( 0, onDrawInvalidated );
		}
	}

	public function move( tx:Number, ty:Number, useScale:Boolean = false ):void {
		if ( useScale ) {
			tx = tx * AppUtils.scale;
			ty = ty * AppUtils.scale;
		}
		this.x = tx;
		this.y = ty;
	}


	public function ss( value:Number ):Number {
		return AppUtils.scale * value;
	}

	public function get w():int {
		return _w;
	}

	public function get h():int {
		return _h;
	}

	public function set h( value:int ):void {
		if ( _h == value )
			return;
		_h = value;
		invalidateDraw();
	}

	public function set w( value:int ):void {
		if ( _w == value )
			return;
		_w = value;
		invalidateDraw();
	}


	//===================================================================================================================================================
	//
	//      ------  props
	//
	//===================================================================================================================================================
	//===================================================================================================================================================
	//
	//      ------  utils
	//
	//===================================================================================================================================================
	public function setProps( obj:Object, props:Object ):void {
		if ( !obj )
			obj = this;
		props.immediateRender = true;
		TweenLite.to( obj, 0, props );
	}

//		Utility to apply a delay call
	public function dly( seconds:Number, callback:Function, ...args ):void {
		TweenLite.killDelayedCallsTo( callback );
		if ( args && args[0] is Array )
			args = args[0];
		TweenLite.delayedCall( seconds, callback, args );
	}

	public function matchSize( obj:DisplayObject, tw:int = -1, th:int = -1 ):void {
		if ( tw < 0 ) tw = _w;
		if ( th < 0 ) th = _h;
		obj.width = tw;
		obj.height = th;
	}


//	Starling. Utility to color an object (sprite, bitmap, movieclip)
	public function tint( obj:Object, color:* = null ):void {
		if ( !obj )
			return;
		if ( typeof( color ) == "number" && color < 0 )
			color = null;

		if ( color == null ) color = 0xFFFFFF;

		if ( obj is TextField ) {
			TextField( obj ).format.color = color;
		} else {
			setProps( obj, {hexColors: {color: color}} );
		}
	}

	public function kill( ...args ):void {
		var o:Object;
		for each( o in args ) {
			if ( o is Array ) {
				kill.apply( null, args[0] );
			} else {
				TweenLite.killTweensOf( o );
			}
		}
	}

	public function get active():Boolean {
		return _active;
	}
}
}
