/**
 * Code by Rodrigo López Peker on 12/30/15 3:41 PM.
 *
 */
package grar.starling.abs {
import grar.data.CommonDef;

import starling.display.Sprite;

public class AbsSpriteState extends AbsSprite {

	protected var _isOpen:Boolean;
	protected var _autoOpenedClosed:Boolean = false;

	/**
	 * constructor.
	 * @param doc
	 */
	public function AbsSpriteState( doc:Sprite = null ) {
		super( doc );
	}

	public function open():void {
		_isOpen = true;

		if ( !_uiReady )
			setupUI();

		dispatchEventWith( CommonDef.OPEN );
		if ( _autoOpenedClosed )
			opened();
	}

	public function opened():void {
		_isOpen = true;
		if ( !_active )
			activate( true );

		dispatchEventWith( CommonDef.OPENED );
	}

	public function close():void {
		_isOpen = false;
		if ( _active )
			activate( false );

		dispatchEventWith( CommonDef.CLOSE );

		if ( _autoOpenedClosed )
			closed();
	}

	public function closed():void {
		_isOpen = false;
		if ( _active )
			activate( false );

		if ( _uiReady )
			clearUI();

		dispatchEventWith( CommonDef.CLOSED );
	}

	public function get isOpen():Boolean {
		return _isOpen;
	}

}
}
