/**
 * Code by Rodrigo López Peker on 2/4/16 2:00 PM.
 *
 */
package grar.starling.abs {
import grar.utils.LoanShark;

public class AbsListItem extends AbsSprite {

	protected var _pool:LoanShark;

	protected var _data:Object;
	protected var _list:AbsList;
//	protected var _onMouse:Signal;
	protected var _btnMode:Boolean;
	protected var _isPress:Boolean;
	protected var _isOver:Boolean;
	protected var _state:String;
	protected var _selected:Boolean;
	protected var _enabled:Boolean;

	public var onDrawData:Function;

	public var idx:int;
	public var row:int;

	/**
	 * constructor.
	 * @param doc
	 */
	public function AbsListItem() {
		super( null );
	}

	public function returnToPool():void {
		if ( _pool ) _pool.returnObject( this );
		reset();
	}

	override protected function initialize():void {
		super.initialize();
		udata = {};
		_enabled = true;
	}

	override public function reset():void {
		super.reset();
		if ( _active ) {
			activate( false );
		}
		if ( _isPress ) {
//			UIUtils.stageMouseUp( handleMouse, false );
//			removeEventListener( MouseEvent.ROLL_OVER, handleMouse );
//			removeEventListener( MouseEvent.ROLL_OUT, handleMouse );
		}
//		if ( _onMouse ) _onMouse.removeAll();
		removeEventListeners();
		if ( parent ) parent.removeChild( this );
	}

	override public function activate( flag:Boolean ):void {
		if ( flag == _active )
			return;
		super.activate( flag );
		if ( _btnMode ) {
			addListeners( flag );
		}
	}


	//===================================================================================================================================================
	//
	//      ------  button mode
	//
	//===================================================================================================================================================

	protected function onSelected():void {

	}

	protected function onEnabled():void {

	}

	protected function hover( flag:Boolean ):void {

	}

	protected function addListeners( flag:Boolean ):void {
//		UIUtils.starlingListener( this, TouchEvent.TOUCH, handleTouch, flag );
	}

	/*private function handleTouch( event:TouchEvent ):void {
	 var touch:Touch = event.getTouch( this );
	 var isWithinBounds:Boolean;
	 if ( !_enabled ) {
	 return;
	 } else if ( touch == null ) {
	 state = ButtonState.UP;
	 hover( false );
	 } else if ( touch.phase == TouchPhase.HOVER ) {
	 state = ButtonState.OVER;
	 } else if ( touch.phase == TouchPhase.BEGAN && !_isDown ) {
	 _triggerBounds = getBounds( stage, _triggerBounds );
	 _triggerBounds.inflate( maxDragDist, maxDragDist );
	 state = ButtonState.DOWN;
	 } else if ( touch.phase == TouchPhase.MOVED ) {
	 isWithinBounds = _triggerBounds.contains( touch.globalX, touch.globalY );
	 if ( _state == ButtonState.DOWN && !isWithinBounds ) {
	 // reset button when finger is moved too far away ...
	 state = ButtonState.UP;
	 } else if ( _state == ButtonState.UP && isWithinBounds ) {
	 // ... and reactivate when the finger moves back into the bounds.
	 state = ButtonState.DOWN;
	 }
	 } else if ( touch.phase == TouchPhase.ENDED && _state == ButtonState.DOWN ) {
	 state = ButtonState.UP;
	 release();
	 if ( !touch.cancelled ) {
	 tap();
	 dispatchEventWith( Event.TRIGGERED, true );
	 }
	 }
	 }*/
	/*
	 protected function handleMouse( e:MouseEvent ):void {
	 switch ( e.type ) {
	 case MouseEvent.MOUSE_DOWN :
	 _isPress = true;
	 _isOver = true;
	 if ( _onMouse )
	 _onMouse.dispatch( this, Ref.OVER );
	 UIUtils.stageMouseUp( handleMouse, true );
	 addEventListener( MouseEvent.ROLL_OVER, handleMouse );
	 addEventListener( MouseEvent.ROLL_OUT, handleMouse );
	 //				state = Ref.OVER ;
	 press( true );
	 break;
	 case MouseEvent.ROLL_OVER :
	 _isOver = true;
	 break;
	 case MouseEvent.ROLL_OUT:
	 _isOver = false;
	 break;
	 case MouseEvent.MOUSE_UP:
	 UIUtils.stageMouseUp( handleMouse, false );
	 removeEventListener( MouseEvent.ROLL_OVER, handleMouse );
	 removeEventListener( MouseEvent.ROLL_OUT, handleMouse );
	 _isOver = _isPress = false;
	 if ( _onMouse ) _onMouse.dispatch( this, Ref.OUT );
	 press( false );
	 break;
	 }
	 }*/

	public function press( flag:Boolean ):void {
		// change states, override.
	}

	public function get list():AbsList {
		return _list;
	}

	public function set list( value:AbsList ):void {
		_list = value;
	}

	/*
	 public function get onMouse():Signal {
	 return _onMouse;
	 }

	 public function set onMouse( value:Signal ):void {
	 _onMouse = value;
	 }*/

	public function get btnMode():Boolean {
		return _btnMode;
	}

	/*public function set btnMode( value:Boolean ):void {
	 if ( _btnMode == value )
	 return;
	 _btnMode = value;
	 mouseChildren = !_btnMode;
	 if ( !_btnMode ) {
	 addListeners( false );
	 buttonMode = false;
	 } else {
	 buttonMode = true;
	 if ( _active ) addListeners( true );
	 }
	 }*/

	public function get data():Object {
		return _data;
	}

	public function set data( value:Object ):void {
		_data = value;
		drawData();
	}

	protected function drawData():void {
		if ( onDrawData )
			onDrawData( this, _data );
	}

}
}