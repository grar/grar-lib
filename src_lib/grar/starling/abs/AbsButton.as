/**
 * Code by Rodrigo López Peker on 12/30/15 3:54 PM.
 *
 */
package grar.starling.abs {
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.ui.Mouse;
import flash.ui.MouseCursor;

import grar.data.CommonDef;

import grar.utils.AppUtils;

import starling.display.ButtonState;
import starling.display.DisplayObject;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class AbsButton extends AbsSprite {

	private static const MAX_DRAG_DIST:Number = 50;
	public var maxDragDist:Number;
	protected var _useHandCursor:Boolean = true;
	protected var _enabled:Boolean = true;
	protected var _isDown:Boolean = false;
	protected var _triggerBounds:Rectangle;
	protected var _state:String;
	protected var _hitArea:Quad;
	protected var _useHitArea:Boolean = false;
	protected var _hitAreaOffset:int;
	protected var _hitAreaAlpha:Number = 0;
	public var idx:int;

	public var dispatchStateEvents:Boolean = false;

	public function AbsButton( doc:Sprite = null ) {
		maxDragDist = MAX_DRAG_DIST * AppUtils.scale;
		super( doc );
	}

	override protected function initialize():void {
		// initialize hit area properties BEFORE this method call.
		super.initialize();
		this.touchGroup = true;
		_triggerBounds = new Rectangle();
		addEvents( true );
		initializeHitArea();
		if ( _useHitArea ) {
			_hitArea = new Quad( _w + _hitAreaOffset * 2, _h + _hitAreaOffset * 2, 0xff0000 );
			_hitArea.alpha = _hitAreaAlpha;
			_hitArea.x = -_hitAreaOffset;
			_hitArea.y = -_hitAreaOffset;
			addChild( _hitArea );
		}
	}

	protected function initializeHitArea():void {
	}

	override protected function draw():void {
		super.draw();
		if ( _hitArea && _useHitArea ) {
			_hitArea.x = -_hitAreaOffset;
			_hitArea.y = -_hitAreaOffset;
			_hitArea.readjustSize( _w + _hitAreaOffset * 2, _h + _hitAreaOffset * 2 );
		}
	}

	override public function hitTest( localPoint:Point ):DisplayObject {
		if ( !_useHitArea ) {
			return super.hitTest( localPoint );
		}
		if ( !visible || !touchable || !hitTestMask( localPoint ) ) return null;
		localPoint.x -= _hitArea.x;
		localPoint.y -= _hitArea.y;
		return _hitArea.hitTest( localPoint );
	}

	private function addEvents( flag:Boolean ):void {
		if ( flag ) {
			addEventListener( TouchEvent.TOUCH, handleTouch );
		} else {
			removeEventListener( TouchEvent.TOUCH, handleTouch );
		}
	}

	public function killPress():void {
		state = ButtonState.UP;
		_isDown = false;
		_triggerBounds.setEmpty();
		hover( false );
	}

	protected function handleTouch( event:TouchEvent ):void {

		// only works in ADL for desktop :(
		Mouse.cursor = ( _useHandCursor && _enabled && event.interactsWith( this )) ? MouseCursor.BUTTON : MouseCursor.AUTO;

		var touch:Touch = event.getTouch( this );
		var isWithinBounds:Boolean;
		if ( !_enabled ) {
			return;
		} else if ( touch == null ) {
			state = ButtonState.UP;

			if ( dispatchStateEvents )
				dispatchEventWith( CommonDef.OUT, true );

			hover( false );
		} else if ( touch.phase == TouchPhase.HOVER ) {
			state = ButtonState.OVER;
		} else if ( touch.phase == TouchPhase.BEGAN && !_isDown ) {
			_triggerBounds = getBounds( stage, _triggerBounds );
			_triggerBounds.inflate( maxDragDist, maxDragDist );
			state = ButtonState.DOWN;
			if ( dispatchStateEvents )
				dispatchEventWith( CommonDef.HOVER, true );
		} else if ( touch.phase == TouchPhase.MOVED ) {
			isWithinBounds = _triggerBounds.contains( touch.globalX, touch.globalY );
			if ( _state == ButtonState.DOWN && !isWithinBounds ) {
				// reset button when finger is moved too far away ...
				state = ButtonState.UP;
				if ( dispatchStateEvents )
					dispatchEventWith( CommonDef.OUT, true );
			} else if ( _state == ButtonState.UP && isWithinBounds ) {
				if ( dispatchStateEvents )
					dispatchEventWith( CommonDef.HOVER, true );
				// ... and reactivate when the finger moves back into the bounds.
				state = ButtonState.DOWN;
			}
		} else if ( touch.phase == TouchPhase.ENDED && _state == ButtonState.DOWN ) {
			state = ButtonState.UP;
//			event.stopImmediatePropagation() ;
			release( true );
			if ( !touch.cancelled ) {
				tap();
//				dispatchEventWith( Event.TRIGGERED, true );
				dispatchEventWith( CommonDef.TAP, true );
			}
		}
	}

	protected function tap():void {
//		trace("tap")
	}

	protected function release( touching:Boolean ):void {
//		trace("release") ;
	}

	//over/out-
	protected function hover( flag:Boolean ):void {
//		trace("hover=", flag) ;
	}

	protected function press():void {
//		trace("press") ;
	}

	public function get state():String {
		return _state;
	}

	/*override public function hitTest( localPoint:Point ):DisplayObject {
	 trace( "wow, wtf?", localPoint, bounds );
	 _triggerBounds.setTo(0,0,_w,_h) ;
	 return super.hitTest( localPoint );
	 }*/

	public function set state( value:String ):void {
		var oldState:String = _state;
		if ( _state == value )
			return;
		_state = value;
		if ( _state == ButtonState.UP ) {
			if ( oldState == ButtonState.DOWN ) {
				release( false );
			}
		} else if ( _state == ButtonState.DOWN ) {
			press();
		} else if ( _state == ButtonState.OVER ) {
			hover( true );
		} else if ( _state == ButtonState.DISABLED ) {
			enabled = false;
		}
	}

	public function get enabled():Boolean {
		return _enabled;
	}

	public function set enabled( value:Boolean ):void {
		if ( _enabled == value )
			return;
		_enabled = value;
		onEnabled();
	}

	protected function onEnabled():void {
		alpha = _enabled ? 1 : 0.5;
	}

	public function get useHitArea():Boolean {
		return _useHitArea;
	}

	public function set useHitArea( value:Boolean ):void {
		if ( _useHitArea == value )
			return;
		_useHitArea = value;
		if ( _useHitArea ) {
			if ( !_hitArea ) {
				_hitArea = new Quad( _w + _hitAreaOffset * 2, _h + _hitAreaOffset * 2, 0xff000 );
			} else {
				_hitArea.readjustSize( _w + _hitAreaOffset * 2, _h + _hitAreaOffset * 2 );
			}
			_hitArea.x = -_hitAreaOffset;
			_hitArea.y = -_hitAreaOffset;
			_hitArea.alpha = _hitAreaAlpha;
			addChild( _hitArea );
		} else {
			if ( _hitArea && _hitArea.parent )
				_hitArea.parent.removeChild( _hitArea );
		}

	}

	public function get hitAreaOffset():int {
		return _hitAreaOffset;
	}

	public function set hitAreaOffset( value:int ):void {
		if ( _hitAreaOffset == value )
			return;
		_hitAreaOffset = value;
		if ( _hitArea ) {
			_hitArea.x = -value;
			_hitArea.y = -value;
			_hitArea.readjustSize( _w + _hitAreaOffset * 2, _h + _hitAreaOffset * 2 );
		}
	}

	public function get isDown():Boolean {
		return _isDown;
	}
}
}
