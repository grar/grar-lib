// =================================================================================================
//
//	Starling Framework
//	Copyright Gamua GmbH. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================

package grar.starling.fonts.pixelfonts {
import flash.display.BitmapData;
import flash.geom.Rectangle;
import flash.utils.ByteArray;

import starling.text.BitmapFont;
import starling.text.TextField;
import starling.textures.Texture;
import starling.textures.TextureSmoothing;

public class Std95BitmapFont {
	public static const BITMAP_WIDTH:int = 128;
	public static const BITMAP_HEIGHT:int = 128;
	public static const BITMAP_DATA:Array = [
		2027613533,1533942577,214748539,3026070593,2323518504,41628975,1783224830,1231210475,
		3041295564,1218723390,3733977059,3823369159,3823072255,4282703539,2062416833,1677007304,
		3955768829,4238322539,2908643293,3756685796,3661345127,4267638047,846654567,553107582,
		775153323,4218472187,3996758019,2146841361,3156131318,3437733950,1350373243,2646310399,
		361671791,1408255975,83089807,3188134783,2934085354,4086430137,2077110520,603413893,
		4281592912,4223646383,1684817895,412820444,2650747193,2652570387,1486977388,100552380,
		3960615806,3462870143,2864906186,4266729342,134143469,2680521373,3891033948,2113918331,
		1364101746,120889003,1912485116,2438773258,2137513648,4187954023,1575899533,4287175319,
		2475668120,1322712664,4008850247,1358703643,4279623119,419947582,3516679381,2716782719,
		279880678,1386965945,4051244543,3646739909,4008335955,4051152962,3986682333,1853640421,
		1316472564,352209422,2562989849,1193783142,2655742327,3722855527,4234017639,4015255855,
		3403476116,2066215615,1005428729,239730087,1647201762,1284092236,3628051041,3000672403,
		3813655327,3045049582,1994502062,3081040207,3889864163,1068132266,1549657599,4009195253,
		1062481816,2432687553,3148510106,2508146884,4100446109,484679641,887306076,1608574587,
		2847761321,2034140671,2984980652,1562488617,2629636735,1733144755,1867573028,2988427201,
		1607557871,2961139103,1652031095,3858470257,1239601138,22330227,3959473599,3554283419,
		3484481252,3971567864,2172305193,2143810252,334591988,3617455679,2717592088,2002719570,
		4051172386,2474354418,1060466361,4168598812,2919217890,3117218456,2881041148,1338594087,
		3911030268,1339941858,3899291565,2637953920,1003943509,4076831862,3058420554,4163134814,
		1323465131,2039338629,2960787651,3440538011,2312989292,953286563,1967163829,3222617834,
		985639683,2973662922,3739260150,3791273318,222678471,1576751088,2234089185,2436709301,
		866917674,1901845875,2666987411,4032296502,652361707,1469167696,1778346733,1634204885,
		4256426718,1252486739,3241969087,3273374435,4025392970,3864751358,3488482115,3792360379,
		2297801693,3982488063,2310691462,121142303,2642668088,2647064490,3795825463,3419216788,
		3621649827,1408507863,1333121791,1419692460,3844399810,3380478677,2287742671,3302956531,
		1940221774,3243764063,3822507178,973130291,2633978805,2808740959,3439793659,1383335477,
		1867724037,3858685688,3621280586,3215773951,2799927205,4054515198,1414437328,2477439458,
		2495499817,4215798368,2853859178,2942720595,1425326025,4187970367,2446609938,3702131455,
		1400479363,2141888067,4124945590,2494725991,3732376327,2331993908,867569347,2636626941,
		2139424473,3724416461,1424057257,3037021667,2878007251,4153123323,3319093904,1959761556,
		1990226602,4009241579,3215742695,1781983994,286952413,4171300573,3388450668,1448975191,
		3615923633,1969926805,960432511,891253629,3358207547,1520290651,489529971,3181853567,
		322794333,1565970845,1408584487,3801076986,2917794322,2117341667,2939701946,3470539143,
		1783824023,2659415948,2541961123,122610044,2310715124,1496852989,397214070,885909820,
		2681334870,4275355967,3115814893,899016755,2996470621,4257205143,477992877,21882097,
		1820272309,2548271613,60299857,2550083898,2393511339,4259814616,3604086489,1028574904,
		2717700073,1069967493,1848609449,3892062490,1083879335,3613054264,1590813625,3153977217,
		2139444817,2836740963,927532918,1786712822,2952691562,1026083125,3565048605,4072237820,
		1503556743,4255644228,1056685355,4148394559,1959414646,3606682786,4124848438,3378393993,
		3187371631,719778687,2138996478,4210752250,4218643180,1467735481,2103480691,143815939,
		2300486543,2935298043,4286553734,3211502395,3612400794,304453531,2160052975,1422808017,
		2875303399,619735036,3770662715,3803182277,1974997955,3512524452,4141776677,4233345462,
		2329370091,3866098795,4032293668,2566722668,2903489311,3884810134,52942547,2495484105,
		4083637765,2137378092,3254768058,312817568,2722478261,2660228671,226489531,1978202014,
		947365594,1870054783,131552875,3935351639,2944141275,2068501791,2817146857,4078935352,
		3952915950,3632212551,1581071082,3217963470,2516068218,2049389665,3316600301,4256357374,
		2444861670,1966006128,1887364047,3889811262,2134376076,3749700511,4093634617,3927276579,
		1971768644,4238334311,3623620601,506093436,1174097915,2368011914,3601900367,326252278,
		2489213289,3756517484,156977621,2267608703,2182984502,3194720160,1929781355,3353976432,
		332398432,1650738668,1224020851,1899805958,3638130657,3969864910,3478022839,752302581,
		2080859373,1009884748,2912979839,2724173882,2261988544,3837700868,2788058955,4126302331,
		2622416256,3388514254,2342321776,1764593805,1470620047,4034102963,1005961941,4163360388,
		664797160,2929666555,3889535498,3575551289,3790869119,2247766183,3481787003,1715969949,
		1142929513,4259255501,2896330346,2534233577,2685657983,2281542472,4005417614,3701371209,
		4221546239,1439523767,3880481406,972574677,2253584435,890842311,1694234409,3753331203,
		2658596499,2466229227,2991496557,2995655633,1096113191,3942583951,1274680054,2348897963,
		2885059347,943772985,984215923,3795833679,951649844,1225214198,3444308990,1198959742,
		2920499090,3822834951,687644629,2040391644,2118207660,1373012184,4082192343,4277065329,
		417050348,657548687,1491766403,3558419822,2401318287,1711097386,798924543,2817611568,
		3718865309,1358124417,4289617097,3644818680,3035299280,2816802796,1479142342,2389080199,
		507130327,1676404565,1604246535,2462203609,863119879,2899490521,4264382142,2985449262,
		927849951,2314773499,3156962103,2499368377,238086412,3631512979,1931873451,3820157344,
		2060904667,4286290571,335485311,3132254533,3344002301,3467850939,3433514045,1887095956,
		2558653055,3293541860,107925310,4135233790,2271396044,3043487569,4155417826,1499987029,
		3467368059,694385621,535079583,2758448046,108532478,3967328630,2795306387,3897551472,
		2156493633,335063011,926621532,3144903503,644579515,4034391723,3554235283,3463687110,
		4294697601,2355616479,1002019754,934143594,2002759875,4275166381,1110657527,4100249153,
		2800549234,4172985214,2496268242,3124872602,3901029823,1781372778,4293589441,2515255054,
		4272923379,2784804723,2117368422,2246215516,3445857023,3218823081,895499207,1150113199,
		2969040893,2566518787,199783078
	];

	private static const XML_DATA:XML = <font><info face="st_955" size="8" bold="0" italic="0" charset="" unicode="" stretchH="100" smooth="1" aa="1" padding="1,1,1,1" spacing="0,0" outline="0"/><common lineHeight="15" base="12" scaleW="128" scaleH="128" pages="1" packed="0"/><chars count="210"><char id="97" x="1" y="1" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/><char id="98" x="1" y="8" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="99" x="1" y="18" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/><char id="100" x="1" y="25" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="101" x="1" y="35" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/><char id="102" x="1" y="42" width="3" height="9" xoffset="0" yoffset="4" xadvance="4"/><char id="103" x="1" y="52" width="5" height="9" xoffset="0" yoffset="7" xadvance="6"/><char id="104" x="5" y="42" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="105" x="7" y="1" width="1" height="9" xoffset="0" yoffset="4" xadvance="2"/><char id="106" x="7" y="11" width="2" height="12" xoffset="0" yoffset="4" xadvance="3"/><char id="107" x="9" y="1" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="108" x="7" y="24" width="1" height="9" xoffset="0" yoffset="4" xadvance="2"/><char id="109" x="7" y="34" width="9" height="6" xoffset="0" yoffset="7" xadvance="10"/><char id="110" x="9" y="24" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/><char id="111" x="10" y="11" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/><char id="112" x="15" y="1" width="5" height="9" xoffset="0" yoffset="7" xadvance="6"/><char id="113" x="15" y="18" width="5" height="9" xoffset="0" yoffset="7" xadvance="6"/><char id="114" x="16" y="11" width="4" height="6" xoffset="0" yoffset="7" xadvance="5"/><char id="115" x="1" y="62" width="4" height="6" xoffset="0" yoffset="7" xadvance="5"/><char id="116" x="1" y="69" width="3" height="9" xoffset="0" yoffset="4" xadvance="4"/><char id="117" x="1" y="79" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/><char id="118" x="5" y="69" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/><char id="119" x="6" y="62" width="9" height="6" xoffset="0" yoffset="7" xadvance="10"/><char id="120" x="7" y="52" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/><char id="121" x="11" y="41" width="5" height="9" xoffset="0" yoffset="7" xadvance="6"/><char id="122" x="13" y="51" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/><char id="65" x="17" y="28" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="66" x="17" y="38" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="67" x="21" y="1" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="68" x="21" y="11" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="69" x="1" y="86" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="70" x="7" y="76" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="71" x="1" y="96" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="72" x="1" y="106" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="73" x="1" y="116" width="1" height="9" xoffset="0" yoffset="4" xadvance="2"/><char id="74" x="3" y="116" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="75" x="8" y="86" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="76" x="14" y="69" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="77" x="16" y="58" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/><char id="78" x="19" y="48" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="79" x="8" y="96" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="80" x="8" y="106" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="81" x="9" y="116" width="6" height="10" xoffset="0" yoffset="4" xadvance="7"/><char id="82" x="24" y="21" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="83" x="24" y="31" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="84" x="28" y="1" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="85" x="28" y="11" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="86" x="34" y="1" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/><char id="87" x="15" y="79" width="9" height="9" xoffset="0" yoffset="4" xadvance="10"/><char id="88" x="20" y="68" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/><char id="89" x="24" y="58" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/><char id="90" x="26" y="41" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="48" x="31" y="21" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="49" x="35" y="11" width="2" height="9" xoffset="0" yoffset="4" xadvance="3"/><char id="50" x="31" y="31" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="51" x="15" y="89" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="52" x="15" y="99" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="53" x="16" y="109" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="54" x="22" y="89" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="55" x="22" y="99" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="56" x="25" y="78" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="57" x="28" y="68" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="33" x="23" y="109" width="1" height="9" xoffset="1" yoffset="4" xadvance="3"/><char id="8221" x="9" y="31" width="3" height="2" xoffset="0" yoffset="4" xadvance="4"/><char id="35" x="16" y="119" width="7" height="7" xoffset="0" yoffset="5" xadvance="8"/><char id="36" x="25" y="109" width="5" height="11" xoffset="0" yoffset="3" xadvance="6"/><char id="37" x="29" y="88" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/><char id="38" x="32" y="78" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="8217" x="5" y="76" width="1" height="2" xoffset="0" yoffset="4" xadvance="2"/><char id="40" x="32" y="41" width="3" height="11" xoffset="0" yoffset="3" xadvance="4"/><char id="41" x="32" y="53" width="3" height="11" xoffset="0" yoffset="3" xadvance="4"/><char id="42" x="26" y="51" width="5" height="5" xoffset="0" yoffset="6" xadvance="6"/><char id="43" x="24" y="121" width="5" height="5" xoffset="0" yoffset="6" xadvance="6"/><char id="44" x="13" y="58" width="2" height="3" xoffset="0" yoffset="12" xadvance="3"/><char id="45" x="7" y="59" width="5" height="1" xoffset="0" yoffset="8" xadvance="6"/><char id="46" x="17" y="48" width="1" height="1" xoffset="0" yoffset="12" xadvance="2"/><char id="47" x="29" y="98" width="5" height="10" xoffset="0" yoffset="3" xadvance="6"/><char id="58" x="15" y="28" width="1" height="4" xoffset="1" yoffset="7" xadvance="3"/><char id="59" x="35" y="65" width="2" height="8" xoffset="0" yoffset="7" xadvance="3"/><char id="60" x="31" y="109" width="4" height="7" xoffset="0" yoffset="5" xadvance="5"/><char id="61" x="35" y="74" width="5" height="3" xoffset="0" yoffset="7" xadvance="6"/><char id="62" x="35" y="98" width="4" height="7" xoffset="0" yoffset="5" xadvance="5"/><char id="63" x="37" y="88" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="64" x="39" y="78" width="8" height="8" xoffset="0" yoffset="6" xadvance="9"/><char id="91" x="36" y="106" width="2" height="11" xoffset="0" yoffset="3" xadvance="3"/><char id="92" x="39" y="106" width="5" height="10" xoffset="0" yoffset="3" xadvance="6"/><char id="93" x="43" y="87" width="2" height="11" xoffset="0" yoffset="3" xadvance="3"/><char id="94" x="40" y="99" width="5" height="3" xoffset="0" yoffset="5" xadvance="6"/><char id="95" x="1" y="126" width="7" height="1" xoffset="0" yoffset="12" xadvance="8"/><char id="96" x="32" y="65" width="2" height="2" xoffset="0" yoffset="4" xadvance="3"/><char id="123" x="36" y="41" width="3" height="11" xoffset="0" yoffset="3" xadvance="4"/><char id="124" x="36" y="53" width="1" height="11" xoffset="1" yoffset="3" xadvance="4"/><char id="125" x="38" y="53" width="3" height="11" xoffset="0" yoffset="3" xadvance="4"/><char id="126" x="40" y="103" width="6" height="2" xoffset="0" yoffset="4" xadvance="7"/><char id="710" x="10" y="18" width="3" height="2" xoffset="0" yoffset="4" xadvance="4"/><char id="732" x="30" y="121" width="6" height="2" xoffset="0" yoffset="4" xadvance="7"/><char id="8211" x="10" y="21" width="4" height="1" xoffset="0" yoffset="8" xadvance="5"/><char id="8212" x="31" y="118" width="6" height="1" xoffset="0" yoffset="8" xadvance="7"/><char id="8216" x="13" y="31" width="1" height="2" xoffset="0" yoffset="4" xadvance="2"/><char id="8218" x="11" y="69" width="2" height="3" xoffset="0" yoffset="12" xadvance="3"/><char id="8220" x="30" y="124" width="3" height="2" xoffset="0" yoffset="4" xadvance="4"/><char id="8222" x="34" y="124" width="5" height="3" xoffset="0" yoffset="12" xadvance="6"/><char id="8224" x="38" y="118" width="3" height="5" xoffset="0" yoffset="6" xadvance="4"/><char id="8225" x="42" y="117" width="3" height="10" xoffset="0" yoffset="4" xadvance="4"/><char id="8226" x="38" y="65" width="3" height="3" xoffset="0" yoffset="7" xadvance="4"/><char id="8230" x="38" y="69" width="5" height="1" xoffset="0" yoffset="12" xadvance="6"/><char id="8240" x="45" y="106" width="9" height="9" xoffset="0" yoffset="4" xadvance="10"/><char id="8249" x="21" y="21" width="2" height="3" xoffset="0" yoffset="7" xadvance="3"/><char id="8250" x="41" y="71" width="2" height="3" xoffset="0" yoffset="7" xadvance="3"/><char id="8482" x="46" y="116" width="9" height="4" xoffset="0" yoffset="4" xadvance="10"/><char id="39" x="11" y="73" width="1" height="2" xoffset="0" yoffset="4" xadvance="2"/><char id="34" x="41" y="75" width="3" height="2" xoffset="0" yoffset="4" xadvance="4"/><char id="191" x="46" y="87" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="161" x="38" y="11" width="1" height="9" xoffset="1" yoffset="4" xadvance="3"/><char id="162" x="47" y="97" width="5" height="7" xoffset="0" yoffset="7" xadvance="6"/><char id="163" x="38" y="21" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="164" x="40" y="11" width="8" height="9" xoffset="0" yoffset="4" xadvance="9"/><char id="165" x="42" y="1" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/><char id="166" x="38" y="31" width="1" height="9" xoffset="1" yoffset="4" xadvance="4"/><char id="167" x="40" y="31" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="168" x="46" y="121" width="3" height="1" xoffset="0" yoffset="4" xadvance="4"/><char id="169" x="44" y="21" width="8" height="8" xoffset="0" yoffset="5" xadvance="9"/><char id="170" x="49" y="11" width="5" height="8" xoffset="0" yoffset="5" xadvance="6"/><char id="171" x="46" y="123" width="5" height="3" xoffset="0" yoffset="7" xadvance="6"/><char id="172" x="52" y="121" width="6" height="3" xoffset="0" yoffset="8" xadvance="7"/><char id="173" x="52" y="125" width="5" height="1" xoffset="0" yoffset="8" xadvance="6"/><char id="174" x="50" y="1" width="8" height="8" xoffset="0" yoffset="5" xadvance="9"/><char id="175" x="58" y="125" width="5" height="1" xoffset="0" yoffset="4" xadvance="6"/><char id="176" x="40" y="41" width="3" height="3" xoffset="0" yoffset="4" xadvance="4"/><char id="177" x="40" y="45" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/><char id="178" x="42" y="52" width="4" height="5" xoffset="0" yoffset="4" xadvance="5"/><char id="179" x="42" y="58" width="4" height="5" xoffset="0" yoffset="4" xadvance="5"/><char id="180" x="21" y="25" width="2" height="2" xoffset="0" yoffset="4" xadvance="3"/><char id="181" x="44" y="64" width="6" height="9" xoffset="0" yoffset="7" xadvance="7"/><char id="182" x="48" y="74" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/><char id="183" x="50" y="121" width="1" height="1" xoffset="1" yoffset="8" xadvance="4"/><char id="184" x="45" y="74" width="2" height="3" xoffset="0" yoffset="13" xadvance="3"/><char id="185" x="52" y="84" width="2" height="5" xoffset="0" yoffset="4" xadvance="3"/><char id="186" x="53" y="90" width="5" height="8" xoffset="0" yoffset="5" xadvance="6"/><char id="187" x="44" y="41" width="5" height="3" xoffset="0" yoffset="7" xadvance="6"/><char id="188" x="46" y="30" width="10" height="10" xoffset="0" yoffset="3" xadvance="11"/><char id="189" x="55" y="99" width="11" height="10" xoffset="0" yoffset="3" xadvance="12"/><char id="190" x="56" y="110" width="12" height="10" xoffset="0" yoffset="3" xadvance="13"/><char id="192" x="47" y="45" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="193" x="51" y="58" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="194" x="54" y="41" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="195" x="56" y="71" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="196" x="59" y="84" width="6" height="11" xoffset="0" yoffset="2" xadvance="7"/><char id="197" x="58" y="54" width="6" height="13" xoffset="0" yoffset="0" xadvance="7"/><char id="198" x="53" y="20" width="9" height="9" xoffset="0" yoffset="4" xadvance="10"/><char id="199" x="63" y="68" width="6" height="11" xoffset="0" yoffset="4" xadvance="7"/><char id="200" x="66" y="80" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="201" x="67" y="93" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="202" x="59" y="1" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="203" x="69" y="106" width="6" height="11" xoffset="0" yoffset="2" xadvance="7"/><char id="204" x="61" y="30" width="2" height="12" xoffset="0" yoffset="1" xadvance="3"/><char id="205" x="63" y="14" width="2" height="12" xoffset="0" yoffset="1" xadvance="2"/><char id="206" x="64" y="27" width="3" height="12" xoffset="0" yoffset="1" xadvance="3"/><char id="207" x="64" y="40" width="3" height="11" xoffset="0" yoffset="2" xadvance="3"/><char id="208" x="69" y="118" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/><char id="209" x="65" y="52" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="210" x="70" y="65" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="211" x="73" y="78" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="212" x="74" y="91" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="213" x="76" y="104" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="214" x="66" y="1" width="6" height="11" xoffset="0" yoffset="2" xadvance="7"/><char id="215" x="55" y="14" width="5" height="5" xoffset="0" yoffset="6" xadvance="6"/><char id="216" x="77" y="117" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/><char id="217" x="66" y="13" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="218" x="68" y="26" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="219" x="68" y="39" width="6" height="12" xoffset="0" yoffset="1" xadvance="7"/><char id="220" x="72" y="52" width="6" height="11" xoffset="0" yoffset="2" xadvance="7"/><char id="221" x="77" y="64" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/><char id="222" x="80" y="77" width="5" height="12" xoffset="0" yoffset="4" xadvance="6"/><char id="223" x="81" y="90" width="5" height="10" xoffset="0" yoffset="4" xadvance="6"/><char id="224" x="83" y="101" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="225" x="84" y="111" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="226" x="73" y="1" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="227" x="73" y="11" width="6" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="228" x="79" y="1" width="5" height="8" xoffset="0" yoffset="5" xadvance="6"/><char id="229" x="75" y="21" width="5" height="10" xoffset="0" yoffset="3" xadvance="6"/><char id="230" x="84" y="121" width="9" height="6" xoffset="0" yoffset="7" xadvance="10"/><char id="231" x="80" y="10" width="5" height="8" xoffset="0" yoffset="7" xadvance="6"/><char id="232" x="75" y="32" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="233" x="75" y="42" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="234" x="79" y="52" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="235" x="85" y="1" width="5" height="8" xoffset="0" yoffset="5" xadvance="6"/><char id="236" x="61" y="43" width="2" height="9" xoffset="-1" yoffset="4" xadvance="2"/><char id="237" x="57" y="30" width="2" height="9" xoffset="0" yoffset="4" xadvance="2"/><char id="238" x="81" y="19" width="3" height="9" xoffset="-1" yoffset="4" xadvance="2"/><char id="239" x="81" y="29" width="3" height="8" xoffset="-1" yoffset="5" xadvance="2"/><char id="240" x="81" y="38" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="241" x="85" y="19" width="6" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="242" x="91" y="1" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="243" x="85" y="48" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="244" x="87" y="29" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="245" x="92" y="11" width="6" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="246" x="87" y="39" width="5" height="8" xoffset="0" yoffset="5" xadvance="6"/><char id="247" x="86" y="11" width="5" height="5" xoffset="0" yoffset="6" xadvance="6"/><char id="248" x="92" y="21" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/><char id="249" x="97" y="1" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="250" x="85" y="58" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="251" x="86" y="68" width="5" height="9" xoffset="0" yoffset="4" xadvance="6"/><char id="252" x="86" y="78" width="5" height="8" xoffset="0" yoffset="5" xadvance="6"/><char id="253" x="87" y="87" width="5" height="11" xoffset="0" yoffset="4" xadvance="6"/><char id="254" x="89" y="99" width="5" height="8" xoffset="0" yoffset="7" xadvance="6"/><char id="255" x="90" y="108" width="5" height="11" xoffset="0" yoffset="5" xadvance="6"/><char id="8260" x="91" y="48" width="5" height="10" xoffset="0" yoffset="3" xadvance="6"/><char id="937" x="91" y="59" width="5" height="8" xoffset="0" yoffset="5" xadvance="6"/><char id="13" x="92" y="68" width="3" height="12" xoffset="1" yoffset="1" xadvance="4"/><char id="32" x="0" y="0" width="0" height="0" xoffset="1" yoffset="1" xadvance="4"/></chars></font>;

	public static function get texture():Texture {
		var bitmapData:BitmapData = getBitmapData();
		var texture:Texture = Texture.fromBitmapData( bitmapData, false );
		bitmapData.dispose();
		bitmapData = null;

		texture.root.onRestore = function ():void {
			bitmapData = getBitmapData();
			texture.root.uploadBitmapData( bitmapData );
			bitmapData.dispose();
			bitmapData = null;
		};

		return texture;
	}

	private static function getBitmapData():BitmapData {
		var bmpData:BitmapData = new BitmapData( BITMAP_WIDTH, BITMAP_HEIGHT );
		var bmpBytes:ByteArray = new ByteArray();
		var numBytes:int = BITMAP_DATA.length;

		for ( var i:int = 0; i < numBytes; ++i )
			bmpBytes.writeUnsignedInt( BITMAP_DATA[i] );

		bmpBytes.uncompress();
		bmpData.setPixels( new Rectangle( 0, 0, BITMAP_WIDTH, BITMAP_HEIGHT ), bmpBytes );
		bmpBytes.clear();
		trace( "adding std95!" );
		return bmpData;
	}

	public static function register():void {
		var bf:BitmapFont = new BitmapFont( texture, xml );
		bf.smoothing = TextureSmoothing.NONE;
		TextField.registerBitmapFont( bf, "std95" );
	}

	public static function get xml():XML { return XML_DATA; }
}
}