// =================================================================================================
//
//	Starling Framework
//	Copyright Gamua GmbH. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================

package grar.starling.fonts.pixelfonts {

import flash.display.BitmapData;
import flash.geom.Rectangle;
import flash.utils.ByteArray;

import starling.text.BitmapFont;
import starling.text.TextField;
import starling.textures.Texture;
import starling.textures.TextureSmoothing;

public class TobiBitmapFont {
	private static const BITMAP_WIDTH:int = 128;
	private static const BITMAP_HEIGHT:int = 128;
	private static const BITMAP_DATA:Array = [
		2027613533,
		1266074672,
		144440159,
		3135108853,
		1293087745,
		1675816154,
		1764198757,
		1086855295,
		1055341271,
		3118365949,
		4240047871,
		4276807583,
		794293307,
		3221044850,
		3216762815,
		2075125784,
		1560145646,
		4224839492,
		3994098019,
		3858362110,
		3733308885,
		4288770957,
		426206460,
		2574348940,
		4220722492,
		3712720251,
		3756447487,
		2071380286,
		987631967,
		3338191658,
		1602025244,
		2320451270,
		4211768118,
		1446018774,
		1039127672,
		395932584,
		3718369165,
		4161693356,
		4267893018,
		4289163617,
		1658819549,
		1199331318,
		1731133416,
		2352697150,
		3207257643,
		2973626940,
		3405473121,
		4293170095,
		2656952768,
		3464013048,
		1572319098,
		2995611740,
		2096811137,
		1033606004,
		3332279715,
		3470576065,
		2545083325,
		3259178456,
		3019291571,
		2521943527,
		3629916818,
		3676960655,
		3465399992,
		1335399211,
		3733833987,
		300756674,
		3655201207,
		729732559,
		3918018858,
		3211030898,
		3900337308,
		1668436135,
		1713165671,
		850497384,
		756668579,
		1556324727,
		2867934467,
		536272220,
		3126731855,
		2012357668,
		1324093687,
		862795739,
		3878410845,
		3590166876,
		3668308258,
		4289190464,
		1444851396,
		3431634974,
		3852447463,
		1994883865,
		4082692601,
		367256768,
		1047154461,
		2976329902,
		3766102543,
		1148431976,
		3171375863,
		3786443020,
		488069499,
		1520504112,
		4013638955,
		1504310189,
		2886693113,
		3654170799,
		1519902557,
		863302427,
		931616436,
		3306091440,
		871778876,
		1488803550,
		167497402,
		2206438247,
		3917990451,
		2696839885,
		534574514,
		2876516260,
		640676409,
		844571385,
		2177538012,
		407305831,
		306671867,
		2083792721,
		3102272154,
		3682827677,
		3479665803,
		2991023463,
		3965540999,
		733442775,
		4268076649,
		704556083,
		4150488268,
		1633119374,
		2273238875,
		2974280670,
		2005564904,
		513707194,
		3697111740,
		170766218,
		4146232116,
		867556893,
		958122635,
		3572979442,
		1232448735,
		2800198172,
		3602476950,
		3848597243,
		3204010743,
		2838157960,
		2737704645,
		2107833702,
		1916638271,
		1742255811,
		2929966316,
		3676196436,
		2274899503,
		3001917149,
		2750138929,
		1555382639,
		3522072760,
		3421926000,
		1879594111,
		1408649549,
		2968366239,
		732977142,
		1328610034,
		1151213489,
		588772910,
		2370415087,
		1207596108,
		3231623517,
		1412799980,
		2976101501,
		4125613549,
		1056136619,
		3348027771,
		3254157435,
		469126765,
		3487140719,
		1440625449,
		3211227678,
		3601806315,
		659017707,
		3845857884,
		1508242538,
		3754012405,
		1033089351,
		1915254301,
		2173250920,
		239055110,
		3952011195,
		3963493059,
		870562226,
		3862843867,
		2304936702,
		490352554,
		2977420542,
		518796699,
		1926585670,
		4112070382,
		1335498568,
		222204455,
		1005344233,
		3474038191,
		3363553121,
		1877640311,
		3453511544,
		2959066921,
		3655816802,
		3418463887,
		4293590833,
		1050646923,
		1789696904,
		1037779880,
		2184398435,
		4275136890,
		3327362943,
		2112527231,
		2963244029,
		3405697939,
		3950402813,
		2497507646,
		2884571890,
		3648277839,
		1033350378,
		991255759,
		3575380940,
		4112164247,
		3677642127,
		3318325755,
		825876175,
		3310722463,
		3198768819,
		2586032878,
		3287451831,
		1757042023,
		305690045,
		150399673,
		2387688345,
		385858553,
		1181036424,
		66816497,
		1580851108,
		175896350,
		544669170,
		1547499150,
		4088471031,
		995577404,
		1165332490,
		3197749541,
		2800778452,
		380875215,
		518252923,
		3994827031,
		3679686105,
		3152931445,
		2866474996,
		1587130035,
		3432362965,
		2729827212,
		1869738934,
		1232690805,
		1597630462,
		715493086,
		2773204497,
		4222248816,
		3308033926,
		1940967309,
		1026485973,
		4191985309,
		2957496261,
		3369782207,
		3660008678,
		4190640463,
		2885135173,
		3049096189,
		3669758301,
		1936029729,
		2066130259,
		3616402957,
		2040751744,
		4162820030,
		1528143735,
		3959939252,
		400997661,
		3125843710,
		3278521683,
		2975300670,
		842434701,
		591246233,
		965142653,
		1006369676,
		862532272,
		2947571574,
		3203170365,
		2747612150,
		3346448446,
		1139531053,
		4014438946,
		1472108539,
		1592270590,
		938763600,
		3092686383,
		3395060617,
		3333714742,
		886014428,
		3884416411,
		333047740,
		2876486387,
		2033831161,
		1732919261,
		2290041186,
		2880102824,
		971296504,
		1472554871,
		1327932599,
		2328620607,
		3884107641,
		3585651051,
		4100513202,
		1127999944,
		4145397109,
		4126897723,
		4286318572,
		3522837510,
		2068220739,
		3220136159,
		4086897005,
		205376292,
		2208015351,
		4019109242,
		3593599611,
		2778686326,
		1819407254,
		502571082,
		3965580208,
		3422088878,
		4151683147,
		2348075351,
		1963292174,
		1303305709,
		4039937653,
		1140206655,
		1455660192,
		4149583733,
		518355656,
		3194513181,
		2829316052,
		246981558,
		2179781955,
		4079973484,
		3648224604,
		3890016028,
		3817944444,
		1188000485,
		1565433743,
		4060403239,
		1288239065,
		1743626995,
		3134713523,
		3804894444,
		2570925777,
		3174665785,
		3093486651,
		1636814447,
		2260941434,
		1531394434,
		1526685677,
		1798988463,
		3769146977,
		4137948065,
		4090286555,
		2347610587,
		3119426402,
		1849342430,
		3405437033,
		1031173947,
		4151683034,
		3902616700,
		352021150,
		3469113863,
		4016204663,
		1000325529,
		2302520045,
		2959833228,
		3710645209,
		2579492696,
		3619876041,
		1943324333,
		4076456537,
		1916495995,
		3464086488,
		968609555,
		3857698334,
		994770818,
		4000547993,
		3541690274,
		3100100745,
		2947186548,
		3302930734,
		4005748421,
		2677619325,
		1738507790,
		3638553967,
		3918151050,
		2951478365,
		2804774337,
		4268399832,
		1512638203,
		1757175789,
		3547773279,
		100046198,
		2414745486,
		4110191334,
		3918053187,
		3328812478,
		467271572,
		2713563676,
		1256841113,
		828288572,
		2868615842,
		1512004053,
		2540909943,
		1115477738,
		3672247532,
		2872350472,
		258046887,
		678129811,
		3486937534,
		467388293,
		3044792282,
		3676601711,
		100052728,
		3980059595,
		1437305714,
		3407199839,
		959138273,
		3758923591,
		1969100455,
		176009155,
		3132334767,
		955968894,
		3947865556,
		154254555,
		2234475203,
		3719382968,
		1293220795,
		3693974119,
		3132122933,
		223858203,
		3655325845,
		3887331628,
		2210251279,
		2733342361,
		4244400526,
		1305276752,
		2350407657,
		3940281538,
		3198324409,
		2196449789,
		2841385612,
		2744613205,
		2621677221,
		2749185012,
		1305959103,
		4204538858,
		164756737,
		2876488853,
		3635141326,
		779956087,
		3547763738,
		2853103526,
		1960889875,
		1975324399,
		350088155,
		3381018343,
		987216334,
		3051420165,
		1158353441
	];

	private static const XML_DATA:XML = <font>
		<info face="tobi" size="16" bold="0" italic="0" charset="" unicode="" stretchH="100" smooth="1" aa="1"
			  padding="2,2,2,2" spacing="0,0" outline="0"/>
		<common lineHeight="15" base="10" scaleW="128" scaleH="128" pages="1" packed="0"/>
		<chars count="184">
			<char id="97" x="2" y="2" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="98" x="2" y="10" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="99" x="2" y="20" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="100" x="2" y="28" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="101" x="2" y="38" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="102" x="2" y="46" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="103" x="2" y="56" width="5" height="8" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="104" x="2" y="66" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="105" x="2" y="76" width="3" height="8" xoffset="2" yoffset="1" xadvance="7"/>
			<char id="106" x="2" y="86" width="4" height="10" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="107" x="7" y="76" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="108" x="2" y="98" width="3" height="8" xoffset="2" yoffset="1" xadvance="7"/>
			<char id="109" x="2" y="108" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="110" x="7" y="98" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="111" x="8" y="86" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="112" x="2" y="116" width="5" height="8" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="113" x="9" y="106" width="5" height="8" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="114" x="14" y="94" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="115" x="9" y="116" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="116" x="16" y="102" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="117" x="16" y="112" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="118" x="16" y="120" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="119" x="9" y="2" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="120" x="9" y="10" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="121" x="9" y="18" width="5" height="8" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="122" x="9" y="28" width="5" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="65" x="9" y="36" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="66" x="9" y="46" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="67" x="9" y="56" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="68" x="9" y="66" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="69" x="14" y="76" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="70" x="16" y="2" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="71" x="16" y="12" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="72" x="16" y="22" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="73" x="16" y="32" width="3" height="8" xoffset="2" yoffset="1" xadvance="7"/>
			<char id="74" x="16" y="42" width="4" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="75" x="21" y="32" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="76" x="16" y="52" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="77" x="22" y="42" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="78" x="16" y="62" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="79" x="23" y="2" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="80" x="23" y="12" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="81" x="28" y="22" width="5" height="10" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="82" x="30" y="2" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="83" x="30" y="12" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="84" x="21" y="72" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="85" x="23" y="52" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="86" x="23" y="62" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="87" x="29" y="34" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="88" x="35" y="22" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="89" x="37" y="2" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="90" x="37" y="12" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="33" x="21" y="82" width="2" height="9" xoffset="3" yoffset="1" xadvance="7"/>
			<char id="59" x="23" y="22" width="3" height="7" xoffset="2" yoffset="4" xadvance="7"/>
			<char id="37" x="23" y="93" width="6" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="58" x="15" y="86" width="2" height="5" xoffset="3" yoffset="3" xadvance="7"/>
			<char id="63" x="25" y="82" width="5" height="9" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="42" x="29" y="44" width="5" height="5" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="40" x="23" y="103" width="3" height="11" xoffset="2" yoffset="0" xadvance="7"/>
			<char id="41" x="28" y="103" width="3" height="11" xoffset="2" yoffset="0" xadvance="7"/>
			<char id="95" x="9" y="124" width="5" height="1" xoffset="1" yoffset="8" xadvance="7"/>
			<char id="43" x="31" y="93" width="5" height="5" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="45" x="31" y="100" width="5" height="1" xoffset="1" yoffset="5" xadvance="7"/>
			<char id="61" x="28" y="72" width="5" height="3" xoffset="1" yoffset="4" xadvance="7"/>
			<char id="46" x="16" y="72" width="2" height="2" xoffset="3" yoffset="7" xadvance="7"/>
			<char id="44" x="23" y="116" width="3" height="4" xoffset="2" yoffset="7" xadvance="7"/>
			<char id="47" x="28" y="116" width="4" height="8" xoffset="2" yoffset="1" xadvance="7"/>
			<char id="124" x="33" y="103" width="1" height="9" xoffset="3" yoffset="1" xadvance="7"/>
			<char id="34" x="28" y="77" width="5" height="3" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="39" x="23" y="122" width="2" height="3" xoffset="3" yoffset="0" xadvance="7"/>
			<char id="64" x="34" y="114" width="6" height="10" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="35" x="36" y="103" width="6" height="9" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="36" x="42" y="114" width="5" height="10" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="94" x="32" y="82" width="5" height="3" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="38" x="38" y="87" width="6" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="123" x="44" y="97" width="5" height="11" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="125" x="49" y="110" width="5" height="11" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="91" x="30" y="51" width="3" height="11" xoffset="2" yoffset="0" xadvance="7"/>
			<char id="93" x="35" y="51" width="3" height="11" xoffset="2" yoffset="0" xadvance="7"/>
			<char id="8221" x="49" y="123" width="5" height="3" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="8217" x="32" y="87" width="2" height="3" xoffset="3" yoffset="0" xadvance="7"/>
			<char id="48" x="35" y="64" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="49" x="39" y="74" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="50" x="36" y="32" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="51" x="42" y="22" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="52" x="44" y="2" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="53" x="44" y="12" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="54" x="40" y="42" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="55" x="43" y="32" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="56" x="49" y="22" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="57" x="40" y="52" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="60" x="42" y="62" width="4" height="7" xoffset="2" yoffset="2" xadvance="7"/>
			<char id="62" x="51" y="2" width="4" height="7" xoffset="2" yoffset="2" xadvance="7"/>
			<char id="92" x="51" y="11" width="4" height="8" xoffset="2" yoffset="1" xadvance="7"/>
			<char id="96" x="38" y="97" width="4" height="3" xoffset="2" yoffset="0" xadvance="7"/>
			<char id="126" x="47" y="42" width="6" height="4" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="191" x="47" y="48" width="5" height="9" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="161" x="46" y="71" width="1" height="9" xoffset="3" yoffset="0" xadvance="7"/>
			<char id="710" x="44" y="110" width="3" height="2" xoffset="2" yoffset="1" xadvance="7"/>
			<char id="732" x="8" y="94" width="4" height="2" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="8211" x="30" y="64" width="3" height="1" xoffset="2" yoffset="5" xadvance="7"/>
			<char id="8212" x="47" y="59" width="6" height="1" xoffset="1" yoffset="5" xadvance="7"/>
			<char id="8216" x="30" y="67" width="2" height="3" xoffset="3" yoffset="0" xadvance="7"/>
			<char id="8218" x="35" y="74" width="2" height="3" xoffset="3" yoffset="7" xadvance="7"/>
			<char id="8220" x="48" y="62" width="5" height="3" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="8222" x="50" y="32" width="5" height="3" xoffset="1" yoffset="7" xadvance="7"/>
			<char id="8226" x="56" y="21" width="4" height="4" xoffset="2" yoffset="3" xadvance="7"/>
			<char id="8230" x="39" y="84" width="5" height="1" xoffset="1" yoffset="8" xadvance="7"/>
			<char id="8240" x="54" y="48" width="6" height="7" xoffset="1" yoffset="2" xadvance="7"/>
			<char id="8249" x="55" y="37" width="3" height="5" xoffset="2" yoffset="3" xadvance="7"/>
			<char id="8250" x="57" y="27" width="3" height="5" xoffset="2" yoffset="3" xadvance="7"/>
			<char id="8364" x="60" y="34" width="6" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="8482" x="46" y="82" width="6" height="3" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="183" x="36" y="42" width="2" height="2" xoffset="3" yoffset="5" xadvance="7"/>
			<char id="184" x="36" y="46" width="2" height="3" xoffset="3" yoffset="7" xadvance="7"/>
			<char id="185" x="46" y="87" width="3" height="5" xoffset="2" yoffset="0" xadvance="7"/>
			<char id="186" x="49" y="67" width="3" height="4" xoffset="2" yoffset="0" xadvance="7"/>
			<char id="187" x="49" y="73" width="6" height="5" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="188" x="55" y="57" width="6" height="9" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="189" x="62" y="44" width="6" height="9" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="190" x="57" y="2" width="6" height="9" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="192" x="62" y="13" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="193" x="65" y="2" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="194" x="68" y="24" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="195" x="69" y="13" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="196" x="72" y="2" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="197" x="51" y="87" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="198" x="51" y="98" width="6" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="199" x="57" y="68" width="5" height="10" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="200" x="63" y="55" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="201" x="58" y="80" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="202" x="64" y="66" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="203" x="56" y="108" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="204" x="59" y="91" width="3" height="9" xoffset="2" yoffset="0" xadvance="7"/>
			<char id="205" x="63" y="102" width="3" height="9" xoffset="2" yoffset="0" xadvance="7"/>
			<char id="206" x="64" y="91" width="3" height="9" xoffset="2" yoffset="0" xadvance="7"/>
			<char id="207" x="65" y="77" width="3" height="9" xoffset="2" yoffset="0" xadvance="7"/>
			<char id="209" x="63" y="113" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="210" x="68" y="102" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="211" x="69" y="88" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="212" x="70" y="77" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="216" x="68" y="35" width="7" height="7" xoffset="0" yoffset="2" xadvance="7"/>
			<char id="217" x="75" y="24" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="218" x="76" y="13" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="219" x="79" y="2" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="220" x="70" y="113" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="224" x="75" y="99" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="225" x="76" y="88" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="226" x="77" y="110" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="227" x="82" y="99" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="228" x="84" y="110" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="229" x="70" y="44" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="231" x="62" y="24" width="4" height="8" xoffset="2" yoffset="3" xadvance="7"/>
			<char id="232" x="70" y="55" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="233" x="71" y="66" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="234" x="77" y="77" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="235" x="83" y="88" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="236" x="89" y="98" width="3" height="8" xoffset="2" yoffset="1" xadvance="7"/>
			<char id="237" x="91" y="108" width="3" height="8" xoffset="2" yoffset="1" xadvance="7"/>
			<char id="238" x="91" y="118" width="3" height="8" xoffset="2" yoffset="1" xadvance="7"/>
			<char id="239" x="56" y="119" width="3" height="7" xoffset="2" yoffset="2" xadvance="7"/>
			<char id="240" x="77" y="35" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="241" x="82" y="24" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="242" x="83" y="13" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="243" x="86" y="2" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="244" x="77" y="45" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="245" x="77" y="55" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="246" x="78" y="65" width="5" height="7" xoffset="1" yoffset="2" xadvance="7"/>
			<char id="247" x="84" y="120" width="5" height="5" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="248" x="84" y="74" width="6" height="6" xoffset="1" yoffset="2" xadvance="7"/>
			<char id="249" x="90" y="82" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="250" x="94" y="92" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="253" x="96" y="102" width="5" height="11" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="338" x="96" y="115" width="6" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="339" x="104" y="2" width="6" height="6" xoffset="1" yoffset="3" xadvance="7"/>
			<char id="352" x="97" y="2" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="353" x="90" y="12" width="4" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="376" x="112" y="2" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="381" x="104" y="10" width="5" height="9" xoffset="1" yoffset="0" xadvance="7"/>
			<char id="382" x="97" y="13" width="3" height="8" xoffset="2" yoffset="1" xadvance="7"/>
			<char id="402" x="90" y="22" width="4" height="10" xoffset="2" yoffset="1" xadvance="7"/>
			<char id="8224" x="119" y="2" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="8225" x="119" y="12" width="5" height="8" xoffset="1" yoffset="1" xadvance="7"/>
			<char id="32" x="0" y="0" width="0" height="0" xoffset="0" yoffset="9" xadvance="7"/>
		</chars>
	</font>;

	public static function get texture():Texture {
		var bitmapData:BitmapData = getBitmapData();
		var texture:Texture = Texture.fromBitmapData( bitmapData, false );
		bitmapData.dispose();
		bitmapData = null;

		texture.root.onRestore = function ():void {
			bitmapData = getBitmapData();
			texture.root.uploadBitmapData( bitmapData );
			bitmapData.dispose();
			bitmapData = null;
		};

		return texture;
	}

	private static function getBitmapData():BitmapData {
		var bmpData:BitmapData = new BitmapData( BITMAP_WIDTH, BITMAP_HEIGHT );
		var bmpBytes:ByteArray = new ByteArray();
		var numBytes:int = BITMAP_DATA.length;

		for ( var i:int = 0; i < numBytes; ++i )
			bmpBytes.writeUnsignedInt( BITMAP_DATA[i] );

		bmpBytes.uncompress();
		bmpData.setPixels( new Rectangle( 0, 0, BITMAP_WIDTH - 1, BITMAP_HEIGHT ), bmpBytes );
		bmpBytes.clear();
		trace( "adding tobi!" );
		return bmpData;
	}

	public static function register():void {
		var bf:BitmapFont = new BitmapFont( texture, xml );
		bf.smoothing = TextureSmoothing.NONE;
		TextField.registerBitmapFont( bf, "tobi" );
	}

	public static function get xml():XML { return XML_DATA; }
}
}