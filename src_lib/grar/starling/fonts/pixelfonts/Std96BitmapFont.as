// =================================================================================================
//
//	Starling Framework
//	Copyright Gamua GmbH. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================

package grar.starling.fonts.pixelfonts {

import flash.display.BitmapData;
import flash.geom.Rectangle;
import flash.utils.ByteArray;

import starling.text.BitmapFont;
import starling.text.TextField;
import starling.textures.Texture;
import starling.textures.TextureSmoothing;

public class Std96BitmapFont {

	// 1px on the right is lost.
	public static const BITMAP_WIDTH:int = 126;
	public static const BITMAP_HEIGHT:int = 128;
	public static const BITMAP_DATA:Array = [
		2027613596, 1634655050, 277052403, 518872492, 1092547235, 1079040210, 2432481498, 1477857332,
		2389465434, 4000474824, 3657235189, 1055816828, 1043906303, 4276068263, 2678037759, 1010339534,
		4294703561, 3955880744, 4147609007, 861300727, 4125923911, 2671475454, 83342187, 1288654822,
		3904104309, 4017746681, 3011212702, 3583609796, 4121415494, 3795762606, 4200458941, 2762953692,
		2413226282, 2393340065, 2306591261, 333338086, 1483898727, 1907737887, 164570987, 1821514536,
		2090060827, 928927710, 2065078108, 4003819607, 4080678767, 1272331029, 1674721605, 2918704254,
		1239192860, 1243451156, 2012593347, 1265544656, 3471529714, 3161673514, 3999595775, 2313944334,
		3092476224, 2308936497, 1232105429, 4022067454, 1056362707, 2595793845, 2361060872, 223077771,
		2223494394, 2614253142, 3818833483, 1165436142, 1569265102, 1514824495, 2904543851, 643383214,
		3326634285, 2764458096, 4031745569, 3022960411, 3998457128, 3444039019, 615879147, 3109916155,
		3841856206, 1305455486, 303154024, 1315694197, 2883812330, 423065738, 3332231218, 2809814315,
		246870970, 1425792261, 2007352912, 3800623689, 1039884169, 592961694, 1887894364, 2876319695,
		1295868869, 2208492407, 3344934258, 2934226522, 1939526379, 647774932, 3551833813, 3603584744,
		784064750, 2901903767, 3718906837, 4126072910, 2068997231, 147945329, 350198645, 1431975723,
		2037250572, 2383866984, 3176049746, 2051349199, 2540194020, 977074766, 1391946439, 2995136057,
		2903967837, 3909803215, 3606848614, 943326949, 663810988, 3580077034, 632170302, 2374568686,
		694573026, 2663793930, 4156961718, 2034167153, 3754787689, 3785279002, 1649298902, 3247760277,
		3670068607, 2796659841, 165012379, 4166462940, 1567442885, 2364544137, 4277519503, 2747167570,
		3218291114, 2458325754, 1295248977, 4233033035, 1592880301, 4168787326, 870664951, 1552071642,
		1809769476, 2195135903, 1670737623, 4132838072, 661881828, 1671087890, 1567246300, 2841135352,
		1368279028, 742366200, 177483703, 737029814, 2676212509, 526900993, 3508487803, 3837518766,
		3763042942, 1251834422, 1728052729, 4092062559, 2854434298, 3164274316, 2077152663, 465087420,
		3824425509, 3953105103, 1005220874, 3071442284, 2840522234, 3016997295, 3893003310, 2583358382,
		2537681315, 981981803, 2870337309, 2100283773, 3384440069, 3478370490, 2097696351, 540659246,
		1597268867, 2589167706, 3672784516, 737032601, 3484039136, 3322354267, 586634611, 931826586,
		1518058318, 1799126874, 322566522, 848702331, 2082208404, 2741479609, 1422526948, 2049407951,
		157059582, 3727373823, 878664833, 3145949627, 1052641911, 2808521210, 564542868, 2759213966,
		3286226831, 1054068263, 3376055754, 3949473643, 2459675325, 2479758858, 3340717291, 512402263,
		3609765110, 3582634762, 3998420554, 1966360531, 2717607209, 1874692539, 2597835761, 4258921576,
		4042730109, 3180451750, 1014973773, 1020617303, 2910308203, 2759144437, 2595553202, 1107532774,
		2475593440, 3806393588, 612237649, 2668747439, 2747470714, 3715403588, 1536139719, 2971494237,
		534181349, 451131298, 1050884390, 3585165677, 2626479497, 1715727046, 2393877454, 3352754359,
		2023682211, 2302187436, 160825129, 1319631537, 2629166550, 2133454509, 1020362174, 3215966199,
		3766353849, 487037493, 1598242665, 2051394039, 885444821, 3818837351, 2371663413, 1201009000,
		3590547258, 2345904121, 2284841048, 1341417426, 2104755215, 3097106641, 662460023, 501302415,
		903948596, 4218868442, 1353304410, 965714603, 1052497571, 4121799799, 89589623, 2858407821,
		1795854264, 3046417291, 3137180879, 1887422195, 1772084004, 3727129424, 683636037, 776069182,
		2311562247, 2710441309, 3746790772, 2228196846, 2505143745, 2738523816, 1507023092, 3401110523,
		307367875, 3186017431, 1944255445, 2657754103, 2754582516, 2123326172, 616960885, 375258028,
		3328046433, 1402873656, 4063028923, 3093255663, 3130675866, 3608271947, 2901923031, 1995084438,
		125031900, 1220315556, 2119717247, 645480161, 3341139509, 3852842199, 1767057264, 4259951742,
		1809296685, 1567499591, 620648907, 3344217308, 3571152821, 1102403454, 2502334452, 1101574433,
		1501261375, 4229035700, 395504191, 3546785003, 3049545559, 2081376827, 2559372249, 2591027699,
		736210668, 2671432383, 1396526565, 3517166926, 3425782519, 603679319, 2901879732, 2350201162,
		3672064480, 1325116759, 3823007999, 876049267, 1394243367, 2674583922, 1240286305, 1223990034,
		4052532391, 4129200021, 2584714639, 2473972810, 3700027759, 1756277167, 2951645016, 2775421647,
		1221911117, 2110248378, 2473272893, 1959058522, 2516315304, 2846882526, 2843686353, 1034452690,
		2311055084, 3919739284, 3827873206, 2813217250, 2666833821, 4184561581, 970301289, 3772660887,
		3762182651, 3555827772, 3013474556, 2507243430, 2544613859, 2503882929, 1600550814, 2012237026,
		3086208174, 238058815, 4112376935, 162489146, 2539392428, 2243682991, 884272934, 3207076486,
		847205213, 1678192189, 3404569982, 1329984339, 4127350073, 3058331378, 4259108702, 3772718879,
		529528324, 662546030, 4248743397, 454707899, 2952678013, 4125061983, 2558525375, 2247863004,
		2027592673, 1788165090, 4009242372, 3997687621, 3925182935, 2749771566, 939369603, 3271420023,
		156140583, 2599866794, 4059908766, 661739509, 1328881517, 3769554136, 1421859854, 4051559802,
		2682668142, 3199202427, 4007678431, 167195533, 1171063247, 4210498780, 2837118861, 2077635563,
		2054549016, 3539644442, 3756951227, 3151366444, 2498444025, 1755500525, 2331727322, 2458044241,
		532091771, 1419637671, 3597994734, 2489756868, 2089105358, 3867388875, 3103709034, 759904654,
		1960453751, 934863825, 4202575399, 311392823, 1602168111, 1394257363, 3547981537, 679287876,
		2123395803, 3715125098, 2915696296, 1236262420, 4151635423, 32974014, 2813967506, 3617944951,
		1526710050, 4000238759, 3025218062, 2818241307, 2007889785, 2783876558, 2405707749, 3996661746,
		4178952011, 3795049935, 4078154063, 4196176632, 1049472941, 3667516951, 4248753908, 1326709305,
		3855334418, 778657724, 320850429, 3703865034, 1563818054, 2088203247, 874456171, 1064783853,
		2641926314, 3117348583, 278716376, 328844970, 1472784726, 3780010705, 3169233908, 1514062557,
		2582374846, 57138618, 510565051, 3206729555, 988518846, 2599726758, 4114343343, 3193593415,
		3830090143, 1164542542, 222752417, 3108191919, 3289080509, 661780953, 3183115238, 1583175505,
		1651383287, 1775017676, 465931129, 903641033, 2779767862, 2060442703, 3454884533, 1453559661,
		2864008690, 2369514649, 1407661761, 2274653846, 2051074421, 3994214185, 764407864, 1239283085,
		4223580239, 3954046718, 2503470653, 3692277184, 3990339051, 2972970644, 1453909684, 2395405254,
		2646736266, 3215975144, 2423249971, 3700670321, 2427208167, 4013604058, 2591028425, 691845787,
		1178457462, 4020818321, 1479212716, 2720901247, 4293414460, 2146785876, 1027408631, 1005024751,
		3816736767, 1471374851
	];

	private static const XML_DATA:XML = <font>
		<info face="st_965" size="8" bold="0" italic="0" charset="" unicode="" stretchH="100" smooth="1" aa="1"
			  padding="1,1,1,1" spacing="0,0" outline="0"/>
		<common lineHeight="15" base="12" scaleW="128" scaleH="128" pages="1" packed="0"/>
		<chars count="210">
			<char id="97" x="1" y="1" width="6" height="6" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="98" x="1" y="8" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="99" x="1" y="18" width="6" height="6" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="100" x="1" y="25" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="101" x="1" y="35" width="6" height="6" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="102" x="1" y="42" width="4" height="9" xoffset="0" yoffset="4" xadvance="5"/>
			<char id="103" x="1" y="52" width="6" height="9" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="104" x="6" y="42" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="105" x="8" y="1" width="2" height="9" xoffset="0" yoffset="4" xadvance="3"/>
			<char id="106" x="8" y="11" width="3" height="12" xoffset="0" yoffset="4" xadvance="4"/>
			<char id="107" x="11" y="1" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="108" x="8" y="24" width="2" height="9" xoffset="0" yoffset="4" xadvance="3"/>
			<char id="109" x="8" y="34" width="10" height="6" xoffset="0" yoffset="7" xadvance="11"/>
			<char id="110" x="11" y="24" width="6" height="6" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="111" x="12" y="11" width="6" height="6" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="112" x="18" y="1" width="6" height="9" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="113" x="18" y="18" width="6" height="9" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="114" x="19" y="11" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/>
			<char id="115" x="1" y="62" width="5" height="6" xoffset="0" yoffset="7" xadvance="6"/>
			<char id="116" x="1" y="69" width="4" height="9" xoffset="0" yoffset="4" xadvance="5"/>
			<char id="117" x="1" y="79" width="6" height="6" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="118" x="6" y="69" width="6" height="6" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="119" x="7" y="62" width="10" height="6" xoffset="0" yoffset="7" xadvance="11"/>
			<char id="120" x="8" y="52" width="6" height="6" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="121" x="13" y="41" width="6" height="9" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="122" x="15" y="51" width="6" height="6" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="65" x="19" y="28" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="66" x="20" y="38" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="67" x="25" y="1" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="68" x="25" y="11" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="69" x="27" y="21" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="70" x="33" y="1" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="71" x="33" y="11" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="72" x="1" y="86" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="73" x="8" y="76" width="2" height="9" xoffset="0" yoffset="4" xadvance="3"/>
			<char id="74" x="1" y="96" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="75" x="1" y="106" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="76" x="8" y="96" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="77" x="9" y="86" width="9" height="9" xoffset="0" yoffset="4" xadvance="10"/>
			<char id="78" x="11" y="76" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="79" x="1" y="116" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="80" x="18" y="58" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="81" x="9" y="106" width="7" height="10" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="82" x="22" y="48" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="83" x="15" y="96" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="84" x="9" y="117" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="85" x="16" y="117" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="86" x="17" y="106" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="87" x="24" y="116" width="10" height="9" xoffset="0" yoffset="4" xadvance="11" page="0"
				  chnl="15"/>
			<char id="88" x="28" y="31" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="89" x="35" y="21" width="8" height="9" xoffset="0" yoffset="4" xadvance="9"/>
			<char id="90" x="41" y="1" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="48" x="41" y="11" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="49" x="48" y="1" width="3" height="9" xoffset="0" yoffset="4" xadvance="5"/>
			<char id="50" x="19" y="68" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="51" x="26" y="58" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="52" x="30" y="41" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="53" x="36" y="31" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="54" x="19" y="78" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="55" x="23" y="88" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="56" x="25" y="98" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="57" x="27" y="68" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="33" x="27" y="78" width="2" height="9" xoffset="1" yoffset="4" xadvance="5"/>
			<char id="8221" x="30" y="51" width="6" height="3" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="35" x="25" y="108" width="7" height="7" xoffset="0" yoffset="5" xadvance="9"/>
			<char id="36" x="34" y="55" width="7" height="11" xoffset="0" yoffset="3" xadvance="8"/>
			<char id="37" x="30" y="78" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="38" x="31" y="88" width="8" height="9" xoffset="0" yoffset="4" xadvance="9"/>
			<char id="8217" x="19" y="88" width="3" height="3" xoffset="0" yoffset="4" xadvance="4"/>
			<char id="40" x="38" y="41" width="4" height="11" xoffset="0" yoffset="3" xadvance="5"/>
			<char id="41" x="33" y="98" width="4" height="11" xoffset="0" yoffset="3" xadvance="5"/>
			<char id="42" x="12" y="18" width="5" height="5" xoffset="0" yoffset="6" xadvance="6"/>
			<char id="43" x="35" y="67" width="6" height="6" xoffset="0" yoffset="6" xadvance="7"/>
			<char id="44" x="19" y="92" width="3" height="3" xoffset="0" yoffset="12" xadvance="4"/>
			<char id="45" x="11" y="31" width="6" height="2" xoffset="0" yoffset="8" xadvance="7"/>
			<char id="46" x="15" y="58" width="2" height="2" xoffset="0" yoffset="11" xadvance="3"/>
			<char id="47" x="37" y="74" width="6" height="10" xoffset="0" yoffset="3" xadvance="7"/>
			<char id="58" x="33" y="110" width="2" height="5" xoffset="1" yoffset="7" xadvance="4"/>
			<char id="59" x="35" y="116" width="3" height="8" xoffset="0" yoffset="7" xadvance="4"/>
			<char id="60" x="38" y="98" width="5" height="7" xoffset="0" yoffset="5" xadvance="6"/>
			<char id="61" x="36" y="110" width="6" height="5" xoffset="0" yoffset="6" xadvance="7"/>
			<char id="62" x="39" y="116" width="5" height="7" xoffset="0" yoffset="5" xadvance="6"/>
			<char id="63" x="43" y="106" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="64" x="45" y="116" width="10" height="8" xoffset="0" yoffset="6" xadvance="11" page="0"
				  chnl="15"/>
			<char id="91" x="40" y="85" width="3" height="11" xoffset="0" yoffset="3" xadvance="4"/>
			<char id="92" x="42" y="53" width="6" height="10" xoffset="0" yoffset="3" xadvance="7"/>
			<char id="93" x="43" y="41" width="3" height="11" xoffset="0" yoffset="3" xadvance="4"/>
			<char id="94" x="13" y="69" width="5" height="4" xoffset="0" yoffset="4" xadvance="6"/>
			<char id="95" x="1" y="126" width="7" height="1" xoffset="0" yoffset="12" xadvance="9"/>
			<char id="96" x="30" y="55" width="3" height="2" xoffset="0" yoffset="4" xadvance="4"/>
			<char id="123" x="44" y="21" width="4" height="11" xoffset="0" yoffset="3" xadvance="5"/>
			<char id="124" x="47" y="33" width="2" height="11" xoffset="1" yoffset="3" xadvance="5"/>
			<char id="125" x="49" y="11" width="4" height="11" xoffset="0" yoffset="3" xadvance="5"/>
			<char id="126" x="47" y="45" width="6" height="3" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="710" x="8" y="59" width="5" height="2" xoffset="0" yoffset="5" xadvance="6"/>
			<char id="732" x="35" y="125" width="6" height="2" xoffset="0" yoffset="5" xadvance="7"/>
			<char id="8211" x="42" y="125" width="5" height="2" xoffset="0" yoffset="8" xadvance="6" page="0"
				  chnl="15"/>
			<char id="8212" x="48" y="125" width="6" height="2" xoffset="0" yoffset="8" xadvance="7" page="0"
				  chnl="15"/>
			<char id="8216" x="38" y="106" width="3" height="3" xoffset="0" yoffset="4" xadvance="4" page="0"
				  chnl="15"/>
			<char id="8218" x="52" y="1" width="3" height="4" xoffset="0" yoffset="11" xadvance="4"/>
			<char id="8220" x="47" y="49" width="6" height="3" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="8222" x="52" y="6" width="6" height="4" xoffset="0" yoffset="11" xadvance="7"/>
			<char id="8224" x="49" y="23" width="4" height="6" xoffset="0" yoffset="5" xadvance="5"/>
			<char id="8225" x="50" y="30" width="4" height="10" xoffset="0" yoffset="4" xadvance="5" page="0"
				  chnl="15"/>
			<char id="8226" x="50" y="41" width="4" height="3" xoffset="0" yoffset="7" xadvance="5"/>
			<char id="8230" x="55" y="125" width="8" height="2" xoffset="0" yoffset="11" xadvance="9" page="0"
				  chnl="15"/>
			<char id="8240" x="42" y="64" width="8" height="9" xoffset="0" yoffset="4" xadvance="9"/>
			<char id="8249" x="56" y="1" width="3" height="3" xoffset="0" yoffset="7" xadvance="4"/>
			<char id="8250" x="49" y="53" width="3" height="3" xoffset="0" yoffset="7" xadvance="4"/>
			<char id="8482" x="49" y="57" width="9" height="4" xoffset="0" yoffset="4" xadvance="10" page="0"
				  chnl="15"/>
			<char id="39" x="44" y="33" width="2" height="3" xoffset="0" yoffset="4" xadvance="3"/>
			<char id="34" x="53" y="53" width="5" height="3" xoffset="0" yoffset="4" xadvance="6"/>
			<char id="191" x="54" y="11" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="161" x="60" y="1" width="2" height="9" xoffset="1" yoffset="4" xadvance="5"/>
			<char id="162" x="54" y="45" width="7" height="7" xoffset="0" yoffset="7" xadvance="8"/>
			<char id="163" x="55" y="21" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="164" x="61" y="11" width="9" height="9" xoffset="0" yoffset="4" xadvance="10"/>
			<char id="165" x="63" y="1" width="8" height="9" xoffset="0" yoffset="4" xadvance="9"/>
			<char id="166" x="55" y="31" width="2" height="9" xoffset="1" yoffset="4" xadvance="5"/>
			<char id="167" x="58" y="31" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="168" x="55" y="41" width="6" height="2" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="169" x="62" y="21" width="8" height="8" xoffset="0" yoffset="4" xadvance="9"/>
			<char id="170" x="44" y="74" width="6" height="8" xoffset="0" yoffset="5" xadvance="7"/>
			<char id="171" x="44" y="83" width="7" height="3" xoffset="0" yoffset="7" xadvance="8"/>
			<char id="172" x="44" y="87" width="6" height="3" xoffset="0" yoffset="8" xadvance="8"/>
			<char id="173" x="44" y="91" width="6" height="2" xoffset="0" yoffset="8" xadvance="7"/>
			<char id="174" x="44" y="94" width="8" height="8" xoffset="0" yoffset="4" xadvance="9"/>
			<char id="175" x="37" y="53" width="4" height="1" xoffset="0" yoffset="5" xadvance="5"/>
			<char id="176" x="51" y="87" width="5" height="4" xoffset="0" yoffset="4" xadvance="6"/>
			<char id="177" x="50" y="103" width="6" height="6" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="178" x="50" y="110" width="4" height="5" xoffset="0" yoffset="4" xadvance="5"/>
			<char id="179" x="55" y="110" width="4" height="5" xoffset="0" yoffset="4" xadvance="5"/>
			<char id="180" x="44" y="103" width="3" height="2" xoffset="0" yoffset="4" xadvance="4"/>
			<char id="181" x="53" y="92" width="7" height="9" xoffset="0" yoffset="7" xadvance="8"/>
			<char id="182" x="51" y="62" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="183" x="37" y="85" width="2" height="2" xoffset="0" yoffset="8" xadvance="3"/>
			<char id="184" x="57" y="102" width="3" height="2" xoffset="0" yoffset="13" xadvance="4" page="0"
				  chnl="15"/>
			<char id="185" x="56" y="116" width="3" height="5" xoffset="0" yoffset="4" xadvance="4"/>
			<char id="186" x="51" y="72" width="6" height="8" xoffset="0" yoffset="5" xadvance="7"/>
			<char id="187" x="57" y="105" width="7" height="3" xoffset="0" yoffset="7" xadvance="8"/>
			<char id="188" x="57" y="81" width="11" height="10" xoffset="0" yoffset="3" xadvance="12" page="0"
				  chnl="15"/>
			<char id="189" x="61" y="92" width="11" height="10" xoffset="0" yoffset="3" xadvance="12" page="0"
				  chnl="15"/>
			<char id="190" x="60" y="109" width="13" height="10" xoffset="0" yoffset="3" xadvance="14" page="0"
				  chnl="15"/>
			<char id="192" x="59" y="53" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="193" x="59" y="66" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="194" x="65" y="30" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="195" x="71" y="11" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="196" x="67" y="43" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="197" x="73" y="24" width="7" height="13" xoffset="0" yoffset="0" xadvance="8"/>
			<char id="198" x="72" y="1" width="10" height="9" xoffset="0" yoffset="4" xadvance="11"/>
			<char id="199" x="79" y="11" width="7" height="11" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="200" x="67" y="56" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="201" x="69" y="69" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="202" x="73" y="82" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="203" x="73" y="95" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="204" x="74" y="108" width="3" height="12" xoffset="-1" yoffset="1" xadvance="3" page="0"
				  chnl="15"/>
			<char id="205" x="78" y="108" width="3" height="12" xoffset="0" yoffset="1" xadvance="3" page="0"
				  chnl="15"/>
			<char id="206" x="75" y="38" width="6" height="12" xoffset="-2" yoffset="1" xadvance="3" page="0"
				  chnl="15"/>
			<char id="207" x="81" y="23" width="6" height="12" xoffset="-2" yoffset="1" xadvance="3" page="0"
				  chnl="15"/>
			<char id="208" x="83" y="1" width="8" height="9" xoffset="0" yoffset="4" xadvance="9"/>
			<char id="209" x="75" y="51" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="210" x="82" y="36" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="211" x="77" y="64" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="212" x="83" y="49" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="213" x="88" y="11" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="214" x="90" y="24" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="215" x="65" y="103" width="6" height="5" xoffset="0" yoffset="6" xadvance="7"/>
			<char id="216" x="92" y="1" width="7" height="9" xoffset="0" yoffset="4" xadvance="8"/>
			<char id="217" x="96" y="11" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="218" x="104" y="1" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="219" x="112" y="1" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="220" x="120" y="1" width="7" height="12" xoffset="0" yoffset="1" xadvance="8"/>
			<char id="221" x="104" y="14" width="8" height="12" xoffset="0" yoffset="1" xadvance="9" page="0"
				  chnl="15"/>
			<char id="222" x="113" y="14" width="6" height="12" xoffset="0" yoffset="4" xadvance="7" page="0"
				  chnl="15"/>
			<char id="223" x="120" y="14" width="7" height="10" xoffset="0" yoffset="4" xadvance="8" page="0"
				  chnl="15"/>
			<char id="224" x="120" y="25" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="225" x="90" y="37" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="226" x="98" y="27" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="227" x="105" y="27" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="228" x="112" y="27" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="229" x="119" y="35" width="6" height="10" xoffset="0" yoffset="3" xadvance="7" page="0"
				  chnl="15"/>
			<char id="230" x="64" y="121" width="10" height="6" xoffset="0" yoffset="7" xadvance="11" page="0"
				  chnl="15"/>
			<char id="231" x="97" y="37" width="6" height="8" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="232" x="104" y="37" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="233" x="97" y="46" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="234" x="111" y="37" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="235" x="118" y="46" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="236" x="69" y="82" width="3" height="9" xoffset="-1" yoffset="4" xadvance="3"/>
			<char id="237" x="100" y="1" width="3" height="9" xoffset="0" yoffset="4" xadvance="3"/>
			<char id="238" x="104" y="47" width="6" height="9" xoffset="-2" yoffset="4" xadvance="3" page="0"
				  chnl="15"/>
			<char id="239" x="111" y="47" width="6" height="9" xoffset="-2" yoffset="4" xadvance="3" page="0"
				  chnl="15"/>
			<char id="240" x="118" y="56" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="241" x="91" y="56" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="242" x="98" y="57" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="243" x="85" y="66" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="244" x="105" y="57" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="245" x="112" y="66" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="246" x="119" y="66" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="247" x="92" y="67" width="6" height="8" xoffset="0" yoffset="5" xadvance="7"/>
			<char id="248" x="75" y="121" width="6" height="6" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="249" x="99" y="67" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="250" x="85" y="76" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="251" x="92" y="76" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="252" x="81" y="86" width="6" height="9" xoffset="0" yoffset="4" xadvance="7"/>
			<char id="253" x="106" y="76" width="6" height="12" xoffset="0" yoffset="4" xadvance="7" page="0"
				  chnl="15"/>
			<char id="254" x="99" y="77" width="6" height="8" xoffset="0" yoffset="7" xadvance="7"/>
			<char id="255" x="113" y="76" width="6" height="12" xoffset="0" yoffset="4" xadvance="7" page="0"
				  chnl="15"/>
			<char id="8260" x="81" y="96" width="6" height="10" xoffset="0" yoffset="3" xadvance="7" page="0"
				  chnl="15"/>
			<char id="937" x="120" y="76" width="7" height="8" xoffset="0" yoffset="5" xadvance="8"/>
			<char id="13" x="120" y="85" width="3" height="12" xoffset="1" yoffset="1" xadvance="4"/>
			<char id="32" x="0" y="0" width="0" height="0" xoffset="1" yoffset="1" xadvance="4"/>
		</chars>
	</font>;


	public static function get texture():Texture {
		var bitmapData:BitmapData = getBitmapData();
		var texture:Texture = Texture.fromBitmapData( bitmapData, false );
		bitmapData.dispose();
		bitmapData = null;

		texture.root.onRestore = function ():void {
			bitmapData = getBitmapData();
			texture.root.uploadBitmapData( bitmapData );
			bitmapData.dispose();
			bitmapData = null;
		};

		return texture;
	}

	private static function getBitmapData():BitmapData {
		var bmpData:BitmapData = new BitmapData( BITMAP_WIDTH, BITMAP_HEIGHT );
		var bmpBytes:ByteArray = new ByteArray();
		var numBytes:int = BITMAP_DATA.length;

		for ( var i:int = 0; i < numBytes; ++i )
			bmpBytes.writeUnsignedInt( BITMAP_DATA[i] );

		bmpBytes.uncompress();
		bmpData.setPixels( new Rectangle( 0, 0, BITMAP_WIDTH, BITMAP_HEIGHT ), bmpBytes );
		bmpBytes.clear();
		trace( "adding std96!" );
		return bmpData;
	}

	public static function register():void {
		var bf:BitmapFont = new BitmapFont( texture, xml );
		bf.smoothing = TextureSmoothing.NONE;
		TextField.registerBitmapFont( bf, "std96" );
	}

	public static function get xml():XML { return XML_DATA; }
}
}