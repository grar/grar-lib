/**
 * Code by Rodrigo López Peker (grar) on 3/4/16 7:54 PM.
 *
 */
package grar.starling.fonts.pixelfonts {
import flash.display.BitmapData;
import flash.filesystem.File;
import flash.geom.Rectangle;
import flash.utils.ByteArray;

import grar.air.FileUtils;

public class CreateByteArrayFont {

	[Embed(source="assets/tobi.png")]
	public static var tobi:Class ;
	[Embed(source="assets/st_955.png")]
	public static var st95:Class ;
	[Embed(source="assets/st_965.png")]
	public static var st96:Class ;

	public function CreateByteArrayFont() {
	}

	public static function test():void {
		var bd:BitmapData ;
		var r:Rectangle ;
		var ba:ByteArray ;

		bd = new st96().bitmapData ;
		r = bd.rect ;

		// for st96, we dont have space, so we cut 1px w, 1px h
//		r.width--;
//		r.height--;


		var invalid:Boolean = true ;
		while(invalid){
			ba = bd.getPixels(r) ;
			ba.compress() ;
			var len:int = ba.length ;
			if( int(len/4) != len/4){
				invalid = true ;
				r.width--;
//				r.width--;
			} else {
				invalid = false ;
				break ;
			}
		}

		var output:Array = [] ;
		// write to Array.
		ba.position = 0 ;
		while( ba.bytesAvailable ){
			output.push( ba.readUnsignedInt()) ;
		}
		trace("final r:", r ) ;
		trace("final ba len:", len ) ;
		trace("final arr len:", output.length ) ;
		// output format to a file.
		var str:String = "" ;
		var cols:int = 8 ;
		for ( var i:int = 0; i < output.length; i++ ) {
			str += output[i] + "," ;
			if( i % cols == (cols-1) ){
				str += "\n" ;
			}
		}
		str = str.substr(0,str.length-1);
		FileUtils.saveStringToFile(File.desktopDirectory.resolvePath("pixel_format.txt"), str ) ;

	}
}
}
