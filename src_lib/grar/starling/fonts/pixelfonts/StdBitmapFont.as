// =================================================================================================
//
//	Starling Framework
//	Copyright Gamua GmbH. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================

package grar.starling.fonts.pixelfonts {

import flash.display.BitmapData;
import flash.geom.Rectangle;
import flash.utils.ByteArray;

import starling.text.BitmapFont;
import starling.text.TextField;
import starling.textures.Texture;
import starling.textures.TextureAtlas;
import starling.textures.TextureSmoothing;

public class StdBitmapFont {

	// 1px on the right is lost.
	private static const BITMAP_WIDTH:int = 256;
	private static const BITMAP_HEIGHT:int = 128;

	private static var atlasXML:XML =
			<TextureAtlas width="256" height="128">
				<SubTexture name="std96" x="0" y="0" width="128" height="128"/>
				<SubTexture name="std95" x="128" y="0" width="128" height="128"/>
			</TextureAtlas>;

	private static function getBitmapData():BitmapData {
		var bmpData:BitmapData = new BitmapData( BITMAP_WIDTH, BITMAP_HEIGHT );
		var bmpBytes:ByteArray = new ByteArray();
		// 1st bold.
		var bdata:Array = Std96BitmapFont.BITMAP_DATA;
		var numBytes:int = bdata.length;
		for ( var i:int = 0; i < numBytes; ++i ) bmpBytes.writeUnsignedInt( bdata[i] );
		bmpBytes.uncompress();
		bmpData.setPixels( new Rectangle( 0, 0, Std96BitmapFont.BITMAP_WIDTH, Std96BitmapFont.BITMAP_HEIGHT ), bmpBytes );
		bmpBytes.clear();
		trace( "adding std96!" );

		bdata = Std95BitmapFont.BITMAP_DATA;
		numBytes = bdata.length;
		for ( i = 0; i < numBytes; ++i ) bmpBytes.writeUnsignedInt( bdata[i] );
		bmpBytes.uncompress();
		bmpData.setPixels( new Rectangle( Std96BitmapFont.BITMAP_WIDTH + 2, 0, Std95BitmapFont.BITMAP_WIDTH, Std95BitmapFont.BITMAP_HEIGHT ), bmpBytes );
		bmpBytes.clear();

		return bmpData;
	}

	public static function register():void {
		var bitmapData:BitmapData = getBitmapData();
		var texture:Texture = Texture.fromBitmapData( bitmapData, false );
		var ta:TextureAtlas = new TextureAtlas( texture, atlasXML );
		bitmapData.dispose();
		bitmapData = null;
		texture.root.onRestore = function ():void {
			bitmapData = getBitmapData();
			texture.root.uploadBitmapData( bitmapData );
			bitmapData.dispose();
			bitmapData = null;
		};

		var bf:BitmapFont;
		bf = new BitmapFont( ta.getTexture( "std95" ), Std95BitmapFont.xml );
		bf.smoothing = TextureSmoothing.NONE;
		TextField.registerBitmapFont( bf, "std95" );

		bf = new BitmapFont( ta.getTexture( "std96" ), Std96BitmapFont.xml );
		bf.smoothing = TextureSmoothing.NONE;
		TextField.registerBitmapFont( bf, "std96" );
	}

}
}