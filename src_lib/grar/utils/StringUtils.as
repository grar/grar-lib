/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 12:53 PM.
 *
 */
package grar.utils {

import flash.sampler.getMasterString;
import flash.utils.ByteArray;
import flash.utils.getTimer;
import flash.xml.XMLDocument;
import flash.xml.XMLNode;
import flash.xml.XMLNodeType;

public class StringUtils {

	public function StringUtils() {
	}

	private static const DIATRICS_O:String = "áäàâčçďéěëèêíïîĺľňóôöŕšťúůüùûýřžÁÄÀÂČÇĎÉĚËÈÊÍÏÎĹĽŇÓÔŒÖŔŠŤÚŮÜÙÛÝŘŽ";
	private static const DIATRICS_R:String = "aaaaccdeeeeeiiillnooorstuuuuuyrzAAAACCDEEEEEIIILLNOOSORSTUUUUUYRZ";

	public static const REGEXP_HTML_TAGS:RegExp = new RegExp( "<[^<]+?>", "gi" );
	public static const REGEXP_NONALPHA:RegExp = new RegExp( /[^a-zA-Z]+/g );
	public static const REGECP_NONALPHANUMERIC:RegExp = new RegExp( /[^a-zA-Z 0-9]+/g );

	public static var PHONE_VALID_CHARS:String = "1234567890#*";

	private static var xmlNodeForHTML:XMLNode;
	private static var xmlDoc:XMLDocument;

	public static function removeSimpleDiatrics( str:String, lowerCase:Boolean = false ):String {
		if ( !str ) return "";
		var out:String = "";
		if ( lowerCase ) str = str.toLocaleLowerCase();
		var len:int = str.length;
		for ( var i:int = 0; i < len; i++ ) {
			var char:String = str.charAt( i );
			var idx:int = DIATRICS_O.indexOf( char );
			out += idx > -1 ? DIATRICS_R.charAt( idx ) : char;
		}
		return out;
	}

	public static function removeHTMLTags( str:String ):String {
		return str.replace( REGEXP_HTML_TAGS, "" );
	}

	// removes ONLY multiple whitespaces.
	public static function removeDerp( str:String ):String {
		while (str.indexOf("  ") !== -1) {
			str = str.replace(/  /g, " ");
		}
		return str ;
	}

	public static function removeNonNumericChars( str:String ):String {
		if ( !str ) return "";
		var out:String = "";
		var len:int = str.length;
		for ( var i:int = 0; i < len; i++ ) {
			var charCode:int = str.charCodeAt( i );
			if ( charCode >= 48 && charCode <= 57 ) out += str.charAt( i );
		}
		return out;
	}

	public static function removeNonAlphaChars( str:String, replace:String = "" ):String {
		if ( !str ) return "";
		return str.replace( REGEXP_NONALPHA, replace );
	}

	public static function removeNonAlphaNumericChars( str:String, replace:String = "" ):String {
		if ( !str ) return "";
		return str.replace( REGECP_NONALPHANUMERIC, replace );
	}

	public static function removeCharsExcept( str:String, exceptions:String ):String {
		if ( !str ) return "";
		var out:String = "";
		var len:int = str.length;
		var char:String;
		for ( var i:int = 0; i < len; i++ ) {
			char = str.charAt( i );
			if ( exceptions.indexOf( char ) > -1 ) out += char;
		}
		return out;
	}

	public static function removeDuplicatedChars( str:String ):String {
		var out:String = "";
		var len:int = str.length ;
		for ( var i:int = 0; i < len; i++ ) {
			var char:String = str.charAt(i) ;
			if( out.indexOf(char)==-1 ) out += char ;
		}
		return out ;
	}

	public static function removeJsonComments( jsonString:String ):String {
		if ( !jsonString ) return "";
		return jsonString.replace( /\/\*.*?\*\//sg, "" );
	}

	public static function trim( str:String ):String {
		if ( !str ) return "";
		return str.replace( /^\s+|\s+$/g, '' );
	}

	public function rtrim( str:String ):String {
		if ( !str ) return "";
		return str.replace( /\s+$/, "" );
	}

	/** Formats a String in .Net-style, with curly braces ("{0}"). Does not support any
	 *  number formatting options yet. */
	public static function formatString( format:String, ...args ):String {
		for ( var i:int = 0; i < args.length; ++i )
			format = format.replace( new RegExp( "\\{" + i + "\\}", "g" ), args[i] );

		return format;
	}

	// -- file stuffs.

	public static function fileExtension( path:String ):String {
		if ( !path ) return "";
		var idx:int = path.lastIndexOf( "." );
		if ( idx == -1 ) return "";
		return path.substr( idx + 1, path.length ).toLowerCase();
	}

	public static function fileName( path:String, includeExtension:Boolean=true ):String {
		if ( !path ) return "";
		var out:String = "";
		var temp:Array = path.split( "/" );
		var len:int = temp.length;
		out = ( len < 2 ) ? temp[0] : temp[int( len - 1 )];
		var extIndex:int = out.lastIndexOf( "." ) ;
		if ( extIndex == -1 ) return "";
		if( !includeExtension ){
			return out.substr(0,extIndex) ;
		}
		return out;
	}

	public static function hostFromUri( uri:String ):String {
		if ( !uri ) return "";
		var idx:int = uri.indexOf( "//" );
		var tmp:String = uri.substr( idx == -1 ? 0 : idx + 2, uri.length );
		tmp = tmp.substr( 0, tmp.indexOf( "/" ) );
		// validate if we have a "."
		if ( tmp.split( "." ).length <= 1 ) return null;
		return tmp;
	}

	public static function getQueryParamValue( url:String, parameterName:String ):String {
		// Finds the value of a parameter given a querystring/url and the parameter name
		var qsRegex:RegExp = new RegExp( "[?&]" + parameterName + "(?:=([^&]*))?", "i" );
		var match:Object = qsRegex.exec( url );
		if ( Boolean( match ) ) return match[1];
		return null;
	}

	public static function formatSeconds( seconds:int, includeHours:Boolean = false, includeMinutes:Boolean = true,
										  includeSeconds:Boolean = true ):String {
		if ( seconds <= 0 ) {
			return includeHours ? "00" : "" + includeMinutes ? "00" : "" + includeSeconds ? "00" : "";
		}
		var output:String = "";
		var h:int = seconds > 3600 ? seconds / 3600 | 0 : 0;
		var m:int = seconds % 3600 / 60 | 0;
		var s:int = seconds % 60;
		if ( includeHours ) output += ( h < 10 ? "0" + h : h ) + ":";
		if ( includeMinutes ) output += ( m < 10 ? "0" + m : m ) + ":";
		if ( includeSeconds ) output += ( s < 10 ? "0" + s : s ) + "";
		return output;
	}

	public static function replace( str:String, search:String, replace:String = "" ):String {
		if ( str.indexOf( search ) == -1 ) return str;
		return str.split( search ).join( replace );
	}

	public static function replaceMultipleLines( str:String, replacer:String="\n" ):String {
		return  str.replace(/[\r\n]+/g, replacer);
	}

	/**
	 * Replace the ${token} inside input
	 * replacePlaceholders("Hello there ${name}, how is the ${noun} today?", {name:"YOUR_NAME_HERE",noun: "YOUR_NOUN_HERE"}));
	 * @param input
	 * @param replacementMap
	 * @return
	 */
	public static function replacePlaceholders( input:String, replacementMap:Object ):String {
		// '${', followed by any char except '}', ended by '}'
		return input.replace( /\${([^}]*)}/g, function ():String {
			return replaceTokenEntities( arguments, replacementMap );
		} );
	}

	private static function replaceTokenEntities( regExpArgs:Array, map:Object ):String {
		var entity:String = String( regExpArgs[0] );
		var entityBody:String = String( regExpArgs[1] );
		return (map[entityBody]) ? map[entityBody] : entity;
	}

	public static function removeMultipleSpaces( str:String ):String {
		return str.replace( /\s{2,}/g, ' ' );
	}

	public static function trimReturns( str:String ):String {
		return replace( replace( str, "\r" ), "\n" );
	}


	//===================================================================================================================================================
	//
	//      ------  encoding
	//
	//===================================================================================================================================================

	public static function encodeHtml( string:String ):String {
		if ( !string ) return "";
		if ( !xmlNodeForHTML ) {
			xmlNodeForHTML = new XMLNode( XMLNodeType.TEXT_NODE, "" );
		}
		xmlNodeForHTML.nodeValue = string;
		return XML( xmlNodeForHTML ).toXMLString();
	}

	public static function decodeHtml( string:String ):String {
		if ( !string ) return "";
		if ( !xmlDoc ) {
			xmlDoc = new XMLDocument();
		}
		xmlDoc.parseXML( string );
		return xmlDoc.firstChild.nodeValue;
	}

	public static function encodeUtf8( text:String ):String {
		var a:uint, n:uint, A:uint;
		var utf:String;
		utf = "";
		A = text.length;
		for ( a = 0; a < A; a++ ) {
			n = text.charCodeAt( a );
			if ( n < 128 ) {
				utf += String.fromCharCode( n );
			} else if ( (n > 127) && (n < 2048) ) {
				utf += String.fromCharCode( (n >> 6) | 192 );
				utf += String.fromCharCode( (n & 63) | 128 );
			} else {
				utf += String.fromCharCode( (n >> 12) | 224 );
				utf += String.fromCharCode( ((n >> 6) & 63) | 128 );
				utf += String.fromCharCode( (n & 63) | 128 );
			}
		}
		return utf;
	}


	//===================================================================================================================================================
	//
	//      ------  HTML and Styling stuffs
	//
	//===================================================================================================================================================

	/**
	 * Wraps the str into a style tag, if no style is passed, we just concatenate str</span>
	 * @param str
	 * @param style
	 * @return
	 */
	public static function wrapSpanStyle( str:String, style:String = null ):String {
		return ( style ? "<span class='" + style + "'>" : "<span>") + str + "</span>";
	}

	public static function wrapCDATA( str:String ):String {
		return "<![CDATA[" + str + "]]>";
	}

	public static function makeUrlStub( str:String ):String {
		// Transforms a title into a stub
		return str.toLowerCase().replace( " ", "-" ).replace( /[^a-z0-9\-]/gi, "" );
	}

	/**
	 * "part of a URL which identifies a page using human-readable keywords"
	 * "10 Tips to a Better Life!" > "10-tips-to-a-better-life".
	 * @param str
	 * @return
	 */
	public static function slugify( str:String ):String {
		// Source: http://active.tutsplus.com/articles/roundups/15-useful-as3-snippets-on-snipplr-com/
		const pattern1:RegExp = /[^\w- ]/g; // Matches anything except word characters, space and -
		const pattern2:RegExp = / +/g; // Matches one or more space characters
		return str.replace( pattern1, "" ).replace( pattern2, "-" ).toLowerCase();
	}

	/**
	 * Still to be tested, the HTML HAS to be well formed (no missing tags).
	 * @param str
	 * @param tagsToKeep
	 * @return
	 */
	public static function getHtmlWithStrippedTags( str:String, tagsToKeep:Array = null ):String {
		if ( tagsToKeep == null ) tagsToKeep = [];
		var xml:XML = new XML( str );
		var __txt:String = "";
		var i:int;
		if ( tagsToKeep.indexOf( String( xml.name() ) ) > -1 ) {
			// Keep tag

			// Tag open begin
			__txt += "<" + xml.name();

			// Add attributes
			for ( i = 0; i < xml.attributes().length(); i++ ) {
				__txt += " " + (xml.attributes()[i] as XML).name() + "=\"" + (xml.attributes()[i] as XML).toString() + "\"";
			}

			// Tag open end
			__txt += ">";
		}

		// Add children
		for ( i = 0; i < xml.children().length(); i++ ) {
			if ( (xml.children()[i] as XML).nodeKind() == "element" ) {
				__txt += getHtmlWithStrippedTags( (xml.children()[i] as XML).toXMLString(), tagsToKeep );
			} else {
				__txt += (xml.children()[i] as XML).toString();
			}
		}

		if ( tagsToKeep.indexOf( String( xml.name() ) ) > -1 ) {
			// Tag close
			__txt += "</" + xml.name() + ">";
		}
		return getCleanString( __txt );
	}


	/**
	 * Calc the Levenshtein distance (similarity/edit distance) between two strings.
	 * @param term1 Start string.
	 * @param term2 End string.
	 */
	public static function levenshtein( term1:String, term2:String ):uint {
		if ( !term1 ) term1 = "";
		if ( !term2 ) term2 = "";
		if ( term1 == term2 ) {
			return 0;
		}
		var a:Array = [];
		var cost:uint;
		var length1:uint = term1.length;
		var length2:uint = term2.length;
		var i:uint;
		var j:uint;

		if ( length1 == 0 ) {
			return length2;
		}

		if ( length2 == 0 ) {
			return length1;
		}

		for ( i = 0; i <= length1; i++ ) {
			a[i] = []
		}

		for ( i = 0; i <= length1; i++ ) {
			a[i][0] = i;
		}

		for ( j = 0; j <= length2; j++ ) {
			a[0][j] = j;
		}

		for ( i = 1; i <= length1; i++ ) {
			var s_i:String = term1.charAt( i - 1 );
			for ( j = 1; j <= length2; j++ ) {
				var t_j:String = term2.charAt( j - 1 );
				if ( s_i == t_j ) {
					cost = 0;
				} else {
					cost = 1;
				}
				a[i][j] = Math.min( a[int( i - 1 )][j] + 1, Math.min( a[i][int( j - 1 )] + 1, Math.min( a[int( i - 1 )][int( j - 1 )] + cost, a[int( i - 1 )][j] + 1 ) ) );
			}
		}
		return a[length1][length2];
	}


	/**
	 * Determine the similarity between two strings using their Levenshtein distance
	 * as a value between 0 and 1.
	 *
	 * @param term1 Start string.
	 * @param term2 End string.
	 */
	public static function similarity( term1:String, term2:String ):Number {
		var lev:uint = levenshtein( term1, term2 );
		var maxLength:uint = Math.max( term1.length, term2.length );
		return maxLength == 0 ? 1 : 1 - (lev / maxLength);
	}

	public static function wordCount( str:String ):uint {
		return str.match( /\b\w+\b/g ).length;
	}


	/**
	 * Restrict a string to a sequence of valid characters. Use to
	 * strip punctuation, for example.
	 */
	public static function restrictTo( str:String, validChars:String = "" ):String {
		if ( validChars == "" ) validChars = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		var out:String = "";
		for ( var i:uint = 0; i < str.length; i++ ) {
			if ( validChars.indexOf( str.charAt( i ) ) > -1 ) {
				out += str.charAt( i );
			}
		}
		return out;
	}

	/**
	 * Intelligently clip a string down to a certain number of characters.
	 *
	 * @param str String to clip.
	 * @param length Length at which to clip.
	 * @param trailChars Optional argument specifying any trailing characters, e.g. "...".
	 */
	public static function clip( str:String, length:uint, trailChars:String = "…" ):String {
		return (str.length > length) ? str.substring( 0, str.substring( 0, length ).lastIndexOf( " " ) ) + trailChars : str;
	}

	/**
	 * Trim spaces and camel notate String.
	 */
	public function trimCamel( str:String ):String {
		if ( !str ) return "";
		var out:String = "";
		var justPassedSpace:Boolean;
		for ( var i:int = 0; i < str.length; i++ ) {
			var char:String = str.charAt( i );
			if ( char != " " ) {
				if ( justPassedSpace ) {
					out += char.toUpperCase();
					justPassedSpace = false;
				}
				else {
					out += char.toLowerCase();
				}
			} else {
				justPassedSpace = true;
			}
		}
		return out;
	}

	/**
	 * Transforms source String to per word capitalization.
	 */
	public static function toTitleCase( str:String ):String {
		if ( !str ) return "";
		var lstr:String = str.toLowerCase();
		return lstr.replace( /\b([a-z])/g, function ( $0:* ):* {
			return $0.toUpperCase();
		} );
	}

	/**
	 * Transform first char in uppercase, rest in lowercase.
	 * @param str
	 * @return
	 */
	public static function capitalize( str:String ):String {
		if ( !str ) return "";
		return str.charAt( 0 ).toUpperCase() + str.substring( 1 );
	}

	/**
	 * Convert double quotes to single quotes.
	 */
	public static function toSingleQuote( str:String ):String {
		if ( !str ) return "";
		return str.split( String.fromCharCode( 34 ) ).join( "'" );
	}

	/**
	 * Convert single quotes to double quotes.
	 */
	public static function toDoubleQuote( str:String ):String {
		if ( !str ) return "";
		return str.split( "'" ).join( String.fromCharCode( 34 ) );
	}


	public static function stringsAreEqual( s1:String, s2:String, caseSensitive:Boolean = false ):Boolean {
		return caseSensitive ? s1 == s2 : s1.toUpperCase() == s2.toUpperCase();
	}

	/**
	 * Sanitize <code>null</code> strings for display purposes.
	 */
	public static function sanitizeNull( str:String ):String {
		return str == null || str == "null" ? "" : str;
	}

	/**
	 * Returns the specified String in reverse word order.
	 * @param str String that will be reversed
	 */
	public static function reverseWords( str:String ):String {
		if ( !str ) return "";
		return str.split( /\s+/ ).reverse().join( "" );
	}

	/**
	 * Returns the specified String in reverse character order.
	 * @param str String that will be reversed
	 */
	public static function reverse( str:String ):String {
		if ( !str ) return "";
		return str.split( "" ).reverse().join( "" );
	}


	/**
	 * Generate a set of random Special and Number characters.
	 * Useful to test if the chars were included with Font on a Textfield.
	 */
	public static function randomSpecialCharacters( amount:Number ):String {
		var str:String = "";
		for ( var i:int = 0; i < amount; i++ )
			str += String.fromCharCode( Math.round( Math.random() * (64 - 33) ) + 33 );
		return str;
	}

	/**
	 * Generate a set of random Number characters.
	 */
	public static function randomNumberString( amount:Number ):String {
		var str:String = "";
		for ( var i:int = 0; i < amount; i++ )
			str += String.fromCharCode( Math.round( Math.random() * (57 - 48) ) + 48 );
		return str;
	}

	/**
	 * Generate a set of random LowerCase characters.
	 */
	public static function randomLowercaseCharacters( amount:Number ):String {
		var str:String = "";
		for ( var i:int = 0; i < amount; i++ )
			str += String.fromCharCode( Math.round( Math.random() * (122 - 97) ) + 97 );
		return str;
	}

	/**
	 * Generate a random String.
	 * @param amount String length (default 10)
	 */
	public static function randomCharacters( amount:Number = 10,
											 charSet:String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" ):String {
		var alphabet:Array = charSet.split( "" );
		var alphabetLength:int = alphabet.length;
		var randomLetters:String = "";
		for ( var j:uint = 0; j < amount; j++ ) {
			var r:int = Math.random() * alphabetLength;
			randomLetters += alphabet[r];
		}
		return randomLetters;
	}

	/**
	 * repeats n times str
	 * @param n
	 * @param str
	 * @return
	 */
	public function repeat( n:uint, str:String = " " ):String {
		return new Array( n + 1 ).join( str );
	}


	//===================================================================================================================================================
	//
	//      ------  VALIDATIONS
	//
	//===================================================================================================================================================

	/**
	 * Fills with 0 the specified int by len amount.
	 * Ex:    zeroPad( 34, 4 ); outputs=0034
	 *        zeroPad( 34, 2 ); outputs=34
	 * @param value
	 * @param len
	 * @return
	 */
	public static function zeroPad( value:int, len:int ):String {
		for ( var val:String = String( value ); val.length < len; val = '0' + val ) {}
		return val;
	}

	public static function validURL( str:String ):Boolean {
		var urlExp:RegExp = new RegExp( "http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?" );
		return urlExp.test( str );
	}

	// isnt a very decent validation.
	public static function validEmail( email:String ):Boolean {
		var emailExpression:RegExp = /([a-z0-9._-]+?)@([a-z0-9.-]+)\.([a-z]{2,4})/i;
		return emailExpression.test( email );
	}

	public static function validPhone( phoneNumber:String ):Boolean {
		var countryCode:String = "((\\+|00)?([1-9]|[1-9][0-9]|[1-9][0-9]{2}))";
		var num:String = "([0-9]{3,10})";
		phoneNumber = phoneNumber.match( /[\+\d]/g ).join( '' );
		var phone:RegExp = new RegExp( "^" + countryCode + num + "$" );
		return phone.test( phoneNumber );
	}

	public static function maskPhone( phone:String ):String {
		if ( !phone ) return "";
		phone = phone.split( "-" ).join( "" );
		var firstChar:String = phone.charAt( 0 );
		phone = removeCharsExcept( phone, PHONE_VALID_CHARS );
		var indices:Array = firstChar == "+" ? [4] : [3, 6];
		var output:String = "";
		for ( var i:int = 0; i < phone.length; i++ ) {
			var idx:int = indices.indexOf( i );
			if ( idx > -1 ) {
				indices.splice( idx, 1 );
				output += "-";
			}
			var char:String = phone.charAt( i );
			if ( i > 0 && char == "+" ) continue;
			output += char;
		}
		return output;
	}

	public static function htmlReplaceTagForSpan( text:String, tag:String, spanClass:String ):String {
		text = replace( text, "<" + tag + ">", '<span class="' + spanClass + '">' );
		text = replace( text, "</" + tag + ">", '</span>' );
//			text = text.replace(/<(?:b|strong)>/gi, '<span class="'+boldClass+'">');// replace <strong>|<b> by <span class="strong">
//			text = text.replace(/<\/(?:b|strong)>/gi, "</span>");
//			text = text.replace(/<br\s?\/>/gi, "<br>");// replace <br /> by <br>
		return text;
	}

	/**
	 * Creates a <FONT> tag with the tagProps defined.
	 * Ex: htmlFontTag( "this is an Arial red text with 16px size", {color:0xff0000, fontFace:"Arial", size:16 });
	 * @param text
	 * @param tagProps
	 * @return
	 */
	public static function htmlFontTag( text:String, tagProps:Object ):String {
		var out:String = "";
		if ( tagProps.underline ) out += "<u>";
		out += "<FONT ";
		for ( var p:String in tagProps ) {
			if ( p == "color" ) {
				if ( typeof( tagProps[p] ) == "number" ) {
					tagProps[p] = "#" + uint( tagProps[p] ).toString( 16 );
				}
			}
			out += p + '="' + tagProps[p] + '" ';
		}
		out += ">" + text + "</FONT>";
		if ( tagProps.underline ) {
			out += "</u>";
		}
		return out;
	}

	/**
	 * Finds URLs in a string and wraps in <a> tags.
	 *
	 * @param url Target string to add links to.
	 * @param _window Window target for the link.
	 */
	public static function addHtmlLinks( url:String, _window:String = "_blank" ):String {
		return url.replace( /((https?|ftp|telnet|file):((\/\/)|(\\\\))+[\w\d:#@%\/;$()~_?\+-=\\\.&]*)/g, "<a href='$1' target='" + _window + "'>$1</a>" );
	}

	public static function parseBBCodeToHTML( str:String ):String {
		var rx:RegExp; // For when /gi does not work
		// \r\n
		str = str.replace( /\r\n/gi, "\n" );

		// [size="n"]...[/size]
		// <font size="n">...</font>
		rx = /\[size=\u0022([0-9]*?)\u0022\]((.|\n|\r)*?)\[\/size\]?/i;
		while ( rx.test( str ) ) str = str.replace( rx, "<font size=\"$1\">$2</font>" );

		// [color="c"]...[/color]
		// <font color="c">...</font>
		rx = /\[color=\u0022(#[0-9a-f]*?)\u0022\]((.|\n|\r)*?)\[\/color\]?/i;
		while ( rx.test( str ) ) str = str.replace( rx, "<font color=\"$1\">$2</font>" );

		// [url="u"]...[/url]
		// <a href="u">...</a>
		rx = /\[url=\u0022(.*?)\u0022\]((.|\n|\r)*?)\[\/url\]?/i;
		while ( rx.test( str ) ) str = str.replace( rx, "<a href=\"$1\">$2</a>" );

		// [b]...[/b]
		// <b>...</b>
		rx = /\[b\]((.|\n|\r)*?)\[\/b\]?/i;
		while ( rx.test( str ) ) str = str.replace( rx, "<b>$1</b>" );

		// [i]...[/i]
		// <i>...</i>
		rx = /\[i\]((.|\n|\r)*?)\[\/i\]?/i;
		while ( rx.test( str ) ) str = str.replace( rx, "<i>$1</i>" );
		return (str);
	}


	public static var reportCleanedStrings:Boolean = false;
	private static var savedBytes:uint = 0;
	private static var savedByteTimes:uint = 0;
	private static var cleanString_ba:ByteArray;

	public static function getCleanString( text:String ):String {
		// "Cleans" a string, by separating it from its "master" string
		// http://jacksondunstan.com/articles/2260
		// http://jacksondunstan.com/articles/2551
		if ( text == null ) return null;
		if ( reportCleanedStrings ) {
			var str:String = getMasterString( text );
			if ( str != null ) {
				savedBytes += str.length;
				savedByteTimes++;
				if ( savedByteTimes % 1000 == 0 ) {
					trace( "Saved " + (savedBytes / 1024 / 1024).toFixed( 2 ) + " string MB so far (" + (savedBytes / (getTimer() / 1000)).toFixed( 2 ) + " bytes/s)" );
				}
			}
		}
		if ( !cleanString_ba ) {
			cleanString_ba = new ByteArray();
		} else {
			cleanString_ba.clear();
		}
		cleanString_ba.writeUTFBytes( text );
		cleanString_ba.position = 0;
		return cleanString_ba.readUTFBytes( cleanString_ba.length );
	}

	/**
	 * Formats a numbers based on the separators.
	 *
	 * @param value
	 * @param thousandsSeparator
	 * @param decimalSeparator
	 * @param decimalPlaces
	 * @return
	 */
	public static function formatNumber(value:Number, thousandsSeparator:String = ",", decimalSeparator:String = ".", decimalPlaces:Number = NaN):String {

		var nInt:Number = value|0;
		var nDec:Number = value - nInt;
		var sInt:String = nInt.toString(10);
		var sDec:String;
		if (!isNaN(decimalPlaces)) {
			sDec = (Math.round(nDec * Math.pow(10, decimalPlaces)) / Math.pow(10, decimalPlaces)).toString(10).substr(2);
		} else {
			sDec = nDec == 0 ? "" : nDec.toString(10).substr(2);
		}
		var fInt:String = "";
		var i:Number;
		for (i = 0; i < sInt.length; i++) {
			fInt += sInt.substr(i, 1);
			if ((sInt.length - i - 1) % 3 == 0 && i != sInt.length - 1) fInt += thousandsSeparator;
		}

		return fInt + (sDec.length > 0 ? decimalSeparator + sDec : "");

	}

	/**
	 * Converts a number to its text equivelant
	 *
	 * @params n The number to convert
	 * @returns String equivelant of the number
	 */
	public static function numberToText( n:Number ):String {
		var output:Vector.<String> = new Vector.<String>();	// output will temporarily hold the strings that make up str
		var isNegative:Boolean = false;                 		// used for removing minus sign.
		var integers:Number;
		var decimals:Number;

		// check for NaN
		if ( isNaN( n ) ) { return N.NAN; }
		// check for zero
		if ( n == 0 ) { return N._0; }
		// check for negatives
		if ( n < 0 ) {
			isNegative = true;
			n *= -1;
		}


		// solve for decimals
		var decimalPointIndex:int = n.toString().indexOf( "." );
		if ( decimalPointIndex > -1 ) {
			digits = Vector.<String>( n.toString().substr( decimalPointIndex + 1 ).split( "" ) );
			digits.reverse();

			var i:int = 0;
			var digit:int;
			for ( ; i < digits.length; i += 1 ) {
				digit = int( digits[i] );
				if ( digit == 0 ) {
					output.push( N._0 + " " );
				} else {
					output.push( N._1to9[digit] );
				}
			}
			output.push( N.decimal );
		}

		// solve for integers
		integers = Math.floor( n );
		var period:int = 0;
		var digits:Vector.<String> = Vector.<String>( integers.toString().split( "" ) ).reverse();
		var hundreds:int;
		var tens:int;
		var ones:int;
		var next3:String;
		while ( digits.length > 0 ) {
			// grab the next three digits and analyze them.
			next3 = digits.slice( 0, 3 ).join( "" );

			if ( next3 != "000" ) {
				output.push( N.periods[period] );
			}

			ones = int( digits[0] );

			try { tens = int( digits[1] ); }
			catch ( e:RangeError ) { tens = NaN;}

			try { hundreds = int( digits[2] ); }
			catch ( e:RangeError ) { hundreds = NaN; }

			if ( !isNaN( tens ) ) {
				if ( tens == 1 ) {
					output.push( N._10to19[ones] );
				} else {
					output.push( N._1to9[ones] );
					output.push( N._10to90[tens] );
				}
			} else {
				output.push( N._1to9[ones] );
			}
			if ( !isNaN( hundreds ) ) {
				output.push( N._100to900[hundreds] );
			}

			// advance the period counter
			period++;
			// remove those three digits from the array of digits
			digits.splice( 0, 3 );
		}


		if ( isNegative == true ) {
			output.push( N.negative );
			n *= -1;
		}

		// reverse the output so that it will look correct
		output.reverse();
		// save the output to the string
		var str:String = output.join( "" );
		// remove any trailing spaces.
		if ( str.charAt( str.length - 1 ) == " " ) {
			str = str.substr( 0, str.length - 1 );
		}
		return str;
	}

	private static var uniqueSerialNumber:int = 1;

	private static function getUniqueSerialNumber():int {
		return uniqueSerialNumber++;
	}

	public static function generateGUID():String {
		// http://en.wikipedia.org/wiki/Globally_unique_identifier
		// This one is actually more unorthodox
		var i:int;
		var nums:Vector.<int> = new Vector.<int>();
		nums.push( getUniqueSerialNumber() );
		nums.push( getUniqueSerialNumber() );
		for ( i = 0; i < 10; i++ ) {
			nums.push( Math.round( Math.random() * 255 ) );
		}

		var strs:Vector.<String> = new Vector.<String>();
		for ( i = 0; i < nums.length; i++ ) {
			strs.push( ("00" + nums[i].toString( 16 )).substr( -2, 2 ) );
		}
		var now:Date = new Date();

		var secs:String = ("0000" + now.getMilliseconds().toString( 16 )).substr( -4, 4 );

		// 4-2-2-6
		return strs[0] + strs[1] + strs[2] + strs[3] + "-" + secs + "-" + strs[4] + strs[5] + "-" + strs[6] + strs[7] + strs[8] + strs[9] + strs[10] + strs[11];
	}



	/**
	 * Replaces the html entities with the utf-8 character.
	 * Kinda slow method.
	 * @param str
	 * @return
	 */
	public static function parseHtmlEntities( str:String, greek:Boolean=false, unicode:Boolean=false ):String {
		str=replace( str, "&quot;", "\"" );
		str=replace( str, "&amp;", "&" );
		str=replace( str, "&apos;", "'" );
		str=replace( str, "&lt;", "<" );
		str=replace( str, "&gt;", ">" );
		str=replace( str, "&nbsp;", " " );
		str=replace( str, "&iexcl;", "¡" );
		str=replace( str, "&cent;", "¢" );
		str=replace( str, "&pound;", "£" );
		str=replace( str, "&curren;", "¤" );
		str=replace( str, "&yen;", "¥" );
		str=replace( str, "&brvbar;", "¦" );
		str=replace( str, "&sect;", "§" );
		str=replace( str, "&uml;", "¨" );
		str=replace( str, "&copy;", "©" );
		str=replace( str, "&ordf;", "ª" );
		str=replace( str, "&laquo;", "«" );
		str=replace( str, "&not;", "¬" );
		str=replace( str, "&reg;", "®" );
		str=replace( str, "&macr;", "¯" );
		str=replace( str, "&deg;", "°" );
		str=replace( str, "&plusmn;", "±" );
		str=replace( str, "&sup2;", "²" );
		str=replace( str, "&sup3;", "³" );
		str=replace( str, "&acute;", "´" );
		str=replace( str, "&micro;", "µ" );
		str=replace( str, "&para;", "¶" );
		str=replace( str, "&middot;", "·" );
		str=replace( str, "&cedil;", "¸" );
		str=replace( str, "&sup1;", "¹" );
		str=replace( str, "&ordm;", "º" );
		str=replace( str, "&raquo;", "»" );
		str=replace( str, "&frac14;", "¼" );
		str=replace( str, "&frac12;", "½" );
		str=replace( str, "&frac34;", "¾" );
		str=replace( str, "&iquest;", "¿" );
		str=replace( str, "&Agrave;", "À" );
		str=replace( str, "&Aacute;", "Á" );
		str=replace( str, "&Acirc;", "Â" );
		str=replace( str, "&Atilde;", "Ã" );
		str=replace( str, "&Auml;", "Ä" );
		str=replace( str, "&Aring;", "Å" );
		str=replace( str, "&AElig;", "Æ" );
		str=replace( str, "&Ccedil;", "Ç" );
		str=replace( str, "&Egrave;", "È" );
		str=replace( str, "&Eacute;", "É" );
		str=replace( str, "&Ecirc;", "Ê" );
		str=replace( str, "&Euml;", "Ë" );
		str=replace( str, "&Igrave;", "Ì" );
		str=replace( str, "&Iacute;", "Í" );
		str=replace( str, "&Icirc;", "Î" );
		str=replace( str, "&Iuml;", "Ï" );
		str=replace( str, "&ETH;", "Ð" );
		str=replace( str, "&Ntilde;", "Ñ" );
		str=replace( str, "&Ograve;", "Ò" );
		str=replace( str, "&Oacute;", "Ó" );
		str=replace( str, "&Ocirc;", "Ô" );
		str=replace( str, "&Otilde;", "Õ" );
		str=replace( str, "&Ouml;", "Ö" );
		str=replace( str, "&times;", "×" );
		str=replace( str, "&Oslash;", "Ø" );
		str=replace( str, "&Ugrave;", "Ù" );
		str=replace( str, "&Uacute;", "Ú" );
		str=replace( str, "&Ucirc;", "Û" );
		str=replace( str, "&Uuml;", "Ü" );
		str=replace( str, "&Yacute;", "Ý" );
		str=replace( str, "&THORN;", "Þ" );
		str=replace( str, "&szlig;", "ß" );
		str=replace( str, "&agrave;", "à" );
		str=replace( str, "&aacute;", "á" );
		str=replace( str, "&acirc;", "â" );
		str=replace( str, "&atilde;", "ã" );
		str=replace( str, "&auml;", "ä" );
		str=replace( str, "&aring;", "å" );
		str=replace( str, "&aelig;", "æ" );
		str=replace( str, "&ccedil;", "ç" );
		str=replace( str, "&egrave;", "è" );
		str=replace( str, "&eacute;", "é" );
		str=replace( str, "&ecirc;", "ê" );
		str=replace( str, "&euml;", "ë" );
		str=replace( str, "&igrave;", "ì" );
		str=replace( str, "&iacute;", "í" );
		str=replace( str, "&icirc;", "î" );
		str=replace( str, "&iuml;", "ï" );
		str=replace( str, "&eth;", "ð" );
		str=replace( str, "&ntilde;", "ñ" );
		str=replace( str, "&Ntilde;", "Ñ" );
		str=replace( str, "&ograve;", "ò" );
		str=replace( str, "&oacute;", "ó" );
		str=replace( str, "&ocirc;", "ô" );
		str=replace( str, "&otilde;", "õ" );
		str=replace( str, "&ouml;", "ö" );
		str=replace( str, "&divide;", "÷" );
		str=replace( str, "&oslash;", "ø" );
		str=replace( str, "&Ugrave;", "ù" );
		str=replace( str, "&Uacute;", "ú" );
		str=replace( str, "&Ucirc;", "û" );
		str=replace( str, "&Uuml;", "ü" );
		str=replace( str, "&yacute;", "ý" );
		str=replace( str, "&thorn;", "þ" );
		str=replace( str, "&yuml;", "ÿ" );
		str=replace( str, "&OElig;", "Œ" );
		str=replace( str, "&oelig;", "œ" );
		str=replace( str, "&Scaron;", "Š" );
		str=replace( str, "&scaron;", "š" );
		str=replace( str, "&Yuml;", "Ÿ" );
		str=replace( str, "&fnof;", "ƒ" );
		str=replace( str, "&circ;", "ˆ" );
		str=replace( str, "&tilde;", "˜" )
		if( greek ){
			str=replace( str, "&Alpha;", "Α" );
			str=replace( str, "&Beta;", "Β" );
			str=replace( str, "&Gamma;", "Γ" );
			str=replace( str, "&Delta;", "Δ" );
			str=replace( str, "&Epsilon;", "Ε" );
			str=replace( str, "&Zeta;", "Ζ" );
			str=replace( str, "&Eta;", "Η" );
			str=replace( str, "&Theta;", "Θ" );
			str=replace( str, "&Iota;", "Ι" );
			str=replace( str, "&Kappa;", "Κ" );
			str=replace( str, "&Lambda;", "Λ" );
			str=replace( str, "&Mu;", "Μ" );
			str=replace( str, "&Nu;", "Ν" );
			str=replace( str, "&Xi;", "Ξ" );
			str=replace( str, "&Omicron;", "Ο" );
			str=replace( str, "&Pi;", "Π" );
			str=replace( str, "&Rho;", "Ρ" );
			str=replace( str, "&Sigma;", "Σ" );
			str=replace( str, "&Tau;", "Τ" );
			str=replace( str, "&Upsilon;", "Υ" );
			str=replace( str, "&Phi;", "Φ" );
			str=replace( str, "&Chi;", "Χ" );
			str=replace( str, "&Psi;", "Ψ" );
			str=replace( str, "&Omega;", "Ω" );
			str=replace( str, "&alpha;", "α" );
			str=replace( str, "&beta;", "β" );
			str=replace( str, "&gamma;", "γ" );
			str=replace( str, "&delta;", "δ" );
			str=replace( str, "&epsilon;", "ε" );
			str=replace( str, "&zeta;", "ζ" );
			str=replace( str, "&eta;", "η" );
			str=replace( str, "&theta;", "θ" );
			str=replace( str, "&iota;", "ι" );
			str=replace( str, "&kappa;", "κ" );
			str=replace( str, "&lambda;", "λ" );
			str=replace( str, "&mu;", "μ" );
			str=replace( str, "&nu;", "ν" );
			str=replace( str, "&xi;", "ξ" );
			str=replace( str, "&omicron;", "ο" );
			str=replace( str, "&pi;", "π" );
			str=replace( str, "&rho;", "ρ" );
			str=replace( str, "&sigmaf;", "ς" );
			str=replace( str, "&sigma;", "σ" );
			str=replace( str, "&tau;", "τ" );
			str=replace( str, "&upsilon;", "υ" );
			str=replace( str, "&phi;", "φ" );
			str=replace( str, "&chi;", "χ" );
			str=replace( str, "&psi;", "ψ" );
			str=replace( str, "&omega;", "ω" );
			str=replace( str, "&thetasym;", "ϑ" );
			str=replace( str, "&Upsih;", "ϒ" );
			str=replace( str, "&piv;", "ϖ" );
		}
		str=replace( str, "&ndash;", "–" );
		str=replace( str, "&mdash;", "—" );
		str=replace( str, "&lsquo;", "‘" );
		str=replace( str, "&rsquo;", "’" );
		str=replace( str, "&sbquo;", "‚" );
		str=replace( str, "&ldquo;", "“" );
		str=replace( str, "&rdquo;", "”" );
		str=replace( str, "&bdquo;", "„" );
		str=replace( str, "&dagger;", "†" );
		str=replace( str, "&Dagger;", "‡" );
		str=replace( str, "&bull;", "•" );
		str=replace( str, "&hellip;", "…" );
		str=replace( str, "&permil;", "‰" );
		str=replace( str, "&prime;", "′" );
		str=replace( str, "&Prime;", "″" );
		str=replace( str, "&lsaquo;", "‹" );
		str=replace( str, "&rsaquo;", "›" );
		str=replace( str, "&oline;", "‾" );
		str=replace( str, "&frasl;", "⁄" );
		str=replace( str, "&euro;", "€" );
		if( unicode ){
			str=replace( str, "&image;", "ℑ" );
			str=replace( str, "&weierp;", "℘" );
			str=replace( str, "&real;", "ℜ" );
			str=replace( str, "&trade;", "™" );
			str=replace( str, "&alefsym;", "ℵ" );
			str=replace( str, "&larr;", "←" );
			str=replace( str, "&uarr;", "↑" );
			str=replace( str, "&rarr;", "→" );
			str=replace( str, "&darr;", "↓" );
			str=replace( str, "&harr;", "↔" );
			str=replace( str, "&crarr;", "↵" );
			str=replace( str, "&lArr;", "⇐" );
			str=replace( str, "&UArr;", "⇑" );
			str=replace( str, "&rArr;", "⇒" );
			str=replace( str, "&dArr;", "⇓" );
			str=replace( str, "&hArr;", "⇔" );
			str=replace( str, "&forall;", "∀" );
			str=replace( str, "&part;", "∂" );
			str=replace( str, "&exist;", "∃" );
			str=replace( str, "&empty;", "∅" );
			str=replace( str, "&nabla;", "∇" );
			str=replace( str, "&isin;", "∈" );
			str=replace( str, "&notin;", "∉" );
			str=replace( str, "&ni;", "∋" );
			str=replace( str, "&prod;", "∏" );
			str=replace( str, "&sum;", "∑" );
			str=replace( str, "&minus;", "−" );
			str=replace( str, "&lowast;", "∗" );
			str=replace( str, "&radic;", "√" );
			str=replace( str, "&prop;", "∝" );
			str=replace( str, "&infin;", "∞" );
			str=replace( str, "&ang;", "∠" );
			str=replace( str, "&and;", "∧" );
			str=replace( str, "&or;", "∨" );
			str=replace( str, "&cap;", "∩" );
			str=replace( str, "&cup;", "∪" );
			str=replace( str, "&int;", "∫" );
			str=replace( str, "&there4;", "∴" );
			str=replace( str, "&sim;", "∼" );
			str=replace( str, "&cong;", "≅" );
			str=replace( str, "&asymp;", "≈" );
			str=replace( str, "&ne;", "≠" );
			str=replace( str, "&equiv;", "≡" );
			str=replace( str, "&le;", "≤" );
			str=replace( str, "&ge;", "≥" );
			str=replace( str, "&sub;", "⊂" );
			str=replace( str, "&sup;", "⊃" );
			str=replace( str, "&nsub;", "⊄" );
			str=replace( str, "&sube;", "⊆" );
			str=replace( str, "&supe;", "⊇" );
			str=replace( str, "&oplus;", "⊕" );
			str=replace( str, "&otimes;", "⊗" );
			str=replace( str, "&perp;", "⊥" );
			str=replace( str, "&sdot;", "⋅" );
			str=replace( str, "&lceil;", "⌈" );
			str=replace( str, "&rceil;", "⌉" );
			str=replace( str, "&lfloor;", "⌊" );
			str=replace( str, "&rfloor;", "⌋" );
			str=replace( str, "&lang;", "⟨" );
			str=replace( str, "&rang;", "⟩" );
			str=replace( str, "&loz;", "◊" );
			str=replace( str, "&spades;", "♠" );
			str=replace( str, "&clubs;", "♣" );
			str=replace( str, "&hearts;", "♥" );
			str=replace( str, "&diams;", "♦" );
		}

		return str ;

		/*
		 -- old
		 str = replace( str, '&ntilde;', 'ñ' );
		 str = replace( str, '&Ntilde;', 'Ñ' );
		 str = replace( str, '&aacute;', 'á' );
		 str = replace( str, '&eacute;', 'é' );
		 str = replace( str, '&iacute;', 'í' );
		 str = replace( str, '&oacute;', 'ó' );
		 str = replace( str, '&uacute;', 'ú' );
		 str = replace( str, '&quot;', '\"' );
		 str = replace( str, '&nbsp;', '-' );
		 str = replace( str, '&frasl;', '/' );
		 str = replace( str, '&rdquo;', '”' );
		 str = replace( str, '&ldquo;', '“' );
		 str = replace( str, '&lsquo;', '‘' );
		 str = replace( str, '&rsquo;', '’' );
		 str = replace( str, '&sbquo;', '‚' );
		 str = replace( str, '&bdquo;', '„' );
		 str = replace( str, '&amp;', '&' );
		 str = replace( str, '&iexcl;', '¡' );
		 str = replace( str, '&iquest;', '¿' );
		 str = replace( str, '&ordm;', 'º' );
		 str = replace( str, '&copy;', '©' );
		 str = replace( str, '&ordf;', 'ª' );
		 str = replace( str, '&lt;', '<' );
		 str = replace( str, '&gt;', '>' );
		 str = replace( str, '&ndash;', '–' );
		 str = replace( str, '&Aacute;', 'Á' );
		 str = replace( str, '&Eacute;', 'É' );
		 str = replace( str, '&Iacute;', 'Í' );
		 str = replace( str, '&Oacute;', 'Ó' );
		 str = replace( str, '&Uacute;', 'Ú' );
		 str = replace( str, '&Auml;', 'Ä' );
		 str = replace( str, '&Euml;', 'Ë' );
		 str = replace( str, '&Iuml;', 'Ï' );
		 str = replace( str, '&Ouml;', 'Ö' );
		 str = replace( str, '&Uuml;', 'Ü' );
		 str = replace( str, '&auml;', 'ä' );
		 str = replace( str, '&euml;', 'ë' );
		 str = replace( str, '&iuml;', 'ï' );
		 str = replace( str, '&ouml;', 'ö' );
		 str = replace( str, '&uuml;', 'ü' );
		 str = replace( str, '&hellip;', '…' );*/
		return str;
	}




}
}


internal class N {
	public static const NAN:String = "not a number";
	public static const decimal:String = "point ";
	public static const negative:String = "negative ";

	public static const _0:String = "zero";
	public static const _1:String = "one ";
	public static const _2:String = "two ";
	public static const _3:String = "three ";
	public static const _4:String = "four ";
	public static const _5:String = "five ";
	public static const _6:String = "six ";
	public static const _7:String = "seven ";
	public static const _8:String = "eight ";
	public static const _9:String = "nine ";
	public static const _10:String = "ten ";
	public static const _11:String = "eleven ";
	public static const _12:String = "twelve ";
	public static const _13:String = "thirteen ";
	public static const _14:String = "fourteen ";
	public static const _15:String = "fifteen ";
	public static const _16:String = "sixteen ";
	public static const _17:String = "seventeen ";
	public static const _18:String = "eighteen ";
	public static const _19:String = "nineteen ";
	public static const _20:String = "twenty ";
	public static const _30:String = "thirty ";
	public static const _40:String = "fourty ";
	public static const _50:String = "fifty ";
	public static const _60:String = "sixty ";
	public static const _70:String = "seventy ";
	public static const _80:String = "eighty ";
	public static const _90:String = "ninety ";
	public static const _100:String = "hundred ";
	public static const _1000:String = "thousand ";
	public static const _1000000:String = "million ";
	public static const _1000000000:String = "billion ";
	public static const _1000000000000:String = "trillion ";
	public static const _1000000000000000:String = "quadrillion ";
	public static const _1000000000000000000:String = "quintillion ";
	public static const _1000000000000000000000:String = "sextillion ";
	public static const _1000000000000000000000000:String = "septillion ";
	public static const _1000000000000000000000000000:String = "octillion ";


	public static const _1to9:Vector.<String> = Vector.<String>( ["", _1, _2, _3, _4, _5, _6, _7, _8, _9] );
	public static const _10to19:Vector.<String> = Vector.<String>( [_10, _11, _12, _13, _14, _15, _16, _17, _18, _19] );
	public static const _10to90:Vector.<String> = Vector.<String>( ["", _10, _20, _30, _40, _50, _60, _70, _80, _90] );
	public static const _100to900:Vector.<String> = Vector.<String>( [
		"",
		_1 + _100,
		_2 + _100,
		_3 + _100,
		_4 + _100,
		_5 + _100,
		_6 + _100,
		_7 + _100,
		_8 + _100,
		_9 + _100
	] );

	public static const periods:Vector.<String> = Vector.<String>(
			[
				"",
				_1000,
				_1000000,
				_1000000000,
				_1000000000000,
				_1000000000000000,
				_1000000000000000000,
				_1000000000000000000000,
				_1000000000000000000000000,
				_1000000000000000000000000000
			] );
}
