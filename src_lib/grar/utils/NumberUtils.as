/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 7:00 PM.
 *
 */
package grar.utils {
public class NumberUtils {
	public function NumberUtils() {}


	//===================================================================================================================================================
	//
	//      ------  ROMAN NUMBERS
	//
	//===================================================================================================================================================
	public static function romanize( num: int ): String {
		const numerals: Array = [
			"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X",
			"IX", "V", "IV", "I"
		];

		const numbers: Array = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
		var roman: String = "";
		var i: int;
		for ( i = 0; i < numbers.length; i++ ) {
			while ( num >= numbers[i] ) {
				roman += numerals[i];
				num -= numbers[i];
			}
		}
		return roman;
	}

	public static function deromanize( roman: String ): int {

		var numerals: Array = ["I", "V", "X", "L", "C", "D", "M"];

		var numbers: Array = [1, 5, 10, 50, 100, 500, 1000];

		roman = roman.toUpperCase();

		var arabic: int = 0;

		var i: int = roman.length;

		var compare1: int;
		var compare2: int;

		while ( i-- ) {

			var letter: String;
			var listLetter: String;

			for ( var j: int = 0; j <= numerals.length - 1; j++ ) {
				letter = roman.charAt( i );
				listLetter = numerals[j];
				if ( listLetter == letter ) {
					compare1 = numbers[j];
				}
			}

			for ( j = 0; j <= numerals.length - 1; j++ ) {
				letter = roman.charAt( i + 1 );
				listLetter = numerals[j];
				if ( listLetter == letter ) {
					compare2 = numbers[j];
				}
			}

			if ( compare1 < compare2 )
				arabic -= compare1;
			else
				arabic += compare1;
		}
		return arabic;
	}

	//===================================================================================================================================================
	//
	//      ------  NUMBER STATUS ODD, EVEN, POSITIVE, NEGATIVE, etc.
	//
	//===================================================================================================================================================
	/**
	 * Determines whether or not the supplied Number is positive.
	 * @param value Number to evaluate
	 * @return Whether or not the supplied Number is positive
	 * @author Aaron Clinger
	 * @author Shane McCartney
	 * @author David Nelson
	 */
	public function isPositive( value: Number ): Boolean {
		return Boolean( value >= 0 );
	}

	/**
	 * Determines whether or not the supplied Number is negative.
	 * @param value Number to evaluate
	 * @return Whether or not the supplied Number is negative
	 * @author Aaron Clinger
	 * @author Shane McCartney
	 * @author David Nelson
	 */
	public function isNegative( value: Number ): Boolean {
		return !isPositive( value );
	}

	/**
	 Determines if the number is odd.

	 @param value: A number to determine if it is not divisible by <code>2</code>.
	 @return Returns <code>true</code> if the number is odd; otherwise <code>false</code>.
	 @example
	 <code>
	 trace(NumberUtil.isOdd(7)); // Traces true
	 trace(NumberUtil.isOdd(12)); // Traces false
	 </code>
	 */
	public function isOdd( value: Number ): Boolean {
		return !isEven( value );
	}

	/**
	 Determines if the number is an integer.

	 @param value: A number to determine if it contains no decimal values.
	 @return Returns <code>true</code> if the number is an integer; otherwise <code>false</code>.
	 @example
	 <code>
	 trace(NumberUtil.isInteger(13)); // Traces true
	 trace(NumberUtil.isInteger(1.2345)); // Traces false
	 </code>
	 */
	public function isInteger( value: Number ): Boolean {
		return (value % 1) == 0;
	}

	/**
	 Determines if the value is included within a range.

	 @param value: Number to determine if it is included in the range.
	 @param firstValue: First value of the range.
	 @param secondValue: Second value of the range.
	 @return Returns <code>true</code> if the number falls within the range; otherwise <code>false</code>.
	 @usageNote The range values do not need to be in order.
	 @example
	 <code>
	 trace(NumberUtil.isBetween(3, 0, 5)); // Traces true
	 trace(NumberUtil.isBetween(7, 0, 5)); // Traces false
	 </code>
	 */
	public function isBetween( value: Number, firstValue: Number, secondValue: Number ): Boolean {
		return !(value < Math.min( firstValue, secondValue ) || value > Math.max( firstValue, secondValue ));
	}

	/**
	 Determines if the two values are equal, with the option to define the precision.

	 @param val1: A value to compare.
	 @param val2: A value to compare.
	 @param precision: The maximum amount the two values can differ and still be considered equal.
	 @return Returns <code>true</code> the values are equal; otherwise <code>false</code>.
	 @example
	 <code>
	 trace(NumberUtil.isEqual(3.042, 3, 0)); // Traces false
	 trace(NumberUtil.isEqual(3.042, 3, 0.5)); // Traces true
	 </code>
	 */
	public function isEqual( val1: Number, val2: Number, precision: Number = 0 ): Boolean {
		return Math.abs( val1 - val2 ) <= Math.abs( precision );
	}

	/**
	 Determines if the number is even.

	 @param value: A number to determine if it is divisible by <code>2</code>.
	 @return Returns <code>true</code> if the number is even; otherwise <code>false</code>.
	 @example
	 <code>
	 trace(NumberUtil.isEven(7)); // Traces false
	 trace(NumberUtil.isEven(12)); // Traces true
	 </code>
	 */
	public function isEven( value: Number ): Boolean {
		return (value & 1) == 0;
	}

	/**
	 Determines if the number is prime.

	 @param value: A number to determine if it is only divisible by <code>1</code> and itself.
	 @return Returns <code>true</code> if the number is prime; otherwise <code>false</code>.
	 @example
	 <code>
	 trace(NumberUtil.isPrime(13)); // Traces true
	 trace(NumberUtil.isPrime(4)); // Traces false
	 </code>
	 */
	public function isPrime( value: Number ): Boolean {
		if ( value == 1 || value == 2 ) return true;
		if ( isEven( value ) ) return false;
		var s: Number = Math.sqrt( value );
		for ( var i: Number = 3; i <= s; i++ ) if ( value % i == 0 ) return false;
		return true;
	}

}
}
