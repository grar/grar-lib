/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 10/9/15 6:27 PM.
 */
package grar.utils {
import flash.desktop.NativeApplication;
import flash.display.Sprite;
import flash.display.Stage;
import flash.display.StageDisplayState;
import flash.display.StageOrientation;
import flash.events.Event;

import org.osflash.signals.Signal;

public class Display {

	private static var _stage:Stage;
	private static var _blocker:Sprite;
	private static var _isBlocked:Boolean;

	// we use main to scale the content accordengly if needed, usually is the StarlingBootstrap class Sprite.
	public static var main:Sprite;

	public static var onStageResize:Signal = new Signal( uint, uint );
	public static var onFullscreen:Signal = new Signal( Boolean );

	public static var originalStageW:uint;
	public static var originalStageH:uint;


	public function Display() {}


	/**
	 * Initializer of the stage.
	 * @param stage
	 * @param align
	 * @param scaleMode
	 * @param quality
	 */
	public static function init( stage:Stage, align:String = 'tl', scaleMode:String = 'noScale',
								 quality:String = 'high' ):void {
		// already inited.
		if ( _stage ) return;

		_stage = stage;
		_stage.align = align;
		_stage.scaleMode = scaleMode;
		_stage.quality = quality;
		_stage.showDefaultContextMenu = false;
		_stage.stageFocusRect = false;

		_stage.addEventListener( Event.RESIZE, handleStageResize );

		// init blocker, a generic approach to block the stage input interaction.
		_blocker = new Sprite();
		_blocker.visible = false;
		_blocker.name = '__stageBlocker';
		_blocker.graphics.beginFill( 0xFF0000, 0 );
		_blocker.graphics.drawRect( 0, 0, 100, 100 );
		_blocker.graphics.endFill();
		_stage.addChild( _blocker );

		// Code to force stage sizes on mobile. (apparently)
		var appXML:XML = NativeApplication.nativeApplication.applicationDescriptor;
		var ns:Namespace = appXML.namespace();
		var isFullScreen:Boolean = appXML.ns::initialWindow.ns::fullScreen == "true";
		var isLandscape:Boolean = appXML.ns::initialWindow.ns::aspectRatio == "landscape";

		if ( isFullScreen ) {
			stage.setOrientation( StageOrientation.DEFAULT );
			originalStageW = stage.fullScreenWidth;
			originalStageH = stage.fullScreenHeight;
			if ( isLandscape )
				stage.setOrientation( StageOrientation.ROTATED_RIGHT );
		} else {
			stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			stage.setOrientation( StageOrientation.DEFAULT );
			stage.displayState = StageDisplayState.NORMAL;
			originalStageW = stage.stageWidth;
			originalStageH = stage.stageHeight;
			if ( isLandscape )
				stage.setOrientation( StageOrientation.ROTATED_RIGHT );
		}
	}

	private static function handleStageResize( event:Event = null ):void {
		onStageResize.dispatch( _stage.stageWidth, _stage.stageHeight );
		if ( _isBlocked ) {
			_blocker.width = Display.stageWidth;
			_blocker.height = Display.stageHeight;
		}
	}

	public static function block(flag:Boolean):void {
		if( _isBlocked == flag ) return ;
		_isBlocked = flag ;
		_blocker.visible = _isBlocked ;
		if( _isBlocked ){
			_stage.addChild( _blocker );
			_blocker.width = Display.stageWidth;
			_blocker.height = Display.stageHeight;
		} else {
			// nothing.
		}
	}

	public static function get mouseX():Number {
		return _stage.mouseX
	}

	public static function get mouseY():Number {
		return _stage.mouseY
	}

	public static function get stageWidth():int {
		return _stage.stageWidth
	}

	public static function get stageHeight():int {
		return _stage.stageHeight
	}

	public static function get stage():Stage {
		return _stage
	}

	static public function get isBlocked():Boolean {
		return _isBlocked;
	}

	public static function get fullscreen():Boolean {
		return _stage.displayState == StageDisplayState.FULL_SCREEN_INTERACTIVE;
	}

	public static function set fullscreen( value:Boolean ):void {
		_stage.displayState = value ? StageDisplayState.FULL_SCREEN_INTERACTIVE : StageDisplayState.NORMAL;
		onFullscreen.dispatch( value );
	}

}
}
