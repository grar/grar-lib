/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 4:18 PM.
 *
 */
package grar.utils {
import flash.text.TextField;

public class TextFieldUtils {
	public function TextFieldUtils() {
	}


	public static function stripText( p_tf:TextField, p_str:String = "", p_dots:String = "..." ):Boolean {
		if ( p_str ) p_tf.text = p_str;
		if ( p_tf.textWidth < p_tf.width ) return false;
		var idx:int = p_tf.getCharIndexAtPoint( p_tf.width - 5, p_tf.textHeight * 0.5 );
		if ( !p_str ) p_str = p_tf.text;
		do {
			idx--;
			p_tf.text = p_str.substr( 0, idx ) + p_dots;
		} while ( p_tf.textWidth > p_tf.width ) ;
		return true;
	}


	public static function shrinkText( p_tf:TextField, p_str:String = "", p_size:int = 0 ):Boolean {
		if ( p_str ) p_tf.text = p_str;
		if ( p_size <= 0 ) p_size = p_tf.getTextFormat().size as int;
		if ( p_tf.textWidth < p_tf.width ) return false;
		if ( !p_str ) p_str = p_tf.text;
		var points:int = p_size;
		do {
			points--;
			p_tf.htmlText = '<font size="' + points + '">' + p_str + '</font>';
		} while ( p_tf.textWidth >= p_tf.width ) ;
//			trace(points)
		return true;
	}


	public static function fillText( _textfieldRef:TextField, _str:String = "", _breakWords:Boolean = true,
									 _dots:String = "..." ):Boolean {
//		var useHTML:Boolean = false ;
		var buildStr:String = _str;
		if ( _textfieldRef.htmlText ) {
			buildStr = _textfieldRef.htmlText;
		}
		_textfieldRef.text = StringUtils.trim( buildStr );
		var endWordIndex:Number;
		var multiline:Boolean = _textfieldRef.multiline;
		if ( (!multiline && _textfieldRef.maxScrollH > 0) || (multiline && _textfieldRef.maxScrollV > 1) ) {
			while ( buildStr.length > 0 ) {
				if ( _breakWords ) {
					buildStr = buildStr.substr( 0, buildStr.length - 1 );
				} else {
					endWordIndex = Math.max( buildStr.lastIndexOf( " " ), 0 );
					buildStr = buildStr.substr( 0, endWordIndex );
				}
				_textfieldRef.text = StringUtils.trim( buildStr ) + _dots;
				if ( (!multiline && _textfieldRef.maxScrollH <= 0) || (multiline && _textfieldRef.maxScrollV <= 1) ) {
					break;
				}
			}
		}

		if ( buildStr != _str ) {
			return (true);
		}
		return (false);
	}
}
}
