package grar.utils {

import flash.utils.getTimer;

public class TimeUtils {

	// - not used.
	public function TimeUtils() {}

	// A safe getTimer() - runs for ~1192 hours instead of ~596
	public static function getUInt():uint {
		var v:int = getTimer();
		return v < 0 ? int.MAX_VALUE + 1 + v - int.MIN_VALUE : v;
	}

}

}
