package grar.utils {
import flash.geom.Point;

public class RandomGenerator {

	// Temp properties for speed
	private static var a:Number;
	private static var b:Number;
	private static var c:Number;
	private static var p:Number;

	private static const TWO_PI:Number = 2 * Math.PI;

	public static function getInCircle( radius:Number ):Point {
		// http://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly

		// Uniform generator (radius-angle would concentrate in middle)
		a = Math.random();
		b = Math.random();
		if ( a > b ) {
			c = b;
			b = a;
			a = c;
		}

		p = TWO_PI * a / b;

		return new Point( b * radius * Math.cos( p ), b * radius * Math.sin( p ) );
	}

	public static function randomSign( chance: Number = 0.5 ): int {
		return(Math.random() < chance) ? 1 : -1;
	}

	public static function getInRange( min:Number, max:Number ):Number {
		return min + Math.random() * (max - min);
	}

	public static function getInIntegerRange( min:Number, max:Number ):Number {
		return Math.round( min + Math.random() * (max - min) );
	}

	public static function getFromVector( vector:Vector.<*> ):Vector.<*> {
		return vector[Math.floor( Math.random() * vector.length )];
	}

	public static function getFromArray( array:Array ):* {
		return array[Math.floor( Math.random() * array.length )];
	}

	public static function getColor():uint {
		return (Math.random() * 0xffffff) & 0xffffff;
	}

	public static function getBoolean():Boolean {
		return Math.random() > 0.5;
	}

	public static function getFromSeed( seed:int = -1 ):Number {
		// Return a predictable pseudo-random number (0..0.999)
		if ( seed < 0 ) {
			return Math.random();
		} else {
			return ((seed * 1.12836) + 0.7) % 1;
		}
	}
}
}
