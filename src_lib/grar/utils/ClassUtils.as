/**
 * Created by tasos on 25/10/15 21:24.
 */
package grar.utils {

import flash.utils.ByteArray;
import flash.utils.Dictionary;
import flash.utils.describeType;
import flash.utils.getDefinitionByName;
import flash.utils.getQualifiedClassName;

public class ClassUtils {

//	[Inline]
//	public static function getClassName( obj:Object ):String {
//		if ( !obj ) {
//			return null;
//		}
//		var s:String = Object( obj ).constructor;
//		var ss:Array = s.split( " " );
//		return ss[1].substring( 0, ss[1].length - 1 );
//	}

	[Inline]
	public static function getClassName( obj:* ):String {
		return getQualifiedClassName( obj );
	}

	[Inline]
	public static function getClassNameSimple( obj:* ):String {
		return ClassUtils.getClassName( obj ).split( "::" )[1];
	}

	[Inline]
	public static function getClass( obj:* ):Class {
		return Class( getDefinitionByName( getClassName( obj ) ) );
	}

	[Inline]
	public static function ofSameClass( obj1:*, obj2:* ):Boolean {
		if ( !obj1 && !obj2 ) {
			return NaN;
		} // cannot tell
		if ( obj1 && !obj2 || !obj1 && obj2 ) {
			return false;
		}
		return ( obj1 is getClass( obj2 ) );
	}

	// Dictionary (containing each class) of dictionaries (containing each field), which maps name to type (String)
	private static var fieldInfoCache:Dictionary = new Dictionary( true );

	/**
	 * Return a dictionary describing every settable field on this object or class.
	 * Fields are indexed by name, and the type is contained as a string.
	 * Typical usage:
	 * var dict:Dictionary = ClassUtils.getFieldsOfClass(myObject);
	 *    trace("Field lala is of type " + dict["lala"]);
	 */
	public static function getFieldsOfClass( c:* ):Dictionary {
		if ( !(c is Class) ) {
			// Convert to its class
			c = getDefinitionByName( getQualifiedClassName( c ) );
		}

		// If cached return it...
		if ( fieldInfoCache.hasOwnProperty( c ) ) {
			return fieldInfoCache[c];
		}

		// ...otherwise describe the type
		var typeXml:XML = describeType( c );

		// Set up the dictionary
		var typeDict:Dictionary = new Dictionary();

		// Walk all the variables
		for each ( var variable:XML in typeXml.factory.variable ) {
			typeDict[variable.@name.toString()] = variable.@type.toString();
		}

		// And all the accessors
		for each ( var accessor:XML in typeXml.factory.accessor ) {
			// Ignore ones we can't write to.
			if ( accessor.@access == "readonly" ) {
				continue;
			}

			typeDict[accessor.@name.toString()] = accessor.@type.toString();
		}

		// Store it in the cache
		fieldInfoCache[c] = typeDict;

		return typeDict;
	}

	/**
	 * Clones an object
	 */
	public static function clone(obj:Object):Object {
		const byte_arr:ByteArray = new ByteArray();
		byte_arr.writeObject(obj);
		byte_arr.position = 0;
		return byte_arr.readObject();
	}
 
	/**
	 * Converts an object to string, by getting deep into the dependency hierarchy
	 * @param obj    Object to scan
	 * @param depth  Depth of dependencies to dig into
	 * @param prefix String to prepend the printouts
	 */
	public static function scan(obj:Object, depth:int, prefix:String=""):String {
		if (depth < 1) return String(obj);
 
		const classDef:XML = describeType(obj);
 
		var str:String = "";
		for each (var variable:XML in classDef.variable) {
			str += prefix + variable.@name + " : " + scan(obj[variable.@name], depth - 1, prefix + "\t") + "\n";
		}
 
//		for each (var accessor:XML in classDef.accessor.(@access == "readwrite" || @access == "readonly")) {
//			str += prefix + accessor.@name + " : " + scan(obj[accessor.@name], depth - 1, prefix + "\t") + "\n";
//		}
 
		for (var s:* in obj) {
			str += prefix + s + " = " + scan(obj[s], depth - 1, prefix + "\t") + "\n";
		}
		return str != "" ? ("[" + classDef.@name + "] {\n" + str + prefix + "}\n") : ((obj != null) ? obj + "" : "null");
	}

	/**
	 * Converts object to a string of form: PROP: VAL<delimiter>PROP: VAL... where:
	 * @param obj Object to convert
	 * @param delimiter (optional) Delimiter of property/value pairs
	 * @return A string of all delimited property/value pairs
	 */
	public static function toString(obj:Object=null, delimiter:String = "\n"): String {
		if (!obj || !delimiter) {
			return "";
		}
		else {
			var ret:Array = [];
			for (var s:Object in obj) {
				ret.push(s + ": " + obj[s]);
			}
			return ret.join(delimiter);
		}
	}

	/**
	 * Merge two objects
	 * (adds or overwrites properties of the 1st object with properties from the 2nd)
	 *   @param obj1 Object to fill/overwrite
	 *   @param obj2 Object to get data from
	 */
	public static function merge(obj1:Object, obj2:Object):void {
		if (!obj2) return;
		if (!obj1) obj1 = {};
		for (var p:* in obj2) {
			obj1[p] = obj2[p];
		}
	}

}

}
