/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 5:34 PM.
 *
 */
package grar.utils {
import flash.utils.getTimer;

public class DateUtils {
	public function DateUtils() {}

	// Replace MONTHS or DAYS with Locale if needed.
	public static var MONTHS:Array =
			[
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			];

	public static var DAYS:Array =
			["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

	private static const TIMEZONES:Array = [
		'IDLW',
		'NT',
		'HST',
		'AKST',
		'PST',
		'MST',
		'CST',
		'EST',
		'AST',
		'ADT',
		'AT',
		'WAT',
		'GMT',
		'CET',
		'EET',
		'MSK',
		'ZP4',
		'ZP5',
		'ZP6',
		'WAST',
		'WST',
		'JST',
		'AEST',
		'AEDT',
		'NZST'
	];

	public static function substraction( date1:Date, date2:Date ):Date {
		var result:Date = new Date();
		result.time = date1.time - date2.time;
		return result;
	}

	public static function msecondsToTimecode( ms:Number, includeHours:Boolean = true ):String {
		var mseconds:Number = ms;
		var seconds:uint = ms / 1000;
		var minutes:uint = seconds / 60;
		var hours:uint = minutes / 60;

		//Storing the remainder of this division problem
		mseconds %= 1000;

		seconds %= 60;
		minutes %= 60;
		hours %= 24;

		//Converting numerical values into strings so that
		//we string all of these numbers together for the display
		var msString:String = int( mseconds ).toString();
		var sec:String = seconds.toString();
		var min:String = minutes.toString();
		var hrs:String = hours.toString();

		//Setting up a few restrictions for when the current time reaches a single digit
		if ( msString.length < 2 ) msString = "0" + msString;
		msString = msString.substr( 0, 2 );
		if ( sec.length < 2 ) sec = "0" + sec;
		if ( min.length < 2 ) min = "0" + min;
		if ( hrs.length < 2 ) hrs = "0" + hrs;
		//Stringing all of the numbers together for the display
		var time:String = hrs + ":" + min + ":" + sec + ":" + msString;
		//Setting the string to the display
		return time;

	}

	public static function addition( date1:Date, date2:Date ):Date {
		var result:Date = new Date();
		result.time = date1.time + date2.time;
		return result;
	}

	public static function timecodeToSeconds( stringTimecode:String ):Number {
		// Returns number of seconds based on a string
		// Examples:
		// 01:30 -> returns 90
		// 30 -> returns 30
		// 90 -> returns 90
		var num:Number = 0;
		var cs:Array = stringTimecode.split( ":" );
		for ( var i:int = 0; i < cs.length; i++ ) {
			num += parseFloat( cs[i] ) * Math.pow( 60, cs.length - i - 1 );
		}

		/*public static function timecodeToSeconds( tcStr:String ):Number {
		 var t:Array = tcStr.split( ":" );
		 return (t[0] * 3600 + t[1] * 60 + t[2] * 1);
		 }*/

		return num;
	}


	public static function xsdDateTimeToDate( dateString:String ):Date {
		// TODO: use RFC 3339? http://www.ietf.org/rfc/rfc3339.txt
		// Converts a data from DateTime XML format to a normal Date
		// This should be the final, universal one
		// Example: 2010-06-30T21:19:01.000Z
		// Or:      2002-05-30T09:30:10.5
		// Or:      2002-05-30T09:30:10Z
		// Or:      2002-05-30T09:30:10-06:00
		// Or:      2002-05-30T09:30:10+06:00
		// Or:      2010-08-13
		// Reference: http://www.w3schools.com/Schema/schema_dtypes_date.asp
		// http://books.xmlschemata.org/relaxng/ch19-77049.html
		// [-]CCYY-MM-DDThh:mm:ss[Z|(+|-)hh:mm]

		// RFC3339 = "2006-01-02T15:04:05Z07:00"

		if ( !Boolean( dateString ) || dateString.length < 10 ) return null;

		var year:int = 0;
		var month:int = 0;
		var day:int = 0;
		var hours:int = 0;
		var minutes:int = 0;
		var seconds:Number = 0;

		var timeZoneHour:int = 0;
		var timeZoneMinutes:int = 0;

		var readOffset:Number = 0;

		// Finally, parses the date

		year = parseInt( dateString.substr( 0, 4 ), 10 );
		month = parseInt( dateString.substr( 5, 2 ), 10 ) - 1;
		day = parseInt( dateString.substr( 8, 2 ), 10 );

		readOffset = 10;

		// Parses the time if available
		if ( dateString.length > readOffset && dateString.substr( readOffset, 1 ).toUpperCase() == "T" ) {
			hours = parseInt( dateString.substr( 11, 2 ), 10 );
			minutes = parseInt( dateString.substr( 14, 2 ), 10 );

			var secondsSize:int = 0;
			var cutPos:int = dateString.length;
			var zPos:int = dateString.toUpperCase().indexOf( "Z", 19 );
			var plusPos:int = dateString.indexOf( "+", 19 );
			var minusPos:int = dateString.indexOf( "-", 19 );

			if ( zPos > -1 ) cutPos = Math.min( cutPos, zPos );
			if ( plusPos > -1 ) cutPos = Math.min( cutPos, plusPos );
			if ( minusPos > -1 ) cutPos = Math.min( cutPos, minusPos );

			secondsSize = cutPos - 17;
			seconds = parseFloat( dateString.substr( 17, secondsSize ) );

			readOffset = 17 + secondsSize;
		}

		if ( dateString.length > readOffset ) {
			var lastSymbol:String = dateString.substr( readOffset, 1 ).toUpperCase();

			if ( lastSymbol == "Z" ) {
				// Universal time
			} else {
				if ( lastSymbol == "+" || lastSymbol == "-" ) {
					// Time adjustments
					var hourSize:int = dateString.indexOf( ":", readOffset ) - readOffset;
					if ( readOffset > 0 ) {
						timeZoneHour = parseInt( dateString.substr( readOffset, hourSize ), 10 );
						timeZoneMinutes = parseInt( dateString.substr( readOffset + hourSize + 1 ), 10 );
					}
				}
			}
		}

		// Set the date
		var tt:Date = new Date( 0 );

		tt.fullYearUTC = year;
		tt.monthUTC = month;
		tt.dateUTC = day;
		tt.hoursUTC = hours;
		tt.minutesUTC = minutes;
		tt.secondsUTC = Math.floor( seconds );
		tt.millisecondsUTC = (seconds - Math.floor( seconds )) * 1000;

		var minutesOffset:int = timeZoneHour * 60 + timeZoneMinutes;

		tt.time -= minutesOffset * 60 * 1000;

		return tt;
	}

	public static function descriptiveDifference( relativeDate:Date ):String {
		// Returns a friendly description of a time difference ("2 hours", "1 day", "10 seconds", "1 year" etc)
		// Full data
		var seconds:Number = relativeDate.time / 1000;
		var minutes:Number = seconds / 60;
		var hours:Number = minutes / 60;
		var days:Number = hours / 24;
		//var weeks:Number = days / 7;
		var months:Number = days / (365.25 / 12);
		var years:Number = days / 365.25;

		seconds = Math.floor( seconds );
		minutes = Math.floor( minutes );
		hours = Math.floor( hours );
		days = Math.floor( days );
		months = Math.floor( months );
		years = Math.floor( years );

		if ( years > 1 )        return years + " years";
		if ( years == 1 )        return years + " year";
		if ( months > 1 )        return months + " months";
		if ( months == 1 )    return months + " month";
		//if (weeks > 1)		return weeks + " weeks";
		//if (weeks == 1)		return weeks + " week";
		if ( days > 1 )        return days + " days";
		if ( days == 1 )        return days + " day";
		if ( hours > 1 )        return hours + " hours";
		if ( hours == 1 )        return hours + " hour";
		if ( minutes > 1 )    return minutes + " minutes";
		if ( minutes == 1 )    return minutes + " minute";
		if ( seconds > 1 )    return seconds + " seconds";
		if ( seconds == 1 )    return seconds + " second";
		return "";
	}


	/**
	 Determines the time zone of the user from a Date object.
	 @param date: Date object to find the time zone of.
	 @return Returns the time zone abbreviation.
	 @example
	 <code>
	 trace(DateUtil.getTimezone(new Date()));
	 </code>
	 */
	public static function getTimezone( date:Date ):String {
		var hour:uint = Math.round( 12 + -(date.getTimezoneOffset() / 60) );
		if ( isDaylightSavings( date ) ) hour--;
		return TIMEZONES[hour];
	}

	/**
	 Determines if or not the date is in daylight saving time.
	 @param date: Date to find if it is during daylight savings time.
	 @return Returns <code>true</code> if daylight savings time; otherwise <code>false</code>.
	 */
	public static function isDaylightSavings( date:Date ):Boolean {
		var months:uint = 12;
		var offset:uint = date.getTimezoneOffset();
		var offsetCheck:Number;
		while ( months-- ) {
			offsetCheck = (new Date( date.getFullYear(), months, 1 )).getTimezoneOffset();
			if ( offsetCheck != offset ) return (offsetCheck > offset);
		}
		return false;
	}


	//===================================================================================================================================================
	//
	//      ------  FORMATTER
	//
	//===================================================================================================================================================

	/**
	 * Receives a string with specific caracters and replace them with date information based on the Date Object passed.
	 * @param formatStyle a string with or more of the caracters bellow (starting at line 15). <br />
	 * @param date the Date object with the date you want to format
	 *
	 * The folowing caracters are the ones to be replaced:
	 * <ul>
	 *         <li><strong>d:</strong> Day of the month, 2 digits with leading zeros. Ex.: 01 to 31</li>
	 *         <li><strong>D:</strong> A textual representation of a day, three letters. Ex.: Mon through Sun</li>
	 *         <li><strong>j:</strong> Day of the month without leading zeros. Ex.: 1 to 31</li>
	 *         <li><strong>l:</strong> A full textual representation of the day of the week. Ex.: Sunday through Saturday</li>
	 *         <li><strong>w:</strong> Numeric representation of the day of the week. Ex.: 0 (for Sunday) through 6 (for Saturday)</li>
	 *         <li><strong>z:</strong> The day of the year (starting from 0). Ex.: 0 through 365</li>
	 *         <li><strong>F:</strong> A full textual representation of a month, such as January or March. Ex.: January through December</li>
	 *         <li><strong>m:</strong> Numeric representation of a month, with leading zeros. Ex.: 01 through 12</li>
	 *         <li><strong>M:</strong> A short textual representation of a month, three letters. Ex.: Jan through Dec</li>
	 *         <li><strong>n:</strong> Numeric representation of a month, without leading zeros. Ex.: 1 through 12</li>
	 *         <li><strong>t:</strong> Number of days in the given month. Ex.: 28 through 31</li>
	 *         <li><strong>L:</strong> Whether it's a leap year. Ex.: 1 if it is a leap year, 0 otherwise.</li>
	 *         <li><strong>Y:</strong> A full numeric representation of a year, 4 digits. Ex.: 1999 or 2003.</li>
	 *         <li><strong>y:</strong> A two digit representation of a year. Ex.: 99 or 03.</li>
	 *         <li><strong>a:</strong> Lowercase Ante meridiem and Post meridiem. Ex.: am or pm.</li>
	 *         <li><strong>A:</strong> Uppercase Ante meridiem and Post meridiem. Ex.: AM or PM.</li>
	 *         <li><strong>g:</strong> 12-hour format of an hour without leading zeros. Ex.: 1 through 12.</li>
	 *         <li><strong>G:</strong> 24-hour format of an hour without leading zeros. Ex.: 0 through 23.</li>
	 *         <li><strong>h:</strong> 12-hour format of an hour with leading zeros. Ex.: 01 through 12.</li>
	 *         <li><strong>H:</strong> 24-hour format of an hour with leading zeros. Ex.: 00 through 23.</li>
	 *         <li><strong>i:</strong> Minutes with leading zeros. Ex.: 00 to 59.</li>
	 *         <li><strong>s:</strong> Seconds, with leading zeros. Ex.: 00 through 59.</li>
	 *         <li><strong>u:</strong> Milliseconds. Ex.: 654.</li>
	 *         <li><strong>U:</strong> Milliseconds since the Unix Epoch (January 1 1970 00:00:00 GMT). Ex.: 1309986224468.</li>
	 * </ul>
	 *
	 * @example
	 * <pre>
	 * var d:Date = new Date(2011, 7, 6, 14, 1, 58, 220);
	 * trace(DateUtil.formatDate("z - l - d/m/Y = h:i:s:u A", d)); // outputs: "186 - Wednesday - 06/07/2011 - 02:01:58:220 PM"
	 * </pre>
	 * @see http://php.net/manual/en/function.date.php
	 */
	public static function formatDate( formatStyle:String, date:Date ):String {
		var DateUtilsed:String = formatStyle;
		DateUtilsed = DateUtilsed.replace( /d/g, leadZero( date.date ) );
		DateUtilsed = DateUtilsed.replace( /j/g, String( date.date ) );
		DateUtilsed = DateUtilsed.replace( /w/g, String( date.day ) );
		if ( DateUtilsed.search( "z" ) != -1 ) DateUtilsed = DateUtilsed.replace( /z/g, String( dayOfYear( date.month, date.date ) ) );
		DateUtilsed = DateUtilsed.replace( /m/g, leadZero( date.month + 1 ) );
		DateUtilsed = DateUtilsed.replace( /n/g, String( date.month + 1 ) );
		if ( DateUtilsed.search( "t" ) != -1 ) DateUtilsed = DateUtilsed.replace( /t/g, String( numberOfDaysInMonth( date.month, leapYear( date.fullYear ) ) ) );
		if ( DateUtilsed.search( "J" ) != -1 ) {
			DateUtilsed = DateUtilsed.replace( /J/g, buildTimeZoneDesignation( date, true ) );
		}
		DateUtilsed = DateUtilsed.replace( /L/g, leapYear( date.fullYear ) ? "1" : "0" );
		DateUtilsed = DateUtilsed.replace( /Y/g, String( date.fullYear ) );
		DateUtilsed = DateUtilsed.replace( /y/g, String( date.fullYear ).substr( 2 ) );
		DateUtilsed = DateUtilsed.replace( /g/g, date.hours <= 12 ? date.hours : date.hours - 12 );
		DateUtilsed = DateUtilsed.replace( /G/g, date.hours );
		DateUtilsed = DateUtilsed.replace( /h/g, leadZero( date.hours <= 12 ? date.hours : date.hours - 12 ) );
		DateUtilsed = DateUtilsed.replace( /H/g, leadZero( date.hours ) );
		DateUtilsed = DateUtilsed.replace( /i/g, leadZero( date.minutes ) );
		DateUtilsed = DateUtilsed.replace( /s/g, leadZero( date.seconds ) );
		DateUtilsed = DateUtilsed.replace( /u/g, leadZero( date.milliseconds ) );
		DateUtilsed = DateUtilsed.replace( /U/g, String( date.time ) );
		DateUtilsed = DateUtilsed.replace( /a/g, "[{(a)}]" );
		DateUtilsed = DateUtilsed.replace( /A/g, "[{(A)}]" );
		DateUtilsed = DateUtilsed.replace( /D/g, "[{(D)}]" );
		DateUtilsed = DateUtilsed.replace( /l/g, "[{(l)}]" );
		DateUtilsed = DateUtilsed.replace( /M/g, "[{(M)}]" );
		DateUtilsed = DateUtilsed.replace( /F/g, "[{(F)}]" );
		DateUtilsed = DateUtilsed.replace( /\[\{\(a\)\}\]/g, date.hours < 12 ? "am" : "pm" );
		DateUtilsed = DateUtilsed.replace( /\[\{\(A\)\}\]/g, date.hours < 12 ? "AM" : "PM" );
		DateUtilsed = DateUtilsed.replace( /\[\{\(D\)\}\]/g, String( DAYS[date.day] ).substr( 0, 3 ) );
		DateUtilsed = DateUtilsed.replace( /\[\{\(l\)\}\]/g, DAYS[date.day] );
		DateUtilsed = DateUtilsed.replace( /\[\{\(M\)\}\]/g, String( MONTHS[date.month] ).substr( 0, 3 ) );
		DateUtilsed = DateUtilsed.replace( /\[\{\(F\)\}\]/g, MONTHS[date.month] );
		return DateUtilsed;
	}

	public static function leadZero( p_num:int ):String {
		return (p_num < 10) ? "0" + String( p_num ) : String( p_num );
	}

	private static function buildTimeZoneDesignation( date:Date, dts:Boolean = true ):String {
		if ( !date ) return "";
		var timeZoneAsString:String = ""; //"GMT";
		var timeZoneOffset:int;
		var offset:int = date.getTimezoneOffset();
		// timezoneoffset is the number that needs to be added to the local time to get to GMT, so
		// a positive number would actually be GMT -X hours
		if ( offset / 60 > 0 && offset / 60 < 10 ) {
			timeZoneOffset = (dts) ? ( offset / 60 ) : ( offset / 60 - 1 );
			timeZoneAsString += "-0" + timeZoneOffset.toString();
		} else if ( offset < 0 && date.timezoneOffset / 60 > -10 ) {
			timeZoneOffset = (dts) ? ( offset / 60 ) : ( offset / 60 + 1 );
			timeZoneAsString += "+0" + ( -1 * timeZoneOffset ).toString();
		} else {
			timeZoneAsString += "+00";
		}
		// add zeros to match standard format
		timeZoneAsString += "00";
		return timeZoneAsString;
	}

	public static function dayOfYear( month:uint, day:uint ):uint {
		if ( month == 0 ) return day - 1;
		var total:uint = 0;
		for ( var i:uint = 0; i < month; i++ ) {
			total += numberOfDaysInMonth( i );
		}
		return total + day - 1;
	}

	public static function leapYear( year:uint ):Boolean {
		return (year % 4 == 0 && year % 100 != 0) || year % 100 == 0;
	}

	public static function numberOfDaysInMonth( month:uint, isLeapYear:Boolean = false ):uint {
		if ( month == 1 ) {
			return isLeapYear ? 29 : 28;
		} else if ( month <= 6 && (month & 1) == 1 ) {
			return 30;
		} else if ( month > 6 && (month & 1) == 0 ) {
			return 30;
		}
		return 31;
	}

	// ===== end of FORMATTER =====

	public static function dateToIso8601( date:Date, includeOffset:Boolean = false ):String {
		return formatDate( "Y-m-dTH:i:s" + ( includeOffset ? "J" : "Z "), date );
	}

	public static function getTimezoneFormatString( date:Date ):String {
		var str:String = "";
		var off:int = date.getTimezoneOffset();
		var off_h:Number = off / 60;
		if ( off_h > 0 && off_h < 10 ) {
			str = "-0" + off_h;
		} else if ( off < 0 && off_h > -10 ) {
			str = "+0" + ( -1 * off_h );
		} else {
			str = "+00";
		}
		// add zeros to match standard format
		str += ":00";
		return str;
	}

	/**
	 * Date includes the current TIMEZONE, so each time u call this function it ADDS the offset.
	 * So, dont call it more than once.
	 * @param date
	 * @return
	 */
	public static function convertToUTC( date:Date = null ):Date {
		if ( !date ) date = new Date();
		date.setTime( date.getTime() + date.getTimezoneOffset() * 60000 );
		return date;
	}

	/**
	 * Converts W3C ISO 8601 date String into a Date object.
	 * Example code:
	 *      <pre>
	 *          trace(iso8601ToDate("1994-11-05T08:15:30-05:00").toString());
	 *      </pre>
	 * @param iso8601 Valid ISO 8601 formatted String
	 * @return Date object of the specified date and time of the ISO 8601 String in universal time
	 * @see http://www.w3.org/TR/NOTE-datetime (W3C ISO 8601 specification)
	 * @author Aaron Clinger
	 * @author Shane McCartney
	 * @author David Nelson
	 */
	public static function iso8601ToDate( iso8601:String ):Date {
		var isEmpty:Function = BooleanUtils.isEmpty;
		var parts:Array = iso8601.toUpperCase().split( "T" );
		var date:Array = parts[0].split( "-" );
		var time:Array = (parts.length <= 1) ? [] : parts[1].split( ":" );
		var year:uint = isEmpty( date[0] ) ? 0 : Number( date[0] );
		var month:uint = isEmpty( date[1] ) ? 0 : Number( date[1] - 1 );
		var day:uint = isEmpty( date[2] ) ? 1 : Number( date[2] );
		var hour:int = isEmpty( time[0] ) ? 0 : Number( time[0] );
		var minute:uint = isEmpty( time[1] ) ? 0 : Number( time[1] );
		var second:uint = isEmpty( time[2] ) ? 0 : Number( time[2] );
		var millisecond:uint = 0;
		if ( time[2] != undefined ) {
			var index:int = String( time[2] ).length;
			var temp:Number;
			if ( time[2].indexOf( "+" ) > -1 ) {
				index = time[2].indexOf( "+" );
			} else if ( time[2].indexOf( "-" ) > -1 ) {
				index = time[2].indexOf( "-" );
			} else if ( time[2].indexOf( "Z" ) > -1 ) {
				index = time[2].indexOf( "Z" );
			}
			if ( isNaN( index ) ) {
				temp = Number( time[2].slice( 0, index ) );
				second = Math.floor( temp );
				millisecond = 1000 * (temp % 1);
			}
			if ( index != time[2].length ) {
				var offset:String = time[2].slice( index );
				// minutes to hours.
				var userOffset:Number = new Date( year, month, day ).getTimezoneOffset() / 60;
				switch ( offset.charAt( 0 ) ) {
					case "+" :
					case "-" :
						hour -= userOffset + Number( offset.slice( 0 ) );
						break;
					case "Z" :
						hour -= userOffset;
						break;

					default:
				}
			}
		}
		return new Date( year, month, day, hour, minute, second, millisecond );
	}

	/**
	 * Takes a Date object and returns a string in the format
	 * "X UNITS ago" where X is a number and UNITS is a unit of
	 * time. Also has some other strings like "yesterday".
	 *
	 * @param date The date to convert to a past string.
	 * @param now Optional time to compare against. Default will be the present.
	 */
	public static function getTimeElapsedString( date:Date, now:Date = null ):String {

		const SEC_PER_MINUTE:int = 60;
		const SEC_PER_HOUR:int = SEC_PER_MINUTE * 60;
		const SEC_PER_DAY:int = SEC_PER_HOUR * 24;
		const SEC_PER_WEEK:int = SEC_PER_DAY * 7;
		const SEC_PER_MONTH:int = SEC_PER_DAY * 30;
		const SEC_PER_YEAR:int = SEC_PER_MONTH * 12;

		// if now isn't defined, make it a new Date object (the present)
		if ( !now ) {
			now = new Date();
		}

		// don't use secondsElapsed here because the values are
		// huge so they use uints and are never negative
		if ( now.time < date.time ) { return "in the future"; }

		// get the difference in seconds between the two values.
		var secondsElapsed:uint = Math.floor( (now.time - date.time) / 1000 );


		// seconds
		if ( secondsElapsed < SEC_PER_MINUTE ) { return "just now"; }

		// minutes
		if ( secondsElapsed < SEC_PER_HOUR ) {
			var minutes:int = int( secondsElapsed / SEC_PER_MINUTE );
			return formatAgoString( minutes, "minute" );
		}

		// hours
		if ( secondsElapsed < SEC_PER_DAY ) {
			var hours:int = int( secondsElapsed / SEC_PER_HOUR );
			return formatAgoString( hours, "hour" );
		}

		// days
		if ( secondsElapsed < SEC_PER_WEEK ) {
			var days:int = int( secondsElapsed / SEC_PER_DAY );
			if ( days == 1 ) { return "yesterday"; }

			return formatAgoString( days, "day" );
		}

		// weeks
		if ( secondsElapsed < SEC_PER_MONTH ) {
			var weeks:int = int( secondsElapsed / SEC_PER_WEEK );
			return formatAgoString( weeks, "week" );
		}

		// months
		if ( secondsElapsed < SEC_PER_YEAR ) {
			var months:int = int( secondsElapsed / SEC_PER_MONTH );
			return formatAgoString( months, "month" );
		}

		// years
		return "more than a year ago";

		// Returns a string in the format "count unit(s) ago"
		function formatAgoString( count:int, unit:String ):String {
			return count + " " + unit + (count > 1 ? "s" : "") + " ago";
		}
	}

	private static function plural( value:int ):String {
		return value > 1 ? 's' : '';
	}

	public static function toSqlDate( date:Date ):String {
		if ( !date ) return null;
		var val:String = date.fullYear + '-' + leadZero( date.month + 1 );
		val += '-' + leadZero( date.date );
		val += ' ' + leadZero( date.hours );
		val += ':' + leadZero( date.minutes );
		val += ':' + leadZero( date.seconds );
		return val;
	}


	public static function getAMPM( date:Date ):String {
		return date.hours > 11 ? "PM" : "AM";
	}

	/**
	 * If a date is AM, changes it to PM and vice versa.
	 * @returns A new date object, 12 hours later or earlier depending.
	 */
	public static function toggleAMPM( date:Date ):Date {
		var d:Date = new Date( date.time );
		if ( getAMPM( date ) == "PM" ) {
			d.hours -= 12;
		} else {
			d.hours += 12;
		}
		return d;
	}

	/**
	 * Returns true if date is in the past.
	 * If the date is exactly equal to the current time, it will return false.
	 */
	public static function isPast( date:Date ):Boolean {
		return (new Date().getTime() - date.getTime()) > 0;
	}


	//===================================================================================================================================================
	//
	//      ------  TIME TRACKERS.
	//
	//===================================================================================================================================================

	public static function getExecTime( p_ts:Number, useMilliseconds:Boolean = true ):String {
		var now:Number = getTimer();
		p_ts = now - p_ts;
		if ( useMilliseconds ) {
			return p_ts + "ms";
		} else {
			p_ts = p_ts / 1000 | 0;
			return StringUtils.formatSeconds( p_ts, false );
		}
	}

	private static var _timeTracker:Object = {};

	public static function track( id:String, autoSpent:Boolean = true ):String {
		if ( _timeTracker[id] && autoSpent ) {
			spent( id );
			// reset the timer
		}
		_timeTracker[id] = getTimer();
		return String( _timeTracker[id] );
	}

	public static function spent( id:String, doTrace:Boolean = true, useMS:Boolean = true ):String {
		if ( !_timeTracker.hasOwnProperty( id ) ) {
			return "trackTime " + id + " dont defined";
		}
		var tt:Number = _timeTracker[id];
		var val:String = getExecTime( tt, useMS );
		if ( doTrace ) {
			trace( ">>> Execution time [" + id + "] = ", val );
		}
		delete _timeTracker[id];
		return val;
	}

}
}
