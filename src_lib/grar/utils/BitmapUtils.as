/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 1/28/14 11:39 PM.
 */
package grar.utils {
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.StageQuality;
	import flash.filters.BitmapFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class BitmapUtils {
		public function BitmapUtils() {
		}

		private static var _dictionary: Object = {};

		public static function getBMPVO( pId: String ): Object {
			return _dictionary[pId];
		}

		// => 'bitmap' must belong to the same parent as 'obj'. 'obj' should be invisible.
		static public function filterSnapshot( obj: DisplayObject, pScale: Number = 1, pCropAlpha: Boolean = false,
											   pId: String = '' ): BitmapData {
			if ( pId && _dictionary[pId] ) return _dictionary[pId];
			// Remember the transform matrix of the text field
			var offset: Matrix = obj.transform.matrix.clone();
			// Get the bounds of just the textfield (does not include filters)
			var bounds: Rectangle = obj.getBounds( obj );
			// Create a bitmapData that is used just to calculate the size of the filters
			var tempBD: BitmapData = new BitmapData( bounds.width, bounds.height, false );
			bounds.width = obj.width;
			bounds.height = obj.height;
			// Make a copy of the textField bounds. We'll adjust this with the filters
			var finalBounds: Rectangle = new Rectangle( 0, 0, bounds.width, bounds.height );
			// Step through each filter in the textField and adjust our bounds to include them all
			var filterBounds: Rectangle;
			for each ( var filter: BitmapFilter in obj.filters ) {
				filterBounds = tempBD.generateFilterRect( tempBD.rect, filter );
				finalBounds = finalBounds.union( filterBounds );
			}
			tempBD.dispose();
			finalBounds.offset( bounds.x, bounds.y );
			finalBounds.x = Math.floor( finalBounds.x );
			finalBounds.y = Math.floor( finalBounds.y );
			finalBounds.width = Math.ceil( finalBounds.width );
			finalBounds.height = Math.ceil( finalBounds.height );
			// Now draw the textfield to a new bitmpaData
			var data: BitmapData = new BitmapData( finalBounds.width * pScale, finalBounds.height * pScale, true, 0x0 );
//		var data:BitmapData = new BitmapData( finalBounds.width*pScale, finalBounds.height*pScale, false, 0xff0000 );
			offset.tx = -finalBounds.x * pScale;
			offset.ty = -finalBounds.y * pScale;
			offset.scale( pScale, pScale );
			data.drawWithQuality( obj, offset, obj.transform.colorTransform, obj.blendMode, null, true, StageQuality.HIGH );
			if ( pCropAlpha ) {
				var rect: Rectangle = data.getColorBoundsRect( 0xFF000000, 0x000000, false );
				tempBD = new BitmapData( rect.width, rect.height, true, 0x0 );
//			tempBD = new BitmapData(rect.width, rect.height, false, 0xff00ff);
				tempBD.copyPixels( data, rect, new Point() );
				data.dispose();
				data = tempBD;
			}
			if ( pId ) _dictionary[pId] = {bd:data, scale:pScale};
			/*if (bitmap) {
				bitmap.bitmapData = data;
				// Position the bitmap in same place as 'obj'
				bitmap.x = obj.transform.matrix.tx + finalBounds.x;
				bitmap.y = obj.transform.matrix.ty + finalBounds.y;
			}*/
			return data;
		}


		private static var _filter_bd: BitmapData = new BitmapData( 2, 2, true );
		private static var _filter_bdRect: Rectangle = _filter_bd.rect;

		public static function analyzeFilterRectsTree( drawing: DisplayObject, r: Rectangle, r2: Rectangle,
														p_totalBounds: Rectangle ): void {
			if ( drawing is DisplayObjectContainer ) {
				var doc: DisplayObjectContainer = drawing as DisplayObjectContainer;
				for ( var i: int = 0; i < doc.numChildren; i++ ) {
					analyzeFilterRectsTree( doc.getChildAt( i ), r, r2, p_totalBounds );
				}
			}
			analyzeFilterRects( drawing, r, r2, p_totalBounds );
		}

		public static function analyzeFilterRects( drawing: DisplayObject, r: Rectangle, r2: Rectangle,
													p_totalBounds: Rectangle ): void {
			if ( drawing.filters && drawing.filters.length > 0 ) {
				var fb: Rectangle;
				for each ( var filter: BitmapFilter in drawing.filters ) {
					fb = _filter_bd.generateFilterRect( _filter_bdRect, filter );
					fb.width -= 2;
					fb.height -= 2;
					var tmp2: Rectangle = r2.union( fb );
					fb.width += p_totalBounds.width;
					fb.height += p_totalBounds.height;
					var tmp1: Rectangle = r.union( fb );
					r.setTo( tmp1.x, tmp1.y, tmp1.width, tmp1.height );
					r2.setTo( tmp2.x, tmp2.y, tmp2.width, tmp2.height );
				}
			}
		}

	}
}
