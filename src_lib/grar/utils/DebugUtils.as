/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 2:27 PM.
 *
 */
package grar.utils {
import flash.system.Capabilities;
import flash.utils.describeType;

public class DebugUtils {
	public function DebugUtils() {
	}

	public static function getFunctionName( callee:Function, parent:Object ):String {
		for each ( var m:XML in describeType( parent )..method ) {
			if ( parent[m.@name] == callee ) return m.@name;
		}
		return "not found";
	}


	public static function getCurrentCallStack():Vector.<Vector.<String>> {
		// you can always analyze the stack trace to found calls!
		if (!Capabilities.isDebugger) {
			// It's not the debug player, so returns nothing
			var tt:Vector.<Vector.<String>> = new Vector.<Vector.<String>>();
			var ll:Vector.<String> = new Vector.<String>();
			ll.push("?");
			ll.push("?");
			ll.push("?");
			ll.push("?");
			tt.push(ll);
			tt.push(ll);
			tt.push(ll);
			tt.push(ll);
			return tt;
		}

		var stackTrace:Array = new Error().getStackTrace().split("\n");
		var cStack:String;

		var cPackage:String;
		var cClass:String;
		var cFunction:String;
		var cFullClass:String;

		var cIsStatic:Boolean;			// Static class method: "com.zehfernando.utils::Log$/echo"
		var cIsConstructor:Boolean;		// Constructor call: "com.coachella.display::Main"
		var cIsFunction:Boolean;		// Anonymous function call: "Function/<anonymous>"

		var call:Vector.<String>;

		var calls:Vector.<Vector.<String>> = new Vector.<Vector.<String>>();

		for (var i:int = 2; i < stackTrace.length; i++) {
			cStack = stackTrace[i];
			cStack = cStack.substr(4, cStack.indexOf("(") - 4);

			cFullClass = StringUtils.getCleanString(cStack.split("/")[0]);
			cFunction = StringUtils.getCleanString(cStack.split("/")[1]);

			// Is a global function?
			cIsFunction = cFullClass == "Function";

			if (cIsFunction) {
				cPackage = "";
				cClass = "(Global)";
			} else {
				cPackage = cFullClass.split("::")[0];
				cClass = cFullClass.split("::")[1];
			}

			// Static calls?
			cIsStatic = false;
			if (cClass != null && cClass.substr(-1, 1) == "$") {
				cIsStatic = true;
				cClass = cClass.substr(0, cClass.length - 1);
			}

			// Constructor calls?
			cIsConstructor = false;
			if (!Boolean(cFunction)) {
				cIsConstructor = true;
				//cFunction = "[constructor]";
			}

			call = new Vector.<String>();

			call.push(cPackage);
			call.push(cClass);

			if (cIsStatic) cFunction = "(s)" + cFunction;

			if (!cIsConstructor) {
				call.push(cFunction);
			} else {
				call.push(cClass);
			}

			//trace ("--> " + call.join("--"));

			calls.push(call);

		}

		return calls;
	}
}
}
