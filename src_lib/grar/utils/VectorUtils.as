/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 4:34 PM.
 *
 * Set of utility classes to work with AS3 Vector<>, mostly related to convertion of Arrays to
 * improve overal loop performance.
 */
package grar.utils {
public class VectorUtils {
	public function VectorUtils() {}

	// Array to vector

	public static function arrayToBooleanVector( array:Array ):Vector.<Boolean> {
		if ( !array ) return null;
		var v:Vector.<Boolean> = new Vector.<Boolean>();
		var len:int = array.length;
		for ( var i:int = 0; i < len; i++ ) v.push( array[i] );
		return v;
	}

	public static function arrayToNumberVector( array:Array ):Vector.<Number> {
		if ( !array ) return null;
		var v:Vector.<Number> = new Vector.<Number>();
		var len:int = array.length;
		for ( var i:int = 0; i < len; i++ ) v.push( array[i] );
		return v;
	}

	public static function arrayToStringVector( array:Array ):Vector.<String> {
		if ( !array ) return null;
		var v:Vector.<String> = new Vector.<String>();
		var len:int = array.length;
		for ( var i:int = 0; i < len; i++ ) v.push( array[i] );
		return v;
	}

	// Vector to array

	public static function booleanVectorToArray( vector:Vector.<Boolean> ):Array {
		if ( !vector ) return null;
		var l:Array = [];
		var len:int = vector.length;
		for ( var i:int = 0; i < len; i++ ) l.push( vector[i] );
		return l;
	}

	public static function intVectorToArray( vector:Vector.<int> ):Array {
		if ( !vector ) return null;
		var l:Array = [];
		var len:int = vector.length;
		for ( var i:int = 0; i < len; i++ ) l.push( vector[i] );
		return l;
	}

	public static function uintVectorToArray( vector:Vector.<uint> ):Array {
		if ( !vector ) return null;
		var l:Array = [];
		var len:int = vector.length;
		for ( var i:int = 0; i < len; i++ ) l.push( vector[i] );
		return l;
	}

	public static function numberVectorToArray( vector:Vector.<Number> ):Array {
		if ( !vector ) return null;
		var l:Array = [];
		var len:int = vector.length;
		for ( var i:int = 0; i < len; i++ ) l.push( vector[i] );
		return l;
	}

	public static function stringVectorToArray( vector:Vector.<String> ):Array {
		if ( !vector ) return null;
		var l:Array = [];
		var len:int = vector.length;
		for ( var i:int = 0; i < len; i++ ) l.push( vector[i] );
		return l;
	}

	// String to Vector

	public static function stringToStringVector( string:String, separator:String ):Vector.<String> {
		if ( !string || !string.length ) return null;
		var v:Vector.<String> = new Vector.<String>();
		var stringList:Array = string.split( separator );
		var len:int = stringList.length;
		for ( var i:int = 0; i < len; i++ ) v.push( stringList[i] );
		return v;
	}

	// Other

	/*public static function getEquivalentItemFromNumberVector( pos:Number, max:int, numbers:Vector.<Number>,
															  average:Boolean = true ):Number {
		// Return an item from a number list mapped from the index of another list
		if ( !average ) {
			// Don't allow average, just find a number
			return numbers[Math.round( MathUtils.map( pos, 0, max, 0, numbers.length - 1 ) )];
		} else {
			// Allow average, find the two nearest items and use it
			var pos:Number = MathUtils.map( pos, 0, max, 0, numbers.length - 1 );
			var pos1:int = Math.floor( pos );
			var pos2:int = Math.min( pos1 + 1, numbers.length - 1 );
			return MathUtils.map( pos - pos1, 0, 1, numbers[pos1], numbers[pos2], true );
		}
	}*/


}
}
