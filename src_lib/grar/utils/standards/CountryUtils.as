/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 7/10/14 21:00.
 */
package grar.utils.standards {
	public class CountryUtils {

		protected static var countryList: Array; //Vector.<CountryInfo>;

		protected static var inited: Boolean;

		// ================================================================================================================
		// INTERNAL INTERFACE ---------------------------------------------------------------------------------------------

		protected static function lazyInit(): void {
			if ( !inited ) {
				countryList = []; //new Vector.<CountryInfo>();
				countryList.push( new CountryInfo( 'US', 'United States' ) );
//				countryList.push( new CountryInfo( 'US', 'United States' ) );
				countryList.push( new CountryInfo( 'ABW', 'Aruba' ) );
				countryList.push( new CountryInfo( 'AFG', 'Afghanistan' ) );
				countryList.push( new CountryInfo( 'AGO', 'Angola' ) );
				countryList.push( new CountryInfo( 'AIA', 'Anguilla' ) );
				countryList.push( new CountryInfo( 'ALA', 'Åland Islands' ) );
				countryList.push( new CountryInfo( 'ALB', 'Albania' ) );
				countryList.push( new CountryInfo( 'AND', 'Andorra' ) );
				countryList.push( new CountryInfo( 'ARE', 'United Arab Emirates' ) );
				countryList.push( new CountryInfo( 'ARG', 'Argentina' ) );
				countryList.push( new CountryInfo( 'ARM', 'Armenia' ) );
				countryList.push( new CountryInfo( 'ASM', 'American Samoa' ) );
				countryList.push( new CountryInfo( 'ATA', 'Antarctica' ) );
				countryList.push( new CountryInfo( 'ATF', 'French Southern Territories' ) );
				countryList.push( new CountryInfo( 'ATG', 'Antigua and Barbuda' ) );
				countryList.push( new CountryInfo( 'AUS', 'Australia' ) );
				countryList.push( new CountryInfo( 'AUT', 'Austria' ) );
				countryList.push( new CountryInfo( 'AZE', 'Azerbaijan' ) );
				countryList.push( new CountryInfo( 'BDI', 'Burundi' ) );
				countryList.push( new CountryInfo( 'BEL', 'Belgium' ) );
				countryList.push( new CountryInfo( 'BEN', 'Benin' ) );
				countryList.push( new CountryInfo( 'BES', 'Bonaire, Sint Eustatius and Saba' ) );
				countryList.push( new CountryInfo( 'BFA', 'Burkina Faso' ) );
				countryList.push( new CountryInfo( 'BGD', 'Bangladesh' ) );
				countryList.push( new CountryInfo( 'BGR', 'Bulgaria' ) );
				countryList.push( new CountryInfo( 'BHR', 'Bahrain' ) );
				countryList.push( new CountryInfo( 'BHS', 'Bahamas' ) );
				countryList.push( new CountryInfo( 'BIH', 'Bosnia and Herzegovina' ) );
				countryList.push( new CountryInfo( 'BLM', 'Saint Barthélemy' ) );
				countryList.push( new CountryInfo( 'BLR', 'Belarus' ) );
				countryList.push( new CountryInfo( 'BLZ', 'Belize' ) );
				countryList.push( new CountryInfo( 'BMU', 'Bermuda' ) );
				countryList.push( new CountryInfo( 'BOL', 'Bolivia, Plurinational State of' ) );
				countryList.push( new CountryInfo( 'BRA', 'Brazil' ) );
				countryList.push( new CountryInfo( 'BRB', 'Barbados' ) );
				countryList.push( new CountryInfo( 'BRN', 'Brunei Darussalam' ) );
				countryList.push( new CountryInfo( 'BTN', 'Bhutan' ) );
				countryList.push( new CountryInfo( 'BVT', 'Bouvet Island' ) );
				countryList.push( new CountryInfo( 'BWA', 'Botswana' ) );
				countryList.push( new CountryInfo( 'CAF', 'Central African Republic' ) );
				countryList.push( new CountryInfo( 'CAN', 'Canada' ) );
				countryList.push( new CountryInfo( 'CCK', 'Cocos (Keeling) Islands' ) );
				countryList.push( new CountryInfo( 'CHE', 'Switzerland' ) );
				countryList.push( new CountryInfo( 'CHL', 'Chile' ) );
				countryList.push( new CountryInfo( 'CHN', 'China' ) );
				countryList.push( new CountryInfo( 'CIV', 'Côte d\'Ivoire' ) );
				countryList.push( new CountryInfo( 'CMR', 'Cameroon' ) );
				countryList.push( new CountryInfo( 'COD', 'Congo, the Democratic Republic of the' ) );
				countryList.push( new CountryInfo( 'COG', 'Congo' ) );
				countryList.push( new CountryInfo( 'COK', 'Cook Islands' ) );
				countryList.push( new CountryInfo( 'COL', 'Colombia' ) );
				countryList.push( new CountryInfo( 'COM', 'Comoros' ) );
				countryList.push( new CountryInfo( 'CPV', 'Cape Verde' ) );
				countryList.push( new CountryInfo( 'CRI', 'Costa Rica' ) );
				countryList.push( new CountryInfo( 'CUB', 'Cuba' ) );
				countryList.push( new CountryInfo( 'CUW', 'Curaçao' ) );
				countryList.push( new CountryInfo( 'CXR', 'Christmas Island' ) );
				countryList.push( new CountryInfo( 'CYM', 'Cayman Islands' ) );
				countryList.push( new CountryInfo( 'CYP', 'Cyprus' ) );
				countryList.push( new CountryInfo( 'CZE', 'Czech Republic' ) );
				countryList.push( new CountryInfo( 'DEU', 'Germany' ) );
				countryList.push( new CountryInfo( 'DJI', 'Djibouti' ) );
				countryList.push( new CountryInfo( 'DMA', 'Dominica' ) );
				countryList.push( new CountryInfo( 'DNK', 'Denmark' ) );
				countryList.push( new CountryInfo( 'DOM', 'Dominican Republic' ) );
				countryList.push( new CountryInfo( 'DZA', 'Algeria' ) );
				countryList.push( new CountryInfo( 'ECU', 'Ecuador' ) );
				countryList.push( new CountryInfo( 'EGY', 'Egypt' ) );
				countryList.push( new CountryInfo( 'ERI', 'Eritrea' ) );
				countryList.push( new CountryInfo( 'ESH', 'Western Sahara' ) );
				countryList.push( new CountryInfo( 'ESP', 'Spain' ) );
				countryList.push( new CountryInfo( 'EST', 'Estonia' ) );
				countryList.push( new CountryInfo( 'ETH', 'Ethiopia' ) );
				countryList.push( new CountryInfo( 'FIN', 'Finland' ) );
				countryList.push( new CountryInfo( 'FJI', 'Fiji' ) );
				countryList.push( new CountryInfo( 'FLK', 'Falkland Islands (Malvinas)' ) );
				countryList.push( new CountryInfo( 'FRA', 'France' ) );
				countryList.push( new CountryInfo( 'FRO', 'Faroe Islands' ) );
				countryList.push( new CountryInfo( 'FSM', 'Micronesia, Federated States of' ) );
				countryList.push( new CountryInfo( 'GAB', 'Gabon' ) );
				countryList.push( new CountryInfo( 'GBR', 'United Kingdom' ) );
				countryList.push( new CountryInfo( 'GEO', 'Georgia' ) );
				countryList.push( new CountryInfo( 'GGY', 'Guernsey' ) );
				countryList.push( new CountryInfo( 'GHA', 'Ghana' ) );
				countryList.push( new CountryInfo( 'GIB', 'Gibraltar' ) );
				countryList.push( new CountryInfo( 'GIN', 'Guinea' ) );
				countryList.push( new CountryInfo( 'GLP', 'Guadeloupe' ) );
				countryList.push( new CountryInfo( 'GMB', 'Gambia' ) );
				countryList.push( new CountryInfo( 'GNB', 'Guinea-Bissau' ) );
				countryList.push( new CountryInfo( 'GNQ', 'Equatorial Guinea' ) );
				countryList.push( new CountryInfo( 'GRC', 'Greece' ) );
				countryList.push( new CountryInfo( 'GRD', 'Grenada' ) );
				countryList.push( new CountryInfo( 'GRL', 'Greenland' ) );
				countryList.push( new CountryInfo( 'GTM', 'Guatemala' ) );
				countryList.push( new CountryInfo( 'GUF', 'French Guiana' ) );
				countryList.push( new CountryInfo( 'GUM', 'Guam' ) );
				countryList.push( new CountryInfo( 'GUY', 'Guyana' ) );
				countryList.push( new CountryInfo( 'HKG', 'Hong Kong' ) );
				countryList.push( new CountryInfo( 'HMD', 'Heard Island and McDonald Islands' ) );
				countryList.push( new CountryInfo( 'HND', 'Honduras' ) );
				countryList.push( new CountryInfo( 'HRV', 'Croatia' ) );
				countryList.push( new CountryInfo( 'HTI', 'Haiti' ) );
				countryList.push( new CountryInfo( 'HUN', 'Hungary' ) );
				countryList.push( new CountryInfo( 'IDN', 'Indonesia' ) );
				countryList.push( new CountryInfo( 'IMN', 'Isle of Man' ) );
				countryList.push( new CountryInfo( 'IND', 'India' ) );
				countryList.push( new CountryInfo( 'IOT', 'British Indian Ocean Territory' ) );
				countryList.push( new CountryInfo( 'IRL', 'Ireland' ) );
				countryList.push( new CountryInfo( 'IRN', 'Iran, Islamic Republic of' ) );
				countryList.push( new CountryInfo( 'IRQ', 'Iraq' ) );
				countryList.push( new CountryInfo( 'ISL', 'Iceland' ) );
				countryList.push( new CountryInfo( 'ISR', 'Israel' ) );
				countryList.push( new CountryInfo( 'ITA', 'Italy' ) );
				countryList.push( new CountryInfo( 'JAM', 'Jamaica' ) );
				countryList.push( new CountryInfo( 'JEY', 'Jersey' ) );
				countryList.push( new CountryInfo( 'JOR', 'Jordan' ) );
				countryList.push( new CountryInfo( 'JPN', 'Japan' ) );
				countryList.push( new CountryInfo( 'KAZ', 'Kazakhstan' ) );
				countryList.push( new CountryInfo( 'KEN', 'Kenya' ) );
				countryList.push( new CountryInfo( 'KGZ', 'Kyrgyzstan' ) );
				countryList.push( new CountryInfo( 'KHM', 'Cambodia' ) );
				countryList.push( new CountryInfo( 'KIR', 'Kiribati' ) );
				countryList.push( new CountryInfo( 'KNA', 'Saint Kitts and Nevis' ) );
				countryList.push( new CountryInfo( 'KOR', 'Korea, Republic of' ) );
				countryList.push( new CountryInfo( 'KWT', 'Kuwait' ) );
				countryList.push( new CountryInfo( 'LAO', 'Lao People\'s Democratic Republic' ) );
				countryList.push( new CountryInfo( 'LBN', 'Lebanon' ) );
				countryList.push( new CountryInfo( 'LBR', 'Liberia' ) );
				countryList.push( new CountryInfo( 'LBY', 'Libya' ) );
				countryList.push( new CountryInfo( 'LCA', 'Saint Lucia' ) );
				countryList.push( new CountryInfo( 'LIE', 'Liechtenstein' ) );
				countryList.push( new CountryInfo( 'LKA', 'Sri Lanka' ) );
				countryList.push( new CountryInfo( 'LSO', 'Lesotho' ) );
				countryList.push( new CountryInfo( 'LTU', 'Lithuania' ) );
				countryList.push( new CountryInfo( 'LUX', 'Luxembourg' ) );
				countryList.push( new CountryInfo( 'LVA', 'Latvia' ) );
				countryList.push( new CountryInfo( 'MAC', 'Macao' ) );
				countryList.push( new CountryInfo( 'MAF', 'Saint Martin (French part)' ) );
				countryList.push( new CountryInfo( 'MAR', 'Morocco' ) );
				countryList.push( new CountryInfo( 'MCO', 'Monaco' ) );
				countryList.push( new CountryInfo( 'MDA', 'Moldova, Republic of' ) );
				countryList.push( new CountryInfo( 'MDG', 'Madagascar' ) );
				countryList.push( new CountryInfo( 'MDV', 'Maldives' ) );
				countryList.push( new CountryInfo( 'MEX', 'Mexico' ) );
				countryList.push( new CountryInfo( 'MHL', 'Marshall Islands' ) );
				countryList.push( new CountryInfo( 'MKD', 'Macedonia, the former Yugoslav Republic of' ) );
				countryList.push( new CountryInfo( 'MLI', 'Mali' ) );
				countryList.push( new CountryInfo( 'MLT', 'Malta' ) );
				countryList.push( new CountryInfo( 'MMR', 'Myanmar' ) );
				countryList.push( new CountryInfo( 'MNE', 'Montenegro' ) );
				countryList.push( new CountryInfo( 'MNG', 'Mongolia' ) );
				countryList.push( new CountryInfo( 'MNP', 'Northern Mariana Islands' ) );
				countryList.push( new CountryInfo( 'MOZ', 'Mozambique' ) );
				countryList.push( new CountryInfo( 'MRT', 'Mauritania' ) );
				countryList.push( new CountryInfo( 'MSR', 'Montserrat' ) );
				countryList.push( new CountryInfo( 'MTQ', 'Martinique' ) );
				countryList.push( new CountryInfo( 'MUS', 'Mauritius' ) );
				countryList.push( new CountryInfo( 'MWI', 'Malawi' ) );
				countryList.push( new CountryInfo( 'MYS', 'Malaysia' ) );
				countryList.push( new CountryInfo( 'MYT', 'Mayotte' ) );
				countryList.push( new CountryInfo( 'NAM', 'Namibia' ) );
				countryList.push( new CountryInfo( 'NCL', 'New Caledonia' ) );
				countryList.push( new CountryInfo( 'NER', 'Niger' ) );
				countryList.push( new CountryInfo( 'NFK', 'Norfolk Island' ) );
				countryList.push( new CountryInfo( 'NGA', 'Nigeria' ) );
				countryList.push( new CountryInfo( 'NIC', 'Nicaragua' ) );
				countryList.push( new CountryInfo( 'NIU', 'Niue' ) );
				countryList.push( new CountryInfo( 'NLD', 'Netherlands' ) );
				countryList.push( new CountryInfo( 'NOR', 'Norway' ) );
				countryList.push( new CountryInfo( 'NPL', 'Nepal' ) );
				countryList.push( new CountryInfo( 'NRU', 'Nauru' ) );
				countryList.push( new CountryInfo( 'NZL', 'New Zealand' ) );
				countryList.push( new CountryInfo( 'OMN', 'Oman' ) );
				countryList.push( new CountryInfo( 'PAK', 'Pakistan' ) );
				countryList.push( new CountryInfo( 'PAN', 'Panama' ) );
				countryList.push( new CountryInfo( 'PCN', 'Pitcairn' ) );
				countryList.push( new CountryInfo( 'PER', 'Peru' ) );
				countryList.push( new CountryInfo( 'PHL', 'Philippines' ) );
				countryList.push( new CountryInfo( 'PLW', 'Palau' ) );
				countryList.push( new CountryInfo( 'PNG', 'Papua New Guinea' ) );
				countryList.push( new CountryInfo( 'POL', 'Poland' ) );
				countryList.push( new CountryInfo( 'PRI', 'Puerto Rico' ) );
				countryList.push( new CountryInfo( 'PRK', 'Korea, Democratic People\'s Republic of' ) );
				countryList.push( new CountryInfo( 'PRT', 'Portugal' ) );
				countryList.push( new CountryInfo( 'PRY', 'Paraguay' ) );
				countryList.push( new CountryInfo( 'PSE', 'Palestinian Territory, Occupied' ) );
				countryList.push( new CountryInfo( 'PYF', 'French Polynesia' ) );
				countryList.push( new CountryInfo( 'QAT', 'Qatar' ) );
				countryList.push( new CountryInfo( 'REU', 'Réunion' ) );
				countryList.push( new CountryInfo( 'ROU', 'Romania' ) );
				countryList.push( new CountryInfo( 'RUS', 'Russian Federation' ) );
				countryList.push( new CountryInfo( 'RWA', 'Rwanda' ) );
				countryList.push( new CountryInfo( 'SAU', 'Saudi Arabia' ) );
				countryList.push( new CountryInfo( 'SDN', 'Sudan' ) );
				countryList.push( new CountryInfo( 'SEN', 'Senegal' ) );
				countryList.push( new CountryInfo( 'SGP', 'Singapore' ) );
				countryList.push( new CountryInfo( 'SGS', 'South Georgia and the South Sandwich Islands' ) );
				countryList.push( new CountryInfo( 'SHN', 'Saint Helena, Ascension and Tristan da Cunha' ) );
				countryList.push( new CountryInfo( 'SJM', 'Svalbard and Jan Mayen' ) );
				countryList.push( new CountryInfo( 'SLB', 'Solomon Islands' ) );
				countryList.push( new CountryInfo( 'SLE', 'Sierra Leone' ) );
				countryList.push( new CountryInfo( 'SLV', 'El Salvador' ) );
				countryList.push( new CountryInfo( 'SMR', 'San Marino' ) );
				countryList.push( new CountryInfo( 'SOM', 'Somalia' ) );
				countryList.push( new CountryInfo( 'SPM', 'Saint Pierre and Miquelon' ) );
				countryList.push( new CountryInfo( 'SRB', 'Serbia' ) );
				countryList.push( new CountryInfo( 'SSD', 'South Sudan' ) );
				countryList.push( new CountryInfo( 'STP', 'Sao Tome and Principe' ) );
				countryList.push( new CountryInfo( 'SUR', 'Suriname' ) );
				countryList.push( new CountryInfo( 'SVK', 'Slovakia' ) );
				countryList.push( new CountryInfo( 'SVN', 'Slovenia' ) );
				countryList.push( new CountryInfo( 'SWE', 'Sweden' ) );
				countryList.push( new CountryInfo( 'SWZ', 'Swaziland' ) );
				countryList.push( new CountryInfo( 'SXM', 'Sint Maarten (Dutch part)' ) );
				countryList.push( new CountryInfo( 'SYC', 'Seychelles' ) );
				countryList.push( new CountryInfo( 'SYR', 'Syrian Arab Republic' ) );
				countryList.push( new CountryInfo( 'TCA', 'Turks and Caicos Islands' ) );
				countryList.push( new CountryInfo( 'TCD', 'Chad' ) );
				countryList.push( new CountryInfo( 'TGO', 'Togo' ) );
				countryList.push( new CountryInfo( 'THA', 'Thailand' ) );
				countryList.push( new CountryInfo( 'TJK', 'Tajikistan' ) );
				countryList.push( new CountryInfo( 'TKL', 'Tokelau' ) );
				countryList.push( new CountryInfo( 'TKM', 'Turkmenistan' ) );
				countryList.push( new CountryInfo( 'TLS', 'Timor-Leste' ) );
				countryList.push( new CountryInfo( 'TON', 'Tonga' ) );
				countryList.push( new CountryInfo( 'TTO', 'Trinidad and Tobago' ) );
				countryList.push( new CountryInfo( 'TUN', 'Tunisia' ) );
				countryList.push( new CountryInfo( 'TUR', 'Turkey' ) );
				countryList.push( new CountryInfo( 'TUV', 'Tuvalu' ) );
				countryList.push( new CountryInfo( 'TWN', 'Taiwan, Province of China' ) );
				countryList.push( new CountryInfo( 'TZA', 'Tanzania, United Republic of' ) );
				countryList.push( new CountryInfo( 'UGA', 'Uganda' ) );
				countryList.push( new CountryInfo( 'UKR', 'Ukraine' ) );
				countryList.push( new CountryInfo( 'UMI', 'United States Minor Outlying Islands' ) );
				countryList.push( new CountryInfo( 'URY', 'Uruguay' ) );
				countryList.push( new CountryInfo( 'UZB', 'Uzbekistan' ) );
				countryList.push( new CountryInfo( 'VAT', 'Holy See (Vatican City State)' ) );
				countryList.push( new CountryInfo( 'VCT', 'Saint Vincent and the Grenadines' ) );
				countryList.push( new CountryInfo( 'VEN', 'Venezuela, Bolivarian Republic of' ) );
				countryList.push( new CountryInfo( 'VGB', 'Virgin Islands, British' ) );
				countryList.push( new CountryInfo( 'VIR', 'Virgin Islands, U.S.' ) );
				countryList.push( new CountryInfo( 'VNM', 'Viet Nam' ) );
				countryList.push( new CountryInfo( 'VUT', 'Vanuatu' ) );
				countryList.push( new CountryInfo( 'WLF', 'Wallis and Futuna' ) );
				countryList.push( new CountryInfo( 'WSM', 'Samoa' ) );
				countryList.push( new CountryInfo( 'YEM', 'Yemen' ) );
				countryList.push( new CountryInfo( 'ZAF', 'South Africa' ) );
				countryList.push( new CountryInfo( 'ZMB', 'Zambia' ) );
				countryList.push( new CountryInfo( 'ZWE', 'Zimbabwe' ) );
				inited = true;
			}
		}

		public static function getObjectArrayForList():Array {
			lazyInit();
			var i: int;
			var len: int = countryList.length;
			var arr:Array = [] ;
			for ( i = 0; i < len; i++ ) {
				var vo:Object = { id : countryList[i].id, label: countryList[i].name } ;
				arr[i] = vo ;
			}
			return arr ;
		}


		// ================================================================================================================
		// PUBLIC INTERFACE -----------------------------------------------------------------------------------------------
		public static function getCountryList(): Array {
			lazyInit();
			return countryList;
		}

		public static function getCountryNameFromId( p_id: String ): String {
			lazyInit();
			var i: int;
			var len: int = countryList.length;
			p_id = p_id.toUpperCase();
			for ( i = 0; i < len; i++ ) {
				if ( countryList[i].id.toUpperCase() == p_id ) return countryList[i].name;
			}
			return "";
		}

		public static function getCountryIdFromName( p_name: String ): String {
			lazyInit();
			var i: int;
			var len: int = countryList.length;
			p_name = p_name.toLowerCase();
			for ( i = 0; i < len; i++ ) {
				if ( countryList[i].name.toLowerCase() == p_name ) return countryList[i].id;
			}
			return "";
		}

		public static function getCountryIds(): Array {
			lazyInit();
			var arr: Array = [];
			var len: int = countryList.length;
			for ( var i: int = 0; i < len; i++ ) {
				if( countryList[i].included ){
					arr[i] = countryList[i].id;
				}
			}
			return arr;
		}
	}
}

class CountryInfo {

	// Properties
	public var id: String;
	public var name: String;
	public var included: Boolean = true ;

	// ================================================================================================================
	// CONSTRUCTOR ----------------------------------------------------------------------------------------------------

	function CountryInfo( __stateID: String, __stateName: String ) {
		id = __stateID;
		name = __stateName;
	}
}

/*

 */
