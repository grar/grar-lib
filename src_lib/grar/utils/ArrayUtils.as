/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 6:27 PM.
 *
 */
package grar.utils {

import flash.utils.ByteArray;

public class ArrayUtils {
	public function ArrayUtils() {}

	public static function getRandomItems( arr:Array, quantity:int ):Array {
		var tmp:Array = arr.concat();
		var result:Array = [];
		for ( var i:int = 0; i < quantity; i++ ) {
			var tmp_len:int = tmp.length;
			if ( tmp_len <= 0 ) break;
			var idx:int = Math.random() * tmp_len | 0;
			result[i] = tmp.splice( idx, 1 )[0];
		}
		return result;
	}

	public static function randomSort( arr:Array, clone:Boolean = false ):Array {
		arr = clone ? arr.concat() : arr;
		arr.sort( __randomize );
		return arr;
	}

	public static function randomNaive( arr:Array ):Array {
		var len:int = arr.length;
		var shuffle:Array = new Array( len );
		var randomPos:int = 0;
		for ( var i:int = 0; i < len; i++ ) {
			randomPos = int( Math.random() * len );
			shuffle[randomPos] = arr[i];
		}
		return shuffle;
	}

	public static function randomMiddleMan( arr:Array ):Array {
		var len:int = arr.length;
		var shuffle_arr:Array = new Array( len );
		var emptySlots:Array = [];
		for ( var n:int = 0; n < len; n++ ) emptySlots[n] = n;
		var randomPos:Number = 0;
		var actualSlot:Number = 0;
		for ( var i:int = 0; i < len; i++ ) {
			randomPos = int( Math.random() * emptySlots.length );
			actualSlot = emptySlots[randomPos];
			shuffle_arr[actualSlot] = arr[i];
			emptySlots.splice( randomPos, 1 );
		}
		return shuffle_arr;
	}

	public static function pickRandom( source:Array ):* {
		var idx:int = Math.round( Math.random() * (source.length - 1) );
		return source[idx];
	}

	private static function __randomize( a:*, b:* ):int {
		return ( Math.random() > .5 ) ? 1 : -1;
	}

	public static function removeDuplicatesInArray( arr:Array ):Array {
		/*var i:int;
		 var j:int;
		 for ( i = 0; i < arr.length - 1; i++ ) {
		 for ( j = i + 1; j < arr.length; j++ ) {
		 if ( arr[i] === arr[j] ) {
		 arr.splice( j, 1 );
		 }
		 }
		 }*/
		return arr.filter(
				function ( e:*, i:int, arr:Array ):Boolean {
					return (i == 0) ? true : arr.lastIndexOf( e, i - 1 ) == -1;
				} );
	}

	public static function sortHiLowString( arr:Array ):void {
		arr.sort( _sorter );
		function _sorter( a:String, b:String ):Number {
			if ( a.length < b.length ) return 1;
			else if ( a.length > b.length ) return -1;
			return 0;
		}
	}


	// clone object or array.
	private static var _tmp_ba:ByteArray;

	public static function clone( obj:Object ):Object {
		if ( !_tmp_ba ) _tmp_ba = new ByteArray();
		_tmp_ba.writeObject( obj );
		_tmp_ba.position = 0;
		var o:Object = _tmp_ba.readObject();
		_tmp_ba.clear();
		return o;
	}

	public static function getIndexFromSearchProp( p_arr:Array, p_value:*, p_prop:String = "label" ):int {
		var obj:Object = searchPropInArray( p_arr, p_value, p_prop );
		if ( !obj ) return -1;
		return p_arr.indexOf( obj );
	}

	public static function searchPropInArray( p_arr:Array, p_value:*, p_prop:String = "label" ):Object {
		var len:int = p_arr.length;
		for ( var i:int = 0; i < len; i++ ) {
			var vo:Object = p_arr[i];
			if ( vo.hasOwnProperty( p_prop ) ) {
				if ( vo[p_prop] == p_value ) return vo;
			}
		}
		return null;
	}

	public static function getDummyArray( len:int ):Array {
		var arr:Array = [];
		for ( var i:int = 0; i < len; i++ ) {
			var vo:Object = {};
			vo.label = "label " + i;
			arr[i] = vo;
		}
		return arr;
	}

	public static function toArray( iterable:* ):Array {
		var ret:Array = [];
		for each ( var elem:* in iterable ) ret.push( elem );
		return ret;
	}

	public static function remove( arr:Array, element:* ):int {
		var index:int = arr.indexOf( element );
		if ( index > -1 ) {
			if ( arr["removeAt"] ) {
				arr["removeAt"]( index );
			} else {
				arr.splice( index, 1 );
			}
			return index;
		}
		return -1;
	}

	public static function insertArrayInPosition( destination:Array, arr:Array, position:int ):void {
		if ( position > destination.length - 1 ) position = destination.length - 1;
		var spliceParams:Array = [position, 0];
		spliceParams = spliceParams.concat( arr );
		destination.splice.apply( null, spliceParams );
	}

	/**
	 Finds the lowest value in <code>arr</code>.
	 @param arr: Array composed only of numbers.
	 @return The lowest value in <code>arr</code>.
	 @example
	 <code>
	 var numberArray:Array = new Array(2, 1, 5, 4, 3);
	 trace("The lowest value is: " + ArrayUtil.getLowestValue(numberArray));
	 </code>
	 */
	public function getLowestValue( arr:Array ):Number {
		return arr[arr.sort( 16 | 8 )[0]];
	}

	/**
	 Finds the highest value in <code>inArray</code>.

	 @param arr: Array composed only of numbers.
	 @return The highest value in <code>inArray</code>.
	 @example
	 <code>
	 var numberArray:Array = new Array(2, 1, 5, 4, 3);
	 trace("The highest value is: " + ArrayUtil.getHighestValue(numberArray));
	 </code>
	 */
	public function getHighestValue( arr:Array ):Number {
		return arr[int( arr.sort( 16 | 8 )[int( arr.length - 1 )])];
	}

	/**
	 Returns every element that matches <code>match</code> for the property <code>key</code>.
	 Useful to inspect Object properties.
	 @param arr: Array to search for object with <code>key</code> that matches <code>match</code>.
	 @param key: Name of the property to match.
	 @param match: Value to match against.
	 @return Returns all the matched elements.
	 */
	public function getItemsByKey( arr:Array, key:String, match:* ):Array {
		var out:Array = [];
		for each ( var item:* in arr ) {
			if ( item[key] == match ) {
				out.push( item );
			}
		}
		return out;
	}

	public static function getDifference( pre:Array, post:Array ):Object {
		var o:Object = {added: [], removed: [], same: []};
		pre = pre.concat();
		post = post.concat();
		var index:int;
		var usingPre:Boolean = pre.length > post.length;
		var e:*;
		if ( usingPre ) {
			for each( e in pre ) {
				index = post.indexOf( e );
				if ( index == -1 ) {
					// removed.
					o.removed.push( e );
				} else {
					// stayed.
					o.same.push( e );
					post.splice( index, 1 );
				}
			}
			o.added = post.concat();
		} else {
			for each( e in post ) {
				index = pre.indexOf( e );
				if ( index == -1 ) {
					// added.
					o.added.push( e );
				} else {
					// stayed.
					o.same.push( e );
					pre.splice( index, 1 );
				}
			}
			o.removed = pre.concat();
		}
		return o;
	}

	public static function fromVector(vec:*):Array {
		var arr:Array = [];
		var vector:Vector.<Object> = vec as Vector.<Object>;
		vector.forEach( arrayConverter );

		function arrayConverter( element:*, index:int, array:Array ):void {
			arr[arr.length] = element;
		}
		return arr;
	}

}

}
