/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 4:45 PM.
 *
 *    General Math functions, that could also be available in other frameworks/libraries.
 *
 *    INFO: try to use Inlining: http://www.bytearray.org/?p=4789 if it doenst fail.
 */
package grar.utils {
public final class MathUtils {
	public function MathUtils() {}


	public static const DEG2RAD:Number = 1 / 180 * Math.PI;
	public static const RAD2DEG:Number = 1 / Math.PI * 180;

	// Temporary vars for faster allocations
	private static var map_p:Number;

	[Inline]
	public static function max( v1:Number, v2:Number ):Number {
		return v1 < v2 ? v2 : v1;
	}

	[Inline]
	public static function min( v1:Number, v2:Number ):Number {
		return v1 > v2 ? v2 : v1;
	}

	/**
	 * Clamps a number to a range, by restricting it to a minimum and maximum values: if the passed value is lower than the minimum value, it's replaced by the minimum; if it's higher than the maximum value, it's replaced by the maximum; if not, it's unchanged.
	 * @param value        The value to be clamped.
	 * @param min        Minimum value allowed.
	 * @param max        Maximum value allowed.
	 * @return            The newly clamped value.
	 */
	public static function clamp( value:Number, min:Number = 0, max:Number = 1 ):Number {
		return value < min ? min : value > max ? max : value;
	}

	public static function clampAuto( value:Number, clamp1:Number = 0, clamp2:Number = 1 ):Number {
		if ( clamp2 < clamp1 ) {
			var v:Number = clamp2;
			clamp2 = clamp1;
			clamp1 = v;
		}
		return value < clamp1 ? clamp1 : value > clamp2 ? clamp2 : value;
	}

	// used to "clamp" values based on a minimun baseNumber, useful for
	// scaling bitmap fonts to correct sizes. (8, 16, 24, 32).
	public static function nextMult( value:Number, baseNumber:Number ):Number {
		return Math.ceil( value / baseNumber ) * baseNumber;
	}


	/**
	 * Maps a value from a range, determined by old minimum and maximum values, to a new range,
	 * determined by new minimum and maximum values. These minimum and maximum values are referential;
	 * the new value is not clamped by them.
	 * @param value    The value to be re-mapped.
	 * @param oldMin    The previous minimum value.
	 * @param oldMax    The previous maximum value.
	 * @param newMin    The new minimum value.
	 * @param newMax    The new maximum value.
	 * @return            The new value, mapped to the new range.
	 */
	public static function map( value:Number, oldMin:Number, oldMax:Number, newMin:Number = 0, newMax:Number = 1,
								doClamp:Boolean = false ):Number {
		if ( oldMin == oldMax ) return newMin;
		map_p = ((value - oldMin) / (oldMax - oldMin) * (newMax - newMin)) + newMin;
		if ( doClamp ) map_p = newMin < newMax ? clamp( map_p, newMin, newMax ) : clamp( map_p, newMax, newMin );
		return map_p;
	}

	/**
	 * Clamps a value to a range, by restricting it to a minimum and maximum values but folding
	 * the value to the range instead of simply resetting to the minimum and maximum.
	 * It works like a more powerful Modulo function.
	 * @param value    The value to be clamped.
	 * @param min        Minimum value allowed.
	 * @param max        Maximum value allowed.
	 * @return            The newly clamped value.
	 * @example Some examples:
	 * <listing version="3.0">
	 *    trace(MathUtils.roundClamp(14, 0, 10));
	 *    // Result: 4
	 *
	 *    trace(MathUtils.roundClamp(360, 0, 360));
	 *    // Result: 0
	 *
	 *    trace(MathUtils.roundClamp(360, -180, 180));
	 *    // Result: 0
	 *
	 *    trace(MathUtils.roundClamp(21, 0, 10));
	 *    // Result: 1
	 *
	 *    trace(MathUtils.roundClamp(-98, 0, 100));
	 *    // Result: 2
	 * </listing>
	 */
// Need a better name?
	public static function rangeMod( value:Number, min:Number, pseudoMax:Number ):Number {
		var range:Number = pseudoMax - min;
		value = (value - min) % range;
		if ( value < 0 ) value = range - (-value % range);
		value += min;
		return value;
	}

	public static function isPowerOfTwo( value:Number ):Boolean {
		// Return true if a number if a power of two (2, 4, 8, etc)
		// There's probably a better way, but trying to avoid bitwise manipulations
		while ( value % 2 == 0 && value > 2 ) value /= 2;
		return value == 2;
	}

	public static function getHighestPowerOfTwo( value:Number ):int {
		// Return a power of two number that is higher than the passed value
		var c:int = 1;
		while ( c < value ) c *= 2;
		return c;
	}

	public static function randomRange( min:Number = 0, max:Number = 1, rounded:Boolean = false ):Number {
		return rounded ? Math.round( RandomGenerator.getInRange( min, max ) ) : RandomGenerator.getInRange( min, max );
	}


	[Inline]
	public static function absUint( value:Number ):uint {
		return (value + (value >> 31)) ^ (value >> 31);
	}

	[Inline]
	public static function abs( value:Number ):Number {
		return value < 0 ? -value : value;
	}

	[Inline]
	public static function round( value:Number ):int {
		return value < 0 ? value + .5 == (value | 0) ? value : value - .5 : value + .5;
	}

//	[Inline]
	// floor() is useless, AS3 is way faster, so u can use value|0  or cast to int()
	/*public static function floor( value:Number ):int {
	 var ni:int = value;
	 return (value < 0 && value != ni) ? ni - 1 : ni;
	 }*/
//	public static function floor( value:Number ):int {
//		return value ;
//		var ni:int = value;
//		return (value < 0 && value != ni) ? ni - 1 : ni;
//	}

	[Inline]
	public static function ceil( value:Number ):int {
		var ni:int = value;
		return (value >= 0 && value != ni) ? ni + 1 : ni;
	}

	public static function decimalPresicion( value:Number, decimal:int = 1 ):Number {
		decimal = decimal < 0 ? 100 : Math.pow( 10, decimal );
		return round( decimal * value ) / decimal;
	}

	public static function percentInRange( value:Number, min:Number = 0, max:Number = 1 ):Number {
		return (value - min) / (max - min);
	}

	public static function distance( px1:Number, py1:Number, px2:Number, py2:Number ):Number {
		var dx:Number = px2 - px1;
		var dy:Number = py2 - py1;
		return Math.sqrt( dx * dx + dy * dy );
	}


	//===================================================================================================================================================
	//
	//      ------  ANIMATIONS / movement
	//
	//===================================================================================================================================================

	public static function euclerMotion( val:Number, target:Number, easing:Number = 0.5 ):Number {
		return val += (target - val) * easing;
	}

	/**
	 * Elastic movement, as we have to keep track of velocity and current value.
	 * props[0] = currentValue, props[1] = currentVelocity.
	 * @param props
	 * @param targetValue
	 * @param spring
	 * @param easing
	 * @return
	 */
	public static function elasticMotion( props:Array, targetValue:Number, spring:Number = 0.001,
										  easing:Number = 0.92 ):Number {
		var value:Number = props[0];
		var vel:Number = props[1];
		vel += (targetValue - value) * spring;
		vel *= easing;
		value += vel;
		props[0] = value;
		props[1] = vel;
		return value;
	}

}
}
