/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 9:17 PM.
 *
 */
package grar.utils {
import flash.geom.ColorTransform;

public class ColorUtils {
	public function ColorUtils() {
	}

	public static function redFromHex( c:uint ):uint {
		return ( c >> 16 ) & 0xff;
	}

	public static function greenFromHex( c:uint ):uint {
		return ( c >> 8 ) & 0xff;
	}

	public static function blueFromHex( c:uint ):uint {
		return c & 0xff;
	}

	/**
	 * Add alpha value (0-255) to the RGB hex.
	 * @param c
	 * @param alpha Send percentage from 0-1
	 * @return
	 */
	public static function addAlphaToHex( c:uint, alphaPercent:Number = 1 ):uint {
		var a:uint = alphaPercent * 255;
		//newAlpha has to be in the 0 to 255 range
		var argb:uint = 0;
		argb += (a << 24);
		argb += c;
		return argb;
	}

	public static function hexToGreyscale( c:uint ):uint {
		var r:Number = redFromHex( c );
		var g:Number = greenFromHex( c );
		var b:Number = blueFromHex( c );
		var av:Number = (r + g + b) / 3;
		r = g = b = av;
		return rgb( r, g, b );
	}

	public static function hex2String( c:uint, prepend:String = "#" ):String {
		return prepend + c.toString( 16 );
	}


	/**
	 * Input colour value such as 0xFF0000, and modifier from -1 to 1.
	 */
	public static function brighten( color:uint, modifier:Number ):uint {
		var z:int = 0xff * modifier;
		var r:uint = trim( ((color & 0xff0000) >> 16) + z );
		var g:uint = trim( ((color & 0x00ff00) >> 8) + z );
		var b:uint = trim( ((color & 0x0000ff)      ) + z );
		return r << 16 | g << 8 | b;
	}

	/**
	 * Blends two colours. Percentage should be 0.5 for an equal blend.
	 */
	public static function blend( first:uint, second:uint, percent:Number ):uint {
		var r:int = ((first & 0xff0000) >> 16) * (1 - percent) + ((second & 0xff0000) >> 16) * percent;
		var g:int = ((first & 0x00ff00) >> 8) * (1 - percent) + ((second & 0x00ff00) >> 8) * percent;
		var b:int = ((first & 0x0000ff)      ) * (1 - percent) + ((second & 0x0000ff)      ) * percent;
		return r << 16 | g << 8 | b;
	}

	public static function desaturate( color:uint, percent:Number ):uint {
		return blend( color, 0x7F7F7F, percent );
	}

	public static function bleach( color:uint, percent:Number ):uint {
		return blend( color, 0xFFFFFF, percent );
	}

	public static function darken( color:uint, percent:Number ):uint {
		return blend( color, 0x000000, percent );
	}

	private static function trim( value:int ):uint {
		return Math.min( Math.max( 0x00, value ), 0xff );
	}

	/**
	 * Get a series of complements of a given color.
	 *
	 * @param hex   Color to get harmonies for
	 * @param weight  Threshold to apply to color harmonies, 0 - 255
	 */
	public static function getHarmonies( hex:uint, weight:Number ):Array {
		var red:uint = hex >> 16;
		var green:uint = (hex ^ (red << 16)) >> 8;
		var blue:uint = (hex ^ (red << 16)) ^ (green << 8);

		var colorHarmonyArray:Array = new Array();
		//weight = red+green+blue/3;

		colorHarmonyArray.push( rgb( red, green, weight ) );
		colorHarmonyArray.push( rgb( red, weight, blue ) );
		colorHarmonyArray.push( rgb( weight, green, blue ) );
		colorHarmonyArray.push( rgb( red, weight, weight ) );
		colorHarmonyArray.push( rgb( weight, green, weight ) );
		colorHarmonyArray.push( rgb( weight, weight, blue ) );

		return colorHarmonyArray;
	}

	public static function hex2rgb( hex:uint ):Object {
		return {
			a: ( hex & 0xff000000 ) >> 24,
			r: ( hex & 0xff0000 ) >> 16,
			g: ( hex & 0x00ff00 ) >> 8,
			b: ( hex & 0x0000ff )
		};
	}

	public static function getR( color:uint ):uint {
		return (color & 0xff0000) >> 16;
	}

	public static function getG( color:uint ):uint {
		return (color & 0x00ff00) >> 8;
	}

	public static function getB( color:uint ):uint {
		return color & 0x0000ff;
	}

	/**
	 * RGBColorTransform Create an instance of the information.
	 * @param rgb RGB integer value that indicates (0x000000 - 0xFFFFFF)
	 * @param amount of fill adaptive value (0.0 - 1.0)
	 * @param alpha transparency (0.0 - 1.0)
	 * @return a new instance ColorTransform
	 * */
	public static function colorTransform( rgb:uint = 0, amount:Number = 1.0,
										   alpha:Number = 1.0 ):ColorTransform {
		amount = (amount > 1) ? 1 : (amount < 0) ? 0 : amount;
		alpha = (alpha > 1) ? 1 : (alpha < 0) ? 0 : alpha;
		var r:Number = ((rgb >> 16) & 0xff) * amount;
		var g:Number = ((rgb >> 8) & 0xff) * amount;
		var b:Number = (rgb & 0xff) * amount;
		var a:Number = 1 - amount;
		return new ColorTransform( a, a, a, alpha, r, g, b, 0 );
	}

	/**
	 * Subtraction.
	 * 2 RGB single number that indicates (0x000000 0xFFFFFF up from) is subtracted
	 * from the return numbers.
	 * @param color1 RGB numbers show (0x000000 0xFFFFFF up from)
	 * @param color2 RGB numbers show (0x000000 0xFFFFFF up from)
	 * @return value subtracted Blend
	 **/
	public static function subtract( color1:uint, color2:uint ):uint {
		var colA:Array = toRGB( color1 );
		var colB:Array = toRGB( color2 );
		var r:uint = Math.max( Math.max( colB [0] - (256 - colA [0]),
				colA [0] - (256 - colB [0]) ), 0 );
		var g:uint = Math.max( Math.max( colB [1] - (256 - colA [1]),
				colA [1] - (256 - colB [1]) ), 0 );
		var b:uint = Math.max( Math.max( colB [2] - (256 - colA [2]),
				colA [2] - (256 - colB [2]) ), 0 );
		return r << 16 | g << 8 | b;
	}

	/**
	 * Additive color.
	 * 2 RGB single number that indicates (0x000000 0xFFFFFF up from) Returns the value
	 * of the additive mixture.
	 * @param color1 RGB numbers show (0x000000 0xFFFFFF up from)
	 * @param color2 RGB numbers show (0x000000 0xFFFFFF up from)
	 * @return the additive color
	 **/
	public static function sum( color1:uint, color2:uint ):uint {
		var c1:Array = toRGB( color1 );
		var c2:Array = toRGB( color2 );
		var r:uint = Math.min( c1 [0] + c2 [0], 255 );
		var g:uint = Math.min( c1 [1] + c2 [1], 255 );
		var b:uint = Math.min( c1 [2] + c2 [2], 255 );
		return r << 16 | g << 8 | b;
	}

	/**
	 * Subtractive.
	 * 2 RGB single number that indicates (0x000000 0xFFFFFF up from) Returns the value
	 * of the subtractive color.
	 * @param color1 RGB numbers show (0x000000 0xFFFFFF up from)
	 * @param color2 RGB numbers show (0x000000 0xFFFFFF up from)
	 * @return the subtractive
	 **/
	public static function sub( color1:uint, color2:uint ):uint {
		var c1:Array = toRGB( color1 );
		var c2:Array = toRGB( color2 );
		var r:uint = Math.max( c1 [0] - c2 [0], 0 );
		var g:uint = Math.max( c1 [1] - c2 [1], 0 );
		var b:uint = Math.max( c1 [2] - c2 [2], 0 );
		return r << 16 | g << 8 | b;
	}

	/**
	 * Comparison (dark).
	 * 2 RGB single number that indicates (0x000000 0xFFFFFF up from) to compare,
	 * RGB lower combined returns a numeric value for each number.
	 * @param color1 RGB numbers show (0x000000 0xFFFFFF up from)
	 * @param color2 RGB numbers show (0x000000 0xFFFFFF up from)
	 * @return comparison (dark) values
	 **/
	public static function min( color1:uint, color2:uint ):uint {
		var c1:Array = toRGB( color1 );
		var c2:Array = toRGB( color2 );
		var r:uint = Math.min( c1 [0], c2 [0] );
		var g:uint = Math.min( c1 [1], c2 [1] );
		var b:uint = Math.min( c1 [2], c2 [2] );
		return r << 16 | g << 8 | b;
	}

	/**
	 * Comparison (light).
	 * 2 RGB single number that indicates (0x000000 0xFFFFFF up from) to compare,
	 * RGB values combined with higher returns to their numbers.
	 * @param color1 RGB numbers show (0x000000 0xFFFFFF up from)
	 * @param color2 RGB numbers show (0x000000 0xFFFFFF up from)
	 * @return comparison (light) value
	 **/
	public static function max( color1:uint, color2:uint ):uint {
		var c1:Array = toRGB( color1 );
		var c2:Array = toRGB( color2 );
		var r:uint = Math.max( c1 [0], c2 [0] );
		var g:uint = Math.max( c1 [1], c2 [1] );
		var b:uint = Math.max( c1 [2], c2 [2] );
		return r << 16 | g << 8 | b;
	}

	/**
	 *   Values calculated from each RGB * RGB color value.
	 * @param r the red (R) indicating the number (0-255)
	 * @param g green (G) indicates the number (0-255)
	 * @param b blue (B) shows the number (0-255)
	 * @return obtained from the RGB color value for each indicating the number
	 **/
	public static function rgb( r:uint, g:uint, b:uint ):uint {
		return r << 16 | g << 8 | b;
	}

	public static function hsvObj2hex( hsvObject:Object):uint {
		return rgb.apply( null, HSVtoRGB( hsvObject.h, hsvObject.s, hsvObject.v ) );
	}

	/**
	 * HSV calculated from the numbers of each RGB color value.
	 * @param h hue (Hue) number that indicates (to 360-0)
	 * @param s the saturation (Saturation) shows the number (0.0 to 1.0)
	 * @param v lightness (Value) indicates the number (0.0 to 1.0)
	 * @return obtained from the RGB color value for each indicating the number
	 **/
	public static function hsv( h:int, s:Number, v:Number ):uint {
		return rgb.apply( null, HSVtoRGB( h, s, v ) );
	}

	/**
	 * RGB figures show (0x000000 0xFFFFFF up from) the
	 * R, G, B returns an array divided into a number from 0 to 255, respectively.
	 *
	 * @param hex RGB numbers show (0x000000 0xFFFFFF up from)
	 * @return array indicates the value of each color [R, G, B]
	 **/
	public static function toRGB( hex:uint ):Array {
		var r:uint = hex >> 16 & 0xFF;
		var g:uint = hex >> 8 & 0xFF;
		var b:uint = hex & 0xFF;
		return [r, g, b];
	}


	/**
	 * RGB from the respective figures, HSV sequences in terms of returns.
	 * RGB values are as follows.
	 * R - a number from 0 to 255
	 * G - a number from 0 to 255
	 * B - a number from 0 to 255
	 *
	 * CMYK values are as follows.
	 * C - a number between 0 to 255 representing cyan
	 * M - number between 0 to 255 representing magenta
	 * Y - number between 0 to 255 representing yellow
	 * K - number between 0 to 255 representing black
	 *
	 * Can not compute, including alpha.
	 * @param r the red (R) indicating the number (0x00 to 0xFF to)
	 * @param g green (G) indicates the number (0x00 to 0xFF to)
	 * @param b blue (B) shows the number (0x00 to 0xFF to)
	 * @return CMYK values into an array of [H, S, V]
	 **/
	public static function RGBtoCMYK( r:Number, g:Number, b:Number ):Array {
		var c:Number = 0, m:Number = 0, y:Number = 0, k:Number = 0, z:Number = 0;
		c = 255 - r;
		m = 255 - g;
		y = 255 - b;
		k = 255;

		if ( c < k )
			k = c;
		if ( m < k )
			k = m;
		if ( y < k )
			k = y;
		if ( k == 255 ) {
			c = 0;
			m = 0;
			y = 0;
		} else {
			c = Math.round( 255 * (c - k) / (255 - k) );
			m = Math.round( 255 * (m - k) / (255 - k) );
			y = Math.round( 255 * (y - k) / (255 - k) );
		}
		return [c, m, y, k];
	}

	/**
	 * HSV from each of the RGB values to determine a return as an array.
	 * RGB values are as follows.
	 * R - a number from 0 to 255
	 * G - a number from 0 to 255
	 * B - a number from 0 to 255
	 *
	 * HSV values are as follows.
	 * H - a number between 360-0
	 * S - number between 0 and 1.0
	 * V - number between 0 and 1.0
	 *
	 * H is replaced with equivalent numbers in the range of the 360-0 that is out of range.
	 * Can not compute, including alpha.
	 *
	 * @param h hue (Hue) number that indicates (to 360-0)
	 * @param s the saturation (Saturation) shows the number (0.0 to 1.0)
	 * @param v lightness (Value) indicates the number (0.0 to 1.0)
	 * @return RGB values into an array of [R, G, B]
	 **/
	public static function HSVtoRGB( h:Number, s:Number, v:Number ):Array {
		var r:Number = 0, g:Number = 0, b:Number = 0;
		var i:Number, x:Number, y:Number, z:Number;
		if ( s < 0 ) s = 0;
		if ( s > 1 ) s = 1;
		if ( v < 0 ) v = 0;
		if ( v > 1 ) v = 1;
		h = h % 360;
		if ( h < 0 ) h += 360;
		h /= 60;
		i = h >> 0;
		x = v * (1 - s);
		y = v * (1 - s * (h - i));
		z = v * (1 - s * (1 - h + i));
		switch ( i ) {
			case 0:
				r = v;
				g = z;
				b = x;
				break;
			case 1:
				r = y;
				g = v;
				b = x;
				break;
			case 2:
				r = x;
				g = v;
				b = z;
				break;
			case 3:
				r = x;
				g = y;
				b = v;
				break;
			case 4:
				r = z;
				g = x;
				b = v;
				break;
			case 5:
				r = v;
				g = x;
				b = y;
				break;
		}
		return [r * 255 >> 0, g * 255 >> 0, b * 255 >> 0];
	}

	public static function hex2hsv( hex:uint ):Object {
		var red:Number = ((hex >> 16) & 0xFF) / 255.0;
		var green:Number = ((hex >> 8) & 0xFF) / 255.0;
		var blue:Number = ((hex) & 0xFF) / 255.0;

		var dmax:Number = Math.max( Math.max( red, green ), blue );
		var dmin:Number = Math.min( Math.min( red, green ), blue );
		var range:Number = dmax - dmin;

		var bright:Number = dmax;
		var sat:Number = 0.0;
		var hue:Number = 0.0;

		if ( dmax != 0.0 ) {
			sat = range / dmax;
		}

		if ( sat != 0.0 ) {
			if ( red == dmax ) {
				hue = (green - blue) / range;
			} else if ( green == dmax ) {
				hue = 2.0 + (blue - red) / range;
			} else if ( blue == dmax ) {
				hue = 4.0 + (red - green) / range;
			}

			hue = hue * 60;
			if ( hue < 0.0 ) {
				hue = hue + 360.0;
			}
		}
		return {v: bright, s: sat, h: hue};
	}

//	public static function saturate(hex:uint, percent:Number)

	/**
	 * RGB from each of the CMYK values to determine a return as an array.
	 * CMYK values are as follows.
	 * C - a number between 0 to 255 representing cyan
	 * M - number between 0 to 255 representing magenta
	 * Y - number between 0 to 255 representing yellow
	 * K - number between 0 to 255 representing black
	 *
	 **/
	public static function CMYKtoRGB( c:Number, m:Number, y:Number, k:Number ):Array {
		c = 255 - c;
		m = 255 - m;
		y = 255 - y;
		k = 255 - k;
		return [
			(255 - c) * (255 - k) / 255,
			(255 - m) * (255 - k) / 255,
			(255 - y) * (255 - k) / 255
		];
	}
}
}
