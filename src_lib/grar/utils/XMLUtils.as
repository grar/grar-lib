/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 4:16 PM.
 *
 */
package grar.utils {
public class XMLUtils {
	public function XMLUtils() {}

	// Constants
	protected static const VALUE_TRUE:String = "true";			// Array considered as explicit "true" when reading data from a XML
	protected static const VALUE_FALSE:String = "false";		// Array considered as explicit "false" when reading data from a XML

	private static var filterListSortAttributeNames:Array;				// Temp for getFilteredNodeList()'s sort function

	public static function hasNode( xml:XML, nodeName:String ):Boolean {
		if ( !Boolean( xml ) ) return false;
		return xml.child( nodeName ).length() > 0;
	}

	public static function getNode( xml:XML, nodeName:String ):XML {
		if ( !Boolean( xml ) ) return null;
		var nodes:XMLList = xml.child( nodeName );
		if ( nodes.length() == 0 ) return null;
		return nodes[0];
	}

	public static function getValue( xml:XML, defaultValue:String = "" ):String {
		return xml == null ? defaultValue : StringUtils.getCleanString( xml );
	}

	public static function getNodeName( xml:XML, defaultValue:String = "" ):String {
		return xml == null ? defaultValue : StringUtils.getCleanString( xml.name() );
	}

	public static function getNodeAsBoolean( xml:XML, nodeName:String, defaultValue:Boolean = false ):Boolean {
		if ( !Boolean( xml ) ) return defaultValue;
		var nodeValue:String = StringUtils.getCleanString( getNodeValue( xml.child( nodeName ) ) );
		if ( nodeValue.toLowerCase() == VALUE_TRUE ) return true;
		if ( nodeValue.toLowerCase() == VALUE_FALSE ) return false;
		return defaultValue;
	}

	public static function getFilteredNodeAsFloat( xml:XML, nodeName:String, attributeNames:Array,
												   attributeFilters:Array, includeEmptyAttribute:Boolean = true,
												   allowAnyPartOfString:Boolean = true,
												   defaultValue:Number = 0 ):Number {
		var str:String = getFilteredNodeAsString( xml, nodeName, attributeNames, attributeFilters, includeEmptyAttribute = true, allowAnyPartOfString = true, null );
		return (str == null || str.length == 0) ? defaultValue : parseFloat( str );
	}

	public static function getFilteredNodeAsInt( xml:XML, nodeName:String, attributeNames:Array,
												 attributeFilters:Array, includeEmptyAttribute:Boolean = true,
												 allowAnyPartOfString:Boolean = true,
												 defaultValue:int = 0 ):int {
		return int( getFilteredNodeAsFloat( xml, nodeName, attributeNames, attributeFilters, includeEmptyAttribute = true, allowAnyPartOfString = true, defaultValue ) );
	}

	public static function getFilteredNodeAsBoolean( xml:XML, nodeName:String, attributeNames:Array,
													 attributeFilters:Array, includeEmptyAttribute:Boolean = true,
													 allowAnyPartOfString:Boolean = true,
													 defaultValue:Boolean = false ):Boolean {
		var str:String = getFilteredNodeAsString( xml, nodeName, attributeNames, attributeFilters, includeEmptyAttribute = true, allowAnyPartOfString = true, null );
		if ( str == null ) return defaultValue;
		str = str.toLowerCase();
		if ( str == VALUE_TRUE ) return true;
		if ( str == VALUE_FALSE ) return false;
		return defaultValue;
	}

	public static function getFilteredNodeAsString( xml:XML, nodeName:String, attributeNames:Array,
													attributeFilters:Array, includeEmptyAttribute:Boolean = true,
													allowAnyPartOfString:Boolean = true,
													defaultValue:String = "" ):String {
		// Returns the value of a subnode as a string, but filtering by attributes of a single type
		// If "includeEmptyAttribute" is true, nodes with an empty or invalid attribute are treated as the default
		// If "allowAnyPartOfString" is true, "stuff" matches "some stuff"
		if ( xml == null ) return defaultValue;
		var node:XML = getFilteredFirstNode( xml.child( nodeName ), attributeNames, attributeFilters, includeEmptyAttribute, allowAnyPartOfString );
		return node == null ? defaultValue : StringUtils.getCleanString( node );
	}

	public static function getNodeAsString( xml:XML, nodeName:String, defaultValue:String = "" ):String {
		if ( !Boolean( xml ) ) return defaultValue;
		var nodeValue:String = StringUtils.getCleanString( getNodeValue( xml.child( nodeName ) ) );
		if ( Boolean( nodeValue ) ) return nodeValue;
		return defaultValue;
	}

	public static function getNodePathAsString( xml:XML, nodeNamePath:String, defaultValue:String = "" ):String {
		if ( !Boolean( xml ) ) return defaultValue;
		if ( !Boolean( nodeNamePath ) ) return defaultValue;

		var path:Array = nodeNamePath.split( "/" );

		if ( path.length == 1 ) return getNodeAsString( xml, path[0] );

		var subNodes:XMLList = xml.child( path[0] );
		if ( subNodes.length() > 0 ) return getNodePathAsString( subNodes[0], path.slice( 1 ).join( "/" ) );

		return defaultValue;
	}

	public static function stripXMLNamespaces( xml:XML ):XML {
		// Source: http://active.tutsplus.com/articles/roundups/15-useful-as3-snippets-on-snipplr-com/
		var s:String = xml.toString();
		var pattern1:RegExp = /\s*xmlns[^\'\"]*=[\'\"][^\'\"]*[\'\"]/gi;
		s = s.replace( pattern1, "" );
		var pattern2:RegExp = /&lt;[\/]{0,1}(\w+:).*?&gt;/i;
		while ( pattern2.test( s ) ) {
			s = s.replace( pattern2.exec( s )[1], "" );
		}
		return XML( StringUtils.getCleanString( s ) );
	}

	public static function stripDefaultXMLNamespace( xml:XML ):XML {
		// Use for SVG
		// Source: http://stackoverflow.com/questions/673412/how-can-i-remove-a-namespace-from-an-xml-document
		var rawXMLString:String = xml.toXMLString();

		/* Define the regex pattern to remove the default namespace from the String representation of the XML result. */
		var xmlnsPattern:RegExp = new RegExp( "xmlns=[^\"]*\"[^\"]*\"", "gi" );

		/* Replace the default namespace from the String representation of the result XML with an empty string. */
		var cleanXMLString:String = StringUtils.getCleanString( rawXMLString.replace( xmlnsPattern, "" ) );

		// Create a new XML Object from the String just created
		return new XML( cleanXMLString );
	}

	public static function getNodeAsInt( xml:XML, nodeName:String, defaultValue:int = 0 ):int {
		return int( getNodeAsFloat( xml, nodeName, defaultValue ) );
	}

	public static function getNodeAsFloat( xml:XML, nodeName:String, defaultValue:Number = 0 ):Number {
		if ( !Boolean( xml ) ) return defaultValue;
		var nodeValue:Number = parseFloat( getNodeValue( xml.child( nodeName ) ) );
		if ( !isNaN( nodeValue ) ) return nodeValue;
		return defaultValue;
	}

	public static function getAttributeAsBoolean( xml:XML, attributeName:String,
												  defaultValue:Boolean = false ):Boolean {
		if ( !Boolean( xml ) ) return defaultValue;
		var attributeValue:String = String( xml.attribute( attributeName ) ).toLowerCase();
		if ( attributeValue == VALUE_TRUE ) return true;
		if ( attributeValue == VALUE_FALSE ) return false;
		return defaultValue;
	}

	public static function getAttributeAsString( xml:XML, attributeName:String, defaultValue:String = "" ):String {
		if ( !Boolean( xml ) ) return defaultValue;
		var attributeValue:String = StringUtils.getCleanString( xml.attribute( attributeName ) );
		if ( Boolean( attributeValue ) ) return attributeValue;
		return defaultValue;
	}

	/*public static function getAttributeAsStringVector( xml:XML, attributeName:String, separator:String,
													   defaultValue:Array = null ):Vector.<String> {
		if ( !Boolean( defaultValue ) ) defaultValue = [];
		if ( !Boolean( xml ) ) return VectorUtils.arrayToStringVector( defaultValue );
		var attributeValue:String = StringUtils.getCleanString( xml.attribute( attributeName ) );
		if ( Boolean( attributeValue ) ) return VectorUtils.arrayToStringVector( attributeValue.split( separator ) );
		return VectorUtils.arrayToStringVector( defaultValue );
	}*/

	public static function getAttributeAsInt( xml:XML, attributeName:String, defaultValue:int = 0 ):Number {
		return int( getAttributeAsFloat( xml, attributeName, defaultValue ) );
	}

	public static function getAttributeAsFloat( xml:XML, attributeName:String, defaultValue:Number = 0 ):Number {
		if ( !Boolean( xml ) ) return defaultValue;
		var attributeValue:Number = parseFloat( xml.attribute( attributeName ) );
		if ( !isNaN( attributeValue ) ) return attributeValue;
		return defaultValue;
	}

	protected static function getNodeValue( xmlList:XMLList ):String {
		var str:String = "";
		if ( xmlList.length() > 0 ) str = StringUtils.getCleanString( xmlList[0] );
		return str;
		//return StringUtils.stripDoubleCRLF(str);
	}

	public static function getFirstNode( xmlList:XMLList ):XML {
		if ( xmlList != null && xmlList.length() > 0 ) return xmlList[0];
		return null;
	}

	public static function getFilteredFirstNode( xmlList:XMLList, attributeNames:Array, attributeFilters:Array,
												 includeEmptyAttribute:Boolean = true,
												 allowAnyPartOfString:Boolean = true ):XML {
		var list:Vector.<XML> = getFilteredNodeList( xmlList, attributeNames, attributeFilters, includeEmptyAttribute, allowAnyPartOfString );
		return list.length > 0 ? list[0] : null;
	}

	public static function getFilteredNodeList( xmlList:XMLList, attributeNames:Array, attributeFilters:Array,
												includeEmptyAttribute:Boolean = true,
												allowAnyPartOfString:Boolean = true,
												followOriginalOrder:Boolean = false ):Vector.<XML> {
		// attributeFilters is an array containing either strings or string lists
		var list:Vector.<XML> = new Vector.<XML>();
		if ( xmlList != null && xmlList.length() >= 0 ) {
			var i:int;

			// Initial list
			for ( i = 0; i < xmlList.length(); i++ ) list.push( xmlList[i] );

//				if (list.length > 0 && list[0].name() == "scaleLogoBrand") {
//					log("--");
//					for (j = 0; j < list.length; j++) {
//						log("  Initial list => " + j + " => ada = [" + list[j].attribute("ada") + "], platform = [" + list[j].attribute("platform") + "], value = " + list[j].valueOf());
//					}
//				}

			// Apply all filters
			for ( i = 0; i < attributeNames.length; i++ ) {
				list = getSingleFilteredNodeList( list, attributeNames[i], convertStringToStringVectorIfNeeded( attributeFilters[i] ), includeEmptyAttribute, allowAnyPartOfString );
//					if (list.length > 0 && list[0].name() == "scaleLogoBrand") {
//						log("  Filtering [" + attributeNames[i] + "] = [" + attributeFilters[i] + "]");
//						for (j = 0; j < list.length; j++) {
//							log("    Iteration [" + i + "] => " + j + " => ada = " + list[j].attribute("ada") + ", platform = " + list[j].attribute("platform") + ", value = " + list[j].valueOf());
//						}
//					}
			}

			// Sort the list by more important items first (by number of items that match, then order of attribute name preference)
			if ( !followOriginalOrder ) {
				filterListSortAttributeNames = attributeNames;
				list = list.sort( sortFilterList );
			}
		}

		return list;
	}

	private static function convertStringToStringVectorIfNeeded( field:* ):Vector.<String> {
		return field is String ? Vector.<String>( [field] ) : field;
	}

	private static function sortFilterList( xml1:XML, xml2:XML ):int {
		// Count the number of items of both lists
		var i:int;
		var filters1:Vector.<int> = new Vector.<int>();
		var filters2:Vector.<int> = new Vector.<int>();
		for ( i = 0; i < filterListSortAttributeNames.length; i++ ) {
			if ( xml1.attribute( filterListSortAttributeNames[i] ).length() > 0 ) filters1.push( i );
			if ( xml2.attribute( filterListSortAttributeNames[i] ).length() > 0 ) filters2.push( i );
		}

		// Check if either node has more valid filter attributes
		if ( filters1.length > filters2.length ) return -1;
		if ( filters2.length > filters1.length ) return 1;

		// Same number of attributes, returns whoever has the more important item
		for ( i = 0; i < filters1.length; i++ ) {
			if ( filters1[i] < filters2[i] ) return -1;
			if ( filters1[i] > filters2[i] ) return 1;
		}

		// They're the same!
		return 0;
	}

	public static function getSingleFilteredNodeList( xmls:Vector.<XML>, attributeName:String,
													  attributeFilter:Vector.<String>,
													  includeEmptyAttribute:Boolean = true,
													  allowAnyPartOfString:Boolean = true ):Vector.<XML> {
		// Attribute filter is either a string or a vector of strings
		var list:Vector.<XML> = new Vector.<XML>();

		if ( xmls.length >= 0 ) {

			var currentAttribute:String;
			for ( var i:int = 0; i < xmls.length; i++ ) {
				currentAttribute = (xmls[i] as XML).attribute( attributeName );
				if ( !Boolean( currentAttribute ) && includeEmptyAttribute ) {
					// No attribute (is a fallback node), still add it
					list.push( xmls[i] );
				} else {
					// Has attribute
					for ( var j:int = 0; j < attributeFilter.length; j++ ) {
						if ( (currentAttribute == attributeFilter[j] || (allowAnyPartOfString && currentAttribute.indexOf( attributeFilter[j] ) > -1)) ) {
							list.push( xmls[i] );
							break;
						}
					}
				}
			}
		}

		return list;
	}

}
}
