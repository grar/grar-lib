package grar.utils {
import flash.display.LoaderInfo;
import flash.display.Stage;
import flash.display.StageAlign;
import flash.display.StageDisplayState;
import flash.display.StageOrientation;
import flash.display.StageQuality;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.system.Capabilities;
import flash.ui.ContextMenu;

import org.osflash.signals.Signal;

public class AppUtils {

	// can be DisplayObjectContainer in STARLING or FLASh.
	public static var main:*;

	private static var _stage:Stage;
	private static var hasDeterminedDebugStatus:Boolean;
	private static var _isDebuggingSWF:Boolean;
	private static var zoomScale:Number = (isAndroid() || isIOS()) ? 1 : 2;

	public static var baseScreen:Point = new Point( 375 * 2, 667 * 2 ); // iphone6
	public static var baseScreenDpi:uint = 326;
	public static var statusbarH:uint = 0; // only applied to iOS.

	// external properties to set global scale based on dpiScale, or screenWidth ratio... whatever u want.
	public static var appW:int = baseScreen.x;
	public static var appH:int = baseScreen.y;
	public static var scale:Number = 1;
	public static var dpi:Number = 0 ;
	public static var dpiScale:Number = 1;
	public static var retinaFactor:Number = 1;


	/**
	 * Utility that uses DevicesDef to set the base resolution.
	 * @param device
	 */
	public static function setDeviceDef( device:Object ):void {
		baseScreen.setTo( device.w, device.h );
		baseScreenDpi = device.dpi;
	}

	/**
	 * Manual size for base screen.
	 * @param screenW
	 * @param screenH
	 * @param screenDpi
	 */
	public static function setBaseScreen( screenW:uint, screenH:uint, screenDpi:uint ):void {
		baseScreen.setTo( screenW, screenH );
		baseScreenDpi = screenDpi;
	}

	/**
	 * This is the ENTRY method that defines sizes and scales!
	 * Requires that baseScreen, and baseScreenDpi to be defined previously
	 * and the stage has the "final" dimension.
	 * and set ::main to scale it properly on ADL.
	 *
	 * useRetinaFactor = false WHEN using Feathers, as it has his own rendering mechanism.
	 */
	// based scale on height, or width, according to device's rotation?
	public static function calculateScreen( scaleBasedOnHeight:Boolean=true, useRetinaFactor:Boolean=true ):void {
		var sw:int = _stage.stageWidth;
		var sh:int = _stage.stageHeight;
		retinaFactor = useRetinaFactor ? _stage.contentsScaleFactor : 1 ;

		if ( isADL()) {
			sw *= retinaFactor;
			sh *= retinaFactor;
			if ( main ) {
				main.scaleX = main.scaleY = 1 / retinaFactor;
			}
		} else {
			// logic to be decided.
		}
		scale = scaleBasedOnHeight ? sh / baseScreen.y : sw / baseScreen.x ;
		dpi = Capabilities.screenDPI;
		if ( ( isADL() && dpi > 300 ) || isIOS ) {
			statusbarH = 40 * scale;
		}

		// use Mac retina DPI.
		if ( isMac() ) {
			dpi = 220;
		}
		dpiScale = dpi / baseScreenDpi;
		appW = sw;
		appH = sh;

		// log the results.
		trace( "[AppUtils-screen] ::size=" + sw + "x" + sh + " scale=" + scale + " retinaFactor=" + retinaFactor + " dpiScale=" + dpiScale + " dpi=", dpi );
	}

	public static function getLaunchImageName( isPortraitOnly:Boolean=false):String{
		var name:String = "" ;
		if( !isIOS()) return name ;
		var isCurrentPortrait:Boolean = _stage.orientation==StageOrientation.DEFAULT||_stage.orientation==StageOrientation.UPSIDE_DOWN ;
		// check resolution?
		const rx:uint = Capabilities.screenResolutionX ;
		const ry:uint = Capabilities.screenResolutionY ;
		if( rx == 1242 && ry == 2208 ){
			// iphone 6+
			name = isCurrentPortrait ? "Default-414w-736h@3x~iphone.png" : "Default-414w-736h-Landscape@3x~iphone.png";
		} else if( rx == 1536 && ry == 2048 ){
			// ipad retina.
			name = isCurrentPortrait ? "Default-Portrait@2x~ipad.png" : "Default-Landscape@2x~ipad.png";
		} else if( rx == 768 && ry == 1024 ){
			// ipad
			name = isCurrentPortrait ? "Default-Portrait~ipad.png" : "Default-Landscape~ipad.png";
		} else if( rx == 750 ){
			// iphone6
			isPortraitOnly = true;
			name = "Default-375w-667h@2x~iphone.png";
		} else if( rx == 640 ){
			//iphone retina.
			isPortraitOnly = true;
			if( ry == 1136 ) {
				// iphone5
				name = "Default-568h@2x~iphone.png";
			} else {
				// iphone 4
				name = "Default@2x~iphone.png";
			}
		} else if( rx == 320 ){
			// iphone3
			isPortraitOnly = true;
			name = "Default~iphone.png";
		}
		return name ;
	}

	public static var onStageResize:Signal = new Signal();

	// relative screen scale
	public static function ss( value:Number ):Number {
		return scale * value;
	}

	// relative dpi scale
	public static function ds( value:Number ):Number {
		return dpiScale * value;
	}

	// relative to Macbook Retina output at 100% useful for Starling's Stats scale.
	public static function dsMac( value:Number, ceil:Boolean=false ):Number {
		value = value * dpi / 110 / retinaFactor ;
		if( ceil ) value = Math.ceil(value) ;
		return value ;
	}

	public static function retinaRect( rect:Rectangle ):Rectangle {
		var sc:Number = 1 / retinaFactor;
		rect.x *= sc;
		rect.y *= sc;
		rect.width *= sc;
		rect.height *= sc;
		return rect;
	}

	// ================================================================================================================
	// CONSTRUCTOR ----------------------------------------------------------------------------------------------------

	public function AppUtils() {
		// No constructor!
	}

	// ================================================================================================================
	// STATIC functions -----------------------------------------------------------------------------------------------

	public static function init( stage:Stage, isADL:Boolean ):void {
		_stage = stage;
		_isADLFlag = isADL ? 1 : 0 ;
		_stage.align = StageAlign.TOP_LEFT;
		_stage.scaleMode = StageScaleMode.NO_SCALE;
		_stage.quality = StageQuality.LOW;
		_stage.showDefaultContextMenu = false;
		_stage.stageFocusRect = false;
		_stage.addEventListener( Event.RESIZE, handleStageResize );
		retinaFactor = _stage.contentsScaleFactor;
		/*var appXML:XML = NativeApplication.nativeApplication.applicationDescriptor;
		 var ns:Namespace = appXML.namespace();
		 var isFullScreen:Boolean = appXML.ns::initialWindow.ns::fullScreen == "true";
		 var isLandscape:Boolean = appXML.ns::initialWindow.ns::aspectRatio == "landscape";
		 var stageWidth:int;
		 var stageHeight:int;
		 if (isFullScreen)
		 {
		 stage.setOrientation(StageOrientation.DEFAULT);

		 stageW = stage.fullScreenWidth;
		 stageH = stage.fullScreenHeight;

		 if (isLandscape)
		 stage.setOrientation(StageOrientation.ROTATED_RIGHT);
		 }
		 else
		 {
		 stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
		 stage.setOrientation(StageOrientation.DEFAULT);
		 stage.displayState = StageDisplayState.NORMAL;

		 stageW = stage.stageWidth;
		 stageH = stage.stageHeight;

		 if (isLandscape)
		 stage.setOrientation(StageOrientation.ROTATED_RIGHT);
		 }*/
	}

	public static function block():void {
		_stage.mouseChildren = false;
	}

	public static function unblock():void {
		_stage.mouseChildren = true;
	}

	private static function handleStageResize( event:Event ):void {
		onStageResize.dispatch();
	}

	public static function get mouseX():Number {
		return _stage.mouseX
	}

	public static function get mouseY():Number {
		return _stage.mouseY
	}

	public static function get stageWidth():int {
		// TODO :adjust according to orientation.
		return _stage.stageWidth;
	}

	public static function get stageHeight():int {
		// TODO :adjust according to orientation.
		return _stage.stageHeight;
	}


	public static function get fullscreen():Boolean {
		return _stage.displayState == StageDisplayState.FULL_SCREEN_INTERACTIVE;
	}

	public static function set fullscreen( value:Boolean ):void {
		_stage.displayState = value ? StageDisplayState.FULL_SCREEN_INTERACTIVE : StageDisplayState.NORMAL;
//		onFullscreen.dispatch( value );
	}

	//===================================================================================================================================================
	//
	//      ------  DEVICE AND APP STUFFS
	//
	//===================================================================================================================================================

	public static function getFlashVar( param:String ):String {
		return LoaderInfo( main.loaderInfo ).parameters[param];
	}

	// localFileReadDisable
	// hasPrinting
	// avHardwareDisable
	// hasAudio
	// language - en, pt
	// manufacturer - "Adobe Windows"
	// os - "Windows XP"
	// version
	// screenResolutionX
	// playerType

	// http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/flash/system/Capabilities.html#playerType
	//  "ActiveX" for the Flash Player ActiveX control used by Microsoft Internet Explorer
	//  "Desktop" for the Adobe AIR runtime (except for SWF content loaded by an HTML page, which has Capabilities.playerType set to "PlugIn")
	//  "External" for the external Flash Player or in test mode
	//  "PlugIn" for the Flash Player browser plug-in (and for SWF content loaded by an HTML page in an AIR application)
	//  "StandAlone" for the stand-alone Flash Player
	public static function isMac():Boolean {
		return Capabilities.os == "MacOS" || Capabilities.os.substr( 0, 6 ) == "Mac OS";
	}

	private static var _isADLFlag:int = -1;

	public static function setADL( flag:Boolean ):void {
		trace("set adll::",flag)
		_isADLFlag = flag ? 1 : 0;
	}

	public static function isADL():Boolean {
		return _isADLFlag > 0;
		/*if ( _isADLFlag == -1 ) {
		 var clase:Class = getClassByAlias( "flash.filesystem.File" );
		 if ( clase ) {
		 var f:Object = new clase();
		 _isADLFlag = f["name"] == "bin" ? 1 : 0;
		 } else {
		 _isADLFlag = 0;
		 }
		 }
		 return _isADLFlag > 0;*/
	}

	public static function isLinux():Boolean {
		// Android: "Linux 3.1.10-g52027f9"
		return Capabilities.os == "Linux";
	}

	public static function isMobile():Boolean {
		return !isDesktop();
	}

	public static function isDesktop():Boolean {
		return isWindows() || isMac() || isLinux();
	}

	public static function isWindows():Boolean {
		//return Capabilities.os == "Windows" || Capabilities.os == "Windows 7" || Capabilities.os == "Windows XP" || Capabilities.os == "Windows 2000" || Capabilities.os == "Windows 98/ME" || Capabilities.os == "Windows 95" || Capabilities.os == "Windows CE";
		return Capabilities.manufacturer == "Adobe Windows";
	}

	public static function isAndroid():Boolean {
		// Android: "Android Linux"
		// TODO: this is true on Linux too?
		return Capabilities.manufacturer == "Android Linux";
	}

	public static function isIOS():Boolean {
		// iOS: "Adobe iOS"
		return Capabilities.manufacturer == "Adobe iOS";
	}

	public static function isAirPlayer():Boolean {
		// This returns true even if it's on android!
		return Capabilities.playerType == "Desktop";
	}

	public static function isWebPlayer():Boolean {
		return Capabilities.playerType == "ActiveX" || Capabilities.playerType == "PlugIn";
	}

	public static function isStandalone():Boolean {
		// Desktop standalone
		return Capabilities.playerType == "StandAlone";
	}

	/**
	 * Tells whether this is being tested (ran on the IDE Flash player), or not.
	 * @return    TRUE if this is a test execution, false if otherwise.
	 */
	public static function isTestingFromFlashIDE():Boolean {
		return Capabilities.playerType == "External";
	}

	public static function isDebugSWF():Boolean {
		// Whether the SWF is compiled for debugging or not (only works on debug players)
		// http://michaelvandaniker.com/blog/2008/11/25/how-to-check-debug-swf/
		if ( !hasDeterminedDebugStatus ) {
			var stackTrace:String = new Error().getStackTrace();
			_isDebuggingSWF = stackTrace != null && stackTrace.indexOf( "[" ) != -1;
			hasDeterminedDebugStatus = true;
		}
		return _isDebuggingSWF;
	}

	// Userlayer stuff
	public static function resetContextMenu():void {
		// Clears the menu
		main.contextMenu = new ContextMenu();
//			root.contextMenu.hideBuiltInItems();

		//var mi:ContextMenuItem = new ContextMenuItem("Toggle Fullscreen");
		//mi.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, toggleFullScreen);
		//root.contextMenu.customItems.push(mi);
	}

	public static function disableContextMenu():void {
		stage.addEventListener( MouseEvent.RIGHT_MOUSE_DOWN, onRightMouseClickDoNothing );
	}

	public static function isDebugPlayer():Boolean {
		return Capabilities.isDebugger;
	}

	public static function getCentimetersInPixels( __centimeters:Number ):Number {
		// Return a certain number of centimeters in pixels
		return getInchesInPixels( __centimeters / 2.54 ); // * 0.393701
	}

	public static function getInchesInPixels( __inches:Number ):Number {
		// Return a certain number of inches in pixels
		//return __inches * Capabilities.screenDPI;
		return (__inches * 96) * getScreenDensityScale();
	}

	public static function getScreenDensityScale():Number {
		// Returns a density scale where 1 = 96dpi, 2 = 192dpi, etc

		if ( (isWebPlayer() || isAirPlayer()) && !isAndroid() && !isIOS() ) {
			// Normal player that always returns 96 as the dpi
			// Otherwise it'd return 72
			return 1;
		}

		// Everything else
		return Capabilities.screenDPI / 96;
	}

	public static function getScreenDensityScaleZoomed():Number {
		// Like getScreenDensityScale, but with a proper zoom
		return getScreenDensityScale() * zoomScale;
	}


	// ================================================================================================================
	// EVENT INTERFACE ------------------------------------------------------------------------------------------------

	private static function onRightMouseClickDoNothing( __e:Event ):void {
	}

	public static function get stage():Stage {
		return _stage;
	}

	public static function get isBlocked():Boolean {
		return _stage.mouseChildren == false;
	}
}
}
