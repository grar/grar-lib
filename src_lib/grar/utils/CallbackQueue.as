/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 1/15/15 11:32 AM.
 */
package grar.utils {
import com.greensock.TweenLite;

import flash.utils.getTimer;

import grar.utils.LoanShark;

import org.osflash.signals.Signal;

/**
 *
 * Simple class to deal with repetitive callbacks based on an array of elements.
 *
 */
public class CallbackQueue {


	// --- POOL ---
	private static var _pool:LoanShark = new LoanShark( CallbackQueue, false, 0, 0, null, "reset" );
	private static var _instanceCounter:int = 0;
	public var instanceCount:int = 0;

	public static function isPoolEmpty():Boolean {return _pool.used == 0;}

	public static function get():CallbackQueue {return _pool.borrowObject() as CallbackQueue;}

	public function returnToPool():void {_pool.returnObject( this );}

	public function reset():void {
		_isProcessing = false;
		data = null;
		counter = 0;
		processMax = 1;
		total = 0;
		remaining = 0;
		delayBulk = 0.1;
		numBatches = 0;
		onComplete.removeAll();
		onBatch.removeAll();
		onProcess.removeAll();
	}

	public function CallbackQueue() {
	}

	private var _isProcessing:Boolean;
	public static var verbose:Boolean = true ;
	public var data:Array;
	public var processMax:int = 1;
	public var counter:int;
	public var total:int;
	public var remaining:int;
	public var delayBulk:Number = 0.1;
	public var onProcess:Signal = new Signal();
	public var onComplete:Signal = new Signal();
	public var onBatch:Signal = new Signal();
	public var numBatches:Number;

	public var startTime:int = 0;
	public var currentDelay:Number;

	public function setData( p_arr:Array ):void {
		data = p_arr.concat();
		remaining = data.length;
	}

	public function start():void {
		if( _isProcessing ){
			return ;
		}
		numBatches = data.length / processMax;
		total = data.length;
		_isProcessing = true;
		log( "num batches=", numBatches) ;
		startTime = getTimer();
		loadNext();
	}

	public function loadNext():void {
		_isProcessing = true;
		var len:int = Math.min( data.length, counter + processMax );
		var tmp_arr:Array = [];
		currentDelay = ( getTimer() - startTime ) * .001;
		for ( var i:int = counter; i < len; i++ ) {
			onProcess.dispatch( i, data[i] );
			tmp_arr.push( data[i] );
			counter++;
		}
		remaining = data.length - len;
		log( "call remain=", remaining ) ;
		if ( remaining > 0 ) {
			TweenLite.delayedCall( delayBulk, loadNext );
			onBatch.dispatch( tmp_arr );
		} else {
			_isProcessing = false;
			onBatch.dispatch( tmp_arr );
			processComplete();
		}
	}

	public function pause():void {
		_isProcessing = false;
		TweenLite.killTweensOf( loadNext );
	}

	public function resume():void {
		if ( remaining > 0 ) {
			loadNext();
		}
	}

	private function processComplete():void {
		log( "complete" ) ;
		_isProcessing = false;
		onComplete.dispatch();
		returnToPool();
	}

	public function get percent():Number {
		return Math.max( 0, Math.min( counter / data.length, 1 ) );
	}

	public function get isProcessing():Boolean {
		return _isProcessing;
	}

	public static function create( data:Array, processMax:int, delayBulk:Number, callbackProgress:Function,
								   callbackBulk:Function, callbackComplete:Function, autoStart:Boolean=true ):CallbackQueue {
		var cbq:CallbackQueue = CallbackQueue.get();
		cbq.setData( data );
		cbq.processMax = processMax;
		cbq.delayBulk = delayBulk;
		if ( callbackProgress ) cbq.onProcess.add( callbackProgress );
		if ( callbackBulk ) cbq.onBatch.add( callbackBulk );
		if ( callbackComplete ) cbq.onComplete.addOnce( callbackComplete );
		if( autoStart ) TweenLite.delayedCall( 0, cbq.start );
		return cbq;
	}


	private function log( ...args ):void {
		if( !verbose ) return ;
		trace("[CallbackQueue] " + args.join(" ")) ;
	}

}
}
