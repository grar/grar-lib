/**
 * Code by Rodrigo López Peker (grar) on 2/21/16 5:54 PM.
 *
 */
package grar.utils {
public class BooleanUtils {
	public function BooleanUtils() {
	}

	public static function isEmpty( obj:* ):Boolean {
		if ( obj == undefined ) return true;
		if ( obj is Number ) return isNaN( obj );
		if ( obj is Array || obj is String ) return obj.length == 0;
		if ( obj is Object ) {
			for ( var prop:String in obj ) return false;
			return true;
		}
		return false;
	}
}
}
