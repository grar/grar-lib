/**
 * Code by Rodrigo López Peker (grar) on 3/2/16 2:06 PM.
 *
 */
package grar.utils.mobile {
public class DevicesDef {

	// -- IPHONES
	public static const IPHONE6SPLUS:Object = {w: 1920, h: 1080, dpi: 401, osid: ["iPhone8,2"], name: "iphone 6s plus"};
	public static const IPHONE6S:Object = {w: 750, h: 1334, dpi: 326, osid: ["iPhone8,1"], name: "iphone 6s"};

	public static const IPHONE6PLUS:Object = {w: 1242, h: 2208, dpi: 401, osid: ["iPhone7,1"], name: "iphone 6 plus"};
	public static const IPHONE6:Object = {w: 750, h: 1334, dpi: 326, osid: ["iPhone7,2"], name: "iphone 6"};
	public static const IPHONE5S:Object = {
		w: 640,
		h: 1136,
		dpi: 326,
		osid: ["iPhone6,1", "iPhone6,2"],
		name: "iphone 5s"
	};
	public static const IPHONE5C:Object = {
		w: 640,
		h: 1136,
		dpi: 326,
		osid: ["iPhone5,3", "iPhone5,4"],
		name: "iphone 5c"
	};
	public static const IPHONE5:Object = {
		w: 640,
		h: 1136,
		dpi: 326,
		osid: ["iPhone5,1", "iPhone5,2"],
		name: "iphone 5"
	};
	public static const IPHONE4S:Object = {w: 640, h: 960, dpi: 326, osid: ["iPhone4,1"], name: "iphone 4s"};
	public static const IPHONE4:Object = {
		w: 640,
		h: 960,
		dpi: 326,
		osid: ["iPhone3,1", "iPhone3,2", "iPhone3,3"],
		name: "iphone 4"
	};
	public static const IPHONE3GS:Object = {w: 320, h: 480, dpi: 163, osid: ["iPhone2,1"], name: "iphone 3gs"};

	// -- IPODS
	public static const IPOD6:Object = {w: 640, h: 1136, dpi: 326, osid: ["iPod7,1"], name: "ipod 6"};
	public static const IPOD5:Object = {w: 640, h: 1136, dpi: 326, osid: ["iPod5,1"], name: "ipod 5"};
	public static const IPOD4:Object = {w: 640, h: 960, dpi: 326, osid: ["iPod4,1"], name: "ipod 4"};
	public static const IPOD3:Object = {w: 320, h: 480, dpi: 163, osid: ["iPod3,1"], name: "ipod 3"};
	public static const IPOD2:Object = {w: 320, h: 480, dpi: 163, osid: ["iPod2,1"], name: "ipod 2"};

	// -- IPADS.
	public static const IPAD1:Object = {w: 768, h: 1024, dpi: 132, osid: ["iPad1,1"], name: "ipad 1"};
	public static const IPAD2:Object = {
		w: 768,
		h: 1024,
		dpi: 132,
		osid: ["iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4"],
		name: "ipad 2"
	};
	public static const IPAD3:Object = {
		w: 536,
		h: 2048,
		dpi: 264,
		osid: ["iPad3,1", "iPad3,2", "iPad3,3"],
		name: "ipad 3"
	};
	public static const IPAD4:Object = {
		w: 1536,
		h: 2048,
		dpi: 264,
		osid: ["iPad3,4", "iPad3,5", "iPad3,6"],
		name: "ipad 4"
	};
	public static const IPAD_AIR:Object = {
		w: 1536,
		h: 2048,
		dpi: 264,
		osid: ["iPad4,1", "iPad4,2", "iPad4,3"],
		name: "ipad air"
	};
	public static const IPAD_AIR2:Object = {
		w: 1536,
		h: 2048,
		dpi: 264,
		osid: ["iPad5,3", "iPad5,4"],
		name: "ipad air 2"
	};
	public static const IPAD_MINI:Object = {
		w: 768,
		h: 1024,
		dpi: 132,
		osid: ["iPad2,5", "iPad2,6", "iPad2,7"],
		name: "ipad mini"
	};
	public static const IPAD_MINI2:Object = {
		w: 1536,
		h: 2048,
		dpi: 264,
		osid: ["iPad4,4", "iPad4,5", "iPad4,6"],
		name: "ipad mini 2"
	};
	public static const IPAD_MINI3:Object = {
		w: 1536,
		h: 2048,
		dpi: 326,
		osid: ["iPad4,7", "iPad4,8", "iPad4,9"],
		name: "ipad mini 3"
	};
	public static const IPAD_MINI4:Object = {
		w: 1536,
		h: 2048,
		dpi: 326,
		osid: ["iPad5,1", "iPad5,2"],
		name: "ipad mini 4"
	};
	public static const IPAD_PRO:Object = {w: 2048, h: 2732, dpi: 264, osid: ["iPad6,7", "iPad6,8"], name: "ipad pro"};


	//===================================================================================================================================================
	//
	//      ------  ANDROID
	//
	//===================================================================================================================================================
	public static const SAMSUNG_GALAXY_NEXUS:Object = {w: 1280, h: 720, dpi: 316, name: "samsung galaxy nexus"};
	public static const SAMSUNG_GALAXY_S3:Object = {w: 720, h: 1280, dpi: 306, name: "samsung galaxy s3"};
	public static const SAMSUNG_GALAXY_S4:Object = {w: 1080, h: 1920, dpi: 441, name: "samsung galaxy s4"};
	public static const SAMSUNG_GALAXY_S5:Object = {w: 1080, h: 1920, dpi: 432, name: "samsung galaxy s5"};
	public static const SAMSUNG_GALAXY_S6:Object = {w: 1440, h: 2560, dpi: 577, name: "samsung galaxy s6/edge"};
	public static const LG_G3:Object = {w: 1440, h: 2560, dpi: 538, name: "lg g3"};
	public static const LG_G4:Object = {w: 1440, h: 2560, dpi: 538, name: "lg g4"};
	public static const AMAZON_KINDLE_FIRE:Object = {w: 600, h: 1024, dpi: 169, name: "amazon kindle fire"};

	public function DevicesDef() {}
}
}
