/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 8/17/15 7:10 PM.
 */
package grar.utils.mobile {
import flash.events.GeolocationEvent;
import flash.filesystem.File;
import flash.geom.Point;
import flash.globalization.LocaleID;
import flash.globalization.StringTools;
import flash.sensors.Geolocation;
import flash.system.Capabilities;

import grar.utils.DateUtils;
import grar.utils.StringUtils;

import org.osflash.signals.Signal;

public class DeviceInfo {

	public function DeviceInfo() {}

	// we can modify this list to show less info.
	private static var ALL_PROPS:Array = [
		"locale",
		"osName",
		"osVersion",
		"model",
		"manufacturer",
		"timezone",
		"kernel",
		"dpi",
		"screenSize",
		"publicIP",
		"gpsLatLon"
	];

	// WARNING: deviceId and others must be updated externally, using an ANE.
	public static var deviceId:String;
	public static var locale:String;
	public static var osName:String;
	public static var osVersion:String;
	public static var model:String;
	public static var manufacturer:String;
	public static var timezone:String;
	public static var publicIP:String;
	public static var kernel:String;
	public static var dpi:int;
	public static var screenSize:String;
	public static var resolution:Point = new Point();
	public static var gpsLatLon:String;
	public static var gpsLocation:Object; // null
	public static var onPropertyUpdated:Signal = new Signal();

	// GPS stuff.
	private static var _geo:Geolocation;
	private static var _gps_request_update_interval:uint = 60000; // 1 min.

	private static var _isAndroid:Boolean;
	private static var _isIOS:Boolean;
	private static var _isOSX:Boolean;
	private static var _isWin:Boolean;

	public static function init():void {

		if ( manufacturer )
			return;

		// includes lang-COUNTRY
		locale = new StringTools( LocaleID.DEFAULT ).actualLocaleIDName;
		if ( !locale ) {
			// fallback to only lang.
			locale = Capabilities.languages[0];
		}

//		timezone = DateUtils.getTimezoneGMT();
		timezone = DateUtils.getTimezone( new Date() );
		manufacturer = Capabilities.manufacturer;
		manufacturer = StringUtils.replace( manufacturer, "Adobe ", "" );
		dpi = Capabilities.screenDPI;
		resolution.setTo( Capabilities.screenResolutionX, Capabilities.screenResolutionY );
		screenSize = resolution.x + " x " + resolution.y;

		if ( !osName )
			initOS();

		var key:String;
		var parts:Array;
		if ( isIOS ) {
			key = "iPhone OS";
			parts = osName.substr( key.length + 1, osName.length ).split( " " );
			osName = key;
			osVersion = parts[0];
			model = parts[1];
		} else if ( isAndroid ) {
			kernel = Capabilities.os;
			osName = "Android";
			// analyze build.prop
//			var buildPropPath:String = File.desktopDirectory.resolvePath("build.prop").nativePath ;
//			var androidInfo:AndroidNativeDeviceInfo = new AndroidNativeDeviceInfo(buildPropPath) ;
//			var androidInfo:AndroidNativeDeviceInfo = new AndroidNativeDeviceInfo();
//			androidInfo.parse();
			/*osVersion = AndroidNativeDeviceProperties.OS_VERSION.value;
			 if ( AndroidNativeDeviceProperties.OS_BUILD.value != "" ) {
			 osVersion += "[" + AndroidNativeDeviceProperties.OS_BUILD.value + "]";
			 }
			 manufacturer = AndroidNativeDeviceProperties.PRODUCT_BRAND.value;
			 model = AndroidNativeDeviceProperties.PRODUCT_MODEL.value;
			 if ( AndroidNativeDeviceProperties.LCD_DENSITY.value ) {
			 dpi = int( AndroidNativeDeviceProperties.LCD_DENSITY.value );
			 }*/
		} else if ( isOSX ) {
			key = "Mac OS";
			parts = osName.substr( key.length + 1, osName.length ).split( " " );
			osName = key;
			osVersion = parts[0];
//			model = null ; Needs ANE to detect mac model.
		}

		if ( _isADL ) {
			osName += " ADL debugger.";
		}

		// get public IP.
//		lookupPublicIP();
	}

	public static var isDebugVersion:Boolean;

	// execute it after everything is loaded.
	public static function checkDebugVersion():void {
		isDebugVersion = new Error().getStackTrace().search( /:[0-9]+]$/m ) > -1;
	}

	private static function initOS():void {
		osName = Capabilities.os;
		_isAndroid = osName.toLowerCase().indexOf( "linux " ) > -1;
		_isIOS = osName.toLowerCase().indexOf( "iphone os " ) > -1;
		_isOSX = osName.toLowerCase().indexOf( "mac os " ) > -1;
		_isWin = osName.toLowerCase().indexOf( "win" ) > -1;
		_isADL = File.applicationDirectory.name == "bin";
	}

	public static function lookupPublicIP():void {
		/*ServiceCall.call( "https://api.ipify.org", function ( res:String ):void {
		 publicIP = res;
		 onPropertyUpdated.dispatch();
		 } );*/
	}


	//===================================================================================================================================================
	//
	//      ------  GPS STUFFS.
	//
	//===================================================================================================================================================
	public static function initGPS():void {
		if ( !Geolocation.isSupported ) {
			trace( "ERROR: GPS not supported!" );
			return;
		}
		if ( _geo ) return;
		_geo = new Geolocation();
		_geo.setRequestedUpdateInterval( 100 );

		if ( _geo.muted ) {
			trace( "ERROR: GPS disabled or blocked for the app." );
		} else {
			_geo.addEventListener( GeolocationEvent.UPDATE, updateGeolocation );
		}
	}

	public static function setGPSRequestInterval( value:uint ):void {
		_gps_request_update_interval = value;
		_geo.setRequestedUpdateInterval( _gps_request_update_interval );
	}

	/**
	 * To be called externally if needed.
	 * @param event
	 */
	public static function updateGeolocation( event:GeolocationEvent ):void {
		if ( !gpsLocation ) {
			// normalize the requestUpdateInterval.
			_geo.setRequestedUpdateInterval( _gps_request_update_interval );
			trace( "GPS Request interval:", _gps_request_update_interval );
			gpsLocation = {};
		}
		gpsLocation = {lat: event.latitude, lon: event.longitude};
		gpsLatLon = gpsLocation.lat + "," + gpsLocation.lon;
		onPropertyUpdated.dispatch();
		// @See http://www.adobe.com/devnet/air/quick_start_as/quickstarts/qs_as_geolocation_api.html
//		event.latitude
//		event.longitude
//		event.altitude ;
//		event.heading
//		event.speed
//		event.verticalAccuracy
//		event.horizontalAccuracy
//		event.timestamp
	}

	// ---------


	//===================================================================================================================================================
	//
	//      ------  UTILS
	//
	//===================================================================================================================================================

	public static function toString( properties:Array = null, sep:String = "|" ):String {
		if ( !properties ) properties = ALL_PROPS;
		const NL:String = " " + sep + " ";
		var output:String = "";
		for each( var prop:String in properties ) {
			if ( prop in DeviceInfo && DeviceInfo[prop] != null ) {
				output += prop + "=" + DeviceInfo[prop] + NL;
			}
		}
		output = output.substr( 0, output.length - NL.length );
		return output;
	}

	public static function toJSON( properties:Array = null ):Object {
		if ( !properties ) properties = ALL_PROPS;
		var output:Object = {};
		for each( var prop:String in properties ) {
			if ( prop in DeviceInfo && DeviceInfo[prop] != null ) {
				output[prop] = DeviceInfo[prop];
			}
		}
		return output;
	}

	public static function toJSONString( properties:Array = null ):String {
		return JSON.stringify( DeviceInfo.toJSON( properties ) );
	}

	private static var _isADL:Boolean;
	public static function get isADL():Boolean {
		if ( !osName ) initOS();
		return _isADL;
	}

	public static function get isAndroid():Boolean {
		if ( !osName ) initOS();
		return _isAndroid;
	}

	public static function get isIOS():Boolean {
		if ( !osName ) initOS();
		return _isIOS;
	}

	public static function get isDesktop():Boolean {
		return isWindows || isOSX;
	}

	public static function get isMobile():Boolean {
		return isIOS || isAndroid;
	}

	public static function get isOSX():Boolean {
		if ( !osName ) initOS();
		return _isOSX;
	}

	public static function get isWindows():Boolean {
		if ( !osName ) initOS();
		return _isWin;
	}


}
}
