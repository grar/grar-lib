/**
 * Created with IntelliJ IDEA.
 * User: rodrigolopez
 * Date: 5/14/12
 * Time: 11:04 AM
 */
package grar.utils {

import flash.display.Stage;
import flash.events.KeyboardEvent;
import flash.ui.Keyboard;
import flash.utils.Dictionary;

import org.osflash.signals.Signal;

public class KeyInput {

	private static var _stage:Stage;
	private static var _keysDown:Vector.<int>;

	private static var _inited:Boolean = false;
	private static var _onKeyDown:Signal;
	private static var _onKeyUp:Signal;
	private static var _listenDict:Dictionary = new Dictionary( true );
	public static var lastEvent:KeyboardEvent;

	public static var stringCodes:Object = {
		"backspace": 8,
		"tab": 9,
		"enter": 13,
		"shift": 16,
		"control": 17,
		"cmd": 15,
		"capslock": 20,
		"esc": 27,
		"space": 32,
		"pageup": 33,
		"pagedown": 34,
		"end": 35,
		"home": 36,
		"left": 37,
		"up": 38,
		"right": 39,
		"down": 40,
		"insert": 45,
		"delete": 46,
		"numlock": 144,
		"scrlk": 145,
		"pause ": 19,
		"a": 65,
		"b": 66,
		"c": 67,
		"d": 68,
		"e": 69,
		"f": 70,
		"g": 71,
		"h": 72,
		"i": 73,
		"j": 74,
		"k": 75,
		"l": 76,
		"m": 77,
		"n": 78,
		"o": 79,
		"p": 80,
		"q": 81,
		"r": 82,
		"s": 83,
		"t": 84,
		"u": 85,
		"v": 86,
		"w": 87,
		"x": 88,
		"y": 89,
		"z": 90,
		"0": 48,
		"1": 49,
		"2": 50,
		"3": 51,
		"4": 52,
		"5": 53,
		"6": 54,
		"7": 55,
		"8": 56,
		"9": 57,
		"+": 221,
		"-": 191,
		"f1": 112,
		"f2": 113,
		"f3": 114,
		"f4": 115,
		"f5": 116,
		"f6": 117,
		"f7": 118,
		"f8": 119,
		"f9": 120,
//		"f10":nokey,
		"f11": 122,
		"f12": 123,
		"f13": 124,
		"f14": 125,
		"f15": 126
	}

	public static function getStringCode( key:String ):uint {
		return stringCodes[key] as uint ;
	}

	public static function init( pStage:Stage ):void {
		if ( _inited ) {
			trace( "KeyInput already initialized." );
			return;
		}
		_stage = pStage;
		_stage.addEventListener( KeyboardEvent.KEY_DOWN, handleKeyDown, false, 0, true );
		_stage.addEventListener( KeyboardEvent.KEY_UP, handleKeyUp, false, 0, true );
		_inited = true;
		_keysDown = new Vector.<int>( 255, true );

		_onKeyDown = new Signal();
		_onKeyUp = new Signal()
	}

	public static function listen( key:uint, callback:Function, listenOnPress:Boolean = true ):void {
		if ( _listenDict[key] != undefined ) {
			trace( "Overriden implementacion de ", key );
		}
		_listenDict[key] = {callback: callback, listenOnPress: listenOnPress};
	}

	public static function stopListen( key:uint ):void {
		delete _listenDict[key];
	}

	private static function handleKeyUp( event:KeyboardEvent ):void {
		var key:uint = event.keyCode;
		_keysDown[key] = -1;
//		trace("key up::", key, event.commandKey, event.keyCode ) ;
		lastEvent = event;
		// special case for COMMAND.
		if ( _listenDict[key] && !_listenDict[key].listenOnPress ) {
			_listenDict[key].callback();
		} else {
			_onKeyUp.dispatch( event.keyCode );
		}
	}

	private static function handleKeyDown( event:KeyboardEvent ):void {
		var key:uint = event.keyCode;
		lastEvent = event;
		if ( _keysDown[key] <= 0 ) {
			_keysDown[key] = 1;
			if ( _listenDict[key] && _listenDict[key].listenOnPress ) {
				_listenDict[key].callback();
			} else {
//					kd.dispatch(event);
				_onKeyDown.dispatch( event.keyCode );
			}
		}
		// if we were pressing cmd+shift, release that missing letter.
//		trace(event.shiftKey, event.commandKey, event.controlKey ) ;
		/*if( event.shiftKey && event.commandKey ){
			trace("code::", key) ;
			for( var i:int; i < _keysDown.length;++i ){
				if( i < 15 && i > 16 ){
					_keysDown[i]=-1 ;
				}
			}
		}*/
	}

	public static var kd:Signal = new Signal();

	public static function shiftPressed():Boolean { return isDown( Keyboard.SHIFT );}

	public static function isUp( keyCode:uint ):Boolean {
		return _keysDown[keyCode] <= 0;
	}

	public static function isDown( keyCode:uint ):Boolean {
		return _keysDown[keyCode] > 0;
	}

	public static function dispose():void {
		_stage.removeEventListener( KeyboardEvent.KEY_DOWN, handleKeyDown );
		_stage.removeEventListener( KeyboardEvent.KEY_UP, handleKeyUp )
	}

	public function KeyInput() {
	}

	public static function get onKeyDown():Signal {
		return _onKeyDown;
	}

	public static function get onKeyUp():Signal {
		return _onKeyUp;
	}

	public static function checkCombo( p_list:Array ):Boolean {
		for each( var k:uint in p_list ) {
			if ( !isDown( k ) ) return false;
		}
		return true;
	}

	public static function get inited():Boolean {
		return _inited;
	}
}
}