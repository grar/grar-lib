package grar.io {

import flash.data.EncryptedLocalStore;
import flash.net.SharedObject;
import flash.utils.ByteArray;

public class SOManager implements IPersistence {

	private static const objCreationKey:String = "3571836517839456173849"; // used to block unauthorized new

	private static var _instance:SOManager;
	public static function get instance():SOManager {
		if ( !_instance ) {
			_instance = new SOManager( objCreationKey );
		}
		return _instance
	}

	public function SOManager( objCreationKey:String = null ) {
		if ( SOManager.objCreationKey == objCreationKey ) {
			init();
		}
		else {
			throw new Error( "No 'new' allowed; use static 'instance' instead" );
		}
	}

	public static var ID:String = 'data';
	private var so:SharedObject;
	private var ba:ByteArray;

	private function init():void {
		ba = new ByteArray();
		so = SharedObject.getLocal( ID );
	}

	public function get( prop:String, encrypted:Boolean = false ):* {
		if ( !so ) {
			init();
		}
		if ( encrypted ) {
			ba = EncryptedLocalStore.getItem( prop );
			if ( !ba ) {
				return null;
			}
			ba.position = 0;
			return ba.readObject();
		}
		else {
			return so.data[prop];
		}
	}

	public function set( prop:String, value:*, encrypt:Boolean = false ):void {
		if ( !so ) {
			init();
		}
		if ( encrypt && EncryptedLocalStore.isSupported ) {
			if ( !ba ) {
				ba = new ByteArray();
			}
			ba.clear();
			ba.position = 0;
			ba.writeObject( value );
			if ( value == null ) {
				EncryptedLocalStore.removeItem( prop );
			}
			else {
				EncryptedLocalStore.setItem( prop, ba );
			}
		}
		else {
			so.data[prop] = value;
			if ( value == null ) {
				delete so.data[prop];
			}
			so.flush();
		}
	}

	[Inline]
	public function flush():void {
		// Note: there is no benefit to delay flushing. It is always done upon setting/clearing.
	}

	[Inline]
	public function clear( prop:String ):void {
		set( prop, null );
	}

	[Inline]
	public function dispose():void {
		so.clear();
		so = null;
	}

}

}
