/**
 * Code by Rodrigo López Peker on 12/12/15 6:46 PM.
 *
 */
package grar.io {
import flash.filesystem.File;

import grar.utils.AppUtils;

import grar.utils.mobile.DeviceInfo;

/**
 * This is like a template to access all common files in a project.
 * To be proxied by another class if needed. (AppFiles)
 */
public class CommonFiles {

	public static var APP_DIR:File;
	public static var STORAGE_DIR:File;
	public static var DOCS_DIR:File;
	public static var ASSETS_DIR:File;
	public static var CACHE:File;
	public static var DB:File;
	public static var CONFIG_INI:File;
	public static var TEMP_DIR:File;

	public static var winKeyboard:File;
	private static var winTapTipPath:String = "C:\\Program Files\\Common Files\\microsoft shared\\ink\\TabTip.exe";
	private static var winOSKPath:String = "C:\\Windows\\System32\\osk.exe";

	public static var verbose:Boolean = false;

	/**
	 *
	 * @param appBasePath - Used for multiple compilations (use same class on different projects structures).
	 */
	public static function init():void {
		STORAGE_DIR = File.applicationStorageDirectory;
		DOCS_DIR = File.documentsDirectory;
		APP_DIR = File.applicationDirectory;
		if ( AppUtils.isADL() ) {
			APP_DIR = new File( File.applicationDirectory.nativePath ).parent.resolvePath( "bin-assets" );
		}

		if ( DeviceInfo.isIOS ) {
			STORAGE_DIR = DOCS_DIR;
		}
		ASSETS_DIR = APP_DIR.resolvePath( "assets" );
		CACHE = STORAGE_DIR.resolvePath( "cache" );
		DB = STORAGE_DIR.resolvePath( "db.sqlite" );
		CONFIG_INI = STORAGE_DIR.resolvePath( "config.ini" );

		if ( DeviceInfo.isWindows ) {
			// assign onscreen keyboard.
			winKeyboard = new File( winTapTipPath );
			if ( !winKeyboard.exists ) {
				winKeyboard = new File( winOSKPath );
				if ( !winKeyboard.exists ) {
					winKeyboard = null;
				}
			}
		}
		if ( verbose ) {
			trace( "[CommonFiles] File App dir = " + APP_DIR.nativePath );
			trace( "[CommonFiles] File Storage dir = " + STORAGE_DIR.nativePath );
			trace( "[CommonFiles] File Cache = " + CACHE ? CACHE.nativePath : "not defined" );
			trace( "[CommonFiles] File DB = " + DB.nativePath );
		}

		createMissingFolders();
	}

	public static function createMissingFolders():void {
		if ( STORAGE_DIR && !STORAGE_DIR.exists ) STORAGE_DIR.createDirectory();
	}

	public static function setTempDir( tempDir:String = null ):void {
		if ( tempDir != null ) {
			TEMP_DIR = new File();
			TEMP_DIR.url = tempDir;
		}
		if ( TEMP_DIR == null || !TEMP_DIR.exists ) {
			// usually created in cache.
			TEMP_DIR = File.createTempDirectory();
			Cache.set( "temp_dir", TEMP_DIR.url );
		}
		if ( verbose ) {
			trace( "[CommonFiles] Temp dir = ", TEMP_DIR.nativePath );
		}
	}

}
}