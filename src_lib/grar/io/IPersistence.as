/**
 * Created by tasos on 28/11/15 17:01.
 */
package grar.io {

public interface IPersistence {

	function get( prop:String, encrypted:Boolean = false ):*;

	function set( prop:String, value:*, encrypt:Boolean = false ):void;

	function flush():void;

	function dispose():void;

	function clear( prop:String ):void;

}

}
