/**
 * Code by Rodrigo López Peker (GRAR) on 2/17/16 4:51 PM.
 *
 */
package grar.io {
import flash.events.TimerEvent;
import flash.filesystem.File;
import flash.utils.Timer;

import org.osflash.signals.Signal;

public class FileMonitor {

	public static const DEFAULT_INTERVAL:int = 1000;

	private var _file:File;
	private var _timer:Timer;
	private var _interval:Number;
	private var _fileExists:Boolean = false;
	private var _lastModificationTime:Number;
	private var _lastCreationTime:Number;

	public static const REPLACED:String = "replaced";
	public static const CHANGE:String = "change";
	public static const CREATE:String = "create";
	public static const MOVE:String = "move";

	public var onSignal:Signal;
	public var ignoreFileUpdate:Boolean;

	public function FileMonitor( file:File, interval:int = -1 ) {
		_file = file;
		onSignal = new Signal();
		if ( interval < 0 )
			interval = DEFAULT_INTERVAL;
		_interval = interval;
	}

	public function watch( flag:Boolean ):void {
		var running:Boolean = ( _timer && _timer.running );
		if ( flag == running )
			return;

		if ( flag ) {
			if ( !_file ) return;
			if ( !_timer ) {
				_timer = new Timer( _interval );
				_timer.addEventListener( TimerEvent.TIMER, handleTimer );
			}
			_timer.start();
		} else {
			if ( !_timer ) return;
			_timer.addEventListener( TimerEvent.TIMER, handleTimer );
			_timer.stop();
		}
	}


	private function handleTimer( event:TimerEvent ):void {
		var signal:String;
		if ( _fileExists != _file.exists ) {
			if ( _file.exists ) {
				// file created.
				signal = CREATE;
				_lastModificationTime = _file.modificationDate.getTime();
				_lastCreationTime = _file.creationDate.getTime();
			} else {
				// file moved/deleted
				signal = MOVE;
			}
		} else {
			if ( !_file.exists ) return;
			var modifiedTime:Number = _file.modificationDate.getTime();
			var creationTime:Number = _file.creationDate.getTime();
			if ( modifiedTime == _lastModificationTime ) {
				return;
			} else {
				_lastModificationTime = modifiedTime;
				// used when we replace the file (could match the creation date)
				if ( ignoreFileUpdate ) {
//					trace('ignore the update');
					ignoreFileUpdate = false;
					return;
				}
				signal = CHANGE;
			}
			if ( _lastCreationTime != creationTime ) {
				_lastCreationTime = creationTime;
				signal = REPLACED;
			}
		}
		if ( signal ) {
			onSignal.dispatch( signal, this );
		}
	}

	public function get file():File {
		return _file;
	}

	public function set file( value:File ):void {
		if( _file == value ) return ;
		if( _timer && _timer.running ) watch(false) ;
		_file = file;
		if (!_file) {
			_fileExists = false;
			return;
		}
		//note : this will throw an __error if new File() is passed in.
		_fileExists = _file.exists;
		if (_fileExists) {
			_lastModificationTime = _file.modificationDate.getTime();
			_lastCreationTime= _file.creationDate.getTime();
		}
	}
}
}
