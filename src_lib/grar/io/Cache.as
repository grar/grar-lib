/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 10/21/15 12:13 PM.
 */
package grar.io {
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;

import grar.air.FileUtils;

public class Cache {

	public function Cache() {
	}

	public static var verbose:Boolean = true;

	private static var _cacheFile:File;
	private static var _data:Object = {};
	private static var _fs:FileStream;
	private static var _pending:Boolean;

	public static function init( cacheFile:File = null ):void {
		if ( !cacheFile ) {
			cacheFile = File.applicationStorageDirectory.resolvePath( "cache" );
		}
		_cacheFile = cacheFile;
		// initialize reading.
		_fs = new FileStream();
		_fs.open( _cacheFile, FileMode.UPDATE );
		_fs.position = 0;
		log( " path=\"" + _cacheFile.nativePath + "\" size=\"", FileUtils.redeableBytes( _fs.bytesAvailable ) + "\"" );
		if ( _fs.bytesAvailable > 0 ) {
			_data = _fs.readObject();
		}
	}

	private static function log( ...args ):void {
		if ( !verbose )
			return;
		trace( "Cache::", args );
	}

	public static function set( prop:String, value:*, flush:Boolean = true ):void {
		if ( value === null ) {
			delete _data[prop];
		} else {
			_data[prop] = value;
		}
		_pending = true;
		if ( flush ) {
			Cache.flush();
		}
	}

	public static function clear( prop:String, flush:Boolean = true ):void {
		delete _data[prop];
		_pending = true;
		if ( flush ) Cache.flush();
	}

	public static function get( prop:String, def:* = undefined ):* {
		if ( !_data.hasOwnProperty( prop ) || _data[prop] == undefined ) return def;
		return _data[prop];
	}

	public static function flush():void {
		if ( !_fs ) {
			log( " error: call init() first." );
			return;
		}
		if ( _pending ) {
//			var t:int = getTimer();
			_fs.position = 0;
			_fs.truncate();
			_fs.writeObject( _data );
//			trace( "time spent:", getTimer() - t );
		}
		_pending = false;
	}

	public static function dispose():void {
		_data = {};
		_pending = true;
		flush();
	}

}
}
