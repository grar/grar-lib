/**
 * Code by Rodrigo López Peker on 2/11/16 10:46 AM.
 *
 */
package grar.net {

import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.filesystem.File;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;
import flash.utils.ByteArray;
import flash.utils.getTimer;

import grar.air.FileUtils;
import grar.utils.LoanShark;
import grar.utils.StringUtils;
import grar.utils.URIUtils;

import org.osflash.signals.Signal;

public class ServiceCall {

	private static var _pool:LoanShark = new LoanShark( ServiceCall, false, 3, 0, null, "", "dispose" );

	public static function get():ServiceCall {
		var call:ServiceCall = _pool.borrowObject() as ServiceCall;
		call._inPool = true;
		if ( !call._cleaned ) {
			call.reset();
		}
		call.verbose = ServiceCall.verbose;
		return call;
	}

	public function reset():void {
		if ( _inPool ) {
			_pool.returnObject( this );
		}
		setSignal( null, null, null );
		_cleaned = true;
		onStatus.removeAll();
		userData = null;
		if ( _requesting ) {
			// try catch() if there's no current request, but impossible cause this class
			// deals with that, and is not exposed
			_loader.close();
		}
		_requesting = false;
		_request.contentType = null;
		_request.method = URLRequestMethod.GET;
		_request.data = null;
		_request.requestHeaders = [];
		_loader.dataFormat = URLLoaderDataFormat.TEXT;

		debugFile = null;
		statusHeaders = null;
		statusCode = 0;
		statusRedirected = false;
		statusUrl = null;
		verbose = false;
		userData = null;
	}

	public function dispose():void {
		if ( !_cleaned ) {
			reset();
		}
		if ( !_loader ) return;
		_loader.removeEventListener( Event.COMPLETE, handleLoaderComplete );
		_loader.removeEventListener( IOErrorEvent.IO_ERROR, handleLoaderError );
		_loader.removeEventListener( SecurityErrorEvent.SECURITY_ERROR, handleLoaderError );
		_loader.removeEventListener( HTTPStatusEvent.HTTP_RESPONSE_STATUS, handleLoaderResponseStatus );
		_loader.removeEventListener( ProgressEvent.PROGRESS, handleLoaderProgress );
		_loader = null;
		_request = null;
	}

	public static const PROGRESS:String = "downloadProgress";
	public static const COMPLETE:String = "complete";
	public static const CANCEL:String = "cancel";
	public static const ERROR:String = "error";
	public static const STATUS:String = "status";

	public static const GET:String = "GET";
	public static const POST:String = "POST";
	public static const UPDATE:String = "UPDATE";
	public static const PUT:String = "PUT";
	public static const DELETE:String = "DELETE";
	public static const DEL:String = "DEL";

	public var onStatus:Signal;

	// cleaned define the state of the ServiceCall instance, if its "clean" means it's reset() to the default state, and can be used with
	// the default setup.
	private var _cleaned:Boolean;
	private var _request:URLRequest;
	private var _loader:URLLoader;

	private var _requesting:Boolean;
	private var _inPool:Boolean;

	public static var verbose:Boolean = false;

	// for this specific instance, default to false.
	public var verbose:Boolean;

	// stores any information here.
	public var userData:Object;

	// to debug the text output to a File on the system :)
	public var debugFile:File;

	public static var failure:Signal;

	// to track time spent on request.
	private var _deltaTime:uint;

	// signal object used as Event data.
	// target: this object
	// type: event type (CANCEL,ERROR,STATUS,COMPLETE,PROGRESS)
	// data: data per event type ( _loader.data, error text, status code (int) )
	// event: the native event dispatched by the URLLoader
	private var _status:Object;

	// StatusEvent response headers and other info.
	public var statusHeaders:Array;
	public var statusCode:uint;
	public var statusUrl:String;
	public var statusRedirected:Boolean;

	public function ServiceCall() {
		if ( !failure ) {
			failure = new Signal();
		}
		onStatus = new Signal();
		_inPool = false;
		_cleaned = true;
		_status = {target: this};
		_request = new URLRequest();
		_loader = new URLLoader();
	}

	public function setup( url:String, method:String = "GET", contentType:String = "" ):void {
		// lazy initialization (required after calling dispose()).
		if ( !_loader ) {
			_loader = new URLLoader();
			_request = new URLRequest();
		}

		// if required we can set contentType=NULL to send empty ones, default to applicaton/json.
		if ( contentType == "" ) {
			contentType = "application/json";
		}
		method = validateURLRequestMethod( method );
		_cleaned = false;
		_request.url = url;
		_request.contentType = contentType;
		_request.method = method;
		// add loader listeners if needed.
		if ( !_loader.hasEventListener( Event.COMPLETE ) ) {
			_loader.addEventListener( Event.COMPLETE, handleLoaderComplete );
			_loader.addEventListener( IOErrorEvent.IO_ERROR, handleLoaderError );
			_loader.addEventListener( SecurityErrorEvent.SECURITY_ERROR, handleLoaderError );
			_loader.addEventListener( HTTPStatusEvent.HTTP_RESPONSE_STATUS, handleLoaderResponseStatus );
			_loader.addEventListener( ProgressEvent.PROGRESS, handleLoaderProgress );
		}
	}

	public function load():void {
		_deltaTime = getTimer();
		_requesting = true;
		log( "::load()" + _request.url );
		_loader.load( _request );
	}

	public function cancel( dispatch:Boolean = true ):void {
		if ( dispatch ) {
			setSignal( CANCEL );
			onStatus.dispatch( _status );
		}
		reset();
	}


	//===================================================================================================================================================
	//
	//      ------  EVENTS
	//
	//==================================================================================================================================================

	private function handleLoaderComplete( event:Event ):void {
		var result:String = _loader.data;
		log( "::response (" + currentRequestTimeText + ") result=" + result );
		_requesting = false;
		if ( debugFile ) {
			writeDebugFile( debugFile, result );
		}
		dispatchSignal( COMPLETE, result, event );
		reset();
	}

	// Download progress.
	private function handleLoaderProgress( event:ProgressEvent ):void {
		// TODO: maybe we wanna avoid this one?
		dispatchSignal( PROGRESS, {bytes: event.bytesLoaded, percentage: event.bytesLoaded / event.bytesTotal}, event );
	}

	private function handleLoaderError( event:Event ):void { // IOErrorEvent, SecurityErrorEvent
		var msg:String;
		if ( event.hasOwnProperty( "errorID" ) ) {
			msg = event["errorID"] + " ";
		}
		if ( event.hasOwnProperty( "text" ) ) {
			msg += event["text"];
		}
		if ( !msg ) {
			msg = event.toString();
		}
		log_error( msg );
		dispatchSignal( ERROR, msg, event );
		reset();
	}

	private function handleLoaderResponseStatus( event:HTTPStatusEvent ):void {
		statusHeaders = event.responseHeaders;
		statusCode = event.status;
		statusUrl = event.responseURL;
		statusRedirected = event.redirected;

		dispatchSignal( STATUS, statusCode, event );

		if ( verbose ) {
			log( "httpStatus() statusCode=" + statusCode + " url=" + event.responseURL + "\nheaders=" + JSON.stringify( event.responseHeaders ) );
		}

		// TODO: add failure callback message?
		// we dont wanna reset() by default, cause usually we get an error description.
		/*if ( statusCode >= 400 ) {
			failure.dispatch( event );
			reset();
		}*/
	}


	//===================================================================================================================================================
	//
	//      ------  ACCESSORS
	//
	//===================================================================================================================================================
	public function get request():URLRequest {
		return _request;
	}

	// I don't wanna expose this one, but MAYBE can be useful.
	public function get loader():URLLoader {
		return _loader;
	}

	public function get currentRequestTime():uint {
		return getTimer() - _deltaTime;
	}

	public function get currentRequestTimeText():String {
		var responseTime:Number = ( getTimer() - _deltaTime ) / 1000;
		return responseTime + "s";
	}

	//===================================================================================================================================================
	//
	//      ------  utils
	//
	//===================================================================================================================================================

	private function log( ...args ):void {
		if ( !verbose ) {
			return;
		}
		trace( "ServiceCall" + args.join( " " ) );
	}

	private function log_error( text:String ):void {
		trace( "ServiceCall ERROR=", text );
	}

	private function setSignal( type:String, data:* = null, event:* = null ):void {
		_status.type = type;
		_status.isCancel = type == CANCEL;
		_status.isProgress = type == PROGRESS;
		_status.isComplete = type == COMPLETE;
		_status.isStatus = type == STATUS;
		_status.isError = type == ERROR;
		_status.data = data;
		_status.event = event;
	}

	public function dispatchSignal( type:String, data:*, event:* ):void {
		setSignal( type, data, event );
		onStatus.dispatch( _status );
	}

	/**
	 * useful to search for a status header.
	 * @param name
	 * @param headers
	 * @return
	 */
	public function getStatusHeaderValue( name:String, headers:Array = null ):String {
		if ( !headers ) {
			headers = statusHeaders;
			if ( !headers ) return null;
		}
		var header:URLRequestHeader = URIUtils.getResponseHeaderByName( name, headers );
		if ( !header ) return null;
		return header.value;
	}


	//===================================================================================================================================================
	//
	//      ------  STATIC functions
	//
	//===================================================================================================================================================

	/**
	 * Used as shortcut to make a call.
	 * @param url
	 * @param statusCallback
	 * @param props
	 * @return
	 */
	public static function call( url:String, statusCallback:Function, props:Object = null ):ServiceCall {
		var scall:ServiceCall = setup( url, statusCallback, props );
		scall.load();
		return scall;
	}

	public static function setup( url:String, statusCallback:Function, props:Object = null ):ServiceCall {
		if ( !props )
			props = { method: GET };

		var scall:ServiceCall = ServiceCall.get();
		scall.setup( url, props.method || GET );
//		scall.request.cacheResponse = false ;
//		scall.request.useCache = false;
		if ( props.hasOwnProperty( "data" ) ) {
			scall._request.data = props.data;
		}
		if ( props.debugFile && props.debugFile is File ) {
			scall.debugFile = props.debugFile;
		}

		if ( props.hasOwnProperty( "contentType" ) ) {
			scall._request.contentType = props.contentType;
		}
		if ( props.hasOwnProperty( "dataFormat" ) ) {
			scall._loader.dataFormat = props.dataFormat;
		}
		if ( props.hasOwnProperty( "headers" ) ) {
			if ( props.headers is URLRequestHeader ) {
				scall._request.requestHeaders.push( props.headers );
			} else if ( props.headers is Array ) {
				scall._request.requestHeaders = scall._request.requestHeaders.concat( props.headers );
			}
		}
		if ( props.hasOwnProperty( "userData" ) ) {
			scall.userData = props.userData;
		}

		scall.verbose = props.hasOwnProperty( "verbose" ) ? props.verbose : ServiceCall.verbose;

		if ( statusCallback ) {
			scall.onStatus.add( statusCallback );
		}
		return scall;
	}


	public static function validateURLRequestMethod( m:String, fallbackMethod:String = "GET" ):String {
		if ( !m ) return GET;
		m = m.toUpperCase();
		return [GET, PUT, POST, DELETE, DEL, UPDATE].indexOf( m ) > -1 ? m : GET;
	}

	// utility to trace
	public static function onSuccessDebugCallback( status:Object ):void {
		if ( status.isComplete ) {
			var obj:Object = {
				url: status.target.request.url,
				method: status.target.request.method,
				data: status.target.request.data,
				response: status.data
			};
			obj.time = status.target.currentRequestTimeText;
			var msg:String = "[ServiceCall] time=${time}\n\turl = ${url} [${method}]\n\tdata = ${data}\n\tresponse = ${response}";
			trace( StringUtils.replacePlaceholders( msg, obj ) );
//			trace("[ServiceCall] url=" + status.target.request.url + "\n+", status.data ) ;
		}
	}

	private static var _ba:ByteArray;
	// awesome utility to output the result directly to the filesystem.
	// useful when the output is too long for the console.
	private static function writeDebugFile( debugFile:File, text:String ):void {
		if ( !_ba ) {
			_ba = new ByteArray();
		} else {
			_ba.clear();
		}
		_ba.writeUTFBytes( text );
		FileUtils.write( debugFile, _ba, null );
	}

}

}
