/**
 * Created by tasos on 03/03/16 21:29.
 */
package grar.net.clients {
import grar.db.Query;

public interface IBackendObject {

	function get id():String;
	function set id( value:String ):void;

	/**
	 * Mapping function that fills model object's fields from the object in the backend DB
	 * @param backendObj
	 */
	function fromBackendObj( backendObj:Object ):void;

	/**
	 * Mapping function that creates a backend DB object from the current model object
	 * @return the backend object
	 */
	function toBackendObj():Object;

}

}
