/**
 * Code by Rodrigo López Peker on 2/12/16 4:29 PM.
 *
 */
package grar.net.clients.linkedin {

import grar.net.clients.abstracts.AbsAPI;
import grar.net.clients.abstracts.AbsOauth2API;

// OAUTH 2.0
// Reference::
// @see https://developer.linkedin.com/docs/oauth2#permissions
// All token expires after 60 days.

public class LinkedinAPI extends AbsOauth2API {

	private static var _instance:LinkedinAPI;
	public static function get instance():LinkedinAPI {
		if ( !_instance ) _instance = new LinkedinAPI();
		return _instance;
	}

//	public static const OAUTH_URL_TOKEN: String =
//	public static const OAUTH_URL_ENDPOINT: String =
//	private const API_URL: String =
	public var apiUrl:String ;

	public function LinkedinAPI() {
		super(AbsAPI.SERVICE_LINKEDIN);
	}

	override protected function init():void {
		super.init();
		oauthUrlToken = "https://www.linkedin.com/uas/oauth2/accessToken";
		oauthUrlEndpoint = "https://www.linkedin.com/uas/oauth2/authorization";
	}

	override public function setup( env:Object ):void {
		if ( !env.version ) {
			env.version = 1 ;
		}
		super.setup( env );
		apiUrl = "https://api.linkedin.com/v"+ env.version + "/" ;
	}

}

}
