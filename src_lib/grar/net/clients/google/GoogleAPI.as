/**
 * Code by Rodrigo López Peker on 2/11/16 9:56 PM.
 *
 */
package grar.net.clients.google {

import grar.net.*;

import flash.net.URLRequestHeader;

import grar.net.clients.abstracts.AbsAPI;

import grar.net.clients.abstracts.AbsOauth2API;

// @see https://developers.google.com/identity/protocols/OAuth2InstalledApp

public class GoogleAPI extends AbsOauth2API {

	private static var _instance:GoogleAPI;
	public static function get instance():GoogleAPI {
		if ( !_instance ) _instance = new GoogleAPI();
		return _instance;
	}

	public static var defaultRedirectUri:String = "urn:ietf:wg:oauth:2.0:oob";

	public static var scopeList:Array = [
		"https://www.google.com/m8/feeds/",
		"https://www.googleapis.com/auth/userinfo.email",
		"https://www.googleapis.com/auth/userinfo.profile",
		"https://www.googleapis.com/auth/plus.me",
		"https://www.googleapis.com/auth/plus.profiles.read",
		"https://www.googleapis.com/auth/plus.circles.read",
		"https://www.googleapis.com/auth/plus.login",
		"https://www.googleapis.com/auth/drive"
	];

	public static const apiUrlPeople:String = "https://www.googleapis.com/plus/v1/people/";
	public static const apiUrlDrive:String = "https://www.googleapis.com/drive/v2/files";

	public var headerAuthUser:URLRequestHeader;
	public var feedType:String = "json"; // rss, atom, json

	public function GoogleAPI() {
		super(AbsOauth2API.SERVICE_GOOGLE);
	}

	override protected function init():void {
		super.init();
		oauthUrlEndpoint = "https://accounts.google.com/o/oauth2/v2/auth";
		oauthUrlToken = "https://www.googleapis.com/oauth2/v4/token";
		refreshTokenUrl = "https://www.googleapis.com/oauth2/v4/token" ;
//		headerVersion = new URLRequestHeader( "GData-Version", "3.0" );
		headerAuthUser = new URLRequestHeader( "Authorization", null );
		// note: authorization header IS NOT working, use token as part of the URL.
		_defaultRequestHeaders = [headerAuthUser];
		_defaultURLParameters = {alt: feedType};
		this.permissions = scopeList;
	}


	//===================================================================================================================================================
	//
	//      ------  api
	//
	//===================================================================================================================================================

	/**
	 * @param userId
	 * @param onStatusCallback
	 * @param fields   aboutMe,ageRange,birthday,braggingRights,circledByCount,cover,currentLocation,displayName,domain,emails,etag,gender,id,image,isPlusUser,kind,language,name,nickname,objectType,occupation,organizations,placesLived,plusOneCount,relationshipStatus,skills,tagline,url,urls,verified
	 * @return
	 */
	public function getPerson( userId:String, onStatusCallback:Function, fields:String = null ):ServiceCall {
		if ( !userId ) {
			userId = "me";
		}
		var props:Object = {};
		if ( fields ) {
			props.urlParameters = {fields: fields};
		}
		var url:String = apiUrlPeople + userId;
		return serviceCall( url, onStatusCallback, props );
	}

	public function getDriveFiles( onStatusCallback:Function ):ServiceCall {
		var props:Object = {verbose: true};
		return serviceCall( apiUrlDrive, onStatusCallback, props );
	}

	override public function setup( env:Object ):void {
		super.setup( env );
		if ( !redirectUri ) {
			redirectUri = GoogleAPI.defaultRedirectUri;
		}
	}


	//===================================================================================================================================================
	//
	//      ------  PRIVATE functions
	//
	//===================================================================================================================================================

	override protected function requestTokenCode( customParams:Object = null ):void {
		customParams = {};
		customParams.scope = scopeList.join( " " );
		customParams.response_type = "code";
		super.requestTokenCode( customParams );
	}

	override protected function getAccessTokenWithAOuthCode( code:String, extraParams:Object = null,
															 props:Object = null ):void {
		// unlike Facebook, Google uses POST.
		props = {method: ServiceCall.POST};
		super.getAccessTokenWithAOuthCode( code, extraParams, props );
	}

	override protected function updateHeaderToken():void {
		// use header, u can include access_token to the _defaultUrlParams.
		// NOTE: authorization header DOESNT WORK, return code=403.
		headerAuthUser.value = "Bearer " + _token.accessToken;
		_defaultURLParameters.access_token = _token.accessToken;
	}

	override protected function loginComplete():void {
		// This is usually called AFTER getting the token, so it should be valid.
		// just in case, validate the user Login (if needed).
//		super.loginComplete(); // do not call onLogin just now.
		validateUserLogin();
	}

	override public function validateUserLogin():void {
		getPerson( null, handlePeopleMeCall );
	}

	private function handlePeopleMeCall( status:Object ):void {
		if ( status.isComplete ) {
			_user = JSON.parse( status.data );
			log( "user=" + JSON.stringify( _user ) );
			super.loginComplete();
		}
	}

}

}
