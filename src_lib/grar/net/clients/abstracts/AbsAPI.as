/**
 * Code by Rodrigo López Peker on 2/11/16 10:42 AM.
 *
 */
package grar.net.clients.abstracts {

import grar.net.*;

import flash.net.URLVariables;

import grar.media.CommonWebClientHelper;
import grar.utils.URIUtils;

public class AbsAPI {

	protected var _serviceId:String = ""; // backend or OAuth services: kinvey, facebook, twitter, etc.
	protected var _user:Object; // the active user

	protected var _defaultURLParameters:Object;
	protected var _defaultRequestHeaders:Array;

	public var verbose:Boolean = false;

	public function AbsAPI(serviceId:String) {
		this._serviceId = serviceId;
	}

	public function setup( env:Object ):void {
		// setup environment
	}

	public function serviceCall( url:String, callback:Function, props:Object = null ):ServiceCall {
		if ( !props ) props = {};
		if ( props.hasOwnProperty( "data" ) && !props.binaryData ) {
			if ( !(props.data is String ) && !( props.data is URLVariables ) ) {
				props.data = URIUtils.getUrlVariablesFromObject( props.data );
			}
		}
		url = resolveUrlParamsFromProps( url, props );
		if ( !callback ) {
			callback = ServiceCall.onSuccessDebugCallback;
		}
		var scall:ServiceCall = ServiceCall.setup( url, callback, props );
		if ( _defaultRequestHeaders && _defaultRequestHeaders.length > 0 ) {
			scall.request.requestHeaders.concat( _defaultRequestHeaders );
		}
		scall.load();
		return scall;
	}

	protected function handleWebClientUrlChange( url:String ):void {
	}

	protected function showWebBrowser( flag:Boolean, urlToOpen:String = null ):void {
		if ( flag ) {
			CommonWebClientHelper.onURLChange.add( handleWebClientUrlChange );
		} else {
			CommonWebClientHelper.onURLChange.remove( handleWebClientUrlChange );
		}
		CommonWebClientHelper.showWeb( flag );
		if ( flag && urlToOpen ) {
			CommonWebClientHelper.navto( urlToOpen );
		}
	}

	// can be overriden in each Service if u know if the response is URL variables or JSON (to avoid try catch)
	protected function parseAccessTokenResult( response:String ):Object {
		if ( !response ) return null;
		var json:Object;
		try {
			json = JSON.parse( response );
		} catch ( e:Error ) {
			// maybe is url vars.
			try {
				json = new URLVariables( response );
			} catch ( e:Error ) {
				trace( "[" + _serviceId + "] ERROR:: can't read the token response." );
				return null;
			}
		}
		return json;
	}

	// as we use this to add the parameters to the URL also in subclasses that requires signing
	// like Twitter, we move the code to this "util method".
	protected function resolveUrlParamsFromProps( url:String, props:Object ):String {
		if ( props.urlParameters ) {
			if ( _defaultURLParameters ) {
				URIUtils.mergeObjects( props.urlParameters, _defaultURLParameters );
			}
		} else {
			props.urlParameters = _defaultURLParameters;
		}
		if ( props.urlParameters ) {
			url = URIUtils.addParametersToUrl( url, props.urlParameters );
			delete props.urlParameters;
		}
		return url;
	}

	/**
	 * Calls the appropriate success/failure/progress listener according to response from the API call
	 * @param response
	 * @param success
	 * @param failure
	 * @param progress
	 */
	protected function routeResponse( response:Object, success:Function, failure:Function, progress:Function = null ):void {
		if ( response.isError ) {
			if ( failure ) failure( response.data );
		}
		else if ( response.isComplete ) {
			if ( success ) success( response.data );
		}
		else {
			if ( progress ) progress( response.data );
		}
	}

	public function get user():Object {
		return _user;
	}

	public function set user( value:Object ):void {
		_user = value;
	}

	public function log( ...args ):void {
		if ( !verbose ) return;
		trace( "[" + _serviceId + "] " + args.join( " " ) );
	}

	public function log_error( ...args ):void {
		trace( "[" + _serviceId + " ERROR] " + args.join( " " ) );
	}

}

}
