/**
 * Code by Rodrigo López Peker on 2/11/16 10:40 AM.
 *
 */
package grar.net.clients.abstracts {

import grar.net.*;

import flash.net.URLVariables;

import grar.media.CommonWebClientHelper;
import grar.net.clients.ServiceToken;
import grar.utils.URIUtils;

import org.osflash.signals.Signal;

public class AbsOauth2API extends AbsAPI {

	public static const SERVICE_FACEBOOK:String = "facebook"; // Oauth2
	public static const SERVICE_GOOGLE:String = "google"; // Oauth2
	public static const SERVICE_AMAZON:String = "amazon"; // Oauth2
	public static const SERVICE_LINKEDIN:String = "linkedin"; // Oauth2
	public static const SERVICE_TWITTER:String = "twitter"; // Oauth1

	protected var clientId:String;
	protected var clientSecret:String;
	protected var redirectUri:String;

	protected var refreshTokenUrl:String;
	protected var oauthUrlEndpoint:String;
	protected var oauthUrlToken:String;
	protected var permissions:Array;

	protected var _token:ServiceToken;
	protected var _logged:Boolean;

	public var onLogin:Signal;

	public function AbsOauth2API(serviceId:String) {
		super(serviceId);
	}

	protected function init():void {
		// serviceId has to be defined on the constructor.
		_token = ServiceToken.get( _serviceId, 2 );
		onLogin = new Signal();
	}

	override public function setup( env:Object ):void {
		clientId = env.clientId;
		clientSecret = env.clientSecret;
		if ( "redirectUri" in env ) {
			redirectUri = env.redirectUri;
		}
		_token.setAppCredentials( clientId, clientSecret );
	}

	public function login( callback:Function = null ):void {
		if ( callback ) {
			onLogin.addOnce( callback );
		}
		if ( _token.valid() ) {
			log( "valid token" );
			updateHeaderToken();
			validateUserLogin();
		} else {
			requestToken();
		}
	}

	protected function requestToken():void {
		if ( _token && _token.refreshToken ) {
			requestRefreshToken();
		} else {
			requestTokenCode();
		}
	}

	protected function requestTokenCode( customParams:Object = null ):void {
		var url:String = oauthUrlEndpoint;
		var params:Object = {};
		params.response_type = "code"; // code/token
		params.display = "touch"; // touch/popup
		params.state = String( Math.random() );
		params.client_id = clientId;
		params.redirect_uri = redirectUri;
		if ( customParams ) {
			URIUtils.mergeObjects( params, customParams );
		}
		url = URIUtils.addParametersToUrl( url, params );
		log( "requesting token code:", url );
		showWebBrowser( true, url );
	}

	override protected function handleWebClientUrlChange( url:String ):void {
		var responseDetected:Boolean = resolveCodeFromUrlOrTitle( url, CommonWebClientHelper.title );
		if ( responseDetected ) {
			showWebBrowser( false );
		}
	}

	protected function resolveCodeFromUrlOrTitle( url:String, title:String ):Boolean {
		// override to read a specific variable from url or the title.
		var value:String;
		if ( url.indexOf( "code=" ) > -1 ) {
			value = url;
		} else if ( title.indexOf( "code=" ) > -1 ) {
			value = title;
		} else {
			return false;
		}
		if ( value.indexOf( "access_denied" ) > -1 ) {
			accessDenied();
			return true;
		} else if ( value.indexOf( "code=" ) > -1 ) {
			var code:String;
//			if ( title == value ) {
//				code = value.substring( value.indexOf( "code=" ) ).split( "=" )[1];
//			} else {
			var vars:URLVariables = URIUtils.getVariablesFromUrl( value );
			code = vars.code;
//			}
			getAccessTokenWithAOuthCode( code );
			return true;
		}
		return false;
	}

	protected function accessDenied():void {
		// TODO: dispatch event?
		loginValidated( false );
	}

	protected function getAccessTokenWithAOuthCode( code:String, extraParams:Object = null, props:Object = null ):void {
		log( "requesting token with code:", code );

		// store the code just in case, we can use it later avoiding the web browser login?
		_token.accessTokenCode = code;

		var url:String = oauthUrlToken;
		var params:Object = {};
		params.grant_type = "authorization_code";
		params.code = code;
		params.redirect_uri = redirectUri;
		params.client_id = clientId;
		params.client_secret = clientSecret;
		if ( extraParams ) {
			// you can set grant_type, display, etc.
			URIUtils.mergeObjects( params, extraParams );
		}
		if ( !props ) {
			props = { method: ServiceCall.GET };
		}
		props.urlParameters = params;
		serviceCall( url, handleAccessTokenCall, props );
	}

	private function handleAccessTokenCall( status:Object ):void {
		if ( status.isComplete ) {
			log( "access token result::", status.data );
			// validate response type
			var obj:Object = parseAccessTokenResult( status.data );
			if ( obj ) {
				validateAccessTokenResponse( obj );
			}
		}
	}

	private function requestRefreshToken():void {
		var url:String = refreshTokenUrl;
		var params:Object = {
			refresh_token: _token.refreshToken,
			client_id: clientId,
			client_secret: clientSecret,
			grant_type: "refresh_token"
		};
		log( "requesting refresh code" );
		url = URIUtils.addParametersToUrl( url, params );
		serviceCall( url, handleRequestRefreshTokenStatus, {method: ServiceCall.POST} );
	}

	protected function handleRequestRefreshTokenStatus( status:Object ):void {
		if ( status.isComplete ) {
			log( "API access token=" + status.data );
			try {
				var json:Object = JSON.parse( status.data );
			} catch ( e:Error ) {
				trace( "Auth error!" + e.getStackTrace() );
				return
			}
			validateAccessTokenResponse( json );
		}
	}

	protected function validateAccessTokenResponse( data:Object ):void {
		if ( data.error ) {
			if ( data.error == "invalid_grant" ) {
				log( "Token revoked, requesting a new one" );
				// token revoked, request new one!.
				_token.clear();
				loginValidated( false );
			}
		} else {
			_token.setFromJson( data, true );
			updateHeaderToken();
			loginValidated( true );
		}
	}

	protected function loginValidated( flag:Boolean ):void {
		if ( flag ) {
			loginComplete();
		} else {
			requestToken();
		}
	}

	protected function loginComplete():void {
		_logged = true;
		onLogin.dispatch();
	}

	public function validateUserLogin():void {
		// override with custom implementation of /me for the specific service to use.
	}

	protected function updateHeaderToken():void {
		// required by linkedin/google/others
	}

	public function logged():Boolean {
		return _token.valid();
	}

	override public function serviceCall( url:String, callback:Function, props:Object = null ):ServiceCall {
		if ( !props ) props = {};
		// no need to use this, define in _defaultURLParameters = {oauth_token: _token.accessToken};
		/*if ( !props.removeTokenFromUrl && _token.accessToken && _addTokenAsUrlParameter ) {
			if ( !props.urlParameters ) props.urlParameters = {};
			props.urlParameters.access_token = _token.accessToken;
		}*/
		log( "calling url=" + url );
		return super.serviceCall( url, callback, props );
	}

	public function get token():ServiceToken {
		return _token;
	}
}
}
