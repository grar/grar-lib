/**
 * Created by tasos on 01/03/16 00:27.
 */
package grar.net.clients {

import flash.filesystem.File;
import flash.utils.ByteArray;

public interface IBackendFile {

	function reset():void;

	function setFile( file:File ):void;

	function setData( data:ByteArray, name:String, contentType:String = null, metadata:Object = null ):void;

	function getData():Object;

	function setEntity( data:Object ):void;

}

}
