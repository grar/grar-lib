/**
 * Created by tasos on 14/02/16 21:19.
 */
package grar.net.clients {

import grar.net.clients.abstracts.AbsAPI;
import grar.net.clients.kinvey.KinveyAPI;

/**
 * Abstract class the describes a backend service (e.g. Kinvey, AWS, etc.)
 */
public class BackendApiFactory {

	public static const KINVEY:String = "KinveyAPI";
//	public static const PARSE:String = "ParseAPI";
//	public static const AWS:String = "AwsAPI";

	public static function getApi( serviceId:String, env:Object ):IBackendApi {
		var api:IBackendApi;
		switch ( serviceId ) {
			case KINVEY:
				api = KinveyAPI.instance;
				AbsAPI(api).setup( env );
				break;
			default:
				throw new Error( "Unsupported backend" );
		}
		return api;
	}

}

}
