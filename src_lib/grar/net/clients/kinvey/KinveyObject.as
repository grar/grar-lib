package grar.net.clients.kinvey {

import grar.net.clients.IBackendObject;
import grar.utils.ClassUtils;

public class KinveyObject implements IBackendObject {

	// DB definitions
	protected static const ENTITY_INDEX:String = "_id";
	protected static const ENTITY_CREATED_AT:String = "ect";
	protected static const ENTITY_UPDATED_AT:String = "lmt";

	protected var _id:String;
	protected var _createdAt:String;
	protected var _updatedAt:String;

	public function BaseModel( id:String = null ) {
		this._id = id;
	}

	public function get id():String {
		return _id;
	}

	public function set id( value:String ):void {
		_id = value;
	}

	public function get createdAt():String {
		return _createdAt;
	}

//	public function set createdAt( value:String ):void {
//		_createdAt = value;
//	}

	public function get updatedAt():String {
		return _updatedAt;
	}

//	public function set updatedAt( value:String ):void {
//		_updatedAt = value;
//	}

//	public static function backendObj2Model( backendObj:Object, model:BaseModel ):void {
//		model.fromBackendObj( backendObj );
//	}

//	public static function model2BackendObj( model:BaseModel ):Object {
//		return model.toBackendObj();
//	}

	/**
	 * Fills internal fields from the object got from the backend DB
	 * @param backendObj
	 */
	public function fromBackendObj( backendObj:Object ):void {
		_id = backendObj[ENTITY_INDEX];
		if ( backendObj._kmd ) {
			_createdAt = backendObj._kmd[ENTITY_CREATED_AT];
			_updatedAt = backendObj._kmd[ENTITY_UPDATED_AT];
		}
	}

	/**
	 * Provides an object to be sent to the backend DB, built from the internal fields
	 */
	public function toBackendObj():Object {
		var backendObj:Object = new Object;
		backendObj[ENTITY_INDEX] = this.id;
		// these fields are filled automatically by the backend:
//		backendObj._kmd[ENTITY_CREATED_AT] = _createdAt; // filled automatically by Kinvey
//		backendObj._kmd[ENTITY_UPDATED_AT] = _updatedAt; //    -//-         -//-
		return backendObj;
	}

	public function toString():String {
		return ClassUtils.getClassNameSimple( this ) + ": { id=" + _id + ", createdAt=" + _createdAt + ", updatedAt=" + _updatedAt + " }";
	}

}

}
