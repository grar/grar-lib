/**
 * Code by Rodrigo López Peker on 2/12/16 12:06 PM.
 *
 */
package grar.net.clients.kinvey {

import flash.net.URLRequestHeader;
import flash.utils.getTimer;

import grar.db.Query;
import grar.db.QueryExpression;

import grar.net.ServiceCall;
import grar.net.clients.BackendApiFactory;
import grar.net.clients.IBackendApi;
import grar.net.clients.IBackendFile;
import grar.net.clients.ServiceToken;
import grar.net.clients.abstracts.AbsAPI;
import grar.net.clients.abstracts.AbsOauth2API;
import grar.utils.ClassUtils;
import grar.utils.ClassUtils;
import grar.utils.URIUtils;

public class KinveyAPI extends AbsAPI implements IBackendApi {

	public var appId:String;
	public var appSecret:String;
	public var masterSecret:String;
	public var apiVersion:String = "3";

	private var headerApiVersion:URLRequestHeader;
	private var headerAuthorizationMaster:URLRequestHeader;
	private var headerAuthorizationApp:URLRequestHeader;
	private var headerAuthorizationUser:URLRequestHeader;
	private var headerAuthorizationSession:URLRequestHeader;

	private var headerKinveyContentType:URLRequestHeader = new URLRequestHeader( "X-Kinvey-Content-Type" );
	private var headerDeleteCollection:URLRequestHeader = new URLRequestHeader( "X-Kinvey-Delete-Entire-Collection", "true" ) ;

	public static const apiUrl:String = "https://baas.kinvey.com/";
	public static var apiUrlUser:String;
	public static var apiUrlData:String;
	public static var apiUrlRpc:String;
	public static var apiUrlFiles:String;

	private const QUERY_PARAMS:Array = ["query", "fields", "limit", "skip", "sort", "resolve", "retainReferences"];

	public static const RESPONSE_FAIL:Number = 0;
	public static const RESPONSE_SUCCESS:Number = 1;
//	public static const RESPONSE_INCOMPLETE:Number = NaN;

	private static var _instance:KinveyAPI;
	public static function get instance():KinveyAPI {
		if ( !_instance ) {
			_instance = new KinveyAPI();
		}
		return _instance;
	}

	public function KinveyAPI() {
		super(BackendApiFactory.KINVEY);
	}

	override public function setup( env:Object ):void {
		appId = env.appId;
		appSecret = env.appSecret;
		masterSecret = env.masterSecret;
		if ( env.apiVersion ) {
			apiVersion = env.apiVersion;
		}
		headerAuthorizationMaster = new URLRequestHeader( 'Authorization' );
		headerAuthorizationApp = new URLRequestHeader( 'Authorization' );
		headerAuthorizationUser = new URLRequestHeader( 'Authorization' );
		headerAuthorizationSession = new URLRequestHeader( 'Authorization' );
		headerAuthorizationSession.value = null; // signifies absence of session
		headerApiVersion = new URLRequestHeader( 'X-Kinvey-Api-Version' );
		headerApiVersion.value = apiVersion;
		_defaultRequestHeaders = [headerApiVersion];
		updateAuthHeader( headerAuthorizationMaster, appId, masterSecret );
		updateAuthHeader( headerAuthorizationApp, appId, appSecret );

		apiUrlUser = apiUrl + "user/" + appId + "/";
		apiUrlData = apiUrl + "appdata/" + appId + "/"; // collections
		apiUrlRpc = apiUrl + "rpc/" + appId + "/";
		apiUrlFiles = apiUrl + "blob/" + appId + "/";
	}

	public function signup( username:String, password:String, userParameters:Object = null,
	                                 success:Function = null, failure:Function = null ):ServiceCall {
		var props:Object = {};
		props.method = ServiceCall.POST;
		props.data = {username: username, password: password};
		if ( userParameters ) {
			URIUtils.mergeObjects( props.data, userParameters );
		}
		props.headers = headerAuthorizationApp;

		var sc:ServiceCall = serviceCall( apiUrlUser, handleSignupCall, props );
		if ( success || failure ) sc.onStatus.add( statusCallback );
		return sc;

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function login( username:String, password:String,
	                                success:Function = null, failure:Function = null ):ServiceCall {
		var props:Object = {};
		props.method = ServiceCall.POST;
		props.data = {username: username, password: password};
		props.headers = headerAuthorizationApp;

		updateAuthHeader( headerAuthorizationUser, username, password );

		var sc:ServiceCall = serviceCall( apiUrlUser + "login/", handleLoginCall, props );
		if ( success || failure ) sc.onStatus.add( statusCallback );
		return sc;

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function socialLogin( serviceId:String, token:ServiceToken,
	                                      success:Function = null, failure:Function = null ):ServiceCall {
		var props:Object = {};
		props.method = ServiceCall.POST;
		props.headers = headerAuthorizationApp;
		// stupid intellij, shows an error in code when using props.data._socialIdentity, but it's correct.
		var serviceObj:Object = {};
		props.data = {_socialIdentity: {}};
		props.data["_socialIdentity"][serviceId] = serviceObj;
		if ( serviceId == AbsOauth2API.SERVICE_TWITTER ) {
			// twitter uses oauth 1.0, and a different object.
			serviceObj.access_token = token.accessToken;
			serviceObj.access_token_secret = token.accessTokenSecret;
			serviceObj.consumer_key = token.apiKey;
			serviceObj.consumer_secret = token.apiSecret;
		} else {
			serviceObj.access_token = token.accessToken;
			serviceObj.expires = token.expiresIn;
		}

		var sc:ServiceCall = serviceCall( apiUrlUser, handleSocialLoginCall, props );
		if ( statusCallback ) sc.onStatus.add( statusCallback );
		return sc;

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function logout( success:Function = null, failure:Function = null ):ServiceCall {
		var props:Object = {};
		props.method = ServiceCall.POST;
//		props.data = "{}" ; // validated on serviceCall()
		props.headers = headerAuthorizationSession;

		var sc:ServiceCall = serviceCall( apiUrlUser + "_logout", handleLogoutCall, props );
		if ( statusCallback ) sc.onStatus.add( statusCallback );
		return sc;

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function deleteUser( userId:String = null,
	                                     success:Function = null, failure:Function = null ):ServiceCall {
		if ( !userId && _user ) {
			userId = _user.id;
		}
		if ( !userId ) {
			log_error( "deleteUser():: you must provide a userId" );
			return null;
		}

		var sc:ServiceCall = serviceCall( apiUrlUser + userId, handleUserDeleteCall, {
			method: ServiceCall.DELETE,
			headers: headerAuthorizationSession
		} );
		if ( statusCallback ) sc.onStatus.add( statusCallback );
		return sc;

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function updateUser( userId:String = null, userParameters:Object = null,
	                                     success:Function = null, failure:Function = null ):ServiceCall {
		if ( !userParameters ) {
			log_error( "updateUser():: makes no sense to update empty params. Execution skipped." );
			return null;
		}
		if ( !userId && _user ) {
			userId = _user.id;
		}
		if ( !userId ) userId = "";
		/*if ( !userId ) {
			log_error("updateUser():: you must provide a userId") ;
			return null ;
		 }*/

		var props:Object = {};
		props.method = !userId ? ServiceCall.POST : ServiceCall.UPDATE; // create or update.
		props.headers = headerAuthorizationSession.value ? headerAuthorizationSession : headerAuthorizationUser;
		props.data = userParameters;
		var sc:ServiceCall = serviceCall( apiUrlUser + userId, handleUserUpdateCall, props );
		if ( statusCallback ) sc.onStatus.add( statusCallback );
		return sc;

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

//	// we can change it to return a callback with true/false.
//	public function checkUserExistance( username:String, statusCallback:Function ):ServiceCall {
//		return serviceCall( apiUrlRpc + "check-username-exists", statusCallback, {
//			method: ServiceCall.POST,
//			headers:headerAuthorizationMaster,
//			data: {username: username}, verbose:true
//		});
//	}


	//===================================================================================================================================================
	//
	//      ------  data API
	//
	//===================================================================================================================================================

	/**
	 * @see http://devcenter.kinvey.com/rest/guides/datastore#deletecollection
	 * @param collectionName
	 * @param statusCallback
	 * @return
	 */
	public function deleteCollection( collectionName:String,
	                                  success:Function = null, failure:Function = null ):ServiceCall {
		var url:String = apiUrlRpc + "remove-collection" ;
		var props:Object = {};
		props.headers = [ headerDeleteCollection, headerAuthorizationMaster ];
		props.method = ServiceCall.POST;
		props.data = { collectionName: collectionName };
		return serviceCall( url, statusCallback, props );

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function countElements( collection:String,
	                               success:Function = null, failure:Function = null ):ServiceCall {
		return serviceCall( apiUrlData + collection + "/_count", statusCallback, { headers: headerAuthorizationMaster } ) ;

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function find( collection:String, query:Object = null, modifiers:Object = null,
	                      success:Function = null, failure:Function = null ):ServiceCall {
		var url:String = ( collection == "Users" ) ? apiUrlUser : ( apiUrlData + collection + "/" );
		if ( query && query is String ) {
			// record Id?
			url += String( query );
		}
		else if ( query ) {
			// terrible validation!
			if ( query.id ) {
				url += query.id;
				delete query.id;
			}
			if ( query || modifiers ) {
				var params:Object = {};
				params.query = JSON.stringify( query ) ;
				if ( modifiers ){
					URIUtils.mergeObjects(params, modifiers) ;
				}
				url = URIUtils.addParametersToUrl( url, params );
			}
		}
		return serviceCall( url, statusCallback, {headers: headerAuthorizationSession.value ? headerAuthorizationSession : headerAuthorizationMaster} );

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function findUser( userId:String,
	                          success:Function = null, failure:Function = null ):ServiceCall {
		return findUsers( userId, statusCallback );

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function findUsers( query:Object, modifiers:Object = null,
	                           success:Function = null, failure:Function = null ):ServiceCall {
		return find( "Users", query, modifiers, statusCallback );

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function addEntity( collection:String, entity:Object,
	                           success:Function = null, failure:Function = null ):ServiceCall {
		return alterEntity( collection, null, entity, success, failure, ServiceCall.POST );

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function deleteEntity( collection:String, query:Object,
	                              success:Function = null, failure:Function = null ):ServiceCall {
		return alterEntity( collection, query, null, success, failure, ServiceCall.DELETE );

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function updateEntity( collection:String, query:Object, entity:Object,
	                              success:Function = null, failure:Function = null ):ServiceCall {
		return alterEntity( collection, query, entity, success, failure, ServiceCall.PUT );

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	private function alterEntity( collection:String, query:Object, entity:Object,
	                              success:Function = null, failure:Function = null,
								  method:String = "PUT" ):ServiceCall {
		if ( collection == "User" ) {
			log_error( "alterEntity():: use updateUser instead." );
			return null;
		}
		var url:String = apiUrlData + collection;
		var id:String;
		if ( query is String ) {
			id = query as String;
			query = null;
		}
		else if ( query ) {
			if ( method != ServiceCall.DELETE ) {
				log_error( "alterEntity():: query can only be used when deleting." );
				return null;
			}
		}

		if ( entity ) {
			if ( method == ServiceCall.DELETE ) {
				log_error( "alterEntity():: No entity data is allowed when deleting." );
				return null;
			}
			if ( !id && entity.id ) {
				id = entity.id;
			}
			delete entity.id;
		}

		if ( id ) {
			url += id;
			if ( method == ServiceCall.POST ) method = ServiceCall.PUT; // when id is in the url, the creation syntax requires PUT
		}

		if ( query ) {
			var params:Object = {};
			params.query = JSON.stringify( query );
			url = URIUtils.addParametersToUrl( url, params );
		}

		return serviceCall( url, statusCallback, {
			method: method,
			data: entity,
			headers: headerAuthorizationSession.value ? headerAuthorizationSession : headerAuthorizationMaster
		} );

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	private function handleUserUpdateCall( status:Object ):void {
		if ( status.isComplete ) {
			log( "user updated:", status.data );
			var data:Object = JSON.parse( status.data );
			loginSuccess( data );
		} else if ( status.isError ) {
			log_error( "user updated:", status.data );
		}
	}

	private function handleUserDeleteCall( status:Object ):void {
		if ( status.isComplete ) {
			log( "user deleted:", status.data );
			removeAccessToken();
			updateSessionHeader( null );
			updateUserInfo( null );
		} else if ( status.isError ) {
			log_error( "user deleted:", status.data );
		}
	}

	private function handleLogoutCall( status:Object ):void {
		if ( status.isComplete ) {
			log( "user logout:", status.data );
			removeAccessToken();
			updateSessionHeader( null );
		} else if ( status.isError ) {
			log_error( "user logout:", status.data );
		}
	}

	private function handleSocialLoginCall( status:Object ):void {
		if ( status.isComplete ) {
			var data:Object = JSON.parse( status.data );
			if ( data.error ) {
				log_error( "social login:", status.data );
			} else {
				log( "social login:", status.data );
			}

			loginSuccess( data );
		} else if ( status.isError ) {
			log_error( "user social login:", status.data );
		}
	}

	private function handleSignupCall( status:Object ):void {
		if ( status.isComplete ) {
			var data:Object = JSON.parse( status.data );
			// Thankfuly Kinvey has the "error" code in the JSON response with the description, no need to validate statusCode.
			if ( data.error ) {
				log_error( "user signup:", status.data );
			} else {
				log( "user signup:", status.data );
				loginSuccess( data );
			}
		} else if ( status.isError ) {
			log_error( "user signup:", status.data );
		}
	}

	private function handleLoginCall( status:Object ):void {
		if ( status.isComplete ) {
			var data:Object = JSON.parse( status.data );
			if ( data.error ) {
				log_error( "user login:", status.data );
			} else {
				log( "user login:", status.data );
			}
			loginSuccess( data );
		} else if ( status.isError ) {
			log_error( "user login:", status.data );
		}
	}

	private function loginSuccess( data:Object ):void {
		updateUserInfo( data );
		if ( data._kmd && data._kmd.authtoken ) {
			updateSessionHeader( data._kmd.authtoken );
		}
	}


	//===================================================================================================================================================
	//
	//      ------  file operations
	//
	//===================================================================================================================================================

	public function deleteFile( fileId:String, success:Function = null, failure:Function = null ):ServiceCall {
		if ( !fileId ) {
			log_error("deleteFile(), a fileId is required, sadly, can't delete the entire Files collection.") ;
			return null;
		}
		var url:String = apiUrlFiles + fileId;
		var props:Object = {method: ServiceCall.DEL};
		props.headers = headerAuthorizationMaster;
		return serviceCall(url, statusCallback, props );

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	/**
	 * Uploads a file in two steps (blob +
	 * @param file
	 * @return
	 */
	public function uploadFile( ifile:IBackendFile ):ServiceCall {
		var file:KinveyFile = KinveyFile(ifile);
		var url:String = apiUrlFiles;
		var props:Object = {};
		props.headers = [ headerAuthorizationMaster ];
		if ( file.contentType ) {
			headerKinveyContentType.value = file.contentType;
			props.headers.push( headerKinveyContentType );
		}
		props.method = ServiceCall.POST;
		if ( file.id ) {
			url += file.id;
			props.method = ServiceCall.PUT;
		}

		props.data = file.getData();

		return serviceCall( url, onFileApproved, props );

		function onFileApproved( status:Object ):void {
			if ( status.isComplete ) {
				var data:Object = JSON.parse( status.data );
				file.setEntity( data );
				uploadFileToUrl();
			}
		}

		var uploadTime:int;
		function uploadFileToUrl():void {
			var url:String = file.uploadUrl;
			var props:Object = file.getUploadObjectRequest( );
			uploadTime = getTimer();
			// TODO: send a callback from uploadFile? or use the file itself?
			serviceCall( url, handleUploadFile, props );
		}

		function handleUploadFile( status:Object ):void {
			if ( status.isComplete ) {
				trace( "upload time:", getTimer() - uploadTime );
				if ( file.onUploadStatus ) {
					file.onUploadStatus( status );
				}
				// back to pool.
				file.reset();
			}
		}
	}

	public function getFileEntityById( fileId:String,
	                                   success:Function = null, failure:Function = null ):ServiceCall {
		if ( !fileId ) {
			log_error("getFileEntityById() required a fileId");
			return null;
		}
		return getFileEntity( {_id:fileId}, null, statusCallback );

		function statusCallback(res:Object):void {
			routeResponse(res, success, failure);
		}
	}

	public function getFileEntity( query:Object=null, modifiers:Object=null,
	                               success:Function = null, failure:Function = null, progress:Function = null ):ServiceCall {
		var url:String = apiUrlFiles;
		var props:Object = {};
		var params:Object;
		if ( query || modifiers ) {
			params = {};
			if ( query ) params.query = JSON.stringify( query );
			if ( modifiers ) URIUtils.mergeObjects( params, modifiers );
		}
		/*if( query ){
			url = URIUtils.addParametersToUrl(url, {query:JSON.stringify(query)}) ;
		}*/
		props.urlParameters = params;
		props.headers = headerAuthorizationMaster;
		return serviceCall( url, statusCallback, props );

		function statusCallback( res:Object ):void {
			routeResponse( res, success, failure, progress );
		}

		/*function _callback( status:Object ):void {
			if( status.isComplete ){
				// CREATE file.
//				var file:KinveyFile = KinveyFile.get() ;
//				var entities:Array = JSON.parse(status.data) as Array ;
//				file.setEntity(  ) ;
//				status.data = file ;
			}
			if( statusCallback )
				statusCallback( status ) ;
		}*/
	}

	public function addSchema( className:String, modelFields:Object,
	                           success:Function = null, failure:Function = null ):ServiceCall {
		throw new Error( "Not implemented" );
	}

	public function createSchemaFromString( data:String ):Object {
		throw new Error( "Not implemented" );
	}

	public function execute( query:Query, success:Function = null, failure:Function = null ):void {
		var noSQL:Object = toNoSQL( query );
		trace("NoSQL:", JSON.stringify(noSQL));

		var collection:String = ClassUtils.getClassNameSimple( query.getClass() );
		switch ( noSQL.action ) {
			case Query.ACTION_FIND:
				find(
					collection,
					noSQL.query, noSQL.modifiers,
					success, failure
				);
				break;
			case Query.ACTION_SAVE:
				addEntity(
					collection,
					query.getBody(), //???
					success, failure
				);
				break;
			case Query.ACTION_UPDATE:
				updateEntity(
					collection,
					noSQL.query,
					query.getBody(), //???
					success, failure
				);
				break;
			case Query.ACTION_REMOVE:
				deleteEntity(
					collection, noSQL.query,
					success, failure
				);
				break;
			default:
				throw new Error( "query with wrong action!" );
		}
	}

	override public function serviceCall( url:String, callback:Function, props:Object = null ):ServiceCall {

		// so, as a shortcut, we can send just the PATH needed, avoiding the root domain.
		// url = "user/"+appId ;
		/*if ( url.indexOf( apiUrl ) == -1 ) {
			url = apiUrl + url;
		}*/

		if ( props ) {

			if ( props.where ) {
				if ( !props.data ) props.data = {};
				Object( props.data ).where = ( props.where is String ) ? props.where : JSON.stringify( props.where );
				delete props.where;
			}

			if ( props.data && !props.binaryData ) {
				// default content type is json, and POST messages expects a json String.
				if ( !( props.data is String ) ) {
					props.data = JSON.stringify( props.data );
				}
			}

			// maybe we have a POST, and kinvey requires always a body(), so we set the default one.
			if ( props.method == ServiceCall.POST || props.method == ServiceCall.DELETE ) {
				if ( !props.data ) {
					props.data = "{}";
				}
			}
		}

		return super.serviceCall( url, callback, props );
	}

	public function getAccessToken():String {
		if ( _user && _user._kmd ) {
			return _user._kmd.authtoken;
		}
		return null;
	}

	/**
	 * Parses the response from the API callback and returns:
	 * 1: if the operation has completed successfully
	 * 0: if the operation has failed
	 * NaN: if the operation has not yet been completed (e.g. when is on progress)
	 * response can either be:
	 * - error (res.isError == true) where res.data contains a json description of the error
	 * - status (res.isStatus == true) where res.data contains the HTTP status code
	 * - data (res.isComplete == true) where res.data contains the HTTP response json data
	 * - progress (res.isProgress == true) where res.data contains a json data with 'bytes' and 'percentage'
	 * @param res - the response of the API operation as returned from ServiceCall
	 * @return
	 */
	protected function responseVerdict( res:Object ):Number {
		var verdict:Number;
		if ( res.isError ) {
			verdict = RESPONSE_FAIL;
		}
		else if ( res.isComplete ) {
			if ( res.data && JSON.parse( res.data ).hasOwnProperty( "error" ) ) {
				verdict = RESPONSE_FAIL;
			}
			else {
				var errorHeaderValue:String = ServiceCall( res.target ).getStatusHeaderValue( "X-Kinvey-Error" );
				if ( errorHeaderValue ) {
					verdict = RESPONSE_FAIL;
				}
				else {
					verdict = RESPONSE_SUCCESS;
				}
			}
		}

		if ( verdict == RESPONSE_FAIL ) {
			trace( "[" + _serviceId + "] FAILURE / type: " + res.type + " data: " + res.data );
		}
		else if ( verdict == RESPONSE_SUCCESS ) {
			trace( "[" + _serviceId + "] SUCCESS / type: " + res.type + " data: " + res.data );
		}

		return verdict;
	}

	/**
	 * Calls the appropriate success/failure/progress listener according to response from the API call
	 * @param response
	 * @param success
	 * @param failure
	 * @param progress
	 */
	override protected function routeResponse( response:Object,
	                                           success:Function, failure:Function, progress:Function = null ):void {
		var verdict = responseVerdict(response);
		if ( verdict == RESPONSE_FAIL ) {
			if ( failure ) failure( response.data );
		}
		else if ( verdict == RESPONSE_SUCCESS ) {
			if ( success ) success( response.data );
		}
		else {
			if ( progress ) progress( response.data );
		}
	}


	//===================================================================================================================================================
	//
	//      ------  PRIVATE functions
	//
	//===================================================================================================================================================

	private function updateUserInfo( json:Object ):void {
		_user = json;
	}

	private function removeAccessToken():void {
		if ( _user && _user._kmd ) {
			delete _user._kmd.authtoken;
		}
	}

	private function updateAuthHeader( header:URLRequestHeader, key:String, secret:String ):void {
		header.value = "Basic " + URIUtils.base64( key + ":" + secret );
	}

	private function updateSessionHeader( authtoken:String ):void {
		headerAuthorizationSession.value = authtoken ? ( "Kinvey " + authtoken ) : null;
	}

	/**
	 * Converts a Query to NoSQL query format
	 * @param query
	 * @return the NoSQL query
	 */
	private function toNoSQL(query:Query):Object {
		// TODO: finish this
		var noSQL:Object = {};
		var where:QueryExpression = query.getWhere();
		if ( where ) {
			noSQL = parseNoSQLExpression( where );
		}
		var modifiers:Object = {};
		if ( query.getFields() ) modifiers["fields"] = query.getFields();
		if ( query.getLimit() ) modifiers["limit"] = query.getLimit();
		if ( query.getSkip() ) modifiers["skip"] = query.getSkip();
		if ( query.getSort() ) modifiers["sort"] = query.getSort();
		return {
			action: query.getAction(),
			collection: ClassUtils.getClassNameSimple( query.getClass() ),
			query: noSQL,
			modifiers: modifiers
		};
	}

	private function parseNoSQLExpression( expr:Object ):Object {
		// TODO: account for !=, NOT, IN, NOT IN, LIKE
		var obj:Object = {};
		if ( expr is QueryExpression && expr.oper == QueryExpression.Or ) {
			var arr:Array = new Array( 2 );
			arr[0] = parseNoSQLExpression( expr.left );
			arr[1] = parseNoSQLExpression( expr.right );
			obj["$or"] = arr;
		}
		else if ( expr is QueryExpression )
			if ( expr.left is String ) { // column id
				var right = expr.right;
				if ( right is QueryExpression ) right = parseNoSQLExpression( right );
				switch ( expr.oper ) {
					case QueryExpression.EqualTo:
						obj[expr.left] = right;
						break;
					case QueryExpression.DifferentThan:
						obj[expr.left] = {"$ne": right};
						break;
					case QueryExpression.Like:
						obj[expr.left] = {"$regex": "^" + right.replace( "%", ".*" )}; //???
						break;
					case QueryExpression.GreaterThan:
						obj[expr.left] = {"$gt": right};
						break;
					case QueryExpression.GreaterThanOrEqualTo:
						obj[expr.left] = {"$ge": right};
						break;
					case QueryExpression.LessThan:
						obj[expr.left] = {"$lt": right};
						break;
					case QueryExpression.LessThanOrEqualTo:
						obj[expr.left] = {"$le": right};
						break;
					case QueryExpression.In:
						obj[expr.left] = {"$in": right};
						break;
					case QueryExpression.NotIn:
						obj[expr.left] = {"$notin": right};
						break;
					default:
						throw new Error( "Invalid operator " + expr.oper );
				}
			}
			else {
				ClassUtils.merge( obj, parseNoSQLExpression( expr.right ) ); //???
			}
		return obj;
	}

}

}
