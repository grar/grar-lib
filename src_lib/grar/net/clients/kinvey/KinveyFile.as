/**
 * Code by Rodrigo López Peker on 2/13/16 8:38 PM.
 *
 */
package grar.net.clients.kinvey {

import flash.filesystem.File;
import flash.net.URLRequestHeader;
import flash.utils.ByteArray;

import grar.net.*;
import grar.air.FileUtils;
import grar.net.clients.IBackendFile;
import grar.utils.LoanShark;

public class KinveyFile implements IBackendFile {

	private static var _pool:LoanShark = new LoanShark( KinveyFile, false, 3, 0, null, "", "" );

	public static function get():KinveyFile {
		var file:KinveyFile = _pool.borrowObject() as KinveyFile;
		file._inPool = true;
		if ( !file._cleaned ) {
			file.reset();
		}
		file.verbose = KinveyFile.verbose;
		return file;
	}

	public function reset():void {
		id = null;
		name = null;
		size = 0;
		file = null;
		mime = null;
		contentType = null;
		metadata = null;
		_entityData = null;
		_uploadUrl = null;
		_uploadHeaders = null;
		onUploadStatus = null;
		_cleaned = true;
		if ( _inPool ) {
			_pool.returnObject( this );
		}
	}

	public static var verbose:Boolean = false;
	public var verbose:Boolean;

	private var _inPool:Boolean;
	private var _cleaned:Boolean;

	public var id:String;
	public var name:String;
	public var size:int;
	public var file:File;
	public var mime:String;
	public var contentType:String;
	public var metadata:Object;
	public var data:ByteArray;

	// we can use this as a callback outside the KinveyAPI!
	public var onUploadStatus:Function;

	// returned by kinvey
	private var _entityData:Object;

	// PRIVATE properties.
	private var _uploadHeaders:Array;
	private var _uploadUrl:String;

	// required by google to define the contentLength.
	private static var _headerContentLength:URLRequestHeader;

	public function KinveyFile() {
		if ( !_headerContentLength ) {
			_headerContentLength = new URLRequestHeader( "Content-Length" );
		}
		_cleaned = true;
	}

	public function setFile( file:File ):void {
		this.file = file;
		name = file.name;
		mime = FileUtils.getMime( name );
		contentType = mime;
		// read data.
		if ( file.exists ) {
			data = FileUtils.read( file );
			size = data.length;
		}
	}

	public function setData( data:ByteArray, name:String, contentType:String = null, metadata:Object = null ):void {
		if ( !data ) return;
		this.data = data;
		size = data.length;
		if ( !contentType ) {
			contentType = "application/octet-stream";
		}
		if ( metadata ) {
			this.metadata = metadata;
		}
		this.name = name;
		this.contentType = mime = contentType;
		file = null;
		log( "setData()::" )
	}

	public function getData():Object {
		if ( !metadata ) {
			metadata = {};
		}
		if ( name ) metadata._filename = name;
		metadata.size = size;
		if ( mime ) metadata.mimeType = mime;
		return metadata;
	}

	public function setEntity( data:Object ):void {
//		example:: {"size":71,"mimeType":"image/png","_id":"f060cb84-81b0-466c-a236-0e97a1c2e549","_filename":"1ecafeb9-7801-42bd-a8c8-74b451bc58dc","_acl":{"creator":"kid_ZJ86lWfJCe"},"_kmd":{"lmt":"2016-02-13T23:37:14.179Z","ect":"2016-02-13T23:37:14.179Z"},"_uploadURL":"http://storage.googleapis.com/09f64500605b4cac9ddc8e9ceb467302/f060cb84-81b0-466c-a236-0e97a1c2e549/1ecafeb9-7801-42bd-a8c8-74b451bc58dc?GoogleAccessId=558440376631@developer.gserviceaccount.com&Expires=1455406664&Signature=TVcU9%2FK6RYFg2N0UOWikoIgRkjKC9Gbcb6aHeZ28YT2MMS6%2FFsBTBp%2Ba0AHbfR56RlglZGG99clBmFyQN798NoXzpYMbCtHunFE1czaTsPV8AMSxe%2FjZDF%2Fdq%2BGdLMgxNh90juybi1orGmXWctGPNvtXOM3a2M4AqH1U1UBdTNM%3D","_expiresAt":"2016-02-13T23:37:44.185Z","_requiredHeaders":{}}
		_entityData = data;
		if ( data.size ) size = data.size;
		if ( data.mimeType ) mime = data.mimeType;
		id = data._id;
		this.name = data._filename;
//		_uploadExpires = data._expiresAt ; // calculate it later, no time now.
		_uploadUrl = data._uploadURL;
		_uploadHeaders = [];
		// create the required headers, if needed, like ("x-goog-acl": "private")
		for ( var name:String in data._requiredHeaders ) {
			_uploadHeaders.push( new URLRequestHeader( name, data._requiredHeaders[name] ) );
		}
	}

	public function getUploadObjectRequest():Object {
		var props:Object = { method: ServiceCall.PUT };
		props.data = data;
		props.contentType = contentType;
		// required to avoid the JSON.stringify or URLVariables conversion.
		props.binaryData = true;
		_headerContentLength.value = String( size );
		props.headers = _headerContentLength;
		return props;
	}

	public function get uploadUrl():String {
		return _uploadUrl;
	}


	//===================================================================================================================================================
	//
	//      ------  factory
	//
	//===================================================================================================================================================

	public static function getFromFile( file:File, meta:Object, statusCallback:Function = null ):KinveyFile {
		var kfile:KinveyFile = KinveyFile.get();
		kfile.setFile( file );
		kfile.metadata = meta;
		kfile.onUploadStatus = statusCallback;
		return kfile;
	}

	public static function getFromData( data:ByteArray, name:String, contentType:String, meta:Object,
	                                    statusCallback:Function = null ):KinveyFile {
		var kfile:KinveyFile = KinveyFile.get();
		kfile.setData( data, name, contentType, meta );
		kfile.onUploadStatus = statusCallback;
		return kfile;
	}

	private function log( ...args ):void {
		if ( !verbose ) return;
		trace( "[KinveyFile]", args.join( " " ) );
	}

	public static function getRef( entityObject:Object ):Object {
		var o:Object = {};
		o._type = "KinveyFile";
		o._id = entityObject._id;
		return o;
	}

	public function getRef():Object {
		var o:Object = {};
		o._type = "KinveyFile";
		o._id = id;
		return o;
	}

}

}
