/**
 * Code by Rodrigo López Peker on 2/12/16 3:18 PM.
 *
 */
package grar.net.clients.twitter {

import grar.net.*;

import flash.net.URLRequestHeader;
import flash.net.URLVariables;

import grar.net.clients.ServiceToken;

import grar.net.clients.abstracts.AbsAPI;

import grar.utils.OAuthUtil;
import grar.utils.URIUtils;

import org.osflash.signals.Signal;

public class TwitterAPI extends AbsAPI {

	private static var _instance:TwitterAPI;
	public static function get instance():TwitterAPI {
		if ( !_instance ) _instance = new TwitterAPI();
		return _instance;
	}

	public var verbose:Boolean = false;

	public var socialId:String = "twitter";
	public var apiKey:String;
	public var apiSecret:String;
//	public var appToken:String;
//	public var appTokenSecret:String;
	// like redirectURI in oauth2
	public var oauthCallback:String;

	public var userToken:String;
	public var userTokenSecret:String;
	public var userTokenVerifier:String;

	public static var apiUrl:String = "https://api.twitter.com/1.1/";
	public static var oauthUrlAppToken:String = "https://api.twitter.com/oauth2/token";

	// user urls
	public static var oauthUrlRequestToken:String = "https://api.twitter.com/oauth/request_token";
	public static var oauthUrlAccessToken:String = "https://api.twitter.com/oauth/access_token";
	public static var oauthUrlAuthorize:String = "https://api.twitter.com/oauth/authorize";

	private var headerAuthAppBasic:URLRequestHeader;
	private var headerAuthAppBearer:URLRequestHeader;
	private var headerAuthUser:URLRequestHeader;

	private var _token:ServiceToken;

	private static const _contentTypeAppLogin:String = "application/x-www-form-urlencoded;charset=UTF-8";
	public var onUserLogin:Signal;
	public var onAppLogin:Signal;


	public function TwitterAPI() {
		super(AbsAPI.SERVICE_TWITTER);
		_token = ServiceToken.get( socialId, 1 );
	}

	override protected function init():void {
		super.init();
		onUserLogin = new Signal();
		onAppLogin = new Signal();
		headerAuthAppBasic = new URLRequestHeader( "Authorization" );
		headerAuthAppBearer = new URLRequestHeader( "Authorization" );
		headerAuthUser = new URLRequestHeader( "Authorization" );

		// update stuffs from token.
		updateAuthStuffFromToken();
	}


	//===================================================================================================================================================
	//
	//      ------  App login
	//
	//===================================================================================================================================================

	public function loginApp():void {
		if ( _token.appAccessToken ) {
			// validate app token ?
			updateTokenHeader( headerAuthAppBearer, _token.appAccessToken );
			onAppLoginComplete();
		} else {
			requestAppToken();
		}
	}

	public function invalidateAppToken():void {
		var url:String = "https://api.twitter.com/oauth2/invalidate_token";
		var props:Object = {};
		props.method = ServiceCall.POST;
		props.headers = headerAuthAppBasic;
		props.data = {access_token: _token.appAccessToken};
		props.contentType = _contentTypeAppLogin;
		serviceCall( url, ServiceCall.onSuccessDebugCallback, props );
	}

	public function requestAppToken( customParameters:Object = null ):void {
		var props:Object = {};
		props.method = ServiceCall.POST;
		props.headers = headerAuthAppBasic;
		props.contentType = _contentTypeAppLogin;
		props.data = {grant_type: "client_credentials"};
		if ( customParameters ) {
			URIUtils.mergeObjects( props.data, customParameters );
		}
		serviceCall( oauthUrlAppToken, handleAppBearerTokenCall, props );
	}

	private function handleAppBearerTokenCall( status:Object ):void {
		if ( status.isComplete ) {
			var data:Object = JSON.parse( status.data );
			_token.setAppTokenOAuth1FromJSON( data );
			log( "app access token:", status.data );
			updateTokenHeader( headerAuthAppBearer, _token.appAccessToken );
			onAppLoginComplete();
		}
	}

	private function onAppLoginComplete():void {
		onAppLogin.dispatch();
	}


	/**
	 * @see https://dev.twitter.com/rest/public/search
	 * @see https://dev.twitter.com/rest/reference/get/search/tweets (modifiers)
	 * @param query
	 * @param modifiers    geocode,lang,locale,result_type,count,until,since_id,max_id,include_entities,etc
	 * @param statusCallback
	 */
	public function search( query:String, modifiers:Object = null, statusCallback:Function = null ):void {
		var url:String = apiUrl + "search/tweets.json";
		var props:Object = {};
		props.headers = headerAuthAppBearer;

		// validate modifiers.
		if ( !modifiers ) {
			modifiers = {};
			// shrink the result.
			modifiers.include_entities = false;
			modifiers.count = 30;
			// until format: YYYY-MM-DD
		}
		modifiers.q = query;
		props.urlParameters = modifiers;
		serviceCall( url, statusCallback, props );
//		https://api.twitter.com/1.1/search/tweets.json?q=%40twitterapi
//		https://twitter.com/search?q=%23elbarco&src=typd
	}

	/**
	 *
	 * @example https://api.twitter.com/1.1/?q=Twitter%20API&page=1&count=3
	 * @notes:    requires userToken.
	 * @param query
	 * @param urlParameters
	 * @param statusCallback
	 * @return
	 */
	public function searchUser( query:String, urlParameters:Object = null, statusCallback:Function = null ):ServiceCall {
		if ( !query && !urlParameters.q ) {
			log_error( "searchUser() requires a query" );
			return null;
		}
		var url:String = apiUrl + "users/search.json";
		if ( !urlParameters ) urlParameters = {};
		if ( query ) urlParameters.q = query;
		return serviceCall( url, statusCallback, {urlParameters: urlParameters, headers: headerAuthAppBearer} );
	}

	public function getUserTimeline( userNameOrId:String, urlParameters:Object = null,
									 statusCallback:Function = null ):ServiceCall {
		if ( !userNameOrId && ( !urlParameters.user_id || !urlParameters.screen_name ) ) {
			log_error( "getUserTimeline() requires a user screenName or userId" );
			return null;
		}
		if ( !urlParameters ) urlParameters = {};
		if ( userNameOrId ) urlParameters.screen_name = userNameOrId;
		var url:String = apiUrl + "statuses/user_timeline.json";
		return serviceCall( url, statusCallback, {urlParameters: urlParameters, headers: headerAuthAppBearer} );
//		Bearer
//		7interactive
//		GET /1.1/statuses/user_timeline.json?count=100&screen_name=twitterapi HTTP/1.1
	}


	//===================================================================================================================================================
	//
	//      ------  User login
	//
	//===================================================================================================================================================

	public function login():void {
		// validate user token here.
		if ( _token.valid() ) {
			// test the API.
			updateAuthStuffFromToken();
			validateUserLogin();
		} else {
			requestUserToken();
		}
	}

	private function validateUserLogin():void {
		verifyCredentials( null, handleVerifyCredentialsCall );
//		getUserFriendsIds( null, handleUserTokenValidationCall );
	}

	private function handleVerifyCredentialsCall( status:Object ):void {
		if ( status.isComplete ) {
			var data:Object = JSON.parse( status.data );
			updateUserInfo( data );
			log( "user info=", status.data );
			onUserLoginCompleted();
		}
	}

	// Previous approach to validate the user tokens... deprecated to verifyCredentials()
	private function handleUserTokenValidationCall( status:Object ):void {
		if ( status.isStatus ) {
			status.target.verbose = true;
			// if we get 200, is valid!
			if ( status.data == 200 ) {
				log( "validated token in:", status.target.currentRequestTimeText );
				status.target.cancel();
			}
		}
	}

	protected function onUserLoginCompleted():void {
		// validate token.
		onUserLogin.dispatch();
	}

	public function getUserFriendsIds( urlParams:Object = null, statusCallback:Function = null ):ServiceCall {
		return userCall( apiUrl + "friends/ids.json", statusCallback, {urlParameters: urlParams} );
	}

	public function getUserFriends( userId:String, urlParams:Object = null, statusCallback:Function = null ):ServiceCall {
		var url:String = apiUrl + "friends/list.json";
		if ( !urlParams ) urlParams = {};
		urlParams.userId = userId;
		var props:Object = {};
		props.urlParameters = urlParams;
		// we can output to a file!
//		props.debugFile = File.desktopDirectory.resolvePath("callback.json") ;
		return userCall( url, statusCallback, props );
	}

	protected function updateAuthStuffFromToken():void {
		if ( _token.valid() ) {
			userToken = _token.accessToken;
			userTokenSecret = _token.accessTokenSecret;
		}
		if ( _token.appAccessToken ) {
			updateTokenHeader( headerAuthAppBearer, _token.appAccessToken );
		}
	}

	/**
	 * Returns the userObject from twitter.
	 * @see https://dev.twitter.com/rest/reference/get/account/verify_credentials
	 * @param urlParameters
	 * @param statusCallback
	 */
	public function verifyCredentials( urlParameters:Object = null, statusCallback:Function = null ):void {
		/*if ( !_user ) {
			log_error( "::verifyCredentials() You have to login first." );
			return;
		}*/
		var url:String = apiUrl + "account/verify_credentials.json";
		if ( !urlParameters ) {
			urlParameters = {};
			urlParameters.include_entities = false;
			urlParameters.include_email = true;
		}
		var props:Object = {};
		props.urlParameters = urlParameters;
		userCall( url, statusCallback, props );
	}


	private function requestUserToken():ServiceCall {
		return userCall( oauthUrlRequestToken, handleUserRequestTokenCall, {method: ServiceCall.POST} );
	}

	private function handleUserRequestTokenCall( status:Object ):void {
		if ( status.isComplete ) {
			// url encoded OR json...
			var data:Object = parseAccessTokenResult( status.data );
			if ( data.errors ) {
				// JSON error, usually bad signing header.
				log_error( "handleUserRequestTokenCall():", status.data );
				return;
			}
			if ( !data.oauth_callback_confirmed ) {
				// URLVars error, (valid header) but we have to refresh token.
				log_error( "handleUserRequestTokenCall():", status.data );
				return;
			}
			log( "handleUserRequestTokenCall():", status.data );
			//oauth_token=YO_j8QAAAAAAkW1nAAABUtviZ18&oauth_token_secret=7fNEaFUn6qTiZsz5o2TTGFKa2djBX7we&oauth_callback_confirmed=true
			// THIS IS NOT THE FINAL TOKEN!!! this is the request token.
//			_token.setTokenOAuth1( userToken, userTokenSecret );
			userToken = data.oauth_token;
			userTokenSecret = data.oauth_token_secret;
			requestUserAuth();
		}
	}

	private function requestUserAuth( customParameters:Object = null ):void {
		var url:String = oauthUrlAuthorize;
		var params:Object = {};
		params.display = "touch";
//		params.screen_name = "Me" ;
		params.force_login = "false";
		params.oauth_token = userToken;
		if ( customParameters ) {
			URIUtils.mergeObjects( params, customParameters );
		}
		url = URIUtils.addParametersToUrl( url, params );
		showWebBrowser( true, url );
	}

	override protected function handleWebClientUrlChange( url:String ):void {
		var idx:int = url.indexOf( "oauth_verifier=" );
		if ( idx > -1 ) {
			showWebBrowser( false );
			var params:URLVariables = URIUtils.getVariablesFromUrl( url );
			if ( params.oauth_token != userToken ) {
				log_error( "User Authorize: tokens doesnt match! execution ended." );
				return;
			}
			userTokenVerifier = params.oauth_verifier;
			getAccessTokenFromRequestToken();
		}
	}

	private function getAccessTokenFromRequestToken():void {
		var props:Object = {method: ServiceCall.POST};
		props.data = {oauth_verifier: userTokenVerifier};
		// required content type.
		props.contentType = "application/x-www-form-urlencoded";
		userCall( oauthUrlAccessToken, handleUserAccessTokenCall, props );
	}

	private function handleUserAccessTokenCall( status:Object ):void {
		if ( status.isComplete ) {
			//user access token: oauth%5Ftoken%5Fsecret=QtjiK4G3lxBP6BY0LTc159ojK3Zhdcjw2VmFLBcHxuXso&x%5Fauth%5Fexpires=0&user%5Fid=151864677&screen%5Fname=7interactive&oauth%5Ftoken=151864677%2DR1SVyULk6EYxSmVyjqfcguCp8VemXsLSi3MKnao3
			var data:Object = parseAccessTokenResult( status.data );
			log( "user access token:", JSON.stringify( data ) );
			userToken = data.oauth_token;
			userTokenSecret = data.oauth_token_secret;
			_token.setFromJson( data, true );
			// we get basic user info here, verifyCredentials() if you want more data.
			updateUserInfo( data );
			updateAuthStuffFromToken();
			onUserLoginCompleted();
		}
	}

	private function updateUserInfo( data:Object ):void {
//		_user = { id: data.user_id, username: data.screen_name };
		_user = data;
	}


	// Like serviceCall BUT generates/includes the complicated signed header for oauth1 on user behalf.
	// useful to call EVERY user related action, even requesting tokens.
	public function userCall( url:String, statusCallback:Function, props:Object ):ServiceCall {
		if ( !props ) props = {};
		url = resolveUrlParamsFromProps( url, props );
		updateOAuthHeader( url, props.method || "GET" );
		props.headers = headerAuthUser;
		return serviceCall( url, statusCallback, props );
	}

	//===================================================================================================================================================
	//
	//      ------  utils
	//
	//===================================================================================================================================================

	private function updateOAuthHeader( url:String, method:String = "GET",
										excludeCallbackFromUrl:Boolean = false ):String {

		var date:Date = new Date();
		var obj:Object = {};
//		obj.oauth_callback = "twitterclient://callback";
//		obj.oauth_callback = "oob";
		if ( !excludeCallbackFromUrl ) {
			obj.oauth_callback = oauthCallback;
		}

		obj.oauth_consumer_key = apiKey;

		if ( userToken ) {
			obj.oauth_token = userToken;
		}

		obj.oauth_signature_method = "HMAC-SHA1";
		obj.oauth_timestamp = OAuthUtil.generate_timestamp( date );
		obj.oauth_nonce = OAuthUtil.generate_nonce( date );
		obj.oauth_version = "1.0";
		obj.oauth_signature = getSignedSignature( url, obj, method );

		// generate the header "value"
		headerAuthUser.value = URIUtils.getOauth1HeaderValue( obj );
		return headerAuthUser.value;
	}

	private function getSignedSignature( url:String, parameters:Object, method:String = "GET" ):String {
		var sortParams:Array = [];
		var param:String;

		// create the params obj.
		var obj:Object = {};
		for ( param in parameters ) obj[param] = parameters[param];
		var url_arr:Array = url.split( "?" );
		if ( url_arr.length > 1 ) {
			// URL has params.
			var vars:URLVariables = URIUtils.getVariablesFromUrl( url );
			for ( param in vars ) obj[param] = vars[param];
			url = url_arr[0];
		}

		for ( param in obj ) sortParams.push( param );
		sortParams.sort();

		var params_arr:Array = [];
		for ( var i:int = 0; i < sortParams.length; i++ ) {
			param = sortParams[i];
			params_arr.push( param + "=" + encodeURIComponent( obj[param] ) );
		}
		var params:String = params_arr.join( "&" );
		var signBaseString:String = method + "&" + encodeURIComponent( url ) + "&" + encodeURIComponent( params );
		var signKey:String = encodeURIComponent( apiSecret ) + "&" + encodeURIComponent( (userTokenSecret || "") );
		var output:String = OAuthUtil.hmac_sha1( signBaseString, signKey );
		return output;
	}


	public function setup( env:Object ):void {
		apiKey = env.apiKey;
		apiSecret = env.apiSecret;
		oauthCallback = env.oauthCallback;
		_token.setAppCredentials( apiKey, apiSecret );

		// app token?
//		if( env.accessToken ){
//			_token.appAccessToken = env.accessToken ;
//		}
//		appTokenSecret = env.accessTokenSecret;
		updateAuthHeader( headerAuthAppBasic, apiKey, apiSecret );
	}

	private function updateTokenHeader( header:URLRequestHeader, auth_token:String ):void {
		header.value = "Bearer " + auth_token;
	}

	private function updateAuthHeader( header:URLRequestHeader, key:String, secret:String ):void {
		header.value = "Basic " + URIUtils.base64( key + ":" + secret );
	}

	protected function log_error( ...args ):void {
		trace( "[TwitterAPI ERROR] " + args.join( " " ) );
	}

	protected function log( ...args ):void {
		if ( !verbose ) return;
		trace( "[TwitterAPI] " + args.join( " " ) );
	}

	public function get token():ServiceToken {
		return _token;
	}
}
}
