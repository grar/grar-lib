/**
 * Code by Rodrigo López Peker on 2/11/16 4:36 PM.
 *
 */
package grar.net.clients {

import grar.io.Cache;
import grar.utils.URIUtils;

public class ServiceToken {

	public static var verbose:Boolean = false;
	private static var _map:Object = {};

	// service id to identify the token.
	private var _id:String;

	// the app credentials.
	public var apiKey:String;
	public var apiSecret:String;

	public var accessToken:String;
	// only in oauth1
	public var accessTokenSecret:String;

	public var expiresIn:String;
	public var accessType:String;
	public var refreshToken:String;

	public var unixExpirationDate:Number;
	public var unixCreationDate:Number;
	public var isOathu1:Boolean;

	// for twitter app access.
	public var appAccessToken:String;
	public var appTokenType:String;

	private var _isValid:Boolean;
	private var _expires:Boolean;
	private var _json:Object;

	// used in long lived tokens ?
	public var accessTokenCode:String;

	private var __logID:String;

	public function ServiceToken( id:String, oathuVersion:int = 2 ) {
		this._id = id;
		_map[id] = this;
		isOathu1 = oathuVersion == 1;
		__logID = "[Token " + _id + " oauth" + (isOathu1 ? 1 : 2) + "]";
		// try to refresh info from cache?
		init();
	}

	private function init():void {
		_isValid = false;
		_json = getStorage();
		if ( !_json ) {
			log( "no cached token" );
			return;
		}
		setFromJson( _json, false );
		if ( hasExpired() ) {
			_isValid = false;
			log( "expired, revalidate" );
		} else {
			log( "could be a still valid token" );
		}
	}


	//===================================================================================================================================================
	//
	//      ------  oathu 1
	//
	//===================================================================================================================================================
	public function setAppTokenOAuth1FromJSON( data:Object, doSave:Boolean = true ):void {
//		_isValid = true ;
		this.appAccessToken = data.access_token;
		this.appTokenType = data.token_type;
		if ( !_json ) {
			_json = {};
		}
		_json.app_access_token = appAccessToken;
		_json.app_token_type = appTokenType;
		if ( doSave ) {
			log( "saving app token:", appAccessToken );
			setStorage( _json );
		}
	}

	public function setAppCredentials( apiKey:String, apiSecret:String, doSave:Boolean = true ):void {
		this.apiKey = apiKey;
		this.apiSecret = apiSecret;
		if ( !_json ) {
			_json = {};
		}
		_json.api_key = apiKey;
		_json.api_secret = apiSecret;
		if ( doSave ) {
			log( "saving api credentials" );
			setStorage( _json );
		}
	}

	public function setFromJson( json:Object, doSave:Boolean ):void {
		// do not override!
//		_json = json ;
		if ( isOathu1 ) {
			setOathu1( json );
		} else {
			setOauth2( json );
		}

		apiKey = _json.api_key;
		apiSecret = _json.api_secret;
		unixCreationDate = _json.creation_date;
		unixExpirationDate = _json.expiration_date;
		// first validate if it has the required data in setOauthX();
		if ( doSave ) {
			_json.id = _id;
			if ( _expires )
				log( "expires in:", unixExpirationDate - timestampSeconds, "secs." );

			setStorage( _json );
		}
	}

	private function setOauth2( json:Object ):void {
		if ( !_json ) {
			_json = {};
		}
		URIUtils.mergeObjects( _json, json );

		accessToken = _json.access_token;
		// changes according to the API.
		// TODO: maybe we can MODIFY this stuffs from the Client itself.
		accessType = _json.access_type || _json.token_type;
		expiresIn = _json.expires_in || _json.expires;

		// this is also used to store ONLY refreshed tokens.
		if ( _json.refresh_token ) {
			refreshToken = _json.refresh_token;
		}
		_expires = Number( expiresIn ) > 0;
		if ( !_json.creation_date ) {
			_json.creation_date = timestampSeconds;
			_json.expiration_date = Number( expiresIn ) > 0 ? _json.creation_date + Number( expiresIn ) : 0;
		}
		_isValid = false;
		if ( accessToken && !hasExpired() ) {
			_isValid = true;
		}
	}

	private function setOathu1( json:Object ):void {
		if ( !_json )
			_json = {};

		URIUtils.mergeObjects( _json, json );

		if ( _json.app_access_token ) {
			appAccessToken = _json.app_access_token;
		}

		appTokenType = _json.app_token_type;
		accessToken = _json.oauth_token || "";
		accessTokenSecret = _json.oauth_token_secret || "";
		accessType = "Bearer";
		expiresIn = _json.x_auth_expires;
		_expires = Number( expiresIn ) > 0;
		if ( !_json.creation_date ) {
			_json.creation_date = timestampSeconds;
			_json.expiration_date = Number( expiresIn ) > 0 ? _json.creation_date + Number( expiresIn ) : 0;
		}
		trace( "setting this!" );
		_isValid = false;
		if ( accessToken && accessTokenSecret && !hasExpired() ) {
			_isValid = true;
		}
	}

	public function valid():Boolean {
		return _isValid;
	}

	public function hasExpired():Boolean {
		if ( !_expires ) {
			return false;
		}
		// special case for manual tokens.
		if ( unixExpirationDate < 0 ) {
			return true;
		}
		// eternal token for oathu1?
		if ( unixExpirationDate == 0 ) return false;
		var now:Number = timestampSeconds;
		return unixExpirationDate - now < 0;
	}

	public function get timestampSeconds():Number {
		return new Date().time / 1000 | 0;
	}

	public function get id():String {
		return _id;
	}

	private function setStorage( props:Object ):void {
		Cache.set( _id, props );
	}

	private function getStorage():Object {
		return Cache.get( _id );
	}

	public function clear():void {
		appAccessToken = appTokenType = accessType = expiresIn = accessToken = refreshToken = "";
		unixCreationDate = unixExpirationDate = 0;
		_isValid = false;
		_json = null;
		log( " clear()" );
		setStorage( null );
	}

	public static function get( id:String, oathuVersion:int = 2 ):ServiceToken {
		if ( !_map[id] ) {
			new ServiceToken( id, oathuVersion );
		}
		return _map[id];
	}

	public function toString():String {
		return __logID + ":: valid=" + _isValid + " json=" + JSON.stringify( _json );
	}

	private function log( ...args ):void {
		if ( !verbose )
			return;
		trace( __logID, args.join( " " ) );
	}

}
}
