/**
 * Code by Rodrigo López Peker on 2/11/16 10:33 AM.
 *
 */
package grar.net.clients.facebook {

import grar.net.*;
import grar.net.clients.abstracts.AbsAPI;
import grar.net.clients.abstracts.AbsOauth2API;

// keep user login?
// https://developers.facebook.com/docs/facebook-login/access-tokens/expiration-and-extension
//	call to keep an "exchange code" for a token = https://graph.facebook.com/oauth/client_code?access_token=...&amp;client_secret=...&amp;redirect_uri=...&amp;client_id=...

public class FacebookAPI extends AbsOauth2API {

	private static var _instance:FacebookAPI;

	public static var defaultPermissions:Array = [
		"user_photos",
		"user_location",
		"user_about_me",
		"user_friends",
		"public_profile",
		"read_friendlists",
		"user_tagged_places"
	];
	public static var defaultRedirectUri:String = "https://www.facebook.com/connect/login_success.html";
	public static var apiGraphUrl:String = "https://graph.facebook.com/";
	public static var apiGraphMeUrl:String = apiGraphUrl + "me/";

	public static function get instance():FacebookAPI {
		if ( !_instance ) _instance = new FacebookAPI();
		return _instance;
	}

	public function FacebookAPI() {
		super(AbsOauth2API.SERVICE_FACEBOOK);
	}

	override protected function init():void {
		super.init();
		this.oauthUrlToken = apiGraphUrl + "oauth/access_token";
		this.oauthUrlEndpoint = "https://www.facebook.com/dialog/oauth";
		this.permissions = defaultPermissions;
	}

	//===================================================================================================================================================
	//
	//      ------  api
	//
	//===================================================================================================================================================
	public function graphGetUser( userId:String, onStatusCallback:Function, params:Object = null ):ServiceCall {
		if ( !userId ) {
			userId = "me";
		}
		var props:Object;
		var url:String = apiGraphUrl + userId + "/";
		if ( params ) {
			props = {urlParameters: params};
		}
		return serviceCall( url, onStatusCallback, props );
	}

	public function graphGetFriends( userId:String, onStatusCallback:Function ):ServiceCall {
		if ( !userId ) {
			userId = "me/";
		}
		var url:String = apiGraphUrl + userId + "friends/";
		return serviceCall( url, onStatusCallback, null );
	}

	public function graphGetPhotos( userId:String, onStatusCallback:Function, includeUploaded:Boolean = true ):ServiceCall {
		if ( !userId ) {
			userId = "me/";
		}
		var url:String = apiGraphUrl + userId + "photos/";
		if ( includeUploaded ) url += "uploaded/";
		return serviceCall( url, onStatusCallback, null );
	}

	override public function setup( env:Object ):void {
		super.setup( env );
		if ( !redirectUri ) {
			redirectUri = defaultRedirectUri
		}
		if ( "version" in env ) {
			if ( String(env.version).indexOf( "http:" ) >= 0 ) {
				apiGraphUrl = env.version + "";
			}
			else {
				apiGraphUrl += env.version + "/";
			}
		}
		apiGraphMeUrl = apiGraphUrl + "me/";
	}


	//===================================================================================================================================================
	//
	//      ------  PRIVATE functions
	//
	//===================================================================================================================================================


	override public function validateUserLogin():void {
		setDefaultUrlParams();
		// token is apparently valid ,so we recheck that here.
		graphGetUser( null, handleGraphMeCall, {fields: "email,name,first_name,last_name,picture,age_range,gender"} );
	}

	private function handleGraphMeCall( status:Object ):void {
		if ( status.isComplete ) {
			// assign data to user info.
			_user = JSON.parse( status.data );
			log("user=" + JSON.stringify(_user)) ;
			super.loginComplete();
		}
	}

	override protected function loginComplete():void {
		// This is usually called AFTER getting the token, so it should be valid.
		// just in case, validate the user Login (if needed).
//		super.loginComplete(); // do not call onLogin just now.
		setDefaultUrlParams();
		validateUserLogin();
	}

	protected function setDefaultUrlParams():void {
//		_defaultURLParameters = { access_token: _token.accessToken } ;
		_defaultURLParameters = { oauth_token: _token.accessToken };
	}

}

}
