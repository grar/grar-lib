/**
 * Created by tasos on 29/02/16 21:53.
 */
package grar.net.clients {

import grar.db.Query;
import grar.net.ServiceCall;

public interface IBackendApi {

	function signup( username:String, password:String, userParams:Object = null,
	                 success:Function = null, failure:Function = null ):ServiceCall;

	function login( username:String, password:String,
	                success:Function = null, failure:Function = null ):ServiceCall;

	function socialLogin( socialSite:String, accessToken:ServiceToken,
	                      success:Function = null, failure:Function = null ):ServiceCall;

	function logout( success:Function = null, failure:Function = null ):ServiceCall;

	function findUser( userId:String, success:Function = null, failure:Function = null ):ServiceCall;

	function findUsers( query:Object, modifiers:Object = null,
	                    success:Function = null, failure:Function = null ):ServiceCall;

	function find( collection:String, query:Object = null, modifiers:Object = null,
	               success:Function = null, failure:Function = null ):ServiceCall;

	function updateUser( userId:String = null, params:Object = null,
	                     success:Function = null, failure:Function = null ):ServiceCall;

	function deleteUser( userId:String = null, success:Function = null, failure:Function = null ):ServiceCall;

	function deleteCollection( collectionName:String, success:Function = null, failure:Function = null ):ServiceCall;

	function countElements( collection:String, success:Function = null, failure:Function = null ):ServiceCall;

	function addEntity( collection:String, data:Object, success:Function = null, failure:Function = null ):ServiceCall;

	function updateEntity( collection:String, query:Object, data:Object,
	                       success:Function = null, failure:Function = null ):ServiceCall;

	function deleteEntity( collection:String, query:Object, success:Function = null, failure:Function = null ):ServiceCall;

	function uploadFile( file:IBackendFile ):ServiceCall;

	function deleteFile( filename:String, success:Function = null, failure:Function = null ):ServiceCall;

	function getFileEntityById( fileId:String, success:Function = null, failure:Function = null ):ServiceCall;

	function getFileEntity( query:Object = null, modifiers:Object = null,
	                        success:Function = null, failure:Function = null,
	                        progress:Function = null ):ServiceCall;

	function addSchema( className:String, modelFields:Object, success:Function = null, failure:Function = null ):ServiceCall;

	function createSchemaFromString( data:String ):Object;

	function getAccessToken():String;

	function execute( query:Query, success:Function = null, failure:Function = null ):void;

}

}
