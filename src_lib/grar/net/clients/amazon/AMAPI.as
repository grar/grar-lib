/**
 * Code by Rodrigo López Peker on 2/12/16 6:09 PM.
 *
 */
package grar.net.clients.amazon {
import flash.net.URLRequestHeader;
import flash.net.URLVariables;

import grar.net.clients.abstracts.AbsOauth2API;
import grar.utils.URIUtils;

public class AMAPI extends AbsOauth2API {


	private static var _instance:AMAPI;
	public static function get instance():AMAPI {
		if ( !_instance ) _instance = new AMAPI();
		return _instance;
	}

	private var headerAuth:URLRequestHeader;
	public var apiUrl:String = "https://api.amazon.com/";
	public var scopes:Array = ["profile", "profile:user_id", "postal_code"];

	public function AMAPI() {
		_serviceId = "amazon";
		super(_serviceId);
	}

	override protected function init():void {
		super.init();
		headerAuth = new URLRequestHeader( "Authorization" );
		oauthUrlEndpoint = refreshTokenUrl = "https://www.amazon.com/ap/oa";
	}

	override public function setup( env:Object ):void {
		redirectUri = "http://localhost";
		super.setup( env );
		if ( env.scopes ) {
			if ( env.scopes is String ) {
				scopes = env.scopes.split( " " );
			} else if ( env.scopes is Array ) {
				scopes = env.scopes;
			}
		}
	}

	override public function validateUserLogin():void {
		getProfile();
	}

	public function getProfile():void {
		serviceCall( apiUrl + "userInfo/profile", handleProfileCall, null );
	}

	private function handleProfileCall( status:Object ):void {
		if ( status.isComplete ) {
			var data:Object = JSON.parse( status.data );
			if ( data.user_id ) {
				_user = data;
				log( "user=" + JSON.stringify( _user ) );
				super.loginComplete();
			} else {
				log_error( "profile call:Amazon error requesting userInfo.", status.data );
			}
		}
	}

	override protected function requestToken():void {
//		super.requestToken();
		// token comes directly from the web here, no need for code.
		var params:Object = {};
		params.client_id = clientId;
		if ( scopes && scopes.length ) {
			params.scopes = scopes.join( " " );
		}
		params.response_type = "tokenString";
		params.redirect_uri = redirectUri;

		var url:String = URIUtils.addParametersToUrl( oauthUrlEndpoint, params );
		showWebBrowser( true, url );
	}

	override protected function resolveCodeFromUrlOrTitle( url:String, title:String ):Boolean {
		// this comes from handleWebClientUrlChange(), in this case we resolve the access_token, not the code.
		var idx:int = url.indexOf( "access_token=" );
		if ( idx > -1 ) {
			var tokenParams:URLVariables = URIUtils.getVariablesFromUrl( url );
			log( "access_token found in:", tokenParams );
			validateAccessTokenResponse( tokenParams );
			return true;
		}
		return false;
	}

	override protected function updateHeaderToken():void {
		if ( _token.accessToken ) {
			headerAuth.value = "Bearer " + _token.accessToken;
			_defaultRequestHeaders = [headerAuth];
		} else {
			_defaultRequestHeaders = null;
			headerAuth.value = null;
		}
	}

	override protected function loginComplete():void {
//		super.loginComplete();
		validateUserLogin();
	}
}
}
