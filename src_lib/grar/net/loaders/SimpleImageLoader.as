/**
 * Code by rodrigolopezpeker (aka 7interactive™) on 3/30/14 7:19 PM.
 */
package grar.net.loaders {
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.StatusEvent;
import flash.filesystem.File;
import flash.net.URLRequest;
import flash.system.ImageDecodingPolicy;
import flash.system.LoaderContext;
import flash.utils.Dictionary;

import grar.utils.LoanShark;

import org.osflash.signals.Signal;

public class SimpleImageLoader {

	private static var _pool:LoanShark = new LoanShark( SimpleImageLoader, false, 0, 0, null, "" );
	private static var _instanceCounter:int = 0;
	public var instanceCount:int = 0;
	public var dispatchInstance:Boolean = true;

	public static function isPoolEmpty():Boolean {
		return _pool.used == 0;
	}

	public static function get():SimpleImageLoader {
		var loader:SimpleImageLoader = _pool.borrowObject() as SimpleImageLoader;
		loader.belongsToPool = true;
		return loader;
	}

	public static function load( fileOrUrl:*, onComplete:Function, useCache:Boolean = false ):SimpleImageLoader {
		var l:SimpleImageLoader = get();
		trace( "Loading simpleurl", fileOrUrl );
		l.useCache = useCache;
		l.dispatchInstance = false;
		l.onComplete.addOnce( onComplete );
		if ( fileOrUrl is String ) {
			l.loadURL( fileOrUrl as String );
		} else {
			l.loadFile( fileOrUrl as File );
		}
		return l;
	}

	public function returnToPool():void {
		// resets
//			if ( _bitmapData ) _bitmapData.dispose();
		reset();
		userData = {};
		addListeners( false );
		_callbackSuccess = null;
		_callbackError = null;
		if ( _onError ) _onError.removeAll();
		if ( _onProgress ) _onProgress.removeAll();
		if ( _onComplete ) _onComplete.removeAll();
		_pool.returnObject( this );
	}

	// end pool!
	public var belongsToPool:Boolean;

	private var _loader:Loader;
	private var _url:String;
	private var _request:URLRequest;
	public var isLoading:Boolean;
	public var isLoaded:Boolean;
	public var bytesLoaded:Number;
	public var bytesTotal:Number;
	public var percent:Number;

	private var _callbackSuccess:Function;
	private var _callbackError:Function;

	private var _onComplete:Signal;
	private var _onError:Signal;
	private var _onProgress:Signal;
	private var _bitmapData:BitmapData;
	public var userData:Object;

	public static var loaderContext:LoaderContext;

	// cache
	public static var useCacheBitmapData:Boolean = true;
	private static var _cacheDict:Dictionary = new Dictionary( true );

	public var useCache:Boolean;
	public static var verbose:Boolean = false;
	public static var errorImage:BitmapData;

	public static function clearCache():void {
		var d:Dictionary = _cacheDict;
		for ( var key:String in d ) {
			if ( d[key] ) BitmapData( d[key] ).dispose();
			delete d[key];
		}
		_cacheDict = new Dictionary( true );
	}

	/**
	 * Constrcutor.
	 */
	public function SimpleImageLoader() {
		_instanceCounter = ++instanceCount;
		_request = new URLRequest();
		useCache = useCacheBitmapData;
		if ( !loaderContext ) {
			loaderContext = new LoaderContext( false );
			loaderContext.imageDecodingPolicy = ImageDecodingPolicy.ON_LOAD;
		}
	}

	public static function clearCachedBitmap( p_url:String ):void {
		if ( _cacheDict[p_url] ) _cacheDict[p_url].dispose();
		delete _cacheDict[p_url];
	}

	public static function getCachedBitmap( p_url:String ):BitmapData {
		return _cacheDict[p_url];
	}

	public static function addCachedBitmap( p_url:String, p_bd:BitmapData ):void {
		if ( _cacheDict[p_url] ) _cacheDict[p_url].dispose();
		_cacheDict[p_url] = p_bd;
	}

	public function loadFile( p_file:File ):void {
		if ( !p_file.exists ) {
			if ( verbose ) trace( this, 'SimpleImageLoader FILE error:', p_file.url, "inexistent" );
			_bitmapData = null;
			if ( _onError ) _onError.dispatch( this );
			return;
		}
		loadURL( p_file.url );
	}

	public function loadURL( p_url:String ):void {
		if ( !p_url || _url == p_url ) {
//			handleError(null);
			return;
		}
		if ( !_loader ) {
			_loader = new Loader();
		}
		if ( isLoading ) cancel();
		isLoaded = false;
		_url = p_url;
		if ( useCache && _cacheDict[_url] ) {
			// bitmap already in memory.
			if ( verbose ) trace( this, ' - using cached bitmap:', _url );
			_bitmapData = _cacheDict[_url];
			handleComplete( null );
			return;
		}
		if ( verbose ) trace( this, "- ", _url );
		_request.url = _url;
//			trace("Loading url:", _url ) ;
		addListeners( true );
		_loader.load( _request, loaderContext );
	}

	public function cancel():void {
//			if( _onComplete ) _onComplete.removeAll();
		if ( isLoading ) try {
			_loader.close()
		} catch ( e:Error ) {
		}
		_url = null;
		if ( isLoading || isLoaded ) _loader.unload();
		isLoading = false;
	}

	public function reset():void {
		cancel();
		dispatchInstance = true;
		isLoaded = false;
		_bitmapData = null;
		_url = null;
		percent = 0;
	}

	private function handleProgress( event:ProgressEvent ):void {
		bytesLoaded = event.bytesLoaded;
		bytesTotal = event.bytesTotal;
		percent = bytesLoaded / bytesTotal;
		if ( _onProgress ) _onProgress.dispatch( this );
	}

	public function dispose():void {
		if ( _bitmapData ) _bitmapData.dispose();
		reset();
		_url = null;
		addListeners( false );
		if ( _onError ) _onError.removeAll();
		if ( _onProgress ) _onProgress.removeAll();
		if ( _onComplete ) _onComplete.removeAll();
		_onError = null;
		_onProgress = null;
		_onComplete = null;
	}

	private function handleComplete( event:Event = null ):void {
		if ( event ) {
			_bitmapData = Bitmap( event.target.content ).bitmapData;
		}
//			trace("Complete!") ;
		isLoaded = true;
		isLoading = false;
		percent = 1;
		if ( useCache ) {
			if ( _cacheDict[_url] && !_bitmapData ) {
				_bitmapData = _cacheDict[_url];
			} else {
				_cacheDict[_url] = _bitmapData;
			}
		}
		if ( _onComplete ) {
			if ( verbose ) trace( this, 'onComplete:', _url );
			if ( dispatchInstance ) {
				_onComplete.dispatch( this, _bitmapData );
			} else {
				_onComplete.dispatch( _bitmapData );
			}
		}
		_url = null;
		if ( belongsToPool ) {
			returnToPool();
		}
	}

	private function handleError( event:IOErrorEvent ):void {
		if ( verbose ) trace( this, 'SimpleImageLoader error:', _url, event.text );
		_bitmapData = null;
//		addListeners(false);
		if ( _onError ) {
			_onError.dispatch( this );
		}
		if ( errorImage ) {
			if ( dispatchInstance ) {
				_onComplete.dispatch( this, errorImage );
			} else {
				_onComplete.dispatch( errorImage );
			}
		}
		if ( belongsToPool ) {
			returnToPool();
		}
	}

	public function get onError():Signal {
		if ( !_onError ) _onError = new Signal();
		return _onError;
	}

	public function get onProgress():Signal {
		if ( !_onProgress ) _onProgress = new Signal();
		return _onProgress;
	}

	public function get onComplete():Signal {
		if ( !_onComplete ) _onComplete = new Signal();
		return _onComplete;
	}

	private function addListeners( b:Boolean ):void {
		if ( b ) {
			_loader.contentLoaderInfo.addEventListener( StatusEvent.STATUS, trace );
			_loader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, handleError );
			_loader.contentLoaderInfo.addEventListener( Event.COMPLETE, handleComplete );
			_loader.contentLoaderInfo.addEventListener( ProgressEvent.PROGRESS, handleProgress );
		} else {
			_loader.contentLoaderInfo.removeEventListener( IOErrorEvent.IO_ERROR, handleError );
			_loader.contentLoaderInfo.removeEventListener( Event.COMPLETE, handleComplete );
			_loader.contentLoaderInfo.removeEventListener( StatusEvent.STATUS, trace );
			_loader.contentLoaderInfo.removeEventListener( ProgressEvent.PROGRESS, handleProgress );
		}
	}


	public function get url():String {
		return _url;
	}
}
}
