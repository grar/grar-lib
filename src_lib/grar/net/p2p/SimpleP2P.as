/**
 * Code by Rodrigo López Peker (grar) on 2/28/16 6:51 PM.
 *
 */
package grar.net.p2p {
import flash.events.NetStatusEvent;
import flash.net.GroupSpecifier;
import flash.net.NetConnection;
import flash.net.NetGroup;

import org.osflash.signals.Signal;

public class SimpleP2P {

	public var verbose:Boolean = true;

	public static const LOCAL_CONNECTION:String = 'localconnection';
	public static const CIRRUS_CONNECTION:String = 'cirrusconnection';
	public static const CIRRUS_SERVER:String = "rtmfp://p2p.rtmfp.net/";
	public static const LOCAL_SERVER:String = "rtmfp:";

	public static const POST:String = "post";
	public static const CLIENT_DISCONNECT:String = "clientDisconnect";
	public static const CLIENT_CONNECT:String = "clientConnect";
	public static const CONNECT:String = "connect";
	public static const RECIEVE:String = "recieve";


	private var _connected:Boolean;
	private var _connectionType:String;
	private var _gid:String;

	public var neighbors:Array;
	public var peerID:String;

	private var nc:NetConnection;
	private var gs:GroupSpecifier;
	private var ng:NetGroup;

	public var onStatus:Signal;

	//If you're gonna use Adobe's Cirrus server make sure you supply your developer key!
	public function SimpleP2P( connectionType:String = SimpleP2P.LOCAL_CONNECTION, groupId:String = null,
							   multicastIp:String = "239.0.0.255:30304", developerKey:String = "" ) {
		_connected = false;
		onStatus = new Signal() ;
		if ( groupId != null ) {
			connect( connectionType, groupId, multicastIp, developerKey );
		}

	}

	public function connect( connectionType:String = SimpleP2P.LOCAL_CONNECTION, groupId:String = null,
							 multicastIp:String = "239.0.0.255:30304", developerKey:String = "" ):void {
		_connectionType = connectionType;
		_gid = groupId ? groupId : "RandomNetGroup" + (int( Math.random() * 1000000000 )).toString();
		log( "group id=", _gid );

		nc = new NetConnection();
		nc.addEventListener( NetStatusEvent.NET_STATUS, onNetStatus );

		if ( _connectionType == SimpleP2P.LOCAL_CONNECTION ) {

			gs = new GroupSpecifier( _gid );
			gs.postingEnabled = true;
			gs.routingEnabled = true;
			gs.ipMulticastMemberUpdatesEnabled = true;
			gs.addIPMulticastAddress( multicastIp );
			nc.connect( SimpleP2P.LOCAL_SERVER );

			/*groupspec.postingEnabled = true;
			groupspec.routingEnabled = true;
			groupspec.ipMulticastMemberUpdatesEnabled = true;
			groupspec.addIPMulticastAddress(_config.ip);
			groupspec.objectReplicationEnabled = _config.saveState;*/


		} else if ( _connectionType == SimpleP2P.CIRRUS_CONNECTION ) {

			gs = new GroupSpecifier( _gid );
			gs.serverChannelEnabled = true;
			gs.postingEnabled = true;
			gs.routingEnabled = true;

			nc.connect( SimpleP2P.CIRRUS_SERVER + developerKey );
		}
	}

	private function onNetStatus( e:NetStatusEvent ):void {

		switch ( e.info.code ) {

			case "NetConnection.Connect.Success":
				log( 'NetConnection Connected' );
				setupGroup();
				break;

			case "NetGroup.Connect.Success":
				log( 'NetGroup Connected' );
				_connected = true;
				peerID = ng.convertPeerIDToGroupAddress( nc.nearID );
				neighbors = [];
				dispatch( CONNECT, { sender: peerID, group: e.info.group}) ;
//				dispatchEvent( new NetGrouperEvent( NetGrouperEvent.CONNECT, {sender: peerID, group: e.info.group} ) );
				break;
			case "NetGroup.SendTo.Notify":
			case "NetGroup.Posting.Notify":
				log( "got message" );
				receiveMessage( e.info.message );
				break;

			case "NetGroup.Neighbor.Connect":
				log( "Neighbor Connected: ", e.info.neighbor, e.info.peerID );
				dispatch( CLIENT_CONNECT, { id: e.info.neighbor }) ;
//				dispatchEvent( new NetGrouperEvent( NetGrouperEvent.NEIGHBOR_CONNECT, {id: e.info.neighbor} ) );
				addNeighbor( e.info.neighbor, e.info.peerID );
				break;

			case "NetGroup.Neighbor.Disconnect":
				log( "Neighbor Disconnected: ", e.info.neighbor, e.info.peerID );
				dispatch( CLIENT_DISCONNECT, { id: e.info.neighbor }) ;
//				dispatchEvent( new NetGrouperEvent( NetGrouperEvent.NEIGHBOR_DISCONNECT, {id: e.info.neighbor} ) );
				removeNeighbor( e.info.neighbor );
				break;
		}

	}

	public function post( message:Object ):void {
		// TODO: make queue of messages?
		if( !_connected )
			return ;

		message.sender = ng.convertPeerIDToGroupAddress( nc.nearID );

		if ( _connected ) {
			if ( ng.neighborCount < 14 ) {
				ng.sendToAllNeighbors( message );
			} else {
				//post is MUCH slower than sendToAllNeighbors but when there are more than 13 connections
				//however post is more reliable than sendToAllNeighbors because not everyone is neighbors
				//above 13 connections.
				ng.post( message );
			}

			dispatch( POST, message ) ;
//			dispatchEvent( new NetGrouperEvent( NetGrouperEvent.POST, message ) );
		}
	}

	private function receiveMessage( message:Object ):void {
		dispatch( RECIEVE, message ) ;
//		dispatchEvent( new NetGrouperEvent( NetGrouperEvent.RECEIVE, message ) );
	}

	private function setupGroup():void {
		ng = new NetGroup( nc, gs.groupspecWithAuthorizations() );
		ng.addEventListener( NetStatusEvent.NET_STATUS, onNetStatus );
	}

	private function addNeighbor( neighborID:String, peerID:String ):void {
		neighbors.push( {neighborID: neighborID, peerID: peerID} );
	}

	private function removeNeighbor( neighborID:String ):void {
		for ( var i:int = 0; i < neighbors.length; i++ ) {
			if ( neighborID == neighbors[i].neighborID ) {
				neighbors.splice( i, 1 );
				return;
			}
		}
	}

	public function getNeighbor( neighborID:String ):Object {
		for ( var i:int = 0; i < neighbors.length; i++ ) {
			if ( neighborID == neighbors[i].neighborID ) {
				return neighbors[i];
			}
		}
		return undefined;
	}

	public function disconnect():void {
		ng.close();
	}

	public function dispatch( type:String, props:Object ):void {
		if( !props ) props = {} ;
		props.type = type ;
		onStatus.dispatch(props) ;
	}

	private function log( ...args ):void {
		if ( verbose ) {
			trace( "[SimpleP2P]::", args.join( " " ) );
		}
	}

	public function get connected():Boolean {
		return _connected;
	}
}

}
